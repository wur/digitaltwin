import digitalio
import board
import busio
import adafruit_rfm9x
import adafruit_hashlib as hashlib
import time

RADIO_FREQ_MHZ = 867.9
CS = digitalio.DigitalInOut(board.GP8)
RESET = digitalio.DigitalInOut(board.GP9)
spi = busio.SPI(board.GP18, MOSI=board.GP19, MISO=board.GP16)
rfm9x = adafruit_rfm9x.RFM9x(spi, CS, RESET, RADIO_FREQ_MHZ)

while True:
    inpstring = "Hello world!"
    m = hashlib.md5()
    m.update(inpstring)
    md5 = m.hexdigest()
    rfm9x.send(md5[:6] + inpstring)
    time.sleep(10)