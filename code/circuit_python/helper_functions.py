# helper_functions.py
library_metadata = {
                    'title'                 : 'helper_functions',
                    'authors'               : 'BM',
                    'affiliation'           : 'Wageningen University and Research, NL',
                    'latest version date'   : '14-06-2022', 
                    'previous version dates': ['13-06-2022',  '10-06-2022', '02-05-2022',],
                    'language'              : 'Adafruit Circuitpython',
                    'language version'      : '7.3.0',  #'7.2.5',
                    'board'                 : 'Arduino Nano RP2040 Connect with rp2040',  # 'arduino_nano_rp2040_connect',
                    'modules required'      : {'pre-installed'      : ['time',
                                                                       'board',
                                                                       'busio',
                                                                       'DigitalInOut',
                                                                       'displayio',
                                                                       'terminalio',
                                                                       ],
                                                
                                               'add_to_lib_folder'  : ['adafruit_sht4x',
                                                                       'adafruit_requests',
                                                                       'adafruit_esp32spi',
                                                                       'adafruit_bme680',
                                                                       'adafruit_display_text',
                                                                       'adafruit_displayio_sh1107',
                                                                       'pimoroni_circuitpython_ltr559',
                                                                       'adafruit_bh1750',
                                                                       'adafruit_veml6075',
                                                                       'adafruit_pm25',
                                                                       ],
                                                },
                    
                    'scripts required'      : ['secrets', 'params'],
                    'contains'              : {'classes'           : ['MQTT_comms',
                                                                      'board_led',
                                                                      'wifi_connect' ,
                                                                      'sensor_pmsa003i',
                                                                      'sensor_bme680',
                                                                      'sensor_SGP30',
                                                                      'sensor_bme280',
                                                                      'sensor_ltr559',
                                                                      'sensor_veml6075',
                                                                      'sensor_BH1750',
                                                                      'oled_1_12inch',
                                                                     ],
                                               'functions'         : [],
                                               'dictionaries'      : ['library_metadata',
                                                                      'i2c_dictionary',
                                                                      'html_status_code_dictionary',
                                                                     ],
                                               },
                    }



i2c_dictionary = {
                                'BH1745'   : {'function_name' : 'sensor_BH1745'  ,'i2c address' : '0x38', 'i2c address alternative' : '0x39', 'variables' :  ['lux']},
                                'BME680'   : {'function_name' : 'sensor_bme680'  ,'i2c address' : '0x76', 'i2c address alternative' : '0x77', 'variables' :  ['temperature','gas','rel_humidity','pressure','altitude']},
                                'BME280'   : {'function_name' : 'sensor_bme280'  ,'i2c address' : '0x76', 'i2c address alternative' : '0x77', 'variables' :  ['temperature','rel_humidity','pressure','altitude']},
                                'LTR559'   : {'function_name' : 'sensor_ltr559'  ,'i2c address' : '0x23', 'i2c address alternative' : None  , 'variables' :  ['lux','proximity']},                                
                                'SHT40'    : {'function_name' : 'sensor_sht40'   ,'i2c address' : '0x44', 'i2c address alternative' : None  , 'variables' :  ['temperature','rel_humidity']},
                                'SGP30'    : {'function_name' : 'sensor_SGP30'   ,'i2c address' : '0x58', 'i2c address alternative' : None  , 'variables' :  ['eCO2','TVOC','H2','Ethanol']},
                                'VEML6075' : {'function_name' : 'sensor_veml6075','i2c address' : '0x10', 'i2c address alternative' : None  , 'variables' :  ['uv_index','uva','uvb']},
                                'PMSA003i' : {'function_name' : 'sensor_pmsa003i',
                                                'i2c address' : '0x12',
                                              'i2c address alternative' : None, 
                                                  'variables' :  ['pm10_env',
                                                                  'pm25_env',
                                                                  'pm100_env',
                                                                  'pm10_standard',
                                                                  'pm25_standard',
                                                                  'pm100_standard',
                                                                  'particles_03um',
                                                                  'particles_05um',
                                                                  'particles_10um',
                                                                  'particles_25um',
                                                                  'particles_50um',
                                                                  'particles_100um',
                                                            ]
                                              }, 
                                }


html_status_code_dictionary = {'influx_db' :    {204 : 'Success',
                                                 400 : 'Bad request',
                                                 401 : 'Unauthorized, check token',
                                                 404 : 'Not found, check url',
                                                 413 : 'Too large, requested payload is too large',
                                                 429 : 'Too many requests, check plan',
                                                 500 : 'Internal server error, the server has encountered an unexpected error',
                                                 503 : 'Service unavailable, series cardinality exceeeds plan',
                                                },
                               }

######## COMMUNICATION
class MQTT_comms:
    
    import adafruit_esp32spi.adafruit_esp32spi_socket as socket
    import adafruit_minimqtt.adafruit_minimqtt as MQTT
        
    def __init__(self, esp):
        """ initialise class
        
        REF:
            https://learn.adafruit.com/mqtt-in-circuitpython/circuitpython-wifi-usage
        
        """
        # 0. packages
        import adafruit_esp32spi.adafruit_esp32spi_socket as socket
        import adafruit_minimqtt.adafruit_minimqtt as MQTT

        
        # variables
        self.broker   = None
        self.port     = None
        self.username = None
        self.password = None
        self.topic    = None
        
        self.socket   = socket
        self.esp      = esp
        self.MQTT     = MQTT
        
        self.start_mqqt_interface(self.socket,self.esp)   
    
    
    def start_mqqt_interface(self, socket, esp):
        """ FUNC: start MQTT interface
        
        Initialize MQTT interface with the esp interface
        
        INPUT
            socket
            esp
        """
        # packages
        import adafruit_minimqtt.adafruit_minimqtt as MQTT
        
        # functions            
        self.MQTT.set_socket(socket, esp)  
        
        
    def new_client_object(self, broker_to_use = None, port_to_use = None,
                                username_to_use = None, password_to_use = None,
                                topic_to_use = None):
        """FUNC: Initialize a new MQTT Client object

        Initialize a new MQTT Client object
        
        """
        # packages
        
        
        # functions
        self.mqtt_client = self.MQTT.MQTT(
                                            broker     = broker_to_use,
                                            port       = port_to_use,
                                            username   = username_to_use,
                                            password   = password_to_use,
                                           #socket_pool = socket,
                                        )

        # Connect callback handlers to mqtt_client
        self.mqtt_client.on_connect      = self.print_connect
        self.mqtt_client.on_disconnect   = self.print_disconnect
        self.mqtt_client.on_subscribe    = self.print_subscribe
        self.mqtt_client.on_unsubscribe  = self.print_unsubscribe
        self.mqtt_client.on_publish      = self.print_publish
        self.mqtt_client.on_message      = self.print_message

        self.mqtt_topic = topic_to_use

        
    
    def connect(self):
        """ connect to broker

        """
        print("Attempting to connect to %s" % self.mqtt_client.broker)
        self.mqtt_client.connect()
    
    
    def connection_status(self):
        """ check connection status
        
        """
        return self.mqtt_client.is_connected()
    
    
    def publish(self, feed, value):
        """ publish value to feed

        """
        self.mqtt_client.publish(feed,value)
    

    def print_connect(self, mqtt_client, userdata, flags, rc):
        """EVENT: Connect
        This function will be called when the mqtt_client is connected # successfully to the broker.
        
        """
        print("Connected to MQTT Broker!")
        print("Flags: {0}\n RC: {1}".format(flags, rc))


    def print_subscribe(self, client, userdata, topic, granted_qos):
        """EVENT: Subscribe
        This method is called when the client subscribes to a new feed.

        """
        print("Subscribed to {0} with QOS level {1}".format(topic, granted_qos))


    def print_unsubscribe(self, client, userdata, topic, pid):
        """EVENT: unsubscribe
        This method is called when the client unsubscribes from a feed.

        """
        print("Unsubscribed from {0} with PID {1}".format(topic, pid))


    def print_disconnect(self, mqtt_client, userdata, rc):
        """EVENT: disconnect
        Disconnected function will be called when the client disconnects.
        
        """
        print("Disconnected from MQTT Broker!")


    # FUNCTIONS
    def print_message(self, client, feed_id, payload):
        """ FUNC: message
        # Message function will be called when a subscribed feed has a new value.
        # The feed_id parameter identifies the feed, and the payload parameter has
        # the new value.
        """
        print("Feed {0} received new value: {1}".format(feed_id, payload))


    def print_publish(self, mqtt_client, userdata, topic, pid):
        """ FUNC: publish
        # This method is called when the mqtt_client publishes data to a feed.
        """
        print("Published to {0} with PID {1}".format(topic, pid))


######### BOARD
class board_led:
    
    # Packages
    from digitalio import DigitalInOut, Direction
    
    def __init__(self):
        """ Initialise class
        
        REF:
            https://learn.adafruit.com/getting-started-with-raspberry-pi-pico-circuitpython/blinky-and-a-button
        
        NOTEs:
        The LED is connected to pin GP25. GP25 is dedicated to the LED
        
        """
        # packages
        import board
        from digitalio import DigitalInOut, Direction
        
        # variables  - tell the board how to talk to the LED
        self.LED           = DigitalInOut(board.LED) # tell the board the pin
        self.LED.direction = Direction.OUTPUT        # tell the board its an output as opposed to an input
        
        
    def turn_on_led(self):
        """ Turn on the led
        
        """
        # packages
                
        #function
        self.LED.value = True


    def turn_off_led(self):
        """ Turn off the led
        
        """
        # packages
                
        #function
        self.LED.value = False
    
    
    def blink(self,times, status, blink_interval = 0.1):
        """ Blink LED
        
        INPUT
            times          = number of times to blink on and off
            status         = status of LED after blinking
            blink_interval = the time interval between blinks, default is 0.1
        """
        # packages
        import time
        
        # function
        
        # set start of blink by turning on LED
        self.turn_on_led()
        
        for i in range(times):
            
            self.turn_off_led()
            time.sleep(blink_interval)
            
            self.turn_on_led()
            time.sleep(blink_interval)
            
        self.LED.value = status


######### SENSORS


#############
class sensor_sht40:
    """CLASS: MEASURE SHT40
    
    # Purpose: Temperature and Humidity
    # i2C: 0x44
    
    """
    # Packages
    from board import SCL, SDA
    from busio import I2C
    import adafruit_sht4x
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        # packages    
        #import time
        from board import SCL, SDA
        from busio import I2C
        import adafruit_sht4x
        
        # Variables
        # sensor
        self.sensor_name             = "SHT40"
        self.i2c_address             = 0x44
        self.i2c_address_alternative = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.i2c_I2C                 = None
        self.sht                     = None
        self.sht_serial              = None
        self.sht_mode                = None        
                
        # measured variables
        self.temperature             = None
        self.rel_humidity            = None
        
        # metadata
        self.variable_dictionary = {'Temperature' : {'variable name' : 'temperature' , 'units' : 'C'          , 'format' : '0.1f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor SHT40', 'dependent_variable' : None},
                                    'Humidity'    : {'variable name' : 'rel_humidity', 'units' : 'percentage' , 'format' : '0.1f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor SHT40', 'dependent_variable' : None},
                                    }
        
        # calculated variables
        # None
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None,
                     mode = 'No heat',
                     chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        INPUT
            SCL_pin          =      GPIO pin related to SCL
            SDA_pin          =      GPIO pin related to SDA
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        """
        # packages
        from board import SCL, SDA
        from busio import I2C
        import adafruit_sht4x
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port, use 'slow' 100KHz frequency!
        if chain is False:
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
        else:
            raise ValueError()
        
        # Connect to sensor over I2C
        self.sht        = adafruit_sht4x.SHT4x(i2c_address)
        
        # Get serial number of I2C sensor
        self.sht_serial = hex(self.sht.serial_number)
        print("Found SHT4x with serial number", self.sht_serial)
        
        # Set I2C sensor mode
        if mode == 'No heat':
            
            self.sht.mode = adafruit_sht4x.Mode.NOHEAT_HIGHPRECISION
            self.sht_mode = adafruit_sht4x.Mode.string[self.sht.mode]
            
            # modify metadata
            self.variable_dictionary['Temperature']['method'] = 'No heat high precision'
            
        elif mode == 'Low heat 100 ms':
            # Can also set the mode to enable heater
            self.sht.mode = adafruit_sht4x.Mode.LOWHEAT_100MS
            self.sht_mode = adafruit_sht4x.Mode.string[self.sht.mode]
            
            # modify metadata
            self.variable_dictionary['Temperature']['method'] = 'Low heat 100 ms'
            
        else:
            raise ValueError('mode should be "No heat" or "Low heat 100 ms" ')
        
        print("Current mode is: ", self.sht_mode)
        
    
    def measure_temperature(self):
        """ Measure temperature in degrees celcius
        
        """
        temperature = self.sht.measurements[0]
        self.temperature = temperature
        
        return temperature
    
    
    def measure_rel_humidity(self):
        """ Measure relative humidity in rh %, values from 0 to 100%
        
        """
        rel_humidity      = self.sht.measurements[1]
        self.rel_humidity = rel_humidity
        
        return rel_humidity
        
    
    def measure_all(self):
        """ Func: Measure all sensor parameters, returns (temperature_value, rel_humidity_value)
        
        """
        temperature_value  = self.measure_temperature()
        rel_humidity_value = self.measure_rel_humidity()
        
        return temperature_value, rel_humidity_value
    
    def print_sensor_values(self):
        """ Func: Print all sensor values
        
        """
        
        temperature       = self.temperature
        relative_humidity = self.rel_humidity
        
        print("Temperature: %0.1f C" % temperature)
        print("Humidity: %0.1f %%" % relative_humidity)
        print("")
        

#############
class sensor_pmsa003i:
    """CLASS: MEASURE PMSA003I
    
    # Purpose: Particulate matter (PM) air quality sensor PM2.5
    # i2C: 0x12
    
    """
    # Packages
    #import time
    import board
    import busio
    from digitalio import DigitalInOut, Direction, Pull
    from adafruit_pm25.i2c import PM25_I2C
    
    
    def __init__(self):
        """ Initialise class
        
        # MEASURED VARIABLES
            self.pm10_env        'pm10 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            self.pm25_env        'pm25 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            self.pm100_env       'pm100 env'       #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            
            self.pm10_standard   'pm10 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"    
            self.pm25_standard   'pm25 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            self.pm100_standard  'pm100 standard'  #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            
            
            self.particles_03um  'particles 03um'  #"Particles > 0.3um / 0.1L air:"
            self.particles_05um  'particles 05um'  #"Particles > 0.5um / 0.1L air:"
            self.particles_10um  'particles 10um'  #"Particles > 1.0um / 0.1L air:"
            self.particles_25um  'particles 25um'  #"Particles > 2.5um / 0.1L air:"
            self.particles_50um  'particles 50um'  #"Particles > 5.0um / 0.1L air:"
            self.particles_100um 'particles 100um' #"Particles > 10 um / 0.1L air:"
        
        """
        #packages
        import time
        import board
        import busio
        from board import SCL, SDA
        from busio import I2C
        from digitalio import DigitalInOut, Direction, Pull
        from adafruit_pm25.i2c import PM25_I2C
        
        # Variables
        # sensor
        self.sensor_name             = "PMSA003I"
        self.i2c_address             = 0x12
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.reset_pin               = None     # If you have a GPIO, its not a bad idea to connect it to the RESET pin; reset_pin = DigitalInOut(board.G0); reset_pin.direction = Direction.OUTPUT; reset_pin.value = False
        self.pm25                    = None
                                
        # measured variables
        self.pm10_env                = None    # 'pm10 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        self.pm25_env                = None    # 'pm25 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        self.pm100_env               = None    # 'pm100 env'       #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        
        self.pm10_standard           = None    # 'pm10 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"    
        self.pm25_standard           = None    # 'pm25 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        self.pm100_standard          = None    # 'pm100 standard'  #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        
        
        self.particles_03um          = None    # 'particles 03um'  #"Particles > 0.3um / 0.1L air:"
        self.particles_05um          = None    # 'particles 05um'  #"Particles > 0.5um / 0.1L air:"
        self.particles_10um          = None    # 'particles 10um'  #"Particles > 1.0um / 0.1L air:"
        self.particles_25um          = None    # 'particles 25um'  #"Particles > 2.5um / 0.1L air:"
        self.particles_50um          = None    # 'particles 50um'  #"Particles > 5.0um / 0.1L air:"
        self.particles_100um         = None    # 'particles 100um' #"Particles > 10 um / 0.1L air:"
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {}
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency=None, reset_pin = None,chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        INPUT
            SCL_pin          =      GPIO pin related to SCL
            SDA_pin          =      GPIO pin related to SDA
            integration_time =      The integration_time is the amount of time the VEML6075
                                    is sampling data for, in milliseconds. Valid times are:
                                    50, 100, 200, 400 or 800ms. Default is 100
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        """
        # packages
        import time
        import board
        import busio
        from board import SCL, SDA
        from busio import I2C
        from digitalio import DigitalInOut, Direction, Pull
        from adafruit_pm25.i2c import PM25_I2C
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port, use 'slow' 100KHz frequency!
        if chain is False:
            i2c_address  = busio.I2C(SCL_pin,SDA_pin,frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            print('Reminder: PM sensor need to have i2C frequency of 100000 (100 kHz)')
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            raise ValueError()
        
        # Connect to a PM2.5 sensor over I2C
        self.pm25  = PM25_I2C(i2c_address, reset_pin)
    
    
    def measure_all(self):
        """ FUNC: measure all, returns (self.pm10_env, self.pm25_env, self.pm100_env, self.pm10_standard, self.pm25_standard, self.pm100_standard, self.particles_03um, self.particles_05um, self.particles_10um, self.particles_25um, self.particles_50um, self.particles_100um)
        
            INPUT:
            
            OUTPUT:
                data or -99 if an error
        """
        try:
            aqdata = self.pm25.read()
            
            # measured variables
            self.pm10_env                = aqdata["pm10 env"]
            self.pm25_env                = aqdata["pm25 env"]
            self.pm100_env               = aqdata["pm100 env"]
             
            self.pm10_standard           = aqdata["pm10 standard"]
            self.pm25_standard           = aqdata["pm25 standard"]
            self.pm100_standard          = aqdata["pm100 standard"]
            
            self.particles_03um          = aqdata["particles 03um"]
            self.particles_05um          = aqdata["particles 05um"]
            self.particles_10um          = aqdata["particles 10um"]
            self.particles_25um          = aqdata["particles 25um"]
            self.particles_50um          = aqdata["particles 50um"]
            self.particles_100um         = aqdata["particles 100um"]
            
            return self.pm10_env, self.pm25_env, self.pm100_env, self.pm10_standard, self.pm25_standard, self.pm100_standard, self.particles_03um, self.particles_05um, self.particles_10um, self.particles_25um, self.particles_50um, self.particles_100um        
        
        except:
            # measured variables
            self.pm10_env                = -99
            self.pm25_env                = -99
            self.pm100_env               = -99
             
            self.pm10_standard           = -99
            self.pm25_standard           = -99
            self.pm100_standard          = -99
            
            self.particles_03um          = -99
            self.particles_05um          = -99
            self.particles_10um          = -99
            self.particles_25um          = -99
            self.particles_50um          = -99
            self.particles_100um         = -99
            
            return self.pm10_env, self.pm25_env, self.pm100_env, self.pm10_standard, self.pm25_standard, self.pm100_standard, self.particles_03um, self.particles_05um, self.particles_10um, self.particles_25um, self.particles_50um, self.particles_100um        
        
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        """
        # variables
        
        
        print()
        print("Concentration Units (standard)")
        print("---------------------------------------")
        print(
            "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            % (self.pm10_standard, self.pm25_standard, self.pm100_standard)
        )
        print("Concentration Units (environmental)")
        print("---------------------------------------")
        print(
            "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            % (self.pm10_env, self.pm25_env, self.pm100_env)
        )
        print("---------------------------------------")
        print("Particles > 0.3um / 0.1L air:", self.particles_03um)
        print("Particles > 0.5um / 0.1L air:", self.particles_05um)
        print("Particles > 1.0um / 0.1L air:", self.particles_10um)
        print("Particles > 2.5um / 0.1L air:", self.particles_25um)
        print("Particles > 5.0um / 0.1L air:", self.particles_50um)
        print("Particles > 10 um / 0.1L air:", self.particles_100um)
        print("---------------------------------------")
        

#############
class sensor_veml6075:
    """CLASS: MEASURE VEML-6075
    
    # Purpose: UVA/UVB sensor
    # i2C: 0x10
    
    
    """
    # Packages
    import adafruit_veml6075
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        # packages    
        #import time
        from board import SCL, SDA
        from busio import I2C
        import adafruit_veml6075
        
        # Variables
        # sensor
        self.sensor_name             = "VEML-6075"
        self.i2c_address             = 0x10
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.VEML6075                = None
        self.integration_time        = None
                
        # measured variables
        self.uv_index                = None
        self.uva                     = None
        self.uvb                     = None
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {'UV Index' : {'variable name' : 'uv_index' , 'units' : None     , 'format' : '0.2f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor VEML-6075', 'dependent_variable' : None},
                                    'UVA'      : {'variable name' : 'uva'      , 'units' : 'counts' , 'format' : '0.2f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor VEML-6075', 'dependent_variable' : None},
                                    'UVB'      : {'variable name' : 'uvb'      , 'units' : 'counts' , 'format' : '0.2f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor VEML-6075', 'dependent_variable' : None},
                                    }
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None,
                     integration_time=100, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        INPUT
            SCL_pin          =      GPIO pin related to SCL
            SDA_pin          =      GPIO pin related to SDA
            integration_time =      The integration_time is the amount of time the VEML6075
                                    is sampling data for, in milliseconds. Valid times are:
                                    50, 100, 200, 400 or 800ms. Default is 100
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        """
        # packages
        from busio import I2C
        import adafruit_veml6075
        
        # Variables
        # frequency of i2c
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
        
        self.integration_time  = integration_time
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
        elif chain is True:
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
        else:
            raise ValueError()
        
        self.VEML6075  = adafruit_veml6075.VEML6075(i2c_address,integration_time)
        
    
    def measure_uv_index(self):
        """ FUNC: measure uv index
        """
        # packages
        import adafruit_veml6075
        
        # Functions
        uv_index      = self.VEML6075.uv_index   
        self.uv_index = uv_index
        
        return uv_index
    
    
    def measure_uva(self):
        """ FUNC: measure uv index
        """
        # packages
        import adafruit_veml6075
        
        # Functions
        uva      = self.VEML6075.uva   
        self.uva = uva
        
        return uva
    
    
    def measure_uvb(self):
        """ FUNC: measure uv index
        """
        # packages
        import adafruit_veml6075
        
        # Functions
        uvb      = self.VEML6075.uvb   
        self.uvb = uvb
        
        return uvb
    
    
    def measure_all(self):
        """ FUNC: measure all, returns (uv_index_value,uva_value,uvb_value)
        
        """
        uv_index_value = self.measure_uv_index()
        uva_value      = self.measure_uva()
        uvb_value      = self.measure_uvb()
        
        return uv_index_value,uva_value,uvb_value
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        """
        # variables
        uv_index         = self.uv_index
        uva              = self.uva
        uvb              = self.uvb
        integration_time = self.integration_time
        
        print("Integration time: %d ms" % integration_time)
        print("")
        print("\nUV-index: %0.2f" % (uv_index))
        print("\nUV-A: %0.2f counts" % (uva))
        print("\nUV-B: %0.2f counts" % (uvb))
        
        
#########
class sensor_BH1745:
    """CLASS: MEASURE BH1745
    
    PURPOSE:
        LIGHT sensor
        'BH1750 provides 16-bit light measurements in lux, the SI unit
        for measuring light, making it easy to compare against other
        values like references and measurements from other sensors.
        The BH1750 can measure from 0 to 65K+ lux, but with some
        calibration and advanced adjustment of the measurement time,
        it can even be convinced to measure as much as 100,000 lux!'
        - https://www.adafruit.com/product/4681
        
    # Purpose: Light/Colour sensor
    # i2C: 0x38 and 0x39
    
    """
    # Packages
    from board import SCL, SDA
    from busio import I2C
    import adafruit_bh1750
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        # packages    
        #import time
        from board import SCL, SDA
        from busio import I2C
        import adafruit_bh1750
        
        # Variables
        # sensor
        self.sensor_name             = "BH1745"
        self.i2c_address             = 0x38
        self.i2c_address_alternative = 0x39
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.BH1745                  = None
                
        # measured variables
        self.lux                     = None
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {'Light' : {'variable name' : 'lux' , 'units' : 'lux'     , 'format' : '0.2f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor BH1745', 'dependent_variable' : None},
                                    }
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None,frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        INPUT
            SCL_pin          =      GPIO pin related to SCL
            SDA_pin          =      GPIO pin related to SDA
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
        """
        # packages
        from busio import I2C
        import adafruit_bh1750
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
        elif chain is True:
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
        else:
            raise ValueError()
        
        self.BH1750 = adafruit_bh1750.BH1750(i2c_address)
    
    
    def measure_lux(self):
        """Measure Lux
        
        """
        # packages
        import adafruit_bh1750
        
        # Functions
        lux      = self.BH1750.lux   # Get Lux value from light sensor
        self.lux = lux
        
        return lux


    def measure_all(self):
        """ FUNC: measure all, returns (lux_value)
        
        """
        lux_value = self.measure_lux()
        
        return lux_value
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        """
        # functions
        lux          = self.lux
        
        print("\nLux: %0.2f Lux" % (lux))
    
    
######### 
class sensor_ltr559:
    """CLASS: MEASURE LTR559
    
    # Purpose: Light & Proximity sensor/ optical presence & proximity sensor
    # i2C: 0x23
    
    REQUIRES:
        import time
        from adafruit_bus_device.i2c_device import I2CDevice
        from adafruit_register.i2c_bits import RWBits, ROBits
        from adafruit_register.i2c_bit import RWBit, ROBit
        from micropython import const
        import Pimoroni_CircuitPython_LTR559
    
    NOTES:
        > IR/UV-filtering
        > 50.60Hz flicker rejection
        > 0.01 lux to 64,000 lux light detection range
        > ~5cm proximity detection range
        
    REF:
        https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559
        https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559/blob/master/pimoroni_circuitpython_ltr559.py
    
    """
    # Packages
    #import time
    from board import SCL, SDA
    from busio import I2C
    from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
    def __init__(self):
        """ Initialise class
                
        
        """
        # packages    
        #import time
        from board import SCL, SDA
        from busio import I2C
        from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        # Variables
        # sensor
        self.sensor_name             = "LTR559"
        self.i2c_address             = 0x23
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.LTR559                  = None
                
        # measured variables
        self.lux                     = None
        self.proximity               = None
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {'Light'     : {'variable name' : 'lux'       , 'units' : 'lux' , 'format' : '0.2f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor LTR559', 'dependent_variable' : None},
                                    'Proximity' : {'variable name' : 'proximity' , 'units' : None  , 'format' : '0.1f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor LTR559', 'dependent_variable' : None},
                                    }
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
                
        INPUT
            SCL_pin          =      GPIO pin related to SCL
            SDA_pin          =      GPIO pin related to SDA
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
        """
        # packages
        from busio import I2C
        from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port
        
        if chain is False:
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
        elif chain is True:
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
        else:
            raise ValueError()
        self.LTR559 = Pimoroni_LTR559(i2c_address)
    
    
    def measure_lux(self):
        """Measure lux
        
        """
        # packages
        from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        # Functions
        lux      = self.LTR559.lux   # Get Lux value from light sensor
        self.lux = lux
        
        return lux
        
        
    def measure_proximity(self):
        """Measure proximity
        
        """
        # packages
        from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        # Functions
        prox           = self.LTR559.prox   # Get Lux value from light sensor
        self.proximity = prox
        
        return prox
        
    
    def measure_all(self):
        """ FUNC: measure all, returns (lux_value, prox_value)
        
        """
        lux_value   =  self.measure_lux()
        prox_value  =  self.measure_proximity()
        
        return lux_value, prox_value
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        """
        # functions
        lux          = self.lux
        proximity    = self.proximity
        
        print("\nLux: %0.2f Lux" % (lux))
        print("Proximity: %0.1f %%" % proximity)


#########  
class sensor_bme280:
    """CLASS: MEASURE BME280
    
    # Purpose: Pressure/Temperature/Humidity
    # i2C: 0x76 and 0x77
    
    # REF:
    https://docs.circuitpython.org/projects/bme280/en/latest/
    
    # Limitiations:
    pressure - 300 to 1100 hPa
    """
    # Packages
    #import time
    #import board
    from busio import I2C
    import adafruit_bme280.advanced as adafruit_bme280
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        # packages    
        #import time
        #import board
        from busio import I2C
        import adafruit_bme280.advanced as adafruit_bme280
        
        # Variables
        
        # sensor
        self.sensor_name             = "BME280"
        self.i2c_address             = 0x76
        self.i2c_address_alternative = 0x77
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.bme280                  = None
                
        # measured variables
        self.temperature             = None
        self.rel_humidity            = None
        self.pressure                = None
        
        # calculated variables
        self.sea_level_pressure      = None
        #self.abs_humidity            = None
        self.dewpoint                = None
        self.altitude                = None
        
        
        # metadata
        self.variable_dictionary = {'Temperature' : {'variable name' : 'temperature'  , 'units' : 'C'      , 'format' : '0.1f', 'data_type' : 'measured',   'method' : None, 'provenance' : 'sensor BME280', 'dependent_variable' : None},
                                    'Humidity'    : {'variable name' : 'rel_humidity' , 'units' : 'percent', 'format' : '0.1f', 'data_type' : 'measured',   'method' : None, 'provenance' : 'sensor BME280', 'dependent_variable' : None},
                                    'Pressure'    : {'variable name' : 'pressure'     , 'units' : 'hPa'    , 'format' : '0.3f', 'data_type' : 'measured',   'method' : None, 'provenance' : 'sensor BME680', 'dependent_variable' : 'pressure_sea_level'},
                                    'Altitude'    : {'variable name' : 'altitude'     , 'units' : 'meters' , 'format' : '0.2f', 'data_type' : 'measured',   'method' : None, 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Dewpoint'    : {'variable name' : 'dewpoint'     , 'units' : 'C'      , 'format' : '0.2f', 'data_type' : 'calculated', 'method' : None, 'provenance' : 'sensor BME680', 'dependent_variable' : ['temperature','rel_humidity']},
                                    }
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        INPUT
            SCL_pin          =      GPIO pin related to SCL
            SDA_pin          =      GPIO pin related to SDA
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
        """
        # packages
        from busio import I2C
        import adafruit_bme280.advanced as adafruit_bme280
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
                
        # Functions
        # Create library object on our I2C port
        if chain is False:
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
        elif chain is True:
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
        else:
            raise ValueError()
        
        self.bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c_address)
        
      
    def modify_sea_level_pressure(self, sea_level_pressure = 1013.25):
        """ Modify sea level pressure
        
        INPUT:
            sea_level_pressure = sea level pressure, default is generic 1013.25
        
        """
        # packages    
        import time
        import board
        import adafruit_bme280.advanced as adafruit_bme280
        
        # functions
        self.sea_level_pressure  =sea_level_pressure
        self.bme280.sea_level_pressure(sea_level_pressure)
        
        
    def measure_temperature(self):
        """FUNC: measure temperature
        
        """
        # packages
        import adafruit_bme280.advanced as adafruit_bme280
        
        # functions
        temperature      = self.bme280.temperature
        self.temperature = temperature
        
        return temperature
    
    
    def measure_rel_humidity(self):
        """FUNC: measure relative humidity
        
        """
        # packages
        import adafruit_bme280.advanced as adafruit_bme280
        
        # functions
        relative_humidity = self.bme280.relative_humidity
        self.rel_humidity = relative_humidity
        
        return relative_humidity
    
    
    def measure_pressure(self):
        """FUNC: measure pressure
        
        """
        # packages
        import adafruit_bme280.advanced as adafruit_bme280
        
        # functions
        pressure      = self.bme280.pressure
        self.pressure = pressure
        
        return pressure
    
    
    def measure_altitude(self):
        """FUNC: measure altitude
        
        """
        # packages
        import adafruit_bme280.advanced as adafruit_bme280
        
        # functions
        altitude      = self.bme280.altitude
        self.altitude = altitude
        
        return altitude
    
    
    def measure_all(self):
        """ FUNC: measure all, returns (temperature_value, rel_humidity_value, pressure_value, altitude_value)
        
        """
        temperature_value  = self.measure_temperature()
        rel_humidity_value = self.measure_rel_humidity()
        pressure_value     = self.measure_pressure()
        altitude_value     = self.measure_altitude()
        
        return temperature_value, rel_humidity_value, pressure_value, altitude_value
    
    #def convert_to_abs_humidity(self, relative_humidity):
    #    """ FUNC: convert relative humidity to absolute humidity
    #    
    #    """
    #
    
        
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        """
        # functions
        temperature  = self.temperature
        rel_humidity = self.rel_humidity
        #abs_humidity = self.rel_humidity
        pressure     = self.pressure
        altitude     = self.altitude
        
        print("\nTemperature: %0.1f C" % (temperature))
        print("Humidity: %0.1f %%" % rel_humidity)
        print("Pressure: %0.3f hPa" % pressure)
        print("Altitude = %0.2f meters" % altitude)
    
    
    def dew_point(self, temperature, rel_humidity):
        """ FUNC: dew point
        
        INPUT:
            temperature  = temperature in degrees celcius
            rel_humidity = relative humidity in percent
        
        OUTPUT:
            dewpoint     = The dew point is the temperature to which air
                            must be cooled to become saturated with water
                            vapor
        
        REF:
            https://learn.adafruit.com/adafruit-bme280-humidity-barometric-pressure-temperature-sensor-breakout/python-circuitpython-test
        NOTE:
            Dew Point:
            The dew point is the temperature to which air
            must be cooled to become saturated with water
            vapor, assuming constant air pressure and water
            content. When cooled below the dew point, moisture
            capacity is reduced[1] and airborne water vapor
            will condense to form liquid water known as dew.
            When this occurs via contact with a colder surface,
            dew will form on that surface.
        
        """
        # Packages
        import math
        
        # Variables
        b = 17.62
        c = 243.12
        
        # Function
        gamma    = (b * temperature /(c + temperature)) + math.log(rel_humidity / 100.0)
        dewpoint = (c * gamma) / (b - gamma)
        
        # store variable
        self.dewpoint  = dewpoint
        
        return dewpoint
    

#########
class sensor_SGP30:
    """CLASS: MEASURE SGP30
    
    # Purpose: TVOC/CO2eq sensor
    # i2C: 0x58
    
    REF
        https://learn.adafruit.com/adafruit-sgp30-gas-tvoc-eco2-mox-sensor/circuitpython-wiring-test
    """
    # Packages
    import time
    import board
    import busio
    import adafruit_sgp30
    
    
    def __init__(self):
        """ Initiliase class

        """
        # packages
        import time
        import board
        import busio
        import adafruit_sgp30
        
        # variables
        self.sensor_name             = "SGP30"
        self.i2c_address             = 0x58
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        
        self.sgp30                   = None
        self.SGP30_serial            = None
        
        self.eCO2                    = None
        self.TVOC                    = None
        self.H2                      = None
        self.Ethanol                 = None
        self.rel_humidity            = None
        self.Temperature             = None
        
        self.eCO2_baseline           = None
        self.TVOC_baseline           = None
        
        self.H2_raw                  = None
        self.Ethanol_raw             = None
        
        self.recalibration_counter   = 0
        
        # metadata
        self.variable_dictionary = {
            
            
            'Equivalent Carbon dioxide'         : {'variable name' : 'eCO2', 'units' : 'ppm', 'format' : '0.1f', 'data_type' : 'calculated','provenance' : 'sensor SGP30', 'dependent_variable' : ['eCO2_baseline', 'H2', 'Ethanol']},
            'Total Volatile Organic Compound'   : {'variable name' : 'TVOC', 'units' : 'ppb', 'format' : '0.1f', 'data_type' : 'calculated', 'provenance' : 'sensor SGP30', 'dependent_variable' : ['TVOC_baseline', 'H2', 'Ethanol']},
            'Total Volatile Organic Compound'   : {'variable name' : 'TVOC', 'units' : 'ppb', 'format' : '0.1f', 'data_type' : 'calculated', 'provenance' : 'sensor SGP30', 'dependent_variable' : ['TVOC_baseline', 'H2', 'Ethanol']},                        
                                    
                                    'Gas'         : {'variable name' : 'gas'         , 'units' : 'ohm'        , 'format' : 'd'   , 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Humidity'    : {'variable name' : 'rel_humidity', 'units' : 'percentage' , 'format' : '0.1f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Pressure'    : {'variable name' : 'pressure'    , 'units' : 'hPa'        , 'format' : '0.3f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : 'pressure_sea_level'},
                                    'Altitude'    : {'variable name' : 'altitude'    , 'units' : 'meters'     , 'format' : '0.2f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    }    

    def start_sensor(self,
                     SCL_pin = None, SDA_pin = None,
                     frequency = None,
                     eCO2_baseline        = 0x8973, TVOC_baseline = 0x8AAE,
                     temperature_baseline = 22.1, rel_humidity_baseline = 44,
                     chain = False, i2c_chainaddress = None
                     ):
        """ FUNC: create sensor object
        
        INPUT:
            SCL_pin = SCL pin (e.g., board.SCL, board.D11)
            SDA_pin = SDA pin (e.g., board.SDA, board.D13)
        INPUT
            SCL_pin          =      GPIO pin related to SCL
            SDA_pin          =      GPIO pin related to SDA
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
        """
        # Packages
        from busio import I2C
        import board
        import adafruit_sgp30
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
                
        # Functions
        # Create library object on our I2C port
        if chain is False:
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
        elif chain is True:
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
        else:
            raise ValueError()
        
        self.sgp30  = adafruit_sgp30.Adafruit_SGP30(i2c_address,address=self.i2c_address)
        
        # Print serial idea
        self.SGP30_serial = self.sgp30.serial
        print("SGP30 serial #", [hex(i) for i in self.sgp30.serial])
        
        # initialise IAQ sensor
        self.sgp30.iaq_init()
        self.sgp30.set_iaq_baseline(eCO2_baseline, TVOC_baseline)
        self.sgp30.set_iaq_relative_humidity(celcius=temperature_baseline, relative_humidity=rel_humidity_baseline)
    
    
    def set_baseline(self,eCO2_baseline, TVOC_baseline):
        """ FUNC: set baseline eCO2, TVOC
        
        """
        # Packages
        import adafruit_sgp30
        
        # Function        
        self.sgp30.set_iaq_baseline(eCO2_baseline, TVOC_baseline)
    
    
    def measure_eCO2(self):
        """ FUNC: Get eCO2 value
        INPUT
            none
        OUTPUT
            eCO2 = eCarbon dioxide in ppm
        
        """
        # Packages
        import adafruit_sgp30
        
        # Function
        # measure eCO2
        
        eCO2      = self.sgp30.eCO2
        
        # Store value
        self.eCO2 = eCO2
        
        return eCO2
    
    
    def measure_TVOC(self):
        """ FUNC: Get TVOC value
        INPUT
            none
        OUTPUT
            TVOC = Total Volatile Organic Compound in parts per billion
        
        """
        # Packages
        import adafruit_sgp30
        
        # Function
        # measure
        
        TVOC      = self.sgp30.TVOC
        
        # Store value
        self.TVOC = TVOC
        
        return TVOC 
        
 
    def measure_Ethanol(self):
        """ FUNC: Get Ethanol value
        INPUT
            none
        OUTPUT
            Ethanol = Ethanol in ticks
        
        """
        # Packages
        import adafruit_sgp30
        
        # Function
        # measure
        
        Ethanol      = self.sgp30.Ethanol
        
        # Store value
        self.Ethanol = Ethanol
        
        return Ethanol
    
    
    def measure_H2(self):
        """ FUNC: Get  H2 value
        INPUT
            none
        OUTPUT
             H2 =  H2 in ticks
        
        """
        # Packages
        import adafruit_sgp30
        
        # Function
        # measure
        
        H2      = self.sgp30.H2
        
        # Store value
        self.H2 =  H2
        
        return  H2
    
    
    def get_baseline_eCO2(self):
        """ FUNC: Get baseline eCO2
        
        """
        # Packages
        import adafruit_sgp30
        
        # Functions
        eCO2_baseline = self.sgp30.baseline_eCO2
        
        
        #store value        
        self.eCO2_baseline           = eCO2_baseline
        
        return eCO2_baseline
        

    def get_baseline_TVOC(self):
        """ FUNC: Get baseline TVOC
        
        """
        # Packages
        import adafruit_sgp30
        
        # Functions
        TVOC_baseline = self.sgp30.baseline_TVOC
        
        
        #store value        
        self.TVOC_baseline           = TVOC_baseline
        
        return TVOC_baseline
            
    
    def get_raw_Ethanol(self):
        """ FUNC: get raw Ethanol
        
        """
        # Packages
        import adafruit_sgp30
        
        # Function
        Ethanol_raw = self.sgp30.raw_measure()[1]
        self.Ethanol_raw          = Ethanol_raw
        
        return Ethanol_raw
    
    
    def get_raw_H2(self):
        """ FUNC: get raw H2
        
        """
        # Packages
        import adafruit_sgp30
        
        # Function
        H2_raw = self.sgp30.raw_measure()[0]
        self.H2_raw          = H2_raw
        
        return H2_raw
        
    
    def recalibrate_sensor(self, temperature, relative_humidity):
        """ FUNC: recalibrate sensor using relative humidity
        
        """
        # Packages
        import adafruit_sgp30
        
        # Function       
        self.sgp30.set_iaq_relative_humidity(self, temperature, relative_humidity)
        
        # add one to the counter for recalibration
        self.recalibration_counter  +=1
        # store calibration values
        self.rel_humidity = relative_humidity
        self.Temperature  = temperature
        

    def measure_all(self):
        """ FUNC: measure all (eCO2 and TVOC) for sensor SGP30, returns (eCO2_value, TVOC_value, Ethanol_value, H2_value)

        """
        # Packages
        import adafruit_sgp30
        
        # Functions
        # get baselines
        eCO2_baseline       = self.get_baseline_eCO2()
        TVOC_baseline       = self.get_baseline_TVOC()
        
        # get values
        eCO2_value          = self.measure_eCO2()
        TVOC_value          = self.measure_TVOC()
        Ethanol_value       = self.measure_Ethanol()
        H2_value            = self.measure_H2()
                
        return eCO2_value, TVOC_value, Ethanol_value, H2_value        
        
    
    def intial_measurement_SGP30(self,counter_limit_ = 30):
        """ SETUP: start up the sensor have an initial measurement phase
        
        """
        # Packages
        import adafruit_sgp30
        import time
        
        # Function
        self.measure_SGP30_in_def_loop(counter_limit = counter_limit_)
        
    
    
    def measure_SGP30_in_def_loop(self, counter_limit = 10, loop_sleep_value = 1):
        """ LOOP: measure SGP30 (definite)
        
        INPUT:
            counter_limit    = limit for the counter, default is 10
            loop_sleep_value = amount of time between loop iterations, default is 0.1
            
        OUTPUT:
            none
            
        """
        # Packages
        import adafruit_sgp30
        import time
        
        # Function
        counter     = 0
        elapsed_sec = 0
        
        while True:
            print("Time %d  eCO2 = %d ppm \t TVOC = %d ppb" % (elapsed_sec, self.measure_eCO2(), self.measure_TVOC()))
            time.sleep(loop_sleep_value)
            # add to counter
            counter     += 1
            elapsed_sec += 1
            
            if counter >counter_limit:
                self.get_baseline_eCO2()
                self.get_baseline_TVOC()
                break
    
    
    
    def measure_SGP30_in_indef_loop(self, get_baseline_at_count_limit = 10, loop_sleep_value = 1):
        """ LOOP: measure SGP30 (indefininte)
        
        INPUT:
            get_baseline_at_count_limit    = limit for the counter to get baseline value, default is 10 iterations
            loop_sleep_value               = amount of time between loop iterations, default is 1 second
            
        OUTPUT:
            none
            
        """
        # Packages
        import adafruit_sgp30
        import time
        
        # Function
        counter     = 0
        elapsed_sec = 0

        while True:
            
            print("Time %d  eCO2 = %d ppm \t TVOC = %d ppb" % (elapsed_sec, self.measure_eCO2(), self.measure_TVOC()))
            #time.sleep(1)
            
            # add to counter
            counter     += 1
            elapsed_sec += 1
            
            if counter > get_baseline_at_count_limit:
                counter = 0
                print(
                    "**** Baseline values: eCO2 = 0x%x, TVOC = 0x%x"
                    % (self.get_baseline_eCO2(), self.get_baseline_TVOC())

                     )
            time.sleep(loop_sleep_value)
    
                
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        """
        
        # functions
        eCO2_value     = self.eCO2
        TVOC_value     = self.TVOC
        Ethanol_value  = self.Ethanol
        H2_value       = self.H2
        
        print("\nCarbon dioxide (eCO2): %0.1f ppm" % (eCO2_value))
        print("Total Volatile Organic Compound (TVOC): %0.1f ppb " % TVOC_value)
        print("Ethanol: %0.3f " % Ethanol_value)
        print("H2 = %0.2f " % H2_value)
        
    
###########    
class sensor_bme680:
    """ CLASS: MEASURE BME680
        
        0x76 and 0x77
    
    EXAMPLE:
    
        # RUN BME680 sensor
        # import packagges
        from helper_functions import sensor_bme680
        import board
        import time
        
        # intialise sensor
        sensor1 = sensor_bme680()
        sensor1.start_sensor(SCL_pin = board.D11, SDA_pin = board.D13)
        
        # collect data
        while True:
            sensor1.measure_all()
            sensor1.print_sensor_values()
            time.sleep(10)
        
    """
    #Packages
    import time
    import board
    import busio
    from busio import I2C
    import adafruit_bme680
    
    def __init__(self):
        """ Initiliase class

        """
        # packages
        import time
        import board
        import busio
        from busio import I2C
        import adafruit_bme680
        
        # variables
        self.sensor_name             = "BME680"
        self.i2c_address             = 0x76
        self.i2c_address_alternative = 0x77
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        
        self.temperature             = None
        self.gas                     = None
        self.rel_humidity            = None
        self.pressure                = None
        self.altitude                = None
        
        # metadata
        self.variable_dictionary = {'Temperature' : {'variable name' : 'temperature' , 'units' : 'C'          , 'format' : '0.1f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : 'temperature_offset'},
                                    'Gas'         : {'variable name' : 'gas'         , 'units' : 'ohm'        , 'format' : 'd'   , 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Humidity'    : {'variable name' : 'rel_humidity', 'units' : 'percentage' , 'format' : '0.1f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Pressure'    : {'variable name' : 'pressure'    , 'units' : 'hPa'        , 'format' : '0.3f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : 'pressure_sea_level'},
                                    'Altitude'    : {'variable name' : 'altitude'    , 'units' : 'meters'     , 'format' : '0.2f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    }
    
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        INPUT:
            SCL_pin = SCL pin (e.g., board.SCL, board.D11)
            SDA_pin = SDA pin (e.g., board.SDA, board.D13)
        
        INPUT
            SCL_pin          =      GPIO pin related to SCL
            SDA_pin          =      GPIO pin related to SDA
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
        """
        # Packages
        from busio import I2C
        import adafruit_bme680
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
        
        # Functions
        # Create sensor object, communicating over the board's second I2C bus
        if chain is False:
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
        elif chain is True:
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
        else:
            raise ValueError()
        
        #self.i2c_address = i2c_address
        self.bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c_address,address=0x76,  debug=False)
    
    
    def measure_all(self, pressure_sea_level = 1011, temperature_offset = -5):
        """ FUNC: measure all variables, return (gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature)
        
        INPUT:
            none
        OUTPUT:
        
        """
        
        gas_value          = self.measure_gas()
        rel_humidity_value = self.measure_rel_humidity()
        altitude_value     = self.measure_altitude()
        pressure_value     = self.measure_pressure(pressure_sea_level)
        measured_temperature, corrected_temperature = self.measure_temperature(temperature_offset)
        
        return gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature
    
    
    def measure_gas(self):
        """ FUNC: measure gas in ohm
        
        INPUT:
            none
        OUTPUT:
            gas_value = gas value in ohm
        """
        # Packages
        import adafruit_bme680
        
        # Functions
        gas_value = self.bme680.gas
        
        # store value
        self.gas  = gas_value
        
        return gas_value
    
    
    def measure_rel_humidity(self):
        """ FUNC: measure relatiuve humidity in %
        
        INPUT:
            none
        OUTPUT:
            rel_humidity_value = relative humidity value in percentage
        """
        # Packages
        import adafruit_bme680
        
        # Functions
        rel_humidity_value = self.bme680.relative_humidity
        
        # store value
        self.rel_humidity  = rel_humidity_value
        
        return rel_humidity_value
    

    def measure_altitude(self):
        """ FUNC: measure altitude in meters
        
        INPUT:
            none
        OUTPUT:
            altitude_value = altitude in m
        """
        # Packages
        import adafruit_bme680
        
        # Functions
        altitude_value = self.bme680.altitude
        
        # store value
        self.altitude  = altitude_value
        
        return altitude_value
    
    
    def measure_pressure(self, pressure_sea_level = 1011):
        """ FUNC: measure pressure in hpa
        
        INPUT:
            pressure_sea_level = sea level pressure in hPA at location's sea level
        OUTPUT:
            pressure_value     = pressure value in hPA
        """
        # Packages
        import adafruit_bme680
        
        # Function
        # change this to match the location's pressure (hPa) at sea level
        self.bme680.sea_level_pressure = pressure_sea_level
        # get pressure value
        pressure_value = self.bme680.pressure
        # store variable
        self.pressure  = pressure_value
        
        return pressure_value
        
    
    def measure_temperature(self, temperature_offset = -5):
        """ FUNC: Measure temperature in Celcius
            
            NOTE:
                # You will usually have to add an offset to account for the temperature of
                # the sensor. This is usually around 5 degrees but varies by use. Use a
                # separate temperature sensor to calibrate this one.
            
            INPUT:
                temperature_offset    = offset temperature (default -5)
                
            OUTPUT:
                measured_temperature  = measured temperature
                corrected_temperature = corrected temperature (measured_temperature + temperature_offset)
        """
        # Packages
        import adafruit_bme680
        
        # Function    
        measured_temperature  = self.bme680.temperature
        corrected_temperature = measured_temperature + temperature_offset
        
        # store variable
        self.temperature = corrected_temperature
        
        return measured_temperature, corrected_temperature
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        """
        temperature  = self.temperature
        gas          = self.gas
        rel_humidity = self.rel_humidity
        pressure     = self.pressure
        altitude     = self.altitude
        
        print("\nTemperature: %0.1f C" % (temperature))
        print("Gas: %d ohm" % gas)
        print("Humidity: %0.1f %%" % rel_humidity)
        print("Pressure: %0.3f hPa" % pressure)
        print("Altitude = %0.2f meters" % altitude)

        

########## WIFI
class wifi_connect:
    """ CLASS: Wireless networking
    
    RATIONALE:
        make wifi
        
    REFERENCES:
        https://docs.circuitpython.org/projects/esp32spi/en/latest/index.html
    
    EXAMPLE:
    
    # packages
    from helper_functions import wifi_connect
    
    # set up wifi
    get_connected = wifi_connect()
    ap            = get_connected.scan_networks()
    get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
    """
    # PACKAGES
    # include as arduino connect module uf2
    import board
    import busio
    from digitalio import DigitalInOut
    
    # need to add following libraries to lib. folder:
    import adafruit_requests as requests
    import adafruit_esp32spi.adafruit_esp32spi_socket as socket
    from   adafruit_esp32spi import adafruit_esp32spi
    
    
    def __init__(self):
        """ Initiliase class

        """
        # packages
        import board
        import busio
        from digitalio import DigitalInOut
        import adafruit_esp32spi.adafruit_esp32spi_socket as socket
        from   adafruit_esp32spi import adafruit_esp32spi
        import adafruit_requests as requests
        
        # functions
        #  ESP32 pins
        self.esp32_cs      = DigitalInOut(board.CS1)
        self.esp32_ready   = DigitalInOut(board.ESP_BUSY)
        self.esp32_reset   = DigitalInOut(board.ESP_RESET)

        #  uses the secondary SPI connected through the ESP32
        self.spi = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)

        self.esp = adafruit_esp32spi.ESP_SPIcontrol(self.spi, self.esp32_cs, self.esp32_ready, self.esp32_reset)
        
        # as per the docs: "requests library the type of socket we're using (socket type varies by connectivity
        # type - we'll be using the adafruit_esp32spi_socket for this example). We'll also
        # set the interface to an esp object. This is a little bit of a hack, but it lets
        # us use requests like CPython does."
        self.requests.set_socket(socket, self.esp)
        
        # Print board status, firmware version, and mac address
        self.check_status()
        self.print_firmware_vers()
        self.print_mac_address()
        
        
    def get_status(self):
        """ RETURN: ESP board status
        
        INPUT
            none
        OUTPUT
            status_of_ = binary value of status
        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
            
        # function
        status_of_ =  self.esp.status 
        return status_of_
            
            
    def check_status(self):
        """PRINT: Compare status vs idle mode
        
        INPUT
            none
        OUTPUT
            print 'ESP32 found and in idle mode' if get_status == 0
        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
        
        # function
        if self.get_status() == adafruit_esp32spi.WL_IDLE_STATUS:
            print("ESP32 found and in idle mode")
        


    def get_connection_status(self):
        """ RETURN: Connection status
        
        INPUT
            none
        OUTPUT
            connection_status = returns True or False if connected
        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
        
        # function
        connection_status = self.esp.is_connected
        
        #print(connection_status)
        
        return connection_status
        
        
    def get_mac_address(self):
        """ RETURN Mac Address
        
        INPUT
            none
        OUTPUT
            mac_address = MAC address of board
        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
            
        # function
        mac_address = self.esp.MAC_address
        
        return mac_address
            
    
    def print_mac_address(self):
        """PRINT MAC address
        
        INPUT
            none
        OUTPUT
            print mac address 

        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
            
        # function
        print("MAC addr:", [hex(i) for i in self.get_mac_address()])
        
        
    def get_firmware_vers(self):
        """ RETURN: Firmware version
        
        INPUT
            none
        OUTPUT
            firmware = board firmware version
        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
            
        # function
        firmware = self.esp.firmware_version
        return firmware
        
    
    def print_firmware_vers(self):
        """ PRINT: Firmware version
        
        INPUT
            none
        OUTPUT
            print firmware version
        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
            
        # function
        print("Firmware vers.", self.get_firmware_vers())
        
       
        
    def scan_networks(self):
        """ FUNC: Scan for network Access Points
        
        INPUT
            none
        OUTPUT
            scan_result = Returns a list of dictionaries with ‘ssid’, ‘rssi’ and
                            ‘encryption’ entries, one for each AP found
        
        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
         
        # constants
        authmode_dict = {0: 'open',
                             1: 'WEP',
                             2: 'WPA-PSK',
                             3: 'WPA2-PSK',
                             4: 'WPA/WPA2-PSK'}

        hidden_dict   = {0: 'visible',
                             1: 'hidden'}
        
        # function
        # inform user
        print('scanning for access points')
        
        # scan for access points (ap)        
        # returns values as: ssid, bssid, channel, RSSI, authmode, hidden
        scan_result = self.esp.scan_networks()
                
        # print
        for ap in scan_result:
            #print("\t%s\t\tRSSI: %d" % (str(ap['ssid'], 'utf-8'), ap['rssi']))
            print(ap)
                
        return scan_result
    
    
    def print_ssid(self):
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
        
        # function
        ssid = str(self.esp.ssid, "utf-8")
        print(ssid)
        return ssid
    
    
    def connect_to_network(self, ssid_to_connect, ssid_password):
        """ Connect device to network
        
        INPUT
            ssid_to_connect = network ID
            ssid_password   = network password
            
        OUTPUT
            none (connection to network)
            
        EXAMPLE
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
        
        # function
        print("Connecting to AP...")
        
        # check if connected
        while not self.get_connection_status():
            
            try:
                self.esp.connect_AP(ssid_to_connect, ssid_password)
                
            except RuntimeError as e:
                print("could not connect to AP, retrying: ", e)
                continue
        
        print("Connected to", str(self.esp.ssid, "utf-8"), "\tRSSI:", self.esp.rssi)
        print("My IP address is", self.esp.pretty_ip(self.esp.ip_address))
    
    
    def check_connection(self,
                        PING_test = "google.com",
                        HOST_test = "adafruit.com",
                        TEXT_URL  = "http://wifitest.adafruit.com/testwifi/index.html",
                        JSON_URL  = "http://api.coindesk.com/v1/bpi/currentprice/USD.json"
                         ):
        """ FUNC: check wifi connection
        
        REF:
            https://learn.adafruit.com/pyportal-oblique-strategies/internet-connect
            
        INPUT:
            PING_test = destination for test checking timing (returns ms timing)
            HOST_test = destination for test checking ip lookup
            TEXT_URL  = destination for test checking fetching text
            JSON_URL  = destination for test checking fetching json
            
        OUTPUT:
            prints values
        """
        # packages
        from   adafruit_esp32spi import adafruit_esp32spi
        
        # functions
        print("Checking internet connection...")
        
        # IP look up test
        print("IP lookup Test:")
        print("IP lookup adafruit.com: %s" % self.esp.pretty_ip(self.esp.get_host_by_name(HOST_test)))
        
        # PING test, returns a millisecond timing value to a destination IP address or hostname
        print("Ping Test:")
        print("Ping google.com: %d ms" % self.esp.ping(PING_test))
        
        # TEXT retrieval test
        print("Text retrieval Test:")
        # esp._debug = True
        print("Fetching text from", TEXT_URL)
        r = self.requests.get(TEXT_URL)
        print("-" * 40)
        print(r.text)
        print("-" * 40)
        r.close()

        # JSON retrieval test
        print("JSON retrieval Test:")
        print()
        print("Fetching json from", JSON_URL)
        r = self.requests.get(JSON_URL)
        print("-" * 40)
        print(r.json())
        print("-" * 40)
        r.close()

        print("Done!")


############ DISPLAY
class oled_1_12inch:
    """
    NOTE
    
    REF
        https://github.com/adafruit/Adafruit_CircuitPython_DisplayIO_SH1107/blob/main/examples/displayio_sh1107_mono_128x128_test.py
    
    EXAMPLES
    
    EXAMPLE1:
        import board
        display1 = oled_1_12inch(SCL_pin = board.SCL,  SDA_pin = board.SDA)
        display1.draw_rectangle()
        display1.add_text("test", 2,4, 1)
        
    EXAMPLE2:
        # RUN BME680 sensor and DISPLAY
        from helper_functions import sensor_bme680,oled_1_12inch
        import board
        import time

        sensor1 = sensor_bme680()
        sensor1.start_sensor(SCL_pin = board.D11, SDA_pin = board.D13)

        display1 = oled_1_12inch(SCL_pin = board.SCL,  SDA_pin = board.SDA)
        display1.draw_rectangle()

        while True:
            sensor1.measure_all()
            sensor1.print_sensor_values()
            
            display1.draw_rectangle()
            display1.add_text("Temp.: %0.1f C" % sensor1.temperature, 4,20, 1)
            display1.add_text("Gas: %d ohm" % sensor1.gas, 4,40, 1)
            display1.add_text("Hum.: %0.1f %%" %  sensor1.rel_humidity, 4,60, 1)
            display1.add_text("Pres.: %0.3f hPa" % sensor1.pressure, 4,80, 1)
            display1.add_text("Alt. = %0.2f m" % sensor1.altitude, 4,100, 1)
            
            
            time.sleep(10)
    
    EXAMPLE 3
        from helper_functions import sensor_bme680,oled_1_12inch
        import board
        import time

        sensor1 = sensor_bme680()
        sensor1.start_sensor(SCL_pin = board.D11, SDA_pin = board.D13)

        display1 = oled_1_12inch(SCL_pin = board.SCL,  SDA_pin = board.SDA)
        display1.draw_rectangle()
        loading_txt = display1.add_text_to_group(" WUR Digital Twins \n Loading \n Please Wait ", 4,40, 1, group_to_add_to = display1.splash)
        time.sleep(10)

        while True:
            sensor1.measure_all()
            sensor1.print_sensor_values()
            
            #display1.refresh_display()
            #display1.draw_rectangle()
            display1.add_text_group([
                                     ["Temp.: %0.1f C" % sensor1.temperature  , 4,20, 1],
                                     ["Gas: %d ohm" % sensor1.gas             , 4,40, 1],
                                     ["Hum.: %0.1f %%" %  sensor1.rel_humidity, 4,60, 1],
                                     ["Pres.: %0.3f hPa" % sensor1.pressure   , 4,80, 1],
                                     ["Alt. = %0.2f m" % sensor1.altitude    , 4,100, 1],
                                     ]
                                    )    
            time.sleep(10)
    

    """
    #Packages
    import board
    import displayio
    import terminalio
    
    # add to lib
    import adafruit_display_text.bitmap_label as label # from adafruit_display_text
    import adafruit_displayio_sh1107
    from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297


    def __init__(self, using_which ='busio',
                        SCL_pin          = None,
                        SDA_pin          = None,
                        display_width    = 128,
                        display_height   = 128,
                        display_border   = 2,
                        display_rotation = -90,
                 ):
        """ FUNC: Initiliase class
        
        INPUT
            using_which = for an IF statement to change between busio or board library modules
            SCL_pin          = SCL pin default is None,
            SDA_pin          = SDA pin default is None,
            display_width    = display width default is 128,
            display_height   = display height default is 128,
            display_border   = display border default is 2,
            display_rotation = rotation of display default is -90,
            
        OUTPUT
            none
            
        """
        # PACKAGES
        import board
        import displayio
        import terminalio
        import adafruit_display_text.bitmap_label as label # from adafruit_display_text
        import adafruit_displayio_sh1107
        from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
        
        # VARIABLES
        self.WIDTH    = display_width
        self.HEIGHT   = display_height
        self.BORDER   = display_border
        self.ROTATION = display_rotation  # SH1107 is vertically oriented 64x128 so need to modify
        
        # FUNCTION
        displayio.release_displays()
        #oled_reset = board.D9
        
        if using_which == 'board':
            
            # packages
            import board
            
            # Use for I2C
            i2c_address = board.I2C()
            
        
        elif using_which == 'busio':
            
            # packages
            from busio import I2C
        
            # Create sensor object, communicating over the board's second I2C bus
            i2c_address = I2C(SCL_pin,SDA_pin)
            
        else:
            raise ValueError('using_which should be busio or board')
            
        self.display_bus = displayio.I2CDisplay(i2c_address, device_address=0x3c)
        
        # Start display
        self.display = adafruit_displayio_sh1107.SH1107(self.display_bus,
                                                       width=self.WIDTH, height=self.HEIGHT,
                                                       display_offset = DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297,
                                                       rotation       = self.ROTATION)

        # Make the display context
        self.splash = displayio.Group()
        self.display.show(self.splash)

        self.color_bitmap     = displayio.Bitmap(self.WIDTH, self.HEIGHT, 1)
        self.color_palette    = displayio.Palette(1)
        self.color_palette[0] = 0xFFFFFF  # White

        self.bg_sprite        = displayio.TileGrid(self.color_bitmap, pixel_shader=self.color_palette, x=0, y=0)
        self.splash.append(self.bg_sprite)
        
        
    
    def refresh_display(self):
        """ FUNC: refresh display
        
        INPUT
            none
        OUTPUT
            none
        """
        self.display.refresh()
    
    
    def draw_rectangle(self):
        """ FUNC: Draw rectangle
                
        INPUT
            none
        OUTPUT
            none
        """
        # PACKAGES
        import board
        import displayio
        import terminalio
        import adafruit_display_text.bitmap_label as label # from adafruit_display_text
        import adafruit_displayio_sh1107
        from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
        
        # Draw a smaller inner rectangle in black
        self.inner_bitmap     = displayio.Bitmap(self.WIDTH - self.BORDER * 2, self.HEIGHT - self.BORDER * 2, 1)
        self.inner_palette    = displayio.Palette(1)
        self.inner_palette[0] = 0x000000  # Black
        self.inner_sprite     = displayio.TileGrid(self.inner_bitmap, pixel_shader=self.inner_palette,
                                                   x=self.BORDER, y=self.BORDER)
        self.splash.append(self.inner_sprite)
        
    
    def add_text_to_group(self, 
                       text_to_display          = "test",
                       text_to_display_x        =  0,
                       text_to_display_y        =  0,
                       text_to_display_scale    =  1,
                       group_to_add_to          = None,
                       text_to_display_color    =  0xFFFFFF,
                 
                 ):
        """ FUNC: add text to a group
        
            INPUT
                group_to_add_to = self.splash
        """
        # PACKAGES
        import board
        import displayio
        import terminalio
        import adafruit_display_text.bitmap_label as label # from adafruit_display_text
        import adafruit_displayio_sh1107
        from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
        
        # FUNCTION
        
        text_area = label.Label(terminalio.FONT,
                                text  =text_to_display,
                                scale = text_to_display_scale,
                                color =text_to_display_color,
                                x     =text_to_display_x,
                                y     =text_to_display_y)
        group_to_add_to.append(text_area)


    def add_text_to_text_group(self, text_to_display       = "test",
                       
                       text_to_display_x     =  0,
                       text_to_display_y     =  0,
                       text_to_display_scale =  1,
                       text_to_display_color =  0xFFFFFF,
                 
                 ):
        """ FUNC: add text
        
        """
        # PACKAGES
        import board
        import displayio
        import terminalio
        import adafruit_display_text.bitmap_label as label # from adafruit_display_text
        import adafruit_displayio_sh1107
        from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
        
        # FUNCTION
        
        text_area = label.Label(terminalio.FONT,
                                text  =text_to_display,
                                scale = text_to_display_scale,
                                color =text_to_display_color,
                                x     =text_to_display_x,
                                y     =text_to_display_y)
        self.text_to_display.append(text_area)
    
        
    def add_text_group(self,list_of_lists):
        """
            INPUT:
                list_of_lists = a list of lists to add text (see notes below)
                
            OUTPUT:
                none
                
            NOTE:
                text_A = display1.add_text_to_group("Temp.: %0.1f C" % sensor1.temperature, 4,20, 1)
                text_B = display1.add_text_to_group("Gas: %d ohm" % sensor1.gas, 4,40, 1)
                text_C = display1.add_text_to_group("Hum.: %0.1f %%" %  sensor1.rel_humidity, 4,60, 1)
                text_D = display1.add_text_to_group("Pres.: %0.3f hPa" % sensor1.pressure, 4,80, 1)
                text_E = display1.add_text_to_group("Alt. = %0.2f m" % sensor1.altitude, 4,100, 1)
                
                converts to list of lists in the format [TEXT , x_value, y_value, scale] like so:
                
                text_to_add = [  ["Temp.: %0.1f C" % sensor1.temperature  , 4,20, 1],
                                 ["Gas: %d ohm" % sensor1.gas             , 4,40, 1],
                                 ["Hum.: %0.1f %%" %  sensor1.rel_humidity, 4,60, 1],
                                 ["Pres.: %0.3f hPa" % sensor1.pressure   , 4,80, 1],
                                 ["Alt. = %0.2f m" % sensor1.altitude    , 4,100, 1],
                                 ]
             

        """
        # PACKAGES
        import board
        import displayio
        import terminalio
        import adafruit_display_text.bitmap_label as label # from adafruit_display_text
        import adafruit_displayio_sh1107
        from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
        
        # FUNCTION
        # Make the display context
        self.text_to_display = displayio.Group()
        self.display.show(self.text_to_display)
        
        for item in list_of_lists:
            
            self.add_text_to_text_group(item[0],item[1], item[2], item[3])
        






