""" Example 1: LTR-559  with CircuitPython

# NOTES:
        > IR/UV-filtering
        > 50.60Hz flicker rejection
        > 0.01 lux to 64,000 lux light detection range
        > ~5cm proximity detection range
        
# REF:
        https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559
        https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559/blob/master/pimoroni_circuitpython_ltr559.py

"""

# packages
import time
import board
from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559

# variables 

# Define I2C bus, for this example the default board.SCL and board.SDA are used
i2c = board.I2C()  

# Create sensor object, communicating over the board's default I2C bus
ltr559 = Pimoroni_LTR559(i2c, address=0x23)

# initiate loop and print out sensor variables
while True:

    lux       = ltr559.lux 
    proximity = ltr559.prox
    print("\nLux: %0.2f Lux" % (lux))
    print("Proximity: %0.1f %%" % proximity)

    time.sleep(1)