""" Example 2: SGP30 and BME280 with CircuitPython
# Modified from:
# https://learn.adafruit.com/adafruit-sgp30-gas-tvoc-eco2-mox-sensor/circuitpython-wiring-test
# Original: SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries; SPDX-License-Identifier: MIT
"""

# packages
import time
import board
import busio
import adafruit_sgp30
from adafruit_bme280 import basic as adafruit_bme280

# variables
baseline_eCO2         =  0x8973 #in hex
baseline_TVOC         =  0x8AAE #in hex
baseline_temperature  =  22.1   #in celcius
baseline_rel_humidity =  44     #in percent
calibration_time      =  15     #in seconds, time for calibration step
print_baseline_count  =  10     #in seconds, time for printing baseline var.

# Define I2C bus, for this example the default board.SCL and board.SDA are used
i2c = busio.I2C(board.SCL, board.SDA)

# Create sensor object, communicating over the board's default I2C bus
sgp30 = adafruit_sgp30.Adafruit_SGP30(i2c, address=0x58)

# Get serial hex code of the sensor
print("SGP30 serial #", [hex(i) for i in sgp30.serial])   

# Create second object, communicating using the same i2c, give address
# alternate address is 0x77
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c, address=0x76)

# modify output:
# IAQ baseline for TVOC and eCO2 
sgp30.set_iaq_baseline(baseline_TVOC, baseline_eCO2)
# Set baseline temperature and rel. humidity
sgp30.set_iaq_relative_humidity(baseline_temperature,
                                baseline_rel_humidity)


# initiate loop and print out sensor variables
elapsed_second_count = 0
print_count          = 0
measurement          = False

print("Start measuring...")
print("\nTemperature: %0.1f C" % bme280.temperature)
print("Humidity: %0.1f %%"     % bme280.humidity)
print("Pressure: %0.1f hPa"    % bme280.pressure)


while True:
    
    print("t = %i \t eCO2 = %d ppm \t TVOC = %d ppb \t measurement = %s \t Temp =  %0.1f C \t Humid =  %0.1f " % (elapsed_second_count,
                                                                           sgp30.eCO2, sgp30.TVOC, measurement, bme280.temperature,
                                                                            bme280.humidity))
    print_count           += 1
    
    time.sleep(1)
    
    elapsed_second_count += 1
    
    if elapsed_second_count == calibration_time:
        
        print()
        print('Calibration step finished, starting measurements') 
        measurement = True
        
    if print_count > print_baseline_count:
        
        # reset counter
        print_count = 0
        
        print()
        print(
                "**** Baseline values: eCO2 = 0x%x, TVOC = 0x%x"
                % (sgp30.baseline_eCO2, sgp30.baseline_TVOC)
                )
        print()

