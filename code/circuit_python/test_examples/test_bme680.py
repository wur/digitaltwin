""" Example 1: BME680 with CircuitPython
# Modified from:
# https://learn.adafruit.com/adafruit-bme680-humidity-temperature-barometic-pressure-voc-gas/python-circuitpython
# Original: SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries; SPDX-License-Identifier: MIT
"""

# packages
import time
import board
import adafruit_bme680

# variables 
location_sea_level_pressure  = 1013.25   # location's pressure (hPa) at sea level
temperature_offset_from_true = -5        # sensor needs to be calibrated

# Define I2C bus, for this example the default board.SCL and board.SDA are used
i2c = board.I2C()  

# Create sensor object, communicating over the board's default I2C bus
bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, address=0x76, debug=False)
# or use alternate address, by uncommenting:
#bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, address=0x77, debug=False)

# modified output
bme680.sea_level_pressure = location_sea_level_pressure

# initiate loop and print out sensor variables
while True:

    print("\nTemperature: %0.1f C"  % (bme680.temperature + temperature_offset_from_true))
    print("Gas: %d ohm"             % bme680.gas)
    print("Humidity: %0.1f %%"      % bme680.relative_humidity)
    print("Pressure: %0.3f hPa"     % bme680.pressure)
    print("Altitude = %0.2f meters" % bme680.altitude)

    time.sleep(1)