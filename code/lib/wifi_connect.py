class wifi_connect:
    """ To connect the wifi

    """
    # PACKAGES
    # include as arduino connect module uf2
    import board
    import busio
    from digitalio import DigitalInOut
    
    # need to add following libraries to lib. folder:
    import adafruit_requests as requests
    import adafruit_esp32spi.adafruit_esp32spi_socket as socket
    from   adafruit_esp32spi import adafruit_esp32spi
    
    
    def __init__(self):
        """ Initiliase class

        """
        #  ESP32 pins
        self.esp32_cs      = DigitalInOut(board.CS1)
        self.esp32_ready   = DigitalInOut(board.ESP_BUSY)
        self.esp32_reset   = DigitalInOut(board.ESP_RESET)

        #  uses the secondary SPI connected through the ESP32
        self.spi = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)

        self.esp = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)

        self.requests.set_socket(socket, esp)
        
        # Get status, firmware, mac address
        get_status()
        get_firmware_vers()
        get_mac_address()
        
        
        def get_status(self):
            """ Get status

            """
            # packages
            from   adafruit_esp32spi import adafruit_esp32spi
            
            # function
            if esp.status == adafruit_esp32spi.WL_IDLE_STATUS:
                print("ESP32 found and in idle mode")
            
        
        def get_mac_address(self):
            """ Get Mac Address

            """
            # packages
            from   adafruit_esp32spi import adafruit_esp32spi
            
            # function
            print("MAC addr:", [hex(i) for i in self.esp.MAC_address])
            
            
        def get_firmware_vers(self):
            """ Get Firmware version

            """
            # packages
            from   adafruit_esp32spi import adafruit_esp32spi
            
            # function
            print("Firmware vers.", self.esp.firmware_version)
            
        
        def scan_networks(self):
            """ Scan for network Access Points


            """
            # packages
            from   adafruit_esp32spi import adafruit_esp32spi
            
            # constants
            authmode_dict = {0: 'open',
                             1: 'WEP',
                             2: 'WPA-PSK',
                             3: 'WPA2-PSK',
                             4: 'WPA/WPA2-PSK'}

            hidden_dict   = {0: 'visible',
                             1: 'hidden'}
    
            # function
            # inform user
            print('scanning for access points')
            
            # scan for access points (ap)        
            # returns values as: ssid, bssid, channel, RSSI, authmode, hidden
            scan_result = esp.scan_networks():
                
            # print
            for ap in scan_result:
                #print("\t%s\t\tRSSI: %d" % (str(ap['ssid'], 'utf-8'), ap['rssi']))
                print(ap)
                
            return ap