""" wifi_module.py

# Rationale:
    Networking via wifi

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    12/08/2022 

# Licence:
    
    
# URL:
    

# Requirements:
    #Packages
    import board
    import busio
    from digitalio import DigitalInOut
        
    # need to add following libraries to lib. folder:
    import adafruit_requests as requests
    import adafruit_esp32spi.adafruit_esp32spi_socket as socket
    from   adafruit_esp32spi import adafruit_esp32spi

# Contains:
    class : wifi_connect
            which contains: [_init_, get_status, check_status,
                            get_connection_status, get_mac_address,
                            print_mac_address, get_firmware_vers,
                            print_firmware_vers, scan_networks,
                            scan_networks_ALL_INFO, print_ssid,
                            print_ip, connect_to_network, check_connection]
    

# References:
    https://docs.circuitpython.org/projects/esp32spi/en/latest/index.html
    
# Examples:
    ## Example Arduino nano Rp2040 Connect

    # packages
    from wur_circuitpython_functions import wifi_module
    import board

    try:
        from secrets import secrets
    except:
        raise ValueError(' Requires a secrets dictionary containing ssid and password key-value pairs ')
    
    get_connected = wifi_module.connect(pin_esp32_cs       = board.CS1,
                                        pin_esp32_ready    = board.ESP_BUSY,
                                        pin_esp32_reset    = board.ESP_RESET,
                                        pin_SCK            = board.SCK1,
                                        pin_SDO            = board.MOSI1,
                                        pin_SDI            = board.MISO1)
    ap            = get_connected.scan_networks()
    get_connected.connect_to_network(secrets["ssid"], secrets["password"])


    ## Example Raspberry Pico  with ESP32, e.g., Pimoroni WiFi Pack 
        https://shop.pimoroni.com/products/pico-wireless-pack?variant=32369508581459
        
        # packages
        from wur_circuitpython_functions import wifi_module
        import board
        
        try:
            from secrets import secrets
        except:
            raise ValueError(' Requires a secrets dictionary containing ssid and password key-value pairs ')
        
        get_connected = wifi_module.connect(pin_esp32_cs       = board.GP13,
                                            pin_esp32_ready    = board.GP14,
                                            pin_esp32_reset    = board.GP15,
                                            pin_SCK            = board.GP10,
                                            pin_SDO            = board.GP11,
                                            pin_SDI            = board.GP12)
        ap            = get_connected.scan_networks()
        get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
"""
    
#Packages
import board
import busio
from digitalio import DigitalInOut
    
# need to add following libraries to lib. folder:
import adafruit_requests as requests
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from   adafruit_esp32spi import adafruit_esp32spi


# Class
class connect:
    """ CLASS: Wireless networking
    
    Rationale:
        Networking via wifi
        
    REFERENCES:
        https://docs.circuitpython.org/projects/esp32spi/en/latest/index.html
    
    EXAMPLE:
    
    # packages
    from helper_functions import wifi_connect
    
    # set up wifi
    get_connected = connect()
    ap            = get_connected.scan_networks()
    get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
    """

    
    
    def __init__(self, 
                 pin_esp32_cs       = None,
                 pin_esp32_ready    = None,
                 pin_esp32_reset    = None,
                 pin_SCK            = None,
                 pin_SDO            = None,
                 pin_SDI            = None,
                 
                 ):
        """ Initialise class
        
        Input:
            pin_esp32_cs       = CS pin, default = board.CS1,
            pin_esp32_ready    = Ready pin, default = board.ESP_BUSY,
            pin_esp32_reset    = Reset pin, default = board.ESP_RESET,
            pin_SCK            = SCK pin, default = board.SCK1,
            pin_SDO            = SDO pin, default = board.MOSI1,
            pin_SDI            = SDI pin, default = board.MISO1,
        
            
            Note 1: for Arduino Nano Connect RP2040
            connect(
                pin_esp32_cs = board.CS1,
                 pin_esp32_ready    = board.ESP_BUSY,
                 pin_esp32_reset    = board.ESP_RESET,
                 pin_SCK            = board.SCK1,
                 pin_SDO            = board.MOSI1,
                 pin_SDI            = board.MISO1
                 )
            
            Note 2: for rapsberry pi pico with wifi shield the pins are: 
        
                connect(

                        pin_esp32_cs       = board.GP13,
                        pin_esp32_ready    = board.GP14,
                        pin_esp32_reset    = board.GP15,
                        pin_SCK            = board.GP10,
                        pin_SDO            = board.GP11,
                        pin_SDI            = board.GP12)
                
                )
        
        Packages:
            import board
            import busio
            from digitalio import DigitalInOut
                
            # need to add following libraries to lib. folder:
            import adafruit_requests as requests
            import adafruit_esp32spi.adafruit_esp32spi_socket as socket
            from   adafruit_esp32spi import adafruit_esp32spi
            
        """

        # init packages
        import adafruit_requests as requests
        import adafruit_esp32spi.adafruit_esp32spi_socket as socket

        # VARIABLES
        # Internal variables
        
        # Dictionaries
        # used for scan networks give all info (for function
        # scan_networks_ALL_INFO)
        # Authorisation mode dictionaries giving key and value 
        self.authmode_dict = {0: 'open',
                              1: 'WEP',
                              2: 'WPA-PSK',
                              3: 'WPA2-PSK',
                              4: 'WPA/WPA2-PSK'}
        
        # Hidden mode dictionaries giving key and value 
        self.hidden_dict   = {0: 'visible',
                              1: 'hidden'}
        
        
        # ESP32 pins
        self.esp32_cs      = DigitalInOut(pin_esp32_cs)
        self.esp32_ready   = DigitalInOut(pin_esp32_ready)
        self.esp32_reset   = DigitalInOut(pin_esp32_reset)

        # uses the secondary SPI connected through the ESP32
        self.spi = busio.SPI(pin_SCK, pin_SDO, pin_SDI)

        self.esp = adafruit_esp32spi.ESP_SPIcontrol(self.spi, self.esp32_cs, self.esp32_ready, self.esp32_reset)
        
        # as per the docs: "requests library the type of socket we're using (socket type varies by connectivity
        # type - we'll be using the adafruit_esp32spi_socket for this example). We'll also
        # set the interface to an esp object. This is a little bit of a hack, but it lets
        # us use requests like CPython does."
        # socket is called via the package: import adafruit_esp32spi.adafruit_esp32spi_socket as socket
        self.requests = requests
        self.requests.set_socket(socket, self.esp)
        
        # Print board status, firmware version, and mac address
        self.check_status()
        self.print_firmware_vers()
        self.print_mac_address()
        
        
    def get_status(self):
        """ RETURN: ESP board status
        
        Input:
            None
            
        Output:
            status_of_ = binary value of status
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        status_of_ =  self.esp.status
        
        return status_of_
            
      
    def check_status(self):
        """PRINT: Compare status vs idle mode
        
        Input:
            None
            
        Output:
            print 'ESP32 found and in idle mode' if get_status == 0
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        if self.get_status() == adafruit_esp32spi.WL_IDLE_STATUS:
            
            print("ESP32 found and in idle mode")
        
    
    def get_connection_status(self):
        """ RETURN: Connection status
        
        Input:
            None
            
        Output:
            connection_status = returns True or False if connected
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
            
        """
        # function
        connection_status = self.esp.is_connected
        
        #print(connection_status)
        
        return connection_status
        
        
    def get_mac_address(self):
        """ RETURN: Mac Address
        
        Input:
            None

        Output:
            mac_address = MAC address of board
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """ 
        # function
        mac_address = self.esp.MAC_address
        
        return mac_address
            
    
    def print_mac_address(self):
        """PRINT: MAC address
        
        Input:
            None
            
        Output:
            print mac address
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        print("MAC addr:", [hex(i) for i in self.get_mac_address()])
        
      
    def get_firmware_vers(self):
        """ RETURN: Firmware version
        
        Input:
            None
        
        Output:
            firmware = board firmware version
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        firmware = self.esp.firmware_version
        return firmware
        
    
    def print_firmware_vers(self):
        """ PRINT: Firmware version
        
        Input:
            None
            
        Output:
            print firmware version
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        print("Firmware vers.", self.get_firmware_vers())
        
    
    def scan_networks(self):
        """ FUNC: Scan for network Access Points
        
        Input:
            None
        
        Output:
            scan_result = Returns a list of dictionaries with ‘ssid’, ‘rssi’ and
                            ‘encryption’ entries, one for each AP found
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        # inform user
        print('scanning for access points')
        
        # scan for access points (ap)        
        # returns values as: ssid, bssid, channel, RSSI, authmode, hidden
        scan_result = self.esp.scan_networks()
                
        # print
        for ap in scan_result:
            #print("\t%s\t\tRSSI: %d" % (str(ap['ssid'], 'utf-8'), ap['rssi']))
            print(ap)
                
        return scan_result
    
    
    def scan_networks_ALL_INFO(self):
        """ FUNC: Scan for network Access Points give all info
        
        Input:
            None
        
        Output:
            scan_result = Returns a list of dictionaries with ‘ssid’, ‘rssi’ and
                            ‘encryption’ entries, one for each AP found
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        # inform user
        print('scanning for access points')
        
        # scan for access points (ap)        
        # returns values as: ssid, bssid, channel, RSSI, authmode, hidden
        scan_result = self.esp.scan_networks()
                
        # print
        for ap in scan_result:
            
                       
            hidden_value = self.hidden_dict[ap['hidden']]
            autho_value  = self.authmode_dict[ap['authmode']]
            
            print("\tSSID: %s \n\t\tRSSI: %d \n\t\t Channel: %s\t\t Hidden: %s\t\t Authentication: %s" % (str(ap['ssid'], 'utf-8'), ap['rssi'],str(channel),hidden_value,autho_value))
            
           
        return scan_result
    
    
    def print_ssid(self):
        """ PRINT: SSID
        
        Input:
            None

        Output:
            ssid = returns ssid 
            (also prints ssid)

        Package:
            from   adafruit_esp32spi import adafruit_esp32spi

        Example:
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
            get_connected.print_ssid()
        
        """
        # function
        ssid = str(self.esp.ssid, "utf-8")
        print(ssid)
        return ssid
    

    def print_ip(self):
        """ PRINT: IP address
        
        Input:
            None

        Output:
            pretty_ip_address = IP address
            (also prints IP address)

        Package:
            from   adafruit_esp32spi import adafruit_esp32spi

        Example:
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
            get_connected.print_ip()
        
        """
        # function
        pretty_ip_address = str(self.esp.pretty_ip(self.esp.ip_address)) 
        print(pretty_ip_address)

        return pretty_ip_address

    
    def connect_to_network(self, ssid_to_connect, ssid_password):
        """ CONNECT: Connect device to network
        
        Input:
            ssid_to_connect = network ID
            ssid_password   = network password
            
        Output:
            None (connection to network)
        
        Package:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        Example:
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
        
        """
        # function
        print("Connecting to AP...")
        
        # check if connected
        while not self.get_connection_status():
            
            try:
                self.esp.connect_AP(ssid_to_connect, ssid_password)
                
            except RuntimeError as e:
                print("could not connect to AP, retrying: ", e)
                continue
        
        print("Connected to", str(self.esp.ssid, "utf-8"), "\tRSSI:", self.esp.rssi)
        print("My IP address is", self.esp.pretty_ip(self.esp.ip_address))
    
    
    def check_connection(self,
                        PING_test = "google.com",
                        HOST_test = "adafruit.com",
                        TEXT_URL  = "http://wifitest.adafruit.com/testwifi/index.html",
                        JSON_URL  = "http://api.coindesk.com/v1/bpi/currentprice/USD.json"
                         ):
        """ FUNC: check wifi connection
        
        Input:
            PING_test = destination for test checking timing (returns ms timing)
            HOST_test = destination for test checking ip lookup
            TEXT_URL  = destination for test checking fetching text
            JSON_URL  = destination for test checking fetching json
            
        Output:
            prints values
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        Reference:
            https://learn.adafruit.com/pyportal-oblique-strategies/internet-connect
        
        """
        # functions
        print("Checking internet connection...")
        
        # IP look up test
        print("IP lookup Test:")
        print("IP lookup adafruit.com: %s" % self.esp.pretty_ip(self.esp.get_host_by_name(HOST_test)))
        
        # PING test, returns a millisecond timing value to a destination IP address or hostname
        print("Ping Test:")
        print("Ping google.com: %d ms" % self.esp.ping(PING_test))
        
        # TEXT retrieval test
        print("Text retrieval Test:")
        # esp._debug = True
        print("Fetching text from", TEXT_URL)
        r = self.requests.get(TEXT_URL)
        print("-" * 40)
        print(r.text)
        print("-" * 40)
        r.close()

        # JSON retrieval test
        print("JSON retrieval Test:")
        print()
        print("Fetching json from", JSON_URL)
        r = self.requests.get(JSON_URL)
        print("-" * 40)
        print(r.json())
        print("-" * 40)
        r.close()

        print("Done!")


# Functions
# None


# Dictionaries
# None
