""" sensor_TMP117.py

# Rationale:
    Start up, run, and print variables of I2C sensor TMP117
    # Purpose: Precision Temperature sensor
    # i2C: 0x48

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    09/08/2022 

# Licence:
    
    
# URL:
      

# Requirements:
    from board import SCL, SDA
    from busio import I2C
    import adafruit_tmp117

# Contains:
    class : sensor_TMP117
            which contains: [   _init_, start_sensor,
                            set_measurement_delay, set_measurement_average,
                            measure_temperature, measure_all,
                            print_sensor_values, get_alert_status
                            ]

# References:
    https://learn.adafruit.com/adafruit-tmp117-high-accuracy-i2c-temperature-monitor/python-circuitpython


# Notes:
    "The TMP117 Precision Temperature Sensor is an I2C
    temperature sensor that will help you easily add
    temperature measurement and adjustment to your project.
    In addition to the obvious support for reading the temperature,
    the TMP117 can also monitor the temperature and alert you
    when corrective action needs to be taken."
    - https://learn.adafruit.com/adafruit-tmp117-high-accuracy-i2c-temperature-monitor/overview
    
    "TMP117 has 16-bit / 0.0078°C measurement resolution, wide operating range of -55 to 155°C,
    and up to ±0.1°C accuracy. The chip also has high and low temperature alerts and interrupt support,
    and hardware support required for NIST traceability, this temperature sensor is perfect for
    applications where you need to keep a close eye on temperature."
    - https://www.adafruit.com/product/4821
    
    "TMP117 temperature sensor has different accuracy for different ranges, here's what you
    can expect as the maximum variation for popular temperature ranges:

        ±0.1°C (maximum) from –20°C to +50°C
        ±0.15°C (maximum) from –40°C to +70°C
        ±0.2°C (maximum) from –40°C to +100°C
        ±0.25°C (maximum) from –55°C to +125°C
        ±0.3°C (maximum) from –55°C to +150°C "
        
    - https://www.adafruit.com/product/4821
    
    
# Examples:



"""

# Packages
from board import SCL, SDA
from busio import I2C
import adafruit_tmp117


# Class
class sensor_TMP117:
    """CLASS: MEASURE TMP117
    
    
    """
    
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        # VARIABLES
        # Internal class variables
        
        # sensor
        self.sensor_name                   = "TMP117"
        self.i2c_address                   = 0x48
        self.i2c_address_alternative       = None
        self.i2c_I2C                       = None
        self.i2c_frequency                 = 100000   # use 'slow' 100KHz frequency
        self.tmp117                        = None
        self.high_temperature_limit        = None     # high temperature limit for sensor high_limit
        self.low_temperature_limit         = None     # low temperature limit for sensor low_limit
        
        self.sensor_alert_mode             = None     # default: self.tmp117.alert_mode = adafruit_tmp117.AlertMode.WINDOW #default
        #                                                # self.tmp117.alert_mode = adafruit_tmp117.AlertMode.HYSTERESIS
        self.sensor_high_alert_status      = None
        self.sensor_low_alert_status       = None
        self.sensor_delay_measurements     = None
        self.sensor_averaged_measurements  = None
        
        # measured variables
        self.temperature                   = None
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {'Temperature' : {'variable name' : 'temperature'  , 'units symbol' : 'C'     , 'units' : 'degrees Celcius'  , 'format' : '0.2f', 'data_type' : 'measured',   'method' : None, 'provenance' : 'sensor TMP117', 'dependent_variable' : None},
                                    
                                    }
    
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None,frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        Output:
        
        Packages:
            from busio import I2C
            import adafruit_tmp117
            
        """
        
        # Variables
        if frequency is not None:
            
            self.i2c_frequency = frequency
            
        else:
            
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        self.tmp117 = adafruit_tmp117.TMP117(i2c_address)
    
    
    def set_measurement_delay(self, delay = None):
        """ SET: measurement delay
        
        Input:
            delay = should be one of the following numbers [0.0015, 0.125, 0.250, 0.500, 1, 4, 8, 16] in seconds
        
        Output:
            None, modify self.sensor_delay_measurements and self.tmp117.measurement_delay
        
        Packages:
            from busio import I2C
            import adafruit_tmp117
        
        Note:
            # self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_0_0015_S
            # self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_0_125_S
            # self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_0_250_S
            # self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_0_500_S
            # self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_1_S
            # self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_4_S
            # self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_8_S
            # self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_16_S
            
        """
        if delay == None:
            
            print('Delay is None')
            
        elif delay == 0.0015:
            print('Measurement delay is ' + str(delay) + ' seconds')
            self.sensor_delay_measurements = delay
            self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_0_0015_S
        
        elif delay == 0.125:
            print('Measurement delay is ' + str(delay) + ' seconds')
            self.sensor_delay_measurements = delay
            self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_0_125_S
            
        elif delay == 0.250:
            print('Measurement delay is ' + str(delay) + ' seconds')
            self.sensor_delay_measurements = delay
            self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_0_250_S
            
        elif delay == 0.500:
            print('Measurement delay is ' + str(delay) + ' seconds')
            self.sensor_delay_measurements = delay
            self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_0_500_S
            
        elif delay == 1:
            print('Measurement delay is ' + str(delay) + ' seconds')
            self.sensor_delay_measurements = delay
            self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_1_S
            
        elif delay == 4:
            print('Measurement delay is ' + str(delay) + ' seconds')
            self.sensor_delay_measurements = delay
            self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_4_S
            
        elif delay == 8:
            print('Measurement delay is ' + str(delay) + ' seconds')
            self.sensor_delay_measurements = delay
            self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_8_S
        
        elif delay == 16:
            print('Measurement delay is ' + str(delay) + ' seconds')
            self.sensor_delay_measurements = delay
            self.tmp117.measurement_delay = adafruit_tmp117.MeasurementDelay.DELAY_16_S
            
        else:
            
            raise ValueError(' delay should be one of the following numbers [0.0015, 0.125, 0.250, 0.500, 1, 4, 8, 16] in seconds ')
    
    
    def set_measurement_average(self, average = None):
        """ SET: measurement average
        
        Input:
            average = averageing of sensor measurements can be times [1, 8, 32, 64]
        
        Output:
        
        
        Packages:
        
        
        Notes:
        
            The number of measurements that are taken and averaged before updating the temperature
            measurement register. A larger number will reduce measurement noise but may also affect
            the rate at which measurements are updated, depending on the value of `measurement_delay`
            Note that each averaged measurement takes 15.5ms which means that larger numbers of averaged
            measurements may make the delay between new reported measurements to exceed the delay set
            by `measurement_delay 
            
            # tmp117.averaged_measurements = AverageCount.AVERAGE_1X
            # tmp117.averaged_measurements = AverageCount.AVERAGE_8X
            # tmp117.averaged_measurements = AverageCount.AVERAGE_32X
            # tmp117.averaged_measurements = AverageCount.AVERAGE_64X
        
        """
        
        if average == None:
            
            print('Average is None')
        
        elif average == 1:
            print('Measurement average is ' + str(average) + 'x ')
            self.sensor_averaged_measurements = average
            self.tmp117.averaged_measurements = adafruit_tmp117.AverageCount.AVERAGE_1X
        
        elif average == 8:
            print('Measurement average is ' + str(average) + 'x ')
            self.sensor_averaged_measurements = average
            self.tmp117.averaged_measurements = adafruit_tmp117.AverageCount.AVERAGE_8X
            
        elif average == 32:
            print('Measurement average is ' + str(average) + 'x ')
            self.sensor_averaged_measurements = average
            self.tmp117.averaged_measurements = adafruit_tmp117.AverageCount.AVERAGE_32X
            
        elif average == 64:
            print('Measurement average is ' + str(average) + 'x ')
            self.sensor_averaged_measurements = average
            self.tmp117.averaged_measurements = adafruit_tmp117.AverageCount.AVERAGE_64X
            
        else:
            
            raise ValueError(' average should be one of the following numbers [1, 8, 32, 64] x ')    
        
     
    
    def measure_temperature(self):
        """FUNC: measure temperature
        
        Input:
            None
        
        Output:
            temperature = sensor temperature value
        
        Packages:
            from busio import I2C
            import adafruit_tmp117
        
        """
        # functions
        temperature      = self.tmp117.temperature
        self.temperature = temperature
        
        return temperature
    
  
    def measure_all(self):
        """ FUNC: measure all variables, return (temperature)
        
        Input:
            None
            
        Output:
            temperature
        
        Packages:
            from busio import I2C
            import adafruit_tmp117
        
        """
        
        
        temperature = self.measure_temperature()
        
        return temperature
    
   
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            None, print values
        
        Packages:
            from busio import I2C
            import adafruit_tmp117
        
        Notes:
            print("\nTemperature: %0.2f C" % (temperature))
        
        """
        # functions
        temperature  = self.temperature        
        
        # Print values
        # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
        temperature_text = "\nTemperature: %" + self.variable_dictionary['Temperature']['format'] + " " +  self.variable_dictionary['Temperature']['units symbol']
        
        print()
        print(temperature_text % (temperature))

    
    def get_alert_status(self):
        """ GET: alert status for (high_alert_status, low_alert_status)
        
        Input:
            None
            
        Output:
            high_alert_status = status of high temperature alert, boolelaan 
            low_alert_status  = status of low temperature alert, boolelaan 
            (and) modify self.
        
        Packages:
            from busio import I2C
            import adafruit_tmp117
        
        Notes:
        
        """
        
        alert_status                  = self.tmp117.alert_status
        high_alert_status             = alert_status.high_alert
        low_alert_status              = alert_status.low_alert
        
        # modify internal class variables
        self.sensor_high_alert_status = high_alert_status
        self.sensor_low_alert_status  = low_alert_status
        
        
        return high_alert_status, low_alert_status
        
    
    ###### 

# Functions
# None

# Dictionaries
# None

