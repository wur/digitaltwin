""" sensor_pmsa003i.py

# Rationale:
    Start up, run, and print variables of I2C sensor PMSA003i
    # Purpose: Particulate matter (PM) air quality sensor PM2.5
    # i2C: 0x12

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
      

# Requirements:
    import time
    import board
    import busio
    from board import SCL, SDA
    from busio import I2C
    from digitalio import DigitalInOut, Direction, Pull
    from adafruit_pm25.i2c import PM25_I2C
       

# Contains:
    class : sensor_pmsa003i
            which contains: [ _init_, ]

# References:
    

# Notes:
    # MEASURED VARIABLES
    self.pm10_env        'pm10 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
    self.pm25_env        'pm25 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
    self.pm100_env       'pm100 env'       #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            
    self.pm10_standard   'pm10 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"    
    self.pm25_standard   'pm25 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
    self.pm100_standard  'pm100 standard'  #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            
            
    self.particles_03um  'particles 03um'  #"Particles > 0.3um / 0.1L air:"
    self.particles_05um  'particles 05um'  #"Particles > 0.5um / 0.1L air:"
    self.particles_10um  'particles 10um'  #"Particles > 1.0um / 0.1L air:"
    self.particles_25um  'particles 25um'  #"Particles > 2.5um / 0.1L air:"
    self.particles_50um  'particles 50um'  #"Particles > 5.0um / 0.1L air:"
    self.particles_100um 'particles 100um' #"Particles > 10 um / 0.1L air:"
    
    
# Examples:



"""

# Packages
import time
import board
import busio
#from board import SCL, SDA
from busio import I2C
from digitalio import DigitalInOut, Direction, Pull
from adafruit_pm25.i2c import PM25_I2C


class sensor_pmsa003i:
    """CLASS: MEASURE PMSA003I
    
    
    """
    
    
    def __init__(self):
        """ Initialise class
        
        # MEASURED VARIABLES
            self.pm10_env        'pm10 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            self.pm25_env        'pm25 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            self.pm100_env       'pm100 env'       #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            
            self.pm10_standard   'pm10 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"    
            self.pm25_standard   'pm25 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            self.pm100_standard  'pm100 standard'  #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            
            
            self.particles_03um  'particles 03um'  #"Particles > 0.3um / 0.1L air:"
            self.particles_05um  'particles 05um'  #"Particles > 0.5um / 0.1L air:"
            self.particles_10um  'particles 10um'  #"Particles > 1.0um / 0.1L air:"
            self.particles_25um  'particles 25um'  #"Particles > 2.5um / 0.1L air:"
            self.particles_50um  'particles 50um'  #"Particles > 5.0um / 0.1L air:"
            self.particles_100um 'particles 100um' #"Particles > 10 um / 0.1L air:"
        
        """

        
        # Variables
        # sensor
        self.sensor_name             = "PMSA003I"
        self.i2c_address             = 0x12
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.reset_pin               = None     # If you have a GPIO, its not a bad idea to connect it to the RESET pin; reset_pin = DigitalInOut(board.G0); reset_pin.direction = Direction.OUTPUT; reset_pin.value = False
        self.pm25                    = None
                                
        # measured variables
        self.pm10_env                = None    # 'pm10 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        self.pm25_env                = None    # 'pm25 env'        #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        self.pm100_env               = None    # 'pm100 env'       #"Concentration Units (environmental)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        
        self.pm10_standard           = None    # 'pm10 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"    
        self.pm25_standard           = None    # 'pm25 standard'   #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        self.pm100_standard          = None    # 'pm100 standard'  #"Concentration Units (standard)" "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
        
        
        self.particles_03um          = None    # 'particles 03um'  #"Particles > 0.3um / 0.1L air:"
        self.particles_05um          = None    # 'particles 05um'  #"Particles > 0.5um / 0.1L air:"
        self.particles_10um          = None    # 'particles 10um'  #"Particles > 1.0um / 0.1L air:"
        self.particles_25um          = None    # 'particles 25um'  #"Particles > 2.5um / 0.1L air:"
        self.particles_50um          = None    # 'particles 50um'  #"Particles > 5.0um / 0.1L air:"
        self.particles_100um         = None    # 'particles 100um' #"Particles > 10 um / 0.1L air:"
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {}
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency=None, reset_pin = None,chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin          =      GPIO pin related to SCL
            SDA_pin          =      GPIO pin related to SDA
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            integration_time =      The integration_time is the amount of time the VEML6075
                                    is sampling data for, in milliseconds. Valid times are:
                                    50, 100, 200, 400 or 800ms. Default is 100
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        """
        
        # Variables
        if frequency is not None:
            
            self.i2c_frequency = frequency
            
        else:
            
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port, use 'slow' 100KHz frequency!
        if chain is False:
            
            i2c_address  = busio.I2C(SCL_pin,SDA_pin,frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            print('Reminder: PM sensor need to have i2C frequency of 100000 (100 kHz)')
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        # Connect to a PM2.5 sensor over I2C
        self.pm25  = PM25_I2C(i2c_address, reset_pin)
    
    
    def measure_all(self):
        """ FUNC: measure all, returns (self.pm10_env, self.pm25_env, self.pm100_env, self.pm10_standard, self.pm25_standard, self.pm100_standard, self.particles_03um, self.particles_05um, self.particles_10um, self.particles_25um, self.particles_50um, self.particles_100um)
        
            Input:
                None
                
            Output:
            
            
            Packages:
            
            
            Note:
                data or -99 if an error
            
        """
        try:
            aqdata = self.pm25.read()
            
            # measured variables
            self.pm10_env                = aqdata["pm10 env"]
            self.pm25_env                = aqdata["pm25 env"]
            self.pm100_env               = aqdata["pm100 env"]
             
            self.pm10_standard           = aqdata["pm10 standard"]
            self.pm25_standard           = aqdata["pm25 standard"]
            self.pm100_standard          = aqdata["pm100 standard"]
            
            self.particles_03um          = aqdata["particles 03um"]
            self.particles_05um          = aqdata["particles 05um"]
            self.particles_10um          = aqdata["particles 10um"]
            self.particles_25um          = aqdata["particles 25um"]
            self.particles_50um          = aqdata["particles 50um"]
            self.particles_100um         = aqdata["particles 100um"]
            
            return self.pm10_env, self.pm25_env, self.pm100_env, self.pm10_standard, self.pm25_standard, self.pm100_standard, self.particles_03um, self.particles_05um, self.particles_10um, self.particles_25um, self.particles_50um, self.particles_100um        
        
        except:
            # measured variables
            self.pm10_env                = -99
            self.pm25_env                = -99
            self.pm100_env               = -99
             
            self.pm10_standard           = -99
            self.pm25_standard           = -99
            self.pm100_standard          = -99
            
            self.particles_03um          = -99
            self.particles_05um          = -99
            self.particles_10um          = -99
            self.particles_25um          = -99
            self.particles_50um          = -99
            self.particles_100um         = -99
            
            return self.pm10_env, self.pm25_env, self.pm100_env, self.pm10_standard, self.pm25_standard, self.pm100_standard, self.particles_03um, self.particles_05um, self.particles_10um, self.particles_25um, self.particles_50um, self.particles_100um        
        
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            None, print values
            
        """
        # variables
        
        
        print()
        print("Concentration Units (standard)")
        print("---------------------------------------")
        print(
            "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            % (self.pm10_standard, self.pm25_standard, self.pm100_standard)
        )
        print("Concentration Units (environmental)")
        print("---------------------------------------")
        print(
            "PM 1.0: %d\tPM2.5: %d\tPM10: %d"
            % (self.pm10_env, self.pm25_env, self.pm100_env)
        )
        print("---------------------------------------")
        print("Particles > 0.3um / 0.1L air:", self.particles_03um)
        print("Particles > 0.5um / 0.1L air:", self.particles_05um)
        print("Particles > 1.0um / 0.1L air:", self.particles_10um)
        print("Particles > 2.5um / 0.1L air:", self.particles_25um)
        print("Particles > 5.0um / 0.1L air:", self.particles_50um)
        print("Particles > 10 um / 0.1L air:", self.particles_100um)
        print("---------------------------------------")
        



# Functions
# None


# Dictionaries
# None
