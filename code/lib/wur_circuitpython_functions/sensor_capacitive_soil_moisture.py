""" sensor_capacitive_soil_moisture.py

# Rationale:
    Capacitive Soil Moisture Sensor with CircuitPython
    # Purpose: Measure soil moisture using Capacitive Soil Moisture Sensor SKU SEN0193
    # i2C    : None


# Authors:
    WUR Digital Twin Platform Methodology


# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_moisture.html 


# Requirements:
    import board
    import time
    from analogio import AnalogIn
    from digitalio import DigitalInOut, Direction, Pull


# Contains:
    class : soil_sensor
            which contains: [ _init_, start_sensor,
                            measure_soil, measure_all,
                            print_sensor_values,
                            modify_divide, modify_add,         # generic functions
                            modify_subtract, modify_multiply,  # generic functions
                            ]


# References:
    https://wiki.dfrobot.com/Capacitive_Soil_Moisture_Sensor_SKU_SEN0193
    https://andywarburton.co.uk/raspberry-pi-pico-soil-moisture-sensor/


# Notes:
    > Capacitive_Soil_Moisture_Sensor_SKU_SEN0193
        
    > RED = power; BLACK = Ground; BLUE = Analog pin
    connect red power wire to digital pin (e.g., D2),
    connect black ground wire to GND pin, connect
    blue wire to analog pin (e.g., A0)
    
    
    > Typical ranges with value = value/100:
    Dry   = [520, 430]
    Wet   = [430, 350]
    Water = [350, 260]  
        
    
# Examples:



"""

# Packages
import board
import time
from analogio import AnalogIn
from digitalio import DigitalInOut, Direction, Pull


# Class
class soil_sensor:
    """CLASS: Capacitive Soil Moisture Sensor with CircuitPython
    
    
    """
    
    
    def __init__(self):
        """ Initialise class
        
        Input:
            None
            
        Output:
            None
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        """
        
        # sensor
        self.sensor_name               = "Capacitive Soil Moisture Sensor"
        self.analog_soil_sensor        = None
        self.analog_soil_sensor_power  = None
                
        # measured variables
        self.sensor_value              = None
        
        # calculated variables
        self.sensor_value_modified     = None
        self.sensor_interpretation     = None
        
        # metadata
        self.variable_dictionary = {'Moisture'          : {'variable name' : 'soil moisture' , 'units symbol' : ' ' , 'units' : ' ' , 'format' : 'i'   , 'data_type' : 'measured'  , 'method' : None, 'provenance' : 'Capacitive Soil Moisture Sensor', 'dependent_variable' : None},
                                    'Moisture modified' : {'variable name' : 'soil moisture' , 'units symbol' : ' ' , 'units' : ' ' , 'format' : '0.2f', 'data_type' : 'calculated', 'method' : None, 'provenance' : 'Capacitive Soil Moisture Sensor', 'dependent_variable' : 'sensor_value'},
                                    }
    
    
    ###### Generic Functions
    def modify_divide(self, value, modifier = None):
        """ FUNC - DIVIDE: modify value via divide
                
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output = value / modifier
        
        return output 
    
    
    def modify_add(self, value, modifier = None):
        """ FUNC - ADD: modify value via add
                
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output = value + modifier
        
        return output
    
    
    def modify_subtract(self, value, modifier = None):
        """ FUNC - SUBTRACT: modify value via subtract
                
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output =  value - modifier
        
        return output
    

    def modify_multiply(self, value, modifier = None):
        """ FUNC - MULTIPLY: modify value via multiply
        
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output =  value * modifier
        
        return output
    
    
    ###### Sensors
    def start_sensor(self, power_pin = None, analog_pin = None):
        """ FUNC: create sensor object
        
        Input:
            power_pin        =      power pin,  e.g., digital pin board.D2
            analog_pin       =      analog pin, e.g., board.A0
            
        Output:
        
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
        
            
        """
        
        # Start-up SOIL SENSOR
        # setup the moisture sensor power pin and turn it off by default
        self.analog_soil_sensor_power            = DigitalInOut(power_pin)
        self.analog_soil_sensor_power.direction  = Direction.OUTPUT
        self.analog_soil_sensor_power.value      = False
        
        
        # set the analog read pin for the moisture sensor
        self.analog_soil_sensor_analog_in        = AnalogIn(analog_pin)
    
    
    def measure_soil(self):
        """ FUNC: Measure soil moisture
        
        Input:
            None
        
        Output:
            sensor_value = soil analog sensor value
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        """
        # Turn on sensor
        self.analog_soil_sensor_power.value = True
        
        # measured variables
        sensor_value      = self.analog_soil_sensor_analog_in.value
        self.sensor_value = sensor_value
        
        return sensor_value
    
    
    def calculate_soil(self, modifier = 100):
        """ FUNC: Calculate modified soil moisture (= soil moisture/100)
        
        Input:
            modifier = arbitrary value for reducing output value,
                        default = 100
        
        Output:
            sensor_value_mod = soil analog sensor value modified
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        """
        # Check if the sensor value has been measured
        if self.sensor_value is None:
            
            self.analog_soil_sensor_power.value = True
        
        # measured variables
        sensor_value      = self.analog_soil_sensor_analog_in.value
        self.sensor_value = sensor_value
        
        
        # calculated variables
        sensor_value_mod            = self.modify_divide(sensor_value, modifier)
        self.sensor_value_modified  = sensor_value_mod
        
        return sensor_value_mod
    
    
    def measure_all(self, modifier = 100):
        """ FUNC: measure all, returns (sensor_value, sensor_value_mod)
        
        Input:
            modifier = arbitrary value for reducing output value,
                        default = 100
        
        Output:
            sensor_value, sensor_value_mod
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        """
        # Turn on sensor
        self.analog_soil_sensor_power.value = True
        
        # measured variables
        sensor_value      = self.measure_soil()
        self.sensor_value = sensor_value
        
        
        # calculated variables
        sensor_value_mod            = self.calculate_soil(modifier)
        self.sensor_value_modified  = sensor_value_mod
        
        return sensor_value, sensor_value_mod
    
    
    def print_sensor_values(self, modified_or_not = 'modified'):
        """ PRINT: print all sensor values
        
        Input:
            modified_or_not = to print the modified value or not, default is'modified', options are 'modified' or 'not modified'
        
        Output:
            None, print sensor values
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        
        Note:
            print("\Soil Moisture: %0.2f " % (moisture))
        
        """
        # functions
        if modified_or_not == 'modified':
            
            moisture      = self.sensor_value_modified
            
            # Print values
            # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
            moisture_text = "\nSoil Moisture: %"      + self.variable_dictionary['Moisture modified']['format']      + " " +  self.variable_dictionary['Moisture modified']['units symbol']
            print(moisture_text  % (moisture))
        
        
        elif modified_or_not == 'not modified':
            
            moisture      = self.sensor_value
        
            # Print values
            # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
            moisture_text = "\nSoil Moisture: %"      + self.variable_dictionary['Moisture']['format']      + " " +  self.variable_dictionary['Moisture']['units symbol']  
            print(moisture_text  % (moisture))
        
        
        else:
            
            raise ValueError(' modified_or_not should either be modified or not modified ')
        
    
    def print_sensor_interpretation(self,
                                        Dry   = [520, 430, 'Dry'  ],
                                        Wet   = [430, 350, 'Wet'  ],
                                        Water = [350, 260, 'Water'],
        ):
        """ FUNC: print interpretation
        
        Input:
            Dry    = list of: highest value, lowest value, and interpretation for Dry sensor conditions using modified (/100) values
            Wet    = list of: highest value, lowest value, and interpretation for Wet sensor conditions using modified (/100) values
            Water  = list of: highest value, lowest value, and interpretation for sensor in Water using modified (/100) values
            
        Output:
            None, modify self.sensor_interpretation and print interpretation
            
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        Note:
            values for Dry, Wet, and Water come from:
            https://wiki.dfrobot.com/Capacitive_Soil_Moisture_Sensor_SKU_SEN0193
            but can be modified by changing their inputs
            
        """

        if self.sensor_value_modified is not None:
            
            if self.sensor_value_modified > Dry[0]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' GREATER THAN list (Flag)')
                self.sensor_interpretation = 'Flag - Higher than known values'
                
            elif Dry[1] <= self.sensor_value_modified <= Dry[0]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' meaning ' + str(Dry[2]))
                self.sensor_interpretation = Dry[2]
                
            elif Wet[1] <= self.sensor_value_modified <= Wet[0]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' meaning ' + str(Wet[2]))
                self.sensor_interpretation = Wet[2]
                
            elif Water[1] <= self.sensor_value_modified <= Water[0]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' meaning ' + str(Water[2]))
                self.sensor_interpretation = Water[2]
                
            elif self.sensor_value_modified > Water[1]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' LESS THAN list (Flag)')
                self.sensor_interpretation = 'Flag - Lower than known values'
            
            else:
                raise ValueError('Value does not match known values')
                
        else:
            print('No sensor value recorded use measure_soil or measure_all')
            
        
# Functions
# None

# Dictionaries
# None

