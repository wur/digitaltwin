""" sensor_SGP30.py

# Rationale:
    Start up, run, and print variables of I2C sensor SGP30
    # Purpose: TVOC/CO2eq sensor
    # i2C: 0x58

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html

# Requirements:
    

# Contains:
    class : sensor_SGP30
            which contains: [_init_, start_sensor, measure_all,
            
                            ]

# References:
    https://learn.adafruit.com/adafruit-sgp30-gas-tvoc-eco2-mox-sensor/circuitpython-wiring-test

# Examples:



"""

#Packages
import time
import board
import busio
from busio import I2C
import adafruit_sgp30


# Class
class sensor_SGP30:
    """CLASS: MEASURE SGP30
     
    """
    
    
    def __init__(self):
        """ Initiliase class
        
        Input:
            None
            
        Output:
            None
            
        Packages:
            import time
            import board
            import busio
            import adafruit_sgp30
            
        """
        
        # VARIABLES
        # Internal class variables
        self.sensor_name             = "SGP30"
        self.i2c_address             = 0x58
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        
        self.sgp30                   = None
        self.SGP30_serial            = None
        
        self.eCO2                    = None
        self.TVOC                    = None
        self.H2                      = None
        self.Ethanol                 = None
        self.rel_humidity            = None
        self.Temperature             = None
        
        self.eCO2_baseline           = None
        self.TVOC_baseline           = None
        
        self.H2_raw                  = None
        self.Ethanol_raw             = None
        
        self.recalibration_counter   = 0
        
        # metadata
        self.variable_dictionary = {            
            'Equivalent Carbon dioxide'       : {'variable name' : 'eCO2'   , 'units symbol' : 'ppm'   , 'units' : 'parts per million', 'format' : '0.1f', 'data_type' : 'calculated', 'provenance' : 'sensor SGP30', 'dependent_variable' : ['eCO2_baseline', 'H2', 'Ethanol']},
            'Total Volatile Organic Compound' : {'variable name' : 'TVOC'   , 'units symbol' : 'ppb'   , 'units' : 'parts per billion', 'format' : '0.1f', 'data_type' : 'calculated', 'provenance' : 'sensor SGP30', 'dependent_variable' : ['TVOC_baseline', 'H2', 'Ethanol']},
                                  'Ethanol'   : {'variable name' : 'Ethanol', 'units symbol' : ' '     , 'units' : 'counts'           , 'format' : '0.3f', 'data_type' : 'measured'  , 'provenance' : 'sensor SGP30', 'dependent_variable' : None},
                                    'H2'      : {'variable name' : 'H2'     , 'units symbol' : ' '     , 'units' : 'counts'           , 'format' : '0.2f', 'data_type' : 'measured'  , 'provenance' : 'sensor SGP30', 'dependent_variable' : None},
                                    }    
    
    
    def start_sensor(self,
                     SCL_pin = None, SDA_pin = None,
                     frequency = None,
                     eCO2_baseline        = 0x8973, TVOC_baseline = 0x8AAE,
                     temperature_baseline = 22.1, rel_humidity_baseline = 44,
                     chain = False, i2c_chainaddress = None
                     ):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin               =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin               =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency             =      i2c frequency, default is None but uses internal class value of 100,000
            eCO2_baseline         =      recorded baseline value for eCO2, default hex value 0x8973
            TVOC_baseline         =      recorded baseline value for TVOC, default hex value 0x8973
            temperature_baseline  =  sensor temperature baseline, default is 22.1 C
            rel_humidity_baseline = sensor rel. humidity baseline, default is 44%
            chain                 =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress      =      The i2c address of the 'main' sensor
            
        Output:
            
        
        Packages:
            from busio import I2C
            import board
            import adafruit_sgp30
        
        """
        
        # Variables
        if frequency is not None:
            
            self.i2c_frequency = frequency
            
        else:
            
            frequency = self.i2c_frequency
        
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        # Start sensor
        self.sgp30  = adafruit_sgp30.Adafruit_SGP30(i2c_address,address=self.i2c_address)
        
        # Print serial id
        self.SGP30_serial = self.sgp30.serial
        print("SGP30 serial #", [hex(i) for i in self.sgp30.serial])
        
        # initialise IAQ sensor
        self.sgp30.iaq_init()
        self.sgp30.set_iaq_baseline(eCO2_baseline, TVOC_baseline)
        self.sgp30.set_iaq_relative_humidity(celcius=temperature_baseline, relative_humidity=rel_humidity_baseline)
    
    
    def set_baseline(self,eCO2_baseline, TVOC_baseline):
        """ FUNC: set baseline eCO2, TVOC
        
        Input:
            eCO2_baseline = baseline CO2 value
            TVOC_baseline = baseline TVOC value
        
        Output:
        
        
        Package:
            import adafruit_sgp30
            
        """       
        # Function        
        self.sgp30.set_iaq_baseline(eCO2_baseline, TVOC_baseline)
    
    
    def measure_eCO2(self):
        """ FUNC: Get eCO2 value

        Input:
            None
            
        Output:
            eCO2 = eCarbon dioxide in ppm
            
        Packages:
            import adafruit_sgp30
        
        """
        # Function
        # measure eCO2
        
        eCO2      = self.sgp30.eCO2
        
        # Store value
        self.eCO2 = eCO2
        
        return eCO2
    
    
    def measure_TVOC(self):
        """ FUNC: Get TVOC value

        Input:
            None
            
        Output:
            TVOC = Total Volatile Organic Compound in parts per billion
        
        Packages:
            import adafruit_sgp30
            
        """
        # Function
        # measure
        
        TVOC      = self.sgp30.TVOC
        
        # Store value
        self.TVOC = TVOC
        
        return TVOC 
        
 
    def measure_Ethanol(self):
        """ FUNC: Get Ethanol value

        Input:
            None
            
        Output:
            Ethanol = Ethanol in ticks
            
        Packages:
            import adafruit_sgp30
        
        """
        # Function
        # measure
        
        Ethanol      = self.sgp30.Ethanol
        
        # Store value
        self.Ethanol = Ethanol
        
        return Ethanol
    
    
    def measure_H2(self):
        """ FUNC: Get  H2 value
        
        Input:
            None
            
        Output:
             H2 =  H2 in ticks
        
        Packages:
            import adafruit_sgp30
        
        """
        # Function
        # measure
        
        H2      = self.sgp30.H2
        
        # Store value
        self.H2 =  H2
        
        return  H2
    
    
    def get_baseline_eCO2(self):
        """ FUNC: Get baseline eCO2
        
        Input:
            None
            
        Output:
             eCO2_baseline =  eCO2 baseline value
        
        Packages
            import adafruit_sgp30
            
        """
        # 
        
        # Functions
        eCO2_baseline = self.sgp30.baseline_eCO2
        
        
        #store value        
        self.eCO2_baseline           = eCO2_baseline
        
        return eCO2_baseline
        

    def get_baseline_TVOC(self):
        """ FUNC: Get baseline TVOC
        
        Input:
            None
            
        Output:
             TVOC_baseline =  TVOC baseline value
        
        Packages
            import adafruit_sgp30
        
        """
        # Functions
        TVOC_baseline = self.sgp30.baseline_TVOC
        
        
        #store value        
        self.TVOC_baseline           = TVOC_baseline
        
        return TVOC_baseline
            
    
    def get_raw_Ethanol(self):
        """ FUNC: get raw Ethanol
        
        Input:
            None
            
        Output:
             Ethanol_raw =  Raw Ethanol value
        
        Packages
            import adafruit_sgp30
        
        """
        # Function
        Ethanol_raw = self.sgp30.raw_measure()[1]
        self.Ethanol_raw          = Ethanol_raw
        
        return Ethanol_raw
    
    
    def get_raw_H2(self):
        """ FUNC: get raw H2
        
        Input:
            None
            
        Output:
             H2_raw =  Raw H2 value
        
        Packages
            import adafruit_sgp30
        
        """
        # Function
        H2_raw = self.sgp30.raw_measure()[0]
        self.H2_raw          = H2_raw
        
        return H2_raw
        
    
    def recalibrate_sensor(self, temperature, relative_humidity):
        """ FUNC: recalibrate sensor using relative humidity
        
        Input:
            temperature        =
            relative_humidity  = 
            
        Output:
             None, change internal variables
        
        Packages
            import adafruit_sgp30
        
        """
        # Function       
        self.sgp30.set_iaq_relative_humidity(self, temperature, relative_humidity)
        
        # add one to the counter for recalibration
        self.recalibration_counter  +=1
        # store calibration values
        self.rel_humidity = relative_humidity
        self.Temperature  = temperature
        

    def measure_all(self):
        """ FUNC: measure all (eCO2 and TVOC) for sensor SGP30, returns (eCO2_value, TVOC_value, Ethanol_value, H2_value)
        
        Input:
            None
            
        Output:
             eCO2_value, TVOC_value, Ethanol_value, H2_value
        
        Packages
            import adafruit_sgp30
        
        """
        # Functions
        # get baselines
        eCO2_baseline       = self.get_baseline_eCO2()
        TVOC_baseline       = self.get_baseline_TVOC()
        
        # get values
        eCO2_value          = self.measure_eCO2()
        TVOC_value          = self.measure_TVOC()
        Ethanol_value       = self.measure_Ethanol()
        H2_value            = self.measure_H2()
                
        return eCO2_value, TVOC_value, Ethanol_value, H2_value        
        
    
    def intial_measurement_SGP30(self,counter_limit_ = 30):
        """ SETUP: start up the sensor have an initial measurement phase
        
        Input:
            counter_limit_ = number of measurements to measure as a calibration step, default = 30
            
        Output:
             None, call function
        
        Packages
            import adafruit_sgp30
            import time
            
        """
        # Function
        self.measure_SGP30_in_def_loop(counter_limit = counter_limit_)
        
    
    
    def measure_SGP30_in_def_loop(self, counter_limit = 10, loop_sleep_value = 1):
        """ LOOP: measure SGP30 (definite)
        
        Input:
            counter_limit    = limit for the counter, default is 10
            loop_sleep_value = amount of time between loop iterations, default is 0.1
            
        Output:
            none
        
        Packages:
            import adafruit_sgp30
            import time
        
        """
        
        
        # Function
        counter     = 0
        elapsed_sec = 0
        
        while True:
            print("Time %d  eCO2 = %d ppm \t TVOC = %d ppb" % (elapsed_sec, self.measure_eCO2(), self.measure_TVOC()))
            time.sleep(loop_sleep_value)
            # add to counter
            counter     += 1
            elapsed_sec += 1
            
            if counter >counter_limit:
                self.get_baseline_eCO2()
                self.get_baseline_TVOC()
                break
    
    
    
    def measure_SGP30_in_indef_loop(self, get_baseline_at_count_limit = 10, loop_sleep_value = 1):
        """ LOOP: measure SGP30 (indefininte)
        
        Input:
            get_baseline_at_count_limit    = limit for the counter to get baseline value, default is 10 iterations
            loop_sleep_value               = amount of time between loop iterations, default is 1 second
            
        Output:
            none
        
        Packages:
            import adafruit_sgp30
            import time
        
        """
        # Function
        counter     = 0
        elapsed_sec = 0

        while True:
            
            print("Time %d  eCO2 = %d ppm \t TVOC = %d ppb" % (elapsed_sec, self.measure_eCO2(), self.measure_TVOC()))
            #time.sleep(1)
            
            # add to counter
            counter     += 1
            elapsed_sec += 1
            
            if counter > get_baseline_at_count_limit:
                counter = 0
                print(
                    "**** Baseline values: eCO2 = 0x%x, TVOC = 0x%x"
                    % (self.get_baseline_eCO2(), self.get_baseline_TVOC())

                     )
            time.sleep(loop_sleep_value)
    
                
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            call functions, print
        
        Packages:
            import adafruit_sgp30
            import time
        
        Note:
            print("\nCarbon dioxide (eCO2): %0.1f ppm" % (eCO2_value))
            print("Total Volatile Organic Compound (TVOC): %0.1f ppb " % TVOC_value)
            print("Ethanol: %0.3f " % Ethanol_value)
            print("H2 = %0.2f " % H2_value)
        
        """
        
        # functions
        eCO2_value     = self.eCO2
        TVOC_value     = self.TVOC
        Ethanol_value  = self.Ethanol
        H2_value       = self.H2
        
        carbon_dioxide_text = "\nCarbon dioxide (eCO2): %"                + self.variable_dictionary['Equivalent Carbon dioxide']['format']       + " " +  self.variable_dictionary['Equivalent Carbon dioxide']['units symbol']
        tvoc_text           = "Total Volatile Organic Compound (TVOC): %" + self.variable_dictionary['Total Volatile Organic Compound']['format'] + " " +  self.variable_dictionary['Total Volatile Organic Compound']['units symbol']
        ethanol_text        = "Ethanol: %"                                + self.variable_dictionary['Ethanol']['format']                         + " " +  self.variable_dictionary['Ethanol']['units symbol'] 
        h2_text             = "H2 = %"                                    + self.variable_dictionary['H2']['format']                              + " " +  self.variable_dictionary['H2']['units symbol'] 
        print(carbon_dioxide_text  % (eCO2_value))
        print(tvoc_text  % TVOC_value)
        print(ethanol_text % Ethanol_value)
        print(h2_text % H2_value)


# Functions
# None


# Dictionaries
# None
         
        
