""" sensor_ltr559.py

# Rationale:
    Start up, run, and print variables of I2C sensor LTR559
    # Purpose: Light & Proximity sensor/ optical presence & proximity sensor
    # i2C: 0x23
        

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    12/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html    

# Requirements:
    from board import SCL, SDA
    from busio import I2C
    from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559

# Contains:
    class : sensor_ltr559
            which contains: [   _init_, start_sensor,
                                measure_lux,  measure_proximity,
                                measure_all, print_sensor_values]

# References:
    https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559
    https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559/blob/master/pimoroni_circuitpython_ltr559.py

# Notes:
    > IR/UV-filtering
    > 50.60Hz flicker rejection
    > 0.01 lux to 64,000 lux light detection range
    > ~5cm proximity detection range    
    
    
# Examples:



"""

#import time
import board
from busio import I2C
from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        

# Class
class sensor_ltr559:
    """CLASS: MEASURE LTR559
    
    """
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        
        # VARIABLES
        # Internal class variables
        
        # sensor
        self.sensor_name             = "LTR559"
        self.i2c_address             = 0x23
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.LTR559                  = None
                
        # measured variables
        self.lux                     = None
        self.proximity               = None
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {'Light'     : {'variable name' : 'lux'       , 'units symbol' : 'lux' , 'units' : 'lux' , 'format' : '0.2f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor LTR559', 'dependent_variable' : None},
                                    'Proximity' : {'variable name' : 'proximity' , 'units symbol' : '%%'  , 'units' : None  , 'format' : '0.1f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor LTR559', 'dependent_variable' : None},
                                    }
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
                
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        Output:
            None, modify self.LTR559
        
        Packages:
            from busio import I2C
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559  
        
        Notes:
        
        
        """
        
        # Variables
        if frequency is not None:
            
            self.i2c_frequency = frequency
            
        else:
            
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        self.LTR559 = Pimoroni_LTR559(i2c_address)
    
    
    def get_sensor_update(self):
        """Update sensor value
        
        Input:
            None
        
        Output:
            update lux and prox
            
        Packages:
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559

        """
        self.LTR559.update_sensor()
    

    def measure_lux(self):
        """Measure lux
        
        Input:
            None
        
        Output:
            lux = sensor value of lux
            
        Packages:
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        """
        # Functions

        # update sensors
        if update is True:
            self.get_sensor_update()

        #lux      = self.LTR559.lux        # Get Lux value from light sensor
        lux      = self.LTR559.get_lux()   # Get Lux value from light sensor
        self.lux = lux
        
        return lux
        
     
    def measure_proximity(self, update = False):
        """Measure proximity
        
        Input:
            update = update sensors, default is False
        
        Output:
            prox = sensor value of proximity
        
        Packages:
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        """
        # Functions

        # update sensors
        if update is True:
            self.get_sensor_update()
        
        #prox           = self.LTR559.prox            # Get Prox value from Proximity sensor
        prox           = self.LTR559.get_proximity()  # Get Prox value from Proximity sensor
        self.proximity = prox  

        return prox
        
    
    def measure_all(self):
        """ FUNC: measure all, returns (lux_value, prox_value) AND UPDATES sensor values
        
        Input:
            None
        
        Output:
            lux_value, prox_value
        
        Packages:
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        """
        # update sensors
        self.get_sensor_update()

        lux_value   =  self.measure_lux()
        prox_value  =  self.measure_proximity()
        
        return lux_value, prox_value
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
        
        Output:
            None, print output
        
        Packages:
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        Notes:
            print("\nLux: %0.2f Lux" % (lux))
            print("Proximity: %0.1f %%" % proximity)
        
        """
        # functions
        lux          = self.lux
        proximity    = self.proximity
        
        # Print values
        lux_text       = "\nLux: %"      + self.variable_dictionary['Light']['format']      + " " +  self.variable_dictionary['Light']['units symbol']  
        proximity_text = "Proximity: %"  + self.variable_dictionary['Proximity']['format']  + " " +  self.variable_dictionary['Proximity']['units symbol']
        
        print(lux_text    % (lux))
        print(proximity_text % (proximity))


# Functions
# None

# Dictionaries
# None
