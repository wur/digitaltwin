""" board_module.py

# Rationale:
    Set
    (1) a LED to turn on/off or blink, useful for giving
    some indication of progress when the board is not
    connected to a computer.
    
    (2) a RGB LED to turn on/off, blink, or change colour
    useful for giving some indication of progress when
    the board is not connected to a computer.
    
    (3) [*optional*] Arduino Nano RP2040 Connect a
        PDM microphone (= MP34DT05)
    
    (4) [*optional*] Arduino Nano RP2040 Connect an
        Accelerometer and gyroscope (= LSM6DSOXTR)
    

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    08/08/2022 

# Licence:
    
    
# URL:
    

# Requirements:
    import os
    import board
    import time
    import array
    import math
    import pwmio
    import simpleio
    import audiobusio
    from digitalio import DigitalInOut, Direction
    from microcontroller import Pin, cpu
    from adafruit_lsm6ds.lsm6dsox import LSM6DSOX


# Contains:
    class : board_led
            which contains: [_init_, turn_on_led,
                            turn_off_led, blink]
            microctrl   
            which contains: [__init__ , print_attributes,
                            get_attributes]
                            
            board_microphone
            which contains: [__init__, start_sensor,
                            mean, normalized_rms,
                            ready_sampling, calculate_magnitude,
                            measure_sound, measure_all,
                            continuous_recording]
                            
            board_accelerometer_gyroscope
            which contains:  [__init__, start_sensor,
                            measure_acceleration, measure_gyro,
                            measure_temperature, measure_all,
                            print_sensor_values ]
            
    functions:  get_board_id,
                get_modules,
                print_modules,
                get_unique_pins print_pins
    
    
    
    
# References:
    https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-openmv-mlc
    
# Example:
    from wur_circuitpython_functions import board_module
    
"""

#Packages
import board
from digitalio import DigitalInOut, Direction
import time
import os
from microcontroller import Pin, cpu

# Packages for class microctrl
# import microcontroller
# from microcontroller import cpu

# Packages for board microphone
# import board
# import time
import array
import math
import audiobusio
import pwmio
import simpleio

# Packages for board gyroscope
# import time
# import board
from adafruit_lsm6ds.lsm6dsox import LSM6DSOX   


# Class
class board_led:
    
    def __init__(self, LED_pin):
        """ Initialise class
        
        Input:
            LED_pin = GP pin for LED (e.g., board.LED)
        
        Packages:
            import time
            import board
            from digitalio import DigitalInOut, Direction
        
        References:
            https://learn.adafruit.com/getting-started-with-raspberry-pi-pico-circuitpython/blinky-and-a-button
        
        Notes:
            For the Arduino Nano RP2040 Connect the LED is connected to a dedicated pin GP25 called board.LED
        
        """
        # VARIABLES
        # Class variables - tell the board how to talk to the LED
        self.LED           = DigitalInOut(LED_pin)   # tell the board the pin
        self.LED.direction = Direction.OUTPUT        # tell the board its an output as opposed to an input
    
    
    def turn_on_led(self):
        """ Turn on the led
        
        """
              
        #function
        self.LED.value = True


    def turn_off_led(self):
        """ Turn off the led
        
        """
              
        #function
        self.LED.value = False
    
    
    def blink(self,times = 5, status = False, blink_interval = 0.1):
        """ Blink LED
        
        Input:
            times          = number of times to blink on and off, default is 5
            status         = status of LED after blinking, values are True or False, default is False
            blink_interval = the time interval between blinks, default is 0.1
        
        Packages:
            import time
            
        """
        
        # set start of blink by turning on LED
        self.turn_on_led()
        
        for i in range(times):
            
            self.turn_off_led()
            time.sleep(blink_interval)
            
            self.turn_on_led()
            time.sleep(blink_interval)
            
        self.LED.value = status


####
class board_microphone:
    """
    
        
    Packages:
        import board
        import time

        # math packages
        import array
        import math

        # audio packages
        import audiobusio
        import pwmio
        import simpleio
    
    
    Note:
        Arduino Nano RP2040 Connect
        on board PDM microphone (reference number = MP34DT05)
        
    
    References:
        Original code:
        SPDX-FileCopyrightText: 2021 Liz Clark for Adafruit Industries
        SPDX-License-Identifier: MIT
        Adapted from the example code DM_Mic_Sound_Level_Plot.py
        https://github.com/adafruit/Adafruit_Learning_System_Guides/
        blob/master/PDM_Microphone/PDM_Mic_Sound_Level_Plot.py '''


    """
    

    def __init__(self):
        """
        
        Input:
            
        
        Output:
           None, set up  
        
        Packages:
        
            
        """
        
        self.mic                     = None
        self.microphone_sample_rate  = None
        self.microphone_bit_depth    = None
        
        # data
        self.samples                 = None
        
        # modified data
        self.magnitude               = None
        
        # metadata
        self.variable_dictionary = {            
                                    'Sound' : {'variable name' : 'magnitude', 'units symbol' : '', 'units' : '', 'format' : '0.4f', 'data_type' : 'calculated', 'provenance' : 'sensor PDM', 'dependent_variable' : None},
                                    }  
    
 
    def start_sensor(self,
                     pin_microphone_clock   = None,
                     pin_microphone_data    = None,
                     microphone_sample_rate = 16000,
                     microphone_bit_depth   = 16,
                     microphone_mono = True, microphone_oversample = 64,
                                    microphone_startup_delay = 0.11
                     ):
        """
        
        Input: 
            pin_microphone_clock     = pin for microphone clock wire, e.g., board.MICROPHONE_CLOCK, default is None
            pin_microphone_data      = pin for microphone data wire , e.g., board.MICROPHONE_DATA, default is None
            microphone_sample_rate   = Target sample_rate of the resulting samples. Check sample_rate for actual value.
                                       Minimum sample_rate is about 16000 Hz.
                                       Default is 16000
            microphone_bit_depth     = Final number of bits per sample. Must be divisible by 8. Default is 16.
            microphone_mono          = True when capturing a single channel of audio, else captures two channels otherwise. Default is True.
            microphone_oversample    = Number of single bit samples to decimate into a final sample. Must be divisible by 8. Default is 64.
            microphone_startup_delay = Seconds to wait after starting microphone clock to allow microphone to turn on.
                                       Most require only 0.01s; some require 0.1s. Longer is safer. Must be in range 0.0-1.0 seconds.
                                       Default is 0.11
            
        Output:
            None, intialise sensor modify self.mic
        
        Example:
            microphone = board_microphone()
            microphone.start_sensor(board.MICROPHONE_CLOCK, board.MICROPHONE_DATA)
        
        """
        # Modify internal class variables
        self.microphone_sample_rate  = microphone_sample_rate
        self.microphone_bit_depth    = microphone_bit_depth
        
        # Initialise sensor
        self.mic = audiobusio.PDMIn(clock_pin = pin_microphone_clock,
                                    data_pin  = pin_microphone_data,
                                    sample_rate=microphone_sample_rate, bit_depth=microphone_bit_depth,
                                    mono = microphone_mono, oversample = microphone_oversample,
                                    startup_delay = microphone_startup_delay
                                    )
    
     

    def ready_sampling(self,
                       array_typecode    = 'H',
                       array_initializer = [0] * 160):
        """
        Input:
            array_typecode    = typecode for array see Notes, default 'H',
            array_initializer = optional intializer see Notes, default [0] * 160
            
        Output
            None, modifys self.samples
        
        Notes:
            Arrays work like lists but are constrained by object type determined at
            creation
            
            https://docs.circuitpython.org/en/latest/docs/library/array.html
            uses CPython: https://docs.python.org/3/library/array.html#module-array
            
            array.array(typecode[, initializer])
            
            where typcode, represents one of a supported format
            codes: 'b', 'B', 'h', 'H', 'i', 'I', 'l', 'L', 'q', 'Q', 'f', 'd'
            
            Representing:
            
             b   = signed char - int  - min size (bytes): 1
             B   = unsigned char - int - min size (bytes): 1
             h   = wchar_t - unicode character- min size (bytes): 2
             H   = signed short  - int- min size (bytes): 2
             i   = signed int - int- min size (bytes): 2
             I   = unsigned int - int- min size (bytes): 2
             l   = signed long - int- min size (bytes): 4
             L   = unsigned long - int- min size (bytes): 4
             q   = signed long long - int- min size (bytes): 8
             Q   = unsigned long long - int- min size (bytes): 8
             f   = float - float- min size (bytes): 4
             d   = double - float- min size (bytes): 8
            
            initializer is optional
        
        Example:
            microphone = board_microphone()
            microphone.start_sensor(board.MICROPHONE_CLOCK, board.MICROPHONE_DATA)
            microphone.ready_sampling()
            
        """
        
        # array signed short or int with minimum size of 2 bytes
        self.samples = array.array(array_typecode, array_initializer)
    
    

    # Generic Functions
    # Remove DC bias before computing RMS.
    def mean(self, values):
        """ FUNC: mean value 
        
        Input:
        
            values = an array of values
            
        Output:
            mean value
                arithmetic mean = sum of values divided by the number of values (N), here defined by 'length' (len) of values
            
        Packages:
            import array
            import math

        """
        arithmetic_mean = sum(values) / len(values)
        
        return arithmetic_mean


    def normalized_rms(self, values):
        """ FUNC: normalised root mean square values

        """
        
        minbuf = int(self.mean(values))
        
        samples_sum = sum(
            float(sample - minbuf) * (sample - minbuf)
            for sample in values
        )

        return math.sqrt(samples_sum / len(values))


    def calculate_magnitude(self):
        """
        
        """
        if self.samples is None:
            
            print('Sampling not intialised. Initialise sampling via ____.ready_sampling()')
            
            raise ValueError('Initialise sampling via ____.ready_sampling()')
        
        
        magnitude = self.normalized_rms(self.samples)
        
        return magnitude
    

    def measure_sound(self):
        """
        
        Notes:
            https://docs.circuitpython.org/en/latest/shared-bindings/audiobusio/index.html
        
        .record(destination, destination_length) function explained:
        
        Records destination_length bytes of samples to destination. This is 'blocking'.
                record(destination          = in this instance an array called samples,
                         destination_length   = samples being of length defined by this parameter here len(samples)
                         )
        """
        
        self.mic.record(self.samples, len(self.samples))
        
        magnitude      = self.calculate_magnitude()
        self.magnitude = magnitude
        
        # store variable
        self.magnitude = magnitude
        
        return magnitude
    

    def measure_all(self):
        """
        
        Notes:
            https://docs.circuitpython.org/en/latest/shared-bindings/audiobusio/index.html
        
        .record(destination, destination_length) function explained:
        
        Records destination_length bytes of samples to destination. This is 'blocking'.
                record(destination          = in this instance an array called samples,
                         destination_length   = samples being of length defined by this parameter here len(samples)
                         )
        """
        magnitude = self.measure_sound()
        
        return magnitude
        
        
    def continuous_recording(self, time_interval = 0.05, auto_turn_on = False):
        """
        Input:
        

        Example:
            microphone = board_microphone()
            microphone.start_sensor(board.MICROPHONE_CLOCK, board.MICROPHONE_DATA)
            microphone.ready_sampling()
            
        """
        if self.samples is None:
            if auto_turn_on is True:
                print('Sampling not intialised. Programme will start sampling prior to recording')
                self.ready_sampling()
                
            else:
                print('Sampling not intialised. auto_turn_on set to False. Intialise sampling')
                raise ValueError('Initialise sampling via ____.ready_sampling()')
        
        count = 0
        
        while True:
            
            count += 1
            self.mic.record(self.samples, len(self.samples))
            self.magnitude = self.normalized_rms(self.samples)
            #print(str(count))
            #print(str(count/time_interval))
            print("\nCount: %i \t Time: %0.2f" % (count, count/time_interval))
            #print(self.magnitude)
            print("\nMagnitude: %0.4f " % (self.magnitude))
            
            time.sleep(time_interval)
            
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            call functions, print
        
        Packages:
        
        Note:
            
        
        """
        
        # functions
        magnitude_value = self.magnitude  
        
        magnitude_text = "\nMagnitude: %"   + self.variable_dictionary['Sound']['format']  + " " +  self.variable_dictionary['Sound']['units symbol']
        print(magnitude_text  % (magnitude_value))


####
class board_accelerometer_gyroscope:
    """
    Note:
        Arduino Nano RP2040 Connect
        on board Accelerometer and gyroscope (reference number = LSM6DSOXTR)
        
    Packages:
        import board
        import time
        # add to \libs
        from adafruit_lsm6ds.lsm6dsox import LSM6DSOX
    
    Reference:
        https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-openmv-mlc
    
    """
    

    def __init__(self, LED_pin):
        """
        
        Input:
            
        
        Output:
           None, set up  
        
        Packages:
            import time
            import board
            from adafruit_lsm6ds.lsm6dsox import LSM6DSOX
        
        Reference:
            https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-openmv-mlc
            
        """
        # VARIABLES
        # Internal class variables
        self.sensor_name             = "LSM6DSOXTR"
        self.i2c_address             = None
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        
        self.lsm6dsoxtr              = None
                
        self.acceleration            = None
        self.acceleration_X          = None
        self.acceleration_Y          = None
        self.acceleration_Z          = None
        
        self.gyroscope               = None
        self.gyroscope_X             = None
        self.gyroscope_Y             = None
        self.gyroscope_Z             = None
        
        self.temperature             = None
        
        # metadata
        self.variable_dictionary = {            
            'Acceleration' : {'variable name' : 'acceleration', 'units symbol' : 'm/s^2'    , 'units' : 'parts per million', 'format' : '0.2f', 'data_type' : 'calculated', 'provenance' : 'sensor LSM6DSOXTR', 'dependent_variable' : None},
            'Gyro'         : {'variable name' : 'gyroscope'   , 'units symbol' : 'radians/s', 'units' : 'parts per billion', 'format' : '0.2f', 'data_type' : 'calculated', 'provenance' : 'sensor LSM6DSOXTR', 'dependent_variable' : None},
            'Temperature'  : {'variable name' : 'temperature' , 'units symbol' : 'C'        , 'units' : 'degrees Celcius'  , 'format' : '0.2f', 'data_type' : 'calculated', 'provenance' : 'sensor LSM6DSOXTR', 'dependent_variable' : None},
                                   }
        

    
    def start_sensor(self,
                     SCL_pin = None, SDA_pin = None,
                     frequency = None,
                     chain = False, i2c_chainaddress = None
                     ):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin               =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin               =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency             =      i2c frequency, default is None but uses internal class value of 100,000
            *chain                 =      If the i2C sensor is in a chain. True or False. Default is False
            *i2c_chainaddress      =      The i2c address of the 'main' sensor
            
            *Note for Arduino Nano RP2040 Connect has an internal version so chain and i2c_chainaddress not required
            
        Output:
            
        
        Packages:
            import time
            import board
            from adafruit_lsm6ds.lsm6dsox import LSM6DSOX
        
        """
        
        # Variables
        if frequency is not None:
            
            self.i2c_frequency = frequency
            
        else:
            
            frequency = self.i2c_frequency
        
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        # Start sensor
        self.lsm6dsoxtr  = LSM6DSOX(i2c_address) 
        
        

    def measure_acceleration(self):
        """ FUNC: Get acceleration value

        Input:
            None
            
        Output:
            acceleration = Acceleration X, Y, Z in m/s^2
            
        Packages:
            import time
            import board
            from adafruit_lsm6ds.lsm6dsox import LSM6DSOX
        
        """
        # Function
        # measure acceleration
        
        acceleration      = self.lsm6dsoxtr.acceleration
        
        # Store value
        self.acceleration            = acceleration
        self.acceleration_X          = acceleration[0]
        self.acceleration_Y          = acceleration[1]
        self.acceleration_Z          = acceleration[2]
        
        return acceleration, acceleration_X, acceleration_Y, acceleration_Z


    def measure_gyro(self):
        """ FUNC: Get gyroscopic values

        Input:
            None
            
        Output:
            gyro = Gyroscopic values X, Y, Z in radians/s
            
        Packages:
            import time
            import board
            from adafruit_lsm6ds.lsm6dsox import LSM6DSOX
        
        """
        # Function
        # measure gyroscope
        
        gyroscope         = self.lsm6dsoxtr.gyro
        
        # Store value
                
        self.gyroscope               = gyroscope
        self.gyroscope_X             = gyroscope[0]
        self.gyroscope_Y             = gyroscope[1]
        self.gyroscope_Z             = gyroscope[2]
        
        return gyroscope, gyroscope_X, gyroscope_Y, gyroscope_Z

    
    def measure_temperature(self):
        """ FUNC: Get Temperature values

        Input:
            None
            
        Output:
            temperature = Temperature values in C
            
        Packages:
            import time
            import board
            from adafruit_lsm6ds.lsm6dsox import LSM6DSOX
        
        """
        # Function
        # measure temperature
        
        temperature       = self.lsm6dsoxtr.temperature
        
        # Store value
                
        self.temperature  = temperature
        
        return temperature
    
    
    def measure_all(self):
        """ FUNC: measure all (acceleration and gyroscope) for sensor LSM6DSOXTR,
            returns (acceleration, acceleration_X, acceleration_Y, acceleration_Z,
                    gyroscope, gyroscope_X, gyroscope_Y, gyroscope_Z)
        
        Input:
            None
            
        Output:
             acceleration, acceleration_X, acceleration_Y, acceleration_Z,
                    gyroscope, gyroscope_X, gyroscope_Y, gyroscope_Z
        
        Packages
            import time
            import board
            from adafruit_lsm6ds.lsm6dsox import LSM6DSOX
        
        """
        # Functions
        
        # get values
        acceleration, acceleration_X, acceleration_Y, acceleration_Z = self.measure_acceleration()
        gyroscope, gyroscope_X, gyroscope_Y, gyroscope_Z             = self.measure_gyro()
        temperature = self.measure_temperature
                
        return acceleration, acceleration_X, acceleration_Y, acceleration_Z, gyroscope, gyroscope_X, gyroscope_Y, gyroscope_Z, temperature
    

    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            None
        
        Notes:
            print("Acceleration: X:%.2f, Y: %.2f, Z: %.2f m/s^2" % (sensor.acceleration))
            print("Gyro X:%.2f, Y: %.2f, Z: %.2f radians/s" % (sensor.gyro))
            print("Temperature :%0.2f C" % (sensor.temperature))
        
        """
        # Get internal data values
        acceleration      = self.acceleration 
        gyroscope         = self.gyroscope
        temperature       = self.temperature
        
        # Print values
        accel_format       = self.variable_dictionary['Acceleration']['format']
        gyro_format        = self.variable_dictionary['Gyro']['format']
        temperature_format = self.variable_dictionary['Temperature']['format']
        
        # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
        acceleration_text = "\nAcceleration: X%" + accel_format + ", Y: %" + accel_format + ", Z: %" + accel_format + " " + self.variable_dictionary['Acceleration']['units symbol']
        gyroscope_text    = "\nGyro: X%"         + gyro_format + ", Y: %" + gyro_format  + ", Z: %" + gyro_format  + " " + self.variable_dictionary['Gyro']['units symbol']
        temperature_text  = "\nTemperature: %"   + temperature_format + " " + self.variable_dictionary['Temperature']['units symbol']
        
        print()
        print(acceleration_text % (acceleration))
        print(gyroscope_text  % (gyroscope))
        print(temperature_text  % (temperature))
        

#### 
class microctrl:
    """ Attributes of the microcontroller
    
    """
    
    def __init__(self):
        """
        
        Input:
            None
        
        Output:
            None, set up self attributes 
        
        Packages:
            from microcontroller import cpu
            
        """
        
        self.cpu_frequency    = None
        self.cpu_temperature  = None
        self.cpu_reset_reason = None
        self.cpu_uid          = None
        self.cpu_voltage      = None
        
    
    def print_attributes(self):
        """ PRINT: Microcontroller attributes
        
        Input:
            None
            
        Output:
            print attributes
        
        Packages:
            from microcontroller import cpu
            
        """
        for attr in dir(cpu):
            
            if attr.startswith('_'):  continue
            
            print( '{:12s} : {}'.format(attr,getattr(cpu,attr)) )


    def get_attributes(self):
        """ GET: Microcontroller attributes
        
        Input:
            None
            
        Output:
            output_dict = dictionary of variables
        
        Packages:
            from microcontroller import cpu
        
        Note:
            frequency         = The CPU operating frequency in Hertz. (read-only)
            temperature       = The on-chip temperature, in Celsius, as a float. (read-only)
            reset_reason      = The reason the microcontroller started up from reset state.
            uid               = The unique id (aka serial number) of the chip as a bytearray. (read-only)
            voltage           = The input voltage to the microcontroller, as a float. (read-only)
        
        # alternative
        self.cpu_frequency    = microcontroller.cpu.frequency
        self.cpu_temperature  = microcontroller.cpu.temperature
        self.cpu_reset_reason = microcontroller.cpu.reset_reason
        self.cpu_uid          = microcontroller.cpu.uid
        self.cpu_voltage      = microcontroller.cpu.voltage
        
        
        """
        self.cpu_frequency    = cpu.frequency
        self.cpu_temperature  = cpu.temperature
        self.cpu_reset_reason = cpu.reset_reason
        self.cpu_uid          = cpu.uid
        self.cpu_voltage      = cpu.voltage
        





# Functions
def get_board_id():
    """ DICT: get board id

    Rationale:
        get board sysname, nodename, release, version, and machine
        in dictionary
    
    Input:
        None
        
    Output:
        os_output_dict = dictionary containing keys:
                         sysname, nodename, release, version, and machine
    
    Packages:
        import os
        
    """
    
    # Get OS information:
    # [0] sysname='rp2040',
    # [1] nodename='rp2040',
    # [2] release='7.3.1',
    # [3] version='7.3.1 on 2022-06-22',
    # [4] machine='Arduino Nano RP2040 Connect with rp2040')
    os_output = os.uname()
    
    os_output_dict = {
                        'sysname'  :  os_output[0],
                        'nodename' :  os_output[1],
                        'release'  :  os_output[2],
                        'version'  :  os_output[3],
                        'machine'  :  os_output[4],
                        
                        }
    
    return os_output_dict


    
def get_modules():
    """ DICTIONARY: 
    
    Input:
        None
    
    Output:
        output_list = list of main (i.e., not /libs) modules on board
        
    Notes:
        copy and paste of help("modules")
        
    """
    
    
    os_output_dict = get_board_id()
    
    if os_output_dict['machine'] == 'Arduino Nano RP2040 Connect with rp2040':
        
        output_list = {
            
            # MODULE name        : # title and explainer FROM docs.circuitpython.org
            '__main__'           : "",
            '__future__'         : "",
            '_asyncio'           : "",
            '_bleio'             : "",
            '_eve'               : "",
            'adafruit_bus_device': "",
            'adafruit_bus_device.i2c_device': "",
            'adafruit_bus_device.spi_device': "",
            'adafruit_pixelbuf'  : "",
            'aesio'              : "AES encryption routines",
            'alarm'              : "Alarms that trigger based on time intervals or on external events, such as pin change",
            'analogio'           : "Contains classes to provide access to analog IO typically implemented with digital-to-analog (DAC) and analog-to-digital (ADC) converters.",
            'array'              : "",
            'atexit'             : "functions to register and unregister cleanup functions. Functions thus registered are automatically executed upon normal vm termination",
            'audiobusio'         : "classes to provide access to audio IO over digital buses. These protocols are used to communicate audio to other chips in the same circuit. It doesn’t include audio interconnect protocols such as S/PDIF",
            'audiocore'          : "Support for audio samples",
            'audiomixer'         : "Support for audio mixing. Mixes one or more audio samples together into one sample.",
            'audiomp3'           : "Support for MP3-compressed audio files. See https://learn.adafruit.com/circuitpython-essentials/circuitpython-mp3-audio",
            'audiopwmio'         : "Audio output via digital PWM. Contains classes to provide access to audio IO",
            'binascii'           : "",
            'bitbangio'          : "Digital protocols implemented by the CPU contains classes to provide digital bus protocol support regardless of whether the underlying hardware exists to use the protocol",
            'bitmaptools'        : "Collection of bitmap manipulation tools. See https://learn.adafruit.com/circuitpython-display-support-using-displayio",
            'bitops'             : "Routines for low-level manipulation of binary data",
            'board'              : "Board specific pin names",
            'builtins'           : "",
            'busio'              : "Hardware accelerated external bus access  contains classes to support a variety of serial protocols",
            'collections'        : "",
            'countio'            : "Support for edge counting. Contains logic to read and count edge transistions",
            'digitalio'          : "Basic digital pin support  contains classes to provide access to basic digital IO.",
            'displayio'          : "Native helpers for driving displays contains classes to manage display output including synchronizing with refresh rates and partial updating.",
            'errno'              : "",
            'floppyio'           : "Read flux transition information into the buffer",
            'fontio'             : "Core font related data structures. Low level usage, use: https://github.com/adafruit/Adafruit_CircuitPython_Bitmap_Font",
            'framebufferio'      : "Native framebuffer display driving contains classes to manage display output including synchronizing with refresh rates and partial updating. It is used in conjunction with classes from displayio to actually place items on the display; and classes like RGBMatrix to actually drive the display",
            'gc'                 : "",
            'getpass'            : "Getpass Module provides a way to get input (e.g., passwords) from user without echoing it",
            'gifio'              : "",
            'imagecapture'       : "Support for “Parallel capture” interfaces. Capture image frames from a camera with parallel data interface",
            'io'                 : "",
            'json'               : "",
            'keypad'             : "Support for scanning keys and key matrices provides native support to scan sets of keys or buttons, connected independently to individual pins, connected to a shift register, or connected in a row-and-column matrix",
            'math'               : "mathematical functions",
            'microcontroller'    : "Pin references and cpu functionality defines the pins and other bare-metal hardware from the perspective of the microcontroller.",
            'micropython'        : "",
            'msgpack'            : "Pack object in msgpack format. Msgpack format is similar to json, except that the encoded data is binary. See https://msgpack.org for details",
            'neopixel_write'     : "Low-level neopixel implementation contains a helper method to write out bytes in the 800khz neopixel protocol. NOTE: This module is typically not used by user level code.",
            'onewireio'          : "Low-level bit primitives for Maxim (formerly Dallas Semi) one-wire protocol. Protocol definition is here: https://www.maximintegrated.com/en/app-notes/index.mvp/id/126",
            'os'                 : "Functions that an OS normally provides",
            'paralleldisplay'    : "Native helpers for driving parallel displays",
            'pulseio'            : "Support for individual pulse based protocols contains classes to provide access to basic pulse IO. Individual pulses are commonly used in infrared remotes and in DHT temperature sensors.",
            'pwmio'              : "Support for PWM based protocols contains classes to provide access to basic pulse IO.",
            'qrio'               : "Low-level QR code decoding Provides the QRDecoder object used for decoding QR codes (not making, use this for making https://github.com/adafruit/Adafruit_CircuitPython_miniQR). For more information about working with QR codes, https://learn.adafruit.com/scan-qr-codes-with-circuitpython",
            'rainbowio'          : "Provides the colorwheel() function.",
            'random'             : "Pseudo-random numbers and choices. Like its CPython cousin, CircuitPython’s random seeds itself on first use with a true random from os.urandom() when available or the uptime otherwise. Once seeded, it will be deterministic, which is why its bad for cryptography.Numbers from this module are not cryptographically strong! Use bytes from os.urandom directly for true randomness.",
            're'                 : "", 
            'rgbmatrix'          : "Low-level routines for bitbanged LED matrices",
            'rotaryio'           : "Support for reading rotation sensors contains classes to read different rotation encoding schemes. See Wikipedia’s Rotary Encoder page for more background https://en.wikipedia.org/wiki/Rotary_encoder .",
            'rp2pio'             : "Hardware interface to RP2 series’ programmable IO (PIO) peripheral. This module is intended to be used with the adafruit_pioasm library. For an introduction and guide to working with PIO in CircuitPython, see https://learn.adafruit.com/intro-to-rp2040-pio-with-circuitpython",
            'rtc'                : "Real Time Clock provides support for a Real Time Clock. You can access and manage the RTC using rtc.RTC. It also backs the time.time() and time.localtime() functions using the onboard RTC if present.",
            'sdcardio'           : "Interface to an SD card via the SPI bus. SD Card Block Interface Controls an SD card over SPI. This built-in module has higher read performance than the library adafruit_sdcard, but it is only compatible with busio.SPI, not bitbangio.SPI. Usually an SDCard object is used with storage.VfsFat to allow file I/O to an SD card.",
            'select'             : "",
            'sharpdisplay'       : "Support for Sharp Memory Display framebuffers", 
            'storage'            : "Storage management provides storage management functionality such as mounting and unmounting which is typically handled by the operating system hosting Python. CircuitPython does not have an OS, so this module provides this functionality directly. For more information regarding using the storage module, refer to https://learn.adafruit.com/circuitpython-essentials/circuitpython-storage",
            'struct'             : "Manipulation of c-style data. See: https://docs.python.org/3/library/struct.html#module-struct",
            'supervisor'         : "Supervisor settings",
            'synthio'            : "Support for MIDI synthesis - Audio",
            'sys'                : "",
            'terminalio'         : "Displays text in a TileGrid. Contains classes to display a character stream on a display. The built in font is available as terminalio.FONT",
            'time'               : "Time and timing related functions",
            'touchio'            : "Touch related IO. Contains classes to provide access to touch IO typically accelerated by hardware on the onboard microcontroller",
            'traceback'          : "Traceback Module. Provides a standard interface to print stack traces of programs. This is useful when you want to print stack traces under program control.",
            'ulab'               : "Manipulate numeric data similar to numpy a numpy-like module for micropython, meant to simplify and speed up common mathematical operations on arrays. The primary goal was to implement a small subset of numpy that might be useful in the context of a microcontroller. This means low-level data processing of linear (array) and two-dimensional (matrix) data",
            'ulab'               : "Manipulate numeric data similar to numpy",
            'ulab.fft'           : "Frequency-domain functions",
            'ulab.linalg'        : "Functions: https://docs.circuitpython.org/en/latest/shared-bindings/ulab/numpy/linalg/index.html",
            'ulab.numpy'         : "Numerical approximation methods",
            'ulab.scipy'         : "Compatibility layer for scipy",
            'ulab.scipy.linalg'  : "Scipy functions: https://docs.circuitpython.org/en/latest/shared-bindings/ulab/scipy/linalg/index.html",
            'ulab.scipy.optimize': "Scipy functions: https://docs.circuitpython.org/en/latest/shared-bindings/ulab/scipy/optimize/index.html",
            'ulab.scipy.signal'  : "",
            'ulab.scipy.special' : "",
            'ulab.utils'         : "Scipy functions: (only) spectrogram: https://docs.circuitpython.org/en/latest/shared-bindings/ulab/utils/index.html",
            'usb_cdc'            : "USB CDC Serial streams allows access to USB CDC (serial) communications",
            'usb_hid'            : "USB Human Interface Device  allows you to output data as a HID device",
            'usb_midi'           : "MIDI over USB contains classes to transmit and receive MIDI messages over USB.",
            'uselect'            : "",
            'vectorio'           : "Lightweight 2D shapes for displays provide simple filled drawing primitives for use with displayio",
            'watchdog'           : "Watchdog Timer provides support for a Watchdog Timer. This timer will reset the device if it hasn’t been fed after a specified amount of time. This is useful to ensure the board has not crashed or locked up. Note that on some platforms the watchdog timer cannot be disabled once it has been enabled.",
            'zlib'               : "zlib decompression functionality. allows limited functionality similar to the CPython zlib library. This module allows to decompress binary data compressed with DEFLATE algorithm (commonly used in zlib library and gzip archiver)",
            
            
            }
    
    #elif os_output_dict['machine'] == '':
    #    
    
    else:
        print('Machine not found')
                   
                  
    help("modules")
    
    return output_list
    

def print_modules():
    """ PRINT: print module output_list keys
    
    """
    output_list = get_modules()
    
    [print(key) for key in output_list.keys()]
    
    
def get_unique_pins(pins_to_exclude = ['NEOPIXEL',
                                       'APA102_MOSI',
                                       'APA102_SCK']
                    ):
    """ Get Board's unique pins

    Input:
        pins_to_exclude = pins to exclude from list
        
    Output:
        unique_pins  = list of unique pins
    
    Packages:
        import board
        from microcontroller import Pin
    
    Examples:
        print(get_unique_pins())
        
    """
    
    pins = [pin for pin in [
        getattr(board, p) for p in dir(board) if p not in pins_to_exclude]
            if isinstance(pin, Pin)]
    
    # intialise output list
    unique_pins = []
    
    for pin in pins:
        
        # if pin is not in the output list, append
        if pin not in unique_pins:
            
            unique_pins.append(pin)
            
    return unique_pins


def print_pins():
    """ PRINT: print pins from get_unique_pins
    
    """
    output_list = get_unique_pins()
    
    [print(value) for value in output_list]

# Dictionaries
# None
