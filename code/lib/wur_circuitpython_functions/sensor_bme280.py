""" sensor_bme280.py

# Rationale:
    Start up, run, and print variables of I2C sensor BME280
    # Purpose: Pressure/Temperature/Humidity
    # i2C: 0x76 and 0x77

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html    

# Requirements:
    import time
    import adafruit_bme280.advanced as adafruit_bme280
    from busio import I2C    
    import math

# Contains:
    class : sensor_bme280
            which contains: [   _init_, start_sensor,
                                modify_sea_level_pressure, measure_temperature,
                                measure_rel_humidity,measure_pressure,
                                measure_altitude, measure_all,
                                dew_point , print_sensor_values 
                            ]

# References:
    https://docs.circuitpython.org/projects/bme280/en/latest/
    https://learn.adafruit.com/adafruit-bme280-humidity-barometric-pressure-temperature-sensor-breakout/python-circuitpython-test

# Notes:
    Limitiations:
    pressure - 300 to 1100 hPa
    
    
# Examples:



"""

#Packages
import time
import adafruit_bme280.advanced as adafruit_bme280
from busio import I2C    
import math


# Class
class sensor_bme280:
    """CLASS: MEASURE BME280
    
    """
    
    
    
    def __init__(self):
        """ Initialise class
        
        Input:
            
        Output:
        
        Packages:
            #import time
            #import board
            from busio import I2C
            import adafruit_bme280.advanced as adafruit_bme280 
            import math
        """
        
        # VARIABLES
        # Internal class variables
        
        # sensor
        self.sensor_name             = "BME280"
        self.i2c_address             = 0x76
        self.i2c_address_alternative = 0x77
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.bme280                  = None
                
        # measured variables
        self.temperature             = None
        self.rel_humidity            = None
        self.pressure                = None
        
        # calculated variables
        self.sea_level_pressure      = None
        #self.abs_humidity            = None
        self.dewpoint                = None
        self.altitude                = None
        
        
        # metadata
        self.variable_dictionary = {'Temperature' : {'variable name' : 'temperature'  , 'units symbol' : 'C'     , 'units' : 'degrees Celcius'  , 'format' : '0.1f', 'data_type' : 'measured',   'method' : None, 'provenance' : 'sensor BME280', 'dependent_variable' : None},
                                    'Humidity'    : {'variable name' : 'rel_humidity' , 'units symbol' : '%%'    , 'units' : 'percent'          , 'format' : '0.1f', 'data_type' : 'measured',   'method' : None, 'provenance' : 'sensor BME280', 'dependent_variable' : None},
                                    'Pressure'    : {'variable name' : 'pressure'     , 'units symbol' : 'hPa'   , 'units' : 'hPa'              , 'format' : '0.3f', 'data_type' : 'measured',   'method' : None, 'provenance' : 'sensor BME680', 'dependent_variable' : 'pressure_sea_level'},
                                    'Altitude'    : {'variable name' : 'altitude'     , 'units symbol' : 'm'     , 'units' : 'meters'           , 'format' : '0.2f', 'data_type' : 'measured',   'method' : None, 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Dewpoint'    : {'variable name' : 'dewpoint'     , 'units symbol' : 'C'     , 'units' : 'degrees Celcius'  , 'format' : '0.2f', 'data_type' : 'calculated', 'method' : None, 'provenance' : 'sensor BME680', 'dependent_variable' : ['temperature','rel_humidity']},
                                    }
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        Output:
           None, modify self.bme280 
           
        Packages:
            from busio import I2C
            import adafruit_bme280.advanced as adafruit_bme280
        
        """
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
                
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        self.bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c_address)
        
    
    def modify_sea_level_pressure(self, sea_level_pressure = 1013.25):
        """ Modify sea level pressure
        
        Input:
            sea_level_pressure = sea level pressure, default is generic 1013.25
        
        Output:
            None, modify self.sea_level_pressure  
        Package:
            import time
            import board
            import adafruit_bme280.advanced as adafruit_bme280
        
        """
        # functions
        self.sea_level_pressure  =sea_level_pressure
        self.bme280.sea_level_pressure(sea_level_pressure)
        
    
    def measure_temperature(self):
        """FUNC: measure temperature
        
        Input:
            None
        
        Output:
            temperature = sensor temperature value
        
        Packages:
            import adafruit_bme280.advanced as adafruit_bme280
        
        """
        # functions
        temperature      = self.bme280.temperature
        self.temperature = temperature
        
        return temperature
    
        
    def measure_rel_humidity(self):
        """FUNC: measure relative humidity
        
        Input:
            None
        
        Output:
            relative_humidity = sensor value of relative humidity
        
        Packages:
            import adafruit_bme280.advanced as adafruit_bme280
        
        """
        # functions
        relative_humidity = self.bme280.relative_humidity
        self.rel_humidity = relative_humidity
        
        return relative_humidity
    
    
    def measure_pressure(self):
        """FUNC: measure pressure
        
        Input:
            None
        
        Output:
            pressure = sensor value of pressure
        
        Packages:
            import adafruit_bme280.advanced as adafruit_bme280
        
        """
        # functions
        pressure      = self.bme280.pressure
        self.pressure = pressure
        
        return pressure
    
    
    def measure_altitude(self):
        """FUNC: measure altitude
        
        Input:
            None
        
        Output:
            altitude = sensor value of altitude
            
        Packages:
            import adafruit_bme280.advanced as adafruit_bme280
        
        """
        # functions
        altitude      = self.bme280.altitude
        self.altitude = altitude
        
        return altitude
    
    
    def measure_all(self):
        """ FUNC: measure all, returns (temperature_value, rel_humidity_value, pressure_value, altitude_value)
        
        Input:
            None
            
        Output:
            temperature_value, rel_humidity_value, pressure_value, altitude_value
        
        Packages:
            import time
            import adafruit_bme280.advanced as adafruit_bme280
            from busio import I2C
        
        """
        temperature_value  = self.measure_temperature()
        rel_humidity_value = self.measure_rel_humidity()
        pressure_value     = self.measure_pressure()
        altitude_value     = self.measure_altitude()
        
        return temperature_value, rel_humidity_value, pressure_value, altitude_value
    
    
    def dew_point(self, temperature, rel_humidity):
        """ FUNC: dew point
        
        Input:
            temperature  = temperature in degrees celcius
            rel_humidity = relative humidity in percent
        
        Output:
            dewpoint     = The dew point is the temperature to which air
                            must be cooled to become saturated with water
                            vapor
        
        Reference:
            https://learn.adafruit.com/adafruit-bme280-humidity-barometric-pressure-temperature-sensor-breakout/python-circuitpython-test
        
        Notes:
            Dew Point:
            The dew point is the temperature to which air
            must be cooled to become saturated with water
            vapor, assuming constant air pressure and water
            content. When cooled below the dew point, moisture
            capacity is reduced[1] and airborne water vapor
            will condense to form liquid water known as dew.
            When this occurs via contact with a colder surface,
            dew will form on that surface.
        
        """
        # Packages
        import math
        
        # Variables
        b = 17.62
        c = 243.12
        
        # Function
        gamma    = (b * temperature /(c + temperature)) + math.log(rel_humidity / 100.0)
        dewpoint = (c * gamma) / (b - gamma)
        
        # store variable
        self.dewpoint  = dewpoint
        
        return dewpoint
    
     
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            None, print values
        
        Packages:
            import time
            import adafruit_bme280.advanced as adafruit_bme280
            from busio import I2C
        
        Notes:
            print("\nTemperature: %0.1f C" % (temperature))
            print("Humidity: %0.1f %%" % rel_humidity)
            print("Pressure: %0.3f hPa" % pressure)
            print("Altitude = %0.2f meters" % altitude)
        
        """
        # functions
        temperature  = self.temperature
        rel_humidity = self.rel_humidity
        #abs_humidity = self.rel_humidity
        pressure     = self.pressure
        altitude     = self.altitude
        
        
        # Print values
        # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
        temperature_text = "\nTemperature: %" + self.variable_dictionary['Temperature']['format'] + " " +  self.variable_dictionary['Temperature']['units symbol']
        humidity_text    = "Humidity: %" + self.variable_dictionary['Humidity']['format'] + " " +  self.variable_dictionary['Humidity']['units symbol'] 
        pressure_text    = "Pressure: %" + self.variable_dictionary['Pressure']['format'] + " " +  self.variable_dictionary['Pressure']['units symbol']  
        altitude_text    = "Altitude = %" + self.variable_dictionary['Altitude']['format'] + " " +  self.variable_dictionary['Altitude']['units symbol'] 
        
        print()
        print(temperature_text % (temperature))
        print(humidity_text  % rel_humidity)
        print(pressure_text  % pressure)
        print(altitude_text % altitude)

    
    ###### IN PROGRESS
    #def convert_to_abs_humidity(self, relative_humidity):
    #    """ FUNC: convert relative humidity to absolute humidity
    #    
    #    """
    #


# Functions
# None


# Dictionaries
# None