""" sensor_sht40.py

# Rationale:
    Start up, run, and print variables of I2C sensor SHT40
    # Purpose: Temperature and Humidity
    # i2C: 0x44
        

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html    

# Requirements:
    from board import SCL, SDA
    from busio import I2C
    import adafruit_sht4x

# Contains:
    class : sensor_sht40
            which contains: [   _init_, start_sensor,
                                measure_temperature, measure_rel_humidity,
                                measure_all, print_sensor_values
                               ]

# References:


# Notes:

    
# Examples:



"""

#Packages
#from board import SCL, SDA
import board
from busio import I2C
import adafruit_sht4x


# Class
class sensor_sht40:
    """CLASS: MEASURE SHT40
    
    
    """
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        
        # VARIABLES
        # Internal class variables
        
        # sensor
        self.sensor_name             = "SHT40"
        self.i2c_address             = 0x44
        self.i2c_address_alternative = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.i2c_I2C                 = None
        self.sht                     = None
        self.sht_serial              = None
        self.sht_mode                = None        
                
        # measured variables
        self.temperature             = None
        self.rel_humidity            = None
        
        # metadata
        self.variable_dictionary = {'Temperature' : {'variable name' : 'temperature' , 'units symbol' : 'C'  , 'units' : 'degrees Celcius', 'format' : '0.1f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor SHT40', 'dependent_variable' : None},
                                    'Humidity'    : {'variable name' : 'rel_humidity', 'units symbol' : '%%' , 'units' : 'percentage'     , 'format' : '0.1f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor SHT40', 'dependent_variable' : None},
                                    }
        
        # calculated variables
        # None
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None,
                     mode = 'No heat',
                     chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
        
        Output:
            None, print and modify internal variables
        
        Packages:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_sht4x
        
        """
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port, use 'slow' 100KHz frequency!
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        # Connect to sensor over I2C
        self.sht        = adafruit_sht4x.SHT4x(i2c_address)
        
        # Get serial number of I2C sensor
        self.sht_serial = hex(self.sht.serial_number)
        print("Found SHT4x with serial number", self.sht_serial)
        
        # Set I2C sensor mode
        if mode == 'No heat':
            
            self.sht.mode = adafruit_sht4x.Mode.NOHEAT_HIGHPRECISION
            self.sht_mode = adafruit_sht4x.Mode.string[self.sht.mode]
            
            # modify metadata
            self.variable_dictionary['Temperature']['method'] = 'No heat high precision'
            
        elif mode == 'Low heat 100 ms':
            # Can also set the mode to enable heater
            self.sht.mode = adafruit_sht4x.Mode.LOWHEAT_100MS
            self.sht_mode = adafruit_sht4x.Mode.string[self.sht.mode]
            
            # modify metadata
            self.variable_dictionary['Temperature']['method'] = 'Low heat 100 ms'
            
        else:
            raise ValueError('mode should be "No heat" or "Low heat 100 ms" ')
        
        print("Current mode is: ", self.sht_mode)
        
    
    def measure_temperature(self):
        """ Measure temperature in degrees celcius
        
        Input:
            None
        
        Output:
            temperature = sensor temperature
        
        Packages:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_sht4x
        
        """
        temperature = self.sht.measurements[0]
        self.temperature = temperature
        
        return temperature
    
    
    def measure_rel_humidity(self):
        """ Measure relative humidity in rh %, values from 0 to 100%
        
        Input:
            None
        
        Output:
            rel_humidity = sensor relative humidity
        
        Packages:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_sht4x
            
        """
        rel_humidity      = self.sht.measurements[1]
        self.rel_humidity = rel_humidity
        
        return rel_humidity
        
    
    def measure_all(self):
        """ Func: Measure all sensor parameters, returns (temperature_value, rel_humidity_value)
        
        Input:
            None
        
        Output:
            temperature_value, rel_humidity_value
        
        Packages:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_sht4x
        
        """
        temperature_value  = self.measure_temperature()
        rel_humidity_value = self.measure_rel_humidity()
        
        return temperature_value, rel_humidity_value
    
    
    def print_sensor_values(self):
        """ Func: Print all sensor values
        
        Input:
            None
        
        Output:
            None, print values
        
        Packages:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_sht4x
         
        Notes:
            print("Temperature: %0.1f C" % temperature)
            print("Humidity: %0.1f %%" % relative_humidity)
            print("")
         
        """
        # variables
        temperature       = self.temperature
        relative_humidity = self.rel_humidity
        
        # Print values
        # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
        temperature_text = "\nTemperature: %" + self.variable_dictionary['Temperature']['format'] + " " +  self.variable_dictionary['Temperature']['units symbol']
        humidity_text    = "Humidity: %" + self.variable_dictionary['Humidity']['format'] + " " +  self.variable_dictionary['Humidity']['units symbol']
        
        print()
        print(temperature_text % (temperature))
        print(humidity_text  % relative_humidity)


# Functions
# None


# Dictionaries
# None
