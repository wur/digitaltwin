""" sensor_BH1745.py

# Rationale:
    Start up, run, and print variables of I2C sensor BH1745
    # Purpose: Light/Colour sensor
    # i2C: 0x38 and 0x39

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_light.html   

# Requirements:
    from board import SCL, SDA
    from busio import I2C
    import adafruit_bh1750

# Contains:
    class : sensor_BH1745
            which contains: [   _init_, start_sensor,
                             measure_lux, measure_all, 
                             print_sensor_values
                               ]

# References:
    https://www.adafruit.com/product/4681

# Notes:
    LIGHT sensor
    "BH1750 provides 16-bit light measurements in lux, the SI unit
    for measuring light, making it easy to compare against other
    values like references and measurements from other sensors.
    The BH1750 can measure from 0 to 65K+ lux, but with some
    calibration and advanced adjustment of the measurement time,
    it can even be convinced to measure as much as 100,000 lux!"
    - From: https://www.adafruit.com/product/4681
    
# Examples:



"""

# Packages
import board 
from busio import I2C
import adafruit_bh1750


# Class
class sensor_BH1745:
    """CLASS: MEASURE BH1745
    
    
    """
    
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        # VARIABLES
        # Internal class variables
        
        # sensor
        self.sensor_name             = "BH1745"
        self.i2c_address             = 0x38
        self.i2c_address_alternative = 0x39
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.BH1745                  = None
                
        # measured variables
        self.lux                     = None
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {'Light' : {'variable name' : 'lux' , 'units symbol' : 'lux' , 'units' : 'lux' , 'format' : '0.2f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor BH1745', 'dependent_variable' : None},
                                    }
    
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None,frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        Output:
        
        Packages:
            from busio import I2C
            import adafruit_bh1750
            
        """
        
        # Variables
        if frequency is not None:
            
            self.i2c_frequency = frequency
            
        else:
            
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        self.BH1750 = adafruit_bh1750.BH1750(i2c_address)
    
    
    def measure_lux(self):
        """Measure Lux
        
        Input:
            None
        
        Output:
            lux = sensor lux measurement
        
        Packages:
            from busio import I2C
            import adafruit_bh1750
        
        """
        # packages
        import adafruit_bh1750
        
        # Functions
        lux      = self.BH1750.lux   # Get Lux value from light sensor
        self.lux = lux
        
        return lux


    def measure_all(self):
        """ FUNC: measure all, returns (lux_value)
        
        Input:
            None
        
        Output:
            lux_value
        
        Packages:
            from busio import I2C
            import adafruit_bh1750
        
        """
        lux_value = self.measure_lux()
        
        return lux_value
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
        
        Output:
            None, print sensor values
        
        Packages:
            from busio import I2C
            import adafruit_bh1750
        
        Note:
            print("\nLux: %0.2f Lux" % (lux))
        
        """
        # functions
        lux          = self.lux
        
        # Print values
        # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
        lux_text       = "\nLux: %"      + self.variable_dictionary['Light']['format']      + " " +  self.variable_dictionary['Light']['units symbol']
        #print("\nLux: %"      + self.variable_dictionary['Light']['format']      + " " +  self.variable_dictionary['Light']['units symbol']      % (lux))
        
        print(lux_text    % (lux))

# Functions
# None

# Dictionaries
# None
