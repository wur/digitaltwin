""" sensor_as7341.py

# Rationale:
    Start up, run, and print variables of I2C sensor 
    # Purpose: 10-Channel Light / Color Sensor Breakout
    # i2C: 0x39

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    09/08/2022 

# Licence:
    
    
# URL:


# Requirements:
    import time
    import board
    from adafruit_as7341 import AS7341

# Contains:
    class : sensor_as7341
            which contains: [   _init_, start_sensor,
                                
                                
                                measure_all, print_sensor_values 
                            ]

# References:
    https://learn.adafruit.com/adafruit-as7341-10-channel-light-color-sensor-breakout

# Notes:
     "Adafruit AS7341 10-Channel Light / Color Sensor Breakout is a multi-channel spectrometer,
     which is a special type of light sensor that is able to detect not only the amount of
     light present but also the amounts of light within different wavelengths" 
    - https://www.adafruit.com/product/4698
    
    "The AS7341 packs within its 3x2mm footprint 16 different sensors that can detect 8 separate,
    overlapping bands of colored light. As if that weren't enough, it also includes sensors for
    white light as well as Near Infra-red light, and even sensors made specifically for detecting
    light flicker at specific frequencies from things like indoor lighting"
    - https://learn.adafruit.com/adafruit-as7341-10-channel-light-color-sensor-breakout
    
    channel_415nm - Data for F1, the 415nm channel   Violet
    channel_445nm - Data for F2, the 445nm channel   Indigo
    channel_480nm - Data for F3, the 480nm channel   Blue
    channel_515nm - Data for F4, the 515nm channel   Cyan
    channel_555nm - Data for F5, the 555nm channel   Green
    channel_590nm - Data for F6, the 590nm channel   Yellow
    channel_630nm - Data for F7, the 630nm channel   Orange 
    channel_680nm - Data for F8, the 680nm channel   Red

    print("F1 - 415nm/Violet  %s" % sensor.channel_415nm)
    print("F2 - 445nm//Indigo %s" % sensor.channel_445nm)
    print("F3 - 480nm//Blue   %s" % sensor.channel_480nm)
    print("F4 - 515nm//Cyan   %s" % sensor.channel_515nm)
    print("F5 - 555nm/Green   %s" % sensor.channel_555nm)
    print("F6 - 590nm/Yellow  %s" % sensor.channel_590nm)
    print("F7 - 630nm/Orange  %s" % sensor.channel_630nm)
    print("F8 - 680nm/Red     %s" % sensor.channel_680nm)
    print("Clear              %s" % (sensor.channel_clear))
    print("Near-IR (NIR)      %s" % (sensor.channel_nir))


# Examples:



"""

#Packages
import time
import board
from busio import I2C
from adafruit_as7341 import AS7341


# Class
class sensor_as7341:
    """CLASS: MEASURE AS7341
    
    """
    
    
    
    def __init__(self):
        """ Initialise class
        
        Input:
            
        Output:
        
        Packages:
            import time
            import board
            from adafruit_as7341 import AS7341
        
        """
        
        # VARIABLES
        # Internal class variables
        
        # sensor
        self.sensor_name             = "AS7341"
        self.i2c_address             = 0x39
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.as7341                  = None
                
        # measured variables
        self.channel_415nm           = None    # 415nm channel   Violet
        self.channel_445nm           = None    # 445nm channel   Indigo
        self.channel_480nm           = None    # 480nm channel   Blue
        self.channel_515nm           = None    # 515nm channel   Cyan
        self.channel_555nm           = None    # 555nm channel   Green
        self.channel_590nm           = None    # 590nm channel   Yellow
        self.channel_630nm           = None    # 630nm channel   Orange 
        self.channel_680nm           = None    # 680nm channel   Red       
        self.channel_clear           = None    # clear channel   
        self.channel_nir             = None    # nir   channel   
        
        
        # metadata
        self.variable_dictionary = {'Channel 415 nm' : {'variable name' : 'channel_415nm', 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    'Channel 445 nm' : {'variable name' : 'channel_445nm', 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    'Channel 480 nm' : {'variable name' : 'channel_480nm', 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    'Channel 515 nm' : {'variable name' : 'channel_515nm', 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    'Channel 555 nm' : {'variable name' : 'channel_555nm', 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    'Channel 590 nm' : {'variable name' : 'channel_590nm', 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    'Channel 630 nm' : {'variable name' : 'channel_630nm', 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    'Channel 680 nm' : {'variable name' : 'channel_680nm', 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    'Channel Clear ' : {'variable name' : 'channel_clear', 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    'Channel NIR '   : {'variable name' : 'channel_nir'  , 'units symbol' : ' ', 'units' : ' ', 'format' : 'i', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor AS7341', 'dependent_variable' : None},
                                    
                                    }
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        Output:
           None, modify self.as7341 
           
        Packages:
            from busio import I2C
            from adafruit_as7341 import AS7341
        
        """
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
                
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        self.as7341 = AS7341(i2c_address)
        
    
    def measure_all(self):
        """ FUNC: measure all, returns (channel_415nm, channel_445nm, channel_480nm, channel_515nm, channel_555nm, channel_590nm, channel_630nm, channel_680nm, channel_clear,channel_nir)
        
        Input:
            None
            
        Output:
            channel_415nm, channel_445nm, channel_480nm, channel_515nm, channel_555nm, channel_590nm, channel_630nm, channel_680nm, channel_clear,channel_nir
        
        Packages:
            import time
            import adafruit_bme280.advanced as adafruit_bme280
            from busio import I2C
        
        """
        
        # get data
        channel_415nm               = self.as7341.channel_415nm    # 415nm channel   Violet
        channel_445nm               = self.as7341.channel_445nm    # 445nm channel   Indigo
        channel_480nm               = self.as7341.channel_480nm    # 480nm channel   Blue
        channel_515nm               = self.as7341.channel_515nm    # 515nm channel   Cyan
        channel_555nm               = self.as7341.channel_555nm    # 555nm channel   Green
        channel_590nm               = self.as7341.channel_590nm    # 590nm channel   Yellow
        channel_630nm               = self.as7341.channel_630nm    # 630nm channel   Orange 
        channel_680nm               = self.as7341.channel_680nm    # 680nm channel   Red       
        channel_clear               = self.as7341.channel_clear    # clear channel   
        channel_nir                 = self.as7341.channel_nir      # nir   channel   
        
        # modify internal values
        self.channel_415nm           = channel_415nm    # 415nm channel   Violet
        self.channel_445nm           = channel_445nm    # 445nm channel   Indigo
        self.channel_480nm           = channel_480nm    # 480nm channel   Blue
        self.channel_515nm           = channel_515nm    # 515nm channel   Cyan
        self.channel_555nm           = channel_555nm    # 555nm channel   Green
        self.channel_590nm           = channel_590nm    # 590nm channel   Yellow
        self.channel_630nm           = channel_630nm    # 630nm channel   Orange 
        self.channel_680nm           = channel_680nm    # 680nm channel   Red       
        self.channel_clear           = channel_clear    # clear channel   
        self.channel_nir             = channel_nir      # nir   channel   
        
        return channel_415nm, channel_445nm, channel_480nm, channel_515nm, channel_555nm, channel_590nm, channel_630nm, channel_680nm, channel_clear,channel_nir
    
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            None, print values
        
        Packages:
            import time
            import adafruit_bme280.advanced as adafruit_bme280
            from busio import I2C
        
        Notes:
            
            
        """
        # functions
        
        
        
        # Print values
        print("F1 - 415nm/Violet  %s" % self.channel_415nm)
        print("F2 - 445nm//Indigo %s" % self.channel_445nm)
        print("F3 - 480nm//Blue   %s" % self.channel_480nm)
        print("F4 - 515nm//Cyan   %s" % self.channel_515nm)
        print("F5 - 555nm/Green   %s" % self.channel_555nm)
        print("F6 - 590nm/Yellow  %s" % self.channel_590nm)
        print("F7 - 630nm/Orange  %s" % self.channel_630nm)
        print("F8 - 680nm/Red     %s" % self.channel_680nm)
        print("Clear              %s" % (self.channel_clear))
        print("Near-IR (NIR)      %s" % (self.channel_nir))
    


# Functions
# None


# Dictionaries
# None
