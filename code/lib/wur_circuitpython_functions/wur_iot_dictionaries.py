""" wur_iot_dictionaries.py

# Rationale:
    

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
      

# Requirements:
    

# Contains:
    dictionary : html_status_code_dictionary
            which contains: [  'influx_db']

# References:
    

# Notes:
    
    
# Examples:



"""

# Packages
# None

# Class
# None

# Functions
# None

# Dictionaries
html_status_code_dictionary = {'influx_db' :    {204 : 'Success',
                                                 400 : 'Bad request',
                                                 401 : 'Unauthorized, check token',
                                                 404 : 'Not found, check url',
                                                 413 : 'Too large, requested payload is too large',
                                                 429 : 'Too many requests, check plan',
                                                 500 : 'Internal server error, the server has encountered an unexpected error',
                                                 503 : 'Service unavailable, series cardinality exceeeds plan',
                                                },
                               }