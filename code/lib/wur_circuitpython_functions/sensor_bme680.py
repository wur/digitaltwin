""" sensor_bme680.py

# Rationale:
    Start up, run, and print variables of I2C sensor BME680

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html

# Requirements:
    import time
    import board
    import busio
    from busio import I2C
    import adafruit_bme680


# Contains:
    class : sensor_bme680
            which contains: [_init_,
                            start_sensor, measure_all,
                            measure_gas, measure_rel_humidity,
                            measure_altitude, measure_pressure,
                            measure_temperature, print_sensor_values
                            ]

# References:
    
    
# Examples:

    # RUN BME680 sensor
    # import packages
    from helper_functions import sensor_bme680
    import board
    import time
        
    # intialise sensor
    sensor1 = sensor_bme680()
    sensor1.start_sensor(SCL_pin = board.D11, SDA_pin = board.D13)
        
    # collect data
    while True:
        sensor1.measure_all()
        sensor1.print_sensor_values()
        time.sleep(10)
        
"""

#Packages
import time
import board
import busio
from busio import I2C
import adafruit_bme680


# Class
class sensor_bme680:
    """ CLASS: MEASURE BME680 i2c address 0x76 and 0x77  
        
        
    """

    
    def __init__(self):
        """ Initialise class
        
        
        """
        # VARIABLES
        # Internal class variables
        self.sensor_name             = "BME680"
        self.i2c_address             = 0x76
        self.i2c_address_alternative = 0x77
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        
        # Data values
        self.temperature             = None
        self.gas                     = None
        self.rel_humidity            = None
        self.pressure                = None
        self.altitude                = None
        
        self.temperature_correction  = None
        self.sea_level_pressure_correction = None
        
        # Sensor variable metadata
        self.variable_dictionary = {'Temperature' : {'variable name' : 'temperature' , 'units symbol' : 'C'   , 'units' : 'C'          , 'format' : '0.1f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : 'temperature_offset'},
                                    'Gas'         : {'variable name' : 'gas'         , 'units symbol' : 'ohm' , 'units' : 'ohm'        , 'format' : 'd'   , 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Humidity'    : {'variable name' : 'rel_humidity', 'units symbol' : '%%'  , 'units' : 'percentage' , 'format' : '0.1f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Pressure'    : {'variable name' : 'pressure'    , 'units symbol' : 'hPa' , 'units' : 'hPa'        , 'format' : '0.3f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : 'pressure_sea_level'},
                                    'Altitude'    : {'variable name' : 'altitude'    , 'units symbol' : 'm'   , 'units' : 'meters'     , 'format' : '0.2f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    }
    
    
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
               
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
        
        Output:
            self.bme680
        
        Packages
            from busio import I2C
            import adafruit_bme680
        
        """
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
        
        # Functions
        # Create sensor object, communicating over the board's second I2C bus
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        #self.i2c_address = i2c_address
        self.bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c_address,address=0x76,  debug=False)
    
    
    def measure_all(self, pressure_sea_level = 1011, temperature_offset = -5):
        """ FUNC: measure all variables, return (gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature)
        
        Input:
            None
            
        Output:
            gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature
            
        """
        
        gas_value          = self.measure_gas()
        rel_humidity_value = self.measure_rel_humidity()
        altitude_value     = self.measure_altitude()
        pressure_value     = self.measure_pressure(pressure_sea_level)
        measured_temperature, corrected_temperature = self.measure_temperature(temperature_offset)
        
        return gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature
    
    
    def measure_gas(self):
        """ FUNC: measure gas in ohm
        
        Input:
            None
            
        Output:
            gas_value = gas value in ohm
            
        Packages:
            import adafruit_bme680
        
        """
        # Functions
        gas_value = self.bme680.gas
        
        # store value
        self.gas  = gas_value
        
        return gas_value
    
    
    def measure_rel_humidity(self):
        """ FUNC: measure relatiuve humidity in %
        
        Input:
            None
        
        Output:
            rel_humidity_value = relative humidity value in percentage
        
        Packages:
            import adafruit_bme680
        
        """
        # Functions
        rel_humidity_value = self.bme680.relative_humidity
        
        # store value
        self.rel_humidity  = rel_humidity_value
        
        return rel_humidity_value
    
    
    def measure_altitude(self):
        """ FUNC: measure altitude in meters
        
        Input:
            None
            
        Output:
            altitude_value = altitude in m
        
        Packages:
            import adafruit_bme680
        
        """
        # Functions
        altitude_value = self.bme680.altitude
        
        # store value
        self.altitude  = altitude_value
        
        return altitude_value
    
    
    def measure_pressure(self, pressure_sea_level = 1011):
        """ FUNC: measure pressure in hpa
        
        Input:
            pressure_sea_level = sea level pressure in hPA at location's sea level
        
        Output:
            pressure_value     = pressure value in hPA
        
        Packages:
            import adafruit_bme680
        
        """
        # Variable
        # store pressure_sea_level correction parameter
        self.sea_level_pressure_correction = pressure_sea_level
        
        # Function        
        # change this to match the location's pressure (hPa) at sea level
        self.bme680.sea_level_pressure = pressure_sea_level
        # get pressure value
        pressure_value = self.bme680.pressure
        # store variable
        self.pressure  = pressure_value
        
        return pressure_value
        
        
        
    def measure_temperature(self, temperature_offset = -5):
        """ FUNC: Measure temperature in Celcius
            
            Input:
                temperature_offset    = offset temperature (default -5)
                
            Output:
                measured_temperature  = measured temperature
                corrected_temperature = corrected temperature (measured_temperature + temperature_offset)
                
            Package:
                import adafruit_bme680
            
            Notes:
                You will usually have to add an offset to account for the temperature of
                the sensor. This is usually around 5 degrees but varies by use. Use a
                separate temperature sensor to calibrate this one.
            
        """
        # Variable
        # store temperature_offset correction parameter
        self.temperature_correction = temperature_offset
          
        # Function    
        measured_temperature  = self.bme680.temperature
        corrected_temperature = measured_temperature + temperature_offset
        
        # store variable
        self.temperature = corrected_temperature
        
        return measured_temperature, corrected_temperature
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            None
        
        Notes:
            print("\nTemperature: %0.1f C" % (temperature))
            print("Gas: %d ohm" % gas)
            print("Humidity: %0.1f %%" % rel_humidity)
            print("Pressure: %0.3f hPa" % pressure)
            print("Altitude = %0.2f meters" % altitude)
        
        """
        # Get internal data values
        temperature  = self.temperature
        gas          = self.gas
        rel_humidity = self.rel_humidity
        pressure     = self.pressure
        altitude     = self.altitude
        
        # Print values
        # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
        temperature_text = "\nTemperature: %" + self.variable_dictionary['Temperature']['format'] + " " +  self.variable_dictionary['Temperature']['units symbol']
        gas_text         = "Gas: %" + self.variable_dictionary['Gas']['format'] + " " +  self.variable_dictionary['Gas']['units symbol'] 
        humidity_text    = "Humidity: %" + self.variable_dictionary['Humidity']['format'] + " " +  self.variable_dictionary['Humidity']['units symbol'] 
        pressure_text    = "Pressure: %" + self.variable_dictionary['Pressure']['format'] + " " +  self.variable_dictionary['Pressure']['units symbol']  
        altitude_text    = "Altitude = %" + self.variable_dictionary['Altitude']['format'] + " " +  self.variable_dictionary['Altitude']['units symbol'] 
        
        print()
        print(temperature_text % (temperature))
        print(gas_text  % gas)
        print(humidity_text  % rel_humidity)
        print(pressure_text  % pressure)
        print(altitude_text % altitude)

# Functions
# None


# Dictionaries
# None