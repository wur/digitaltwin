""" sensor_veml6075.py

# Rationale:
    Start up, run, and print variables of I2C sensor VEML-6075
    # Purpose: UVA/UVB sensor
    # i2C: 0x10

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
      

# Requirements:
    

# Contains:
    class : sensor_veml6075
            which contains: [_init_, ]

# References:
    

# Notes:
    
    
# Examples:



"""

# Packages
#import time
import board 
#from board import SCL, SDA
from busio import I2C
import adafruit_veml6075


# Class
class sensor_veml6075:
    """CLASS: MEASURE VEML-6075
    
    
    """
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        # VARIABLES
        # Internal class variables
        
        # sensor
        self.sensor_name             = "VEML-6075"
        self.i2c_address             = 0x10
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.VEML6075                = None
        self.integration_time        = None
                
        # measured variables
        self.uv_index                = None
        self.uva                     = None
        self.uvb                     = None
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {'UV Index'         : {'variable name' : 'uv_index'        , 'units symbol' : ''       , 'units' : None           , 'format' : '0.2f', 'data_type' : 'measured'    , 'method' : None, 'provenance' : 'sensor VEML-6075', 'dependent_variable' : 'integration_time'},
                                    'UVA'              : {'variable name' : 'uva'             , 'units symbol' : 'counts' , 'units' : 'counts'       , 'format' : '0.2f', 'data_type' : 'measured'    , 'method' : None, 'provenance' : 'sensor VEML-6075', 'dependent_variable' : 'integration_time'},
                                    'UVB'              : {'variable name' : 'uvb'             , 'units symbol' : 'counts' , 'units' : 'counts'       , 'format' : '0.2f', 'data_type' : 'measured'    , 'method' : None, 'provenance' : 'sensor VEML-6075', 'dependent_variable' : 'integration_time'},
                                    'Integration time' : {'variable name' : 'integration time', 'units symbol' : 'ms'     , 'units' : 'milliseconds' , 'format' : 'd'   , 'data_type' : 'user defined', 'method' : None, 'provenance' : 'sensor VEML-6075', 'dependent_variable' : None},
                                    }
    
    
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None,
                     integration_time=100, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            integration_time =      The integration_time is the amount of time the VEML6075
                                    is sampling data for, in milliseconds. Valid times are:
                                    50, 100, 200, 400 or 800ms. Default is 100
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        Output:
        
        
        Package:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_veml6075
            
        """
        
        # Variables
        # frequency of i2c
        if frequency is not None:
            
            self.i2c_frequency = frequency
            
        else:
            
            frequency = self.i2c_frequency
        
        self.integration_time  = integration_time
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        self.VEML6075  = adafruit_veml6075.VEML6075(i2c_address,integration_time)
        
    
    def measure_uv_index(self):
        """ FUNC: measure uv index
        
        Input:
            None
        
        Output:
            uv_index = sensor UV index
        
        Package:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_veml6075
        
        """
        
        # Functions
        uv_index      = self.VEML6075.uv_index   
        self.uv_index = uv_index
        
        return uv_index
    
    
    def measure_uva(self):
        """ FUNC: measure uv index
        
        Input:
            None
        
        Output:
            uva = sensor UV-A value
        
        Package:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_veml6075
            
        """      
        # Functions
        uva      = self.VEML6075.uva   
        self.uva = uva
        
        return uva
    
    
    def measure_uvb(self):
        """ FUNC: measure uv index
        
        Input:
            None
        
        Output:
            uvb = sensor UV-B value
        
        Package:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_veml6075
        
        """       
        # Functions
        uvb      = self.VEML6075.uvb   
        self.uvb = uvb
        
        return uvb
    
    
    def measure_all(self):
        """ FUNC: measure all, returns (uv_index_value,uva_value,uvb_value)
        
        Input:
            None
        
        Output:
            uv_index_value, uva_value, uvb_value
        
        Package:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_veml6075
        
        """
        uv_index_value = self.measure_uv_index()
        uva_value      = self.measure_uva()
        uvb_value      = self.measure_uvb()
        
        return uv_index_value, uva_value, uvb_value
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            print value
        
        Package:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_veml6075
        
        Notes:
            print("Integration time: %d ms" % integration_time)
            print("")
            print("\nUV-index: %0.2f" % (uv_index))
            print("\nUV-A: %0.2f counts" % (uva))
            print("\nUV-B: %0.2f counts" % (uvb))
        
        """
        # variables
        uv_index         = self.uv_index
        uva              = self.uva
        uvb              = self.uvb
        integration_time = self.integration_time
        
        # Print values
        print("")
        
        # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
        
        uv_index_text    = "\nUV-index: %"  + self.variable_dictionary['UV Index']['format'] + " " +  self.variable_dictionary['UV Index']['units symbol']  
        uva_text         = "\nUV-A: %"      + self.variable_dictionary['UVA']['format']      + " " +  self.variable_dictionary['UVA']['units symbol']  
        uvb_text         = "\nUV-B: %"      + self.variable_dictionary['UVB']['format']      + " " +  self.variable_dictionary['UVB']['units symbol']  
        
        print("Integration time: %d ms" % integration_time)
        print(uv_index_text % (uv_index))
        print(uva_text % (uva))
        print(uvb_text % (uvb))

# Functions
# None


# Dictionaries
# None
