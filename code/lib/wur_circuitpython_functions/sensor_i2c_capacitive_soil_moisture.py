""" sensor_i2c_capacitive_soil_moisture.py

# Rationale:
    Adafruit STEMMA Soil Sensor - I2C Capacitive Moisture Sensor
    # Purpose: Measure soil moisture using Adafruit STEMMA Soil Sensor - I2C Capacitive Moisture Sensor
    # i2C    : 0x36


# Authors:
    WUR Digital Twin Platform Methodology


# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_moisture.html 


# Requirements:
    import time
    import board
    import busio
    from adafruit_seesaw.seesaw import Seesaw


# Contains:
    class : sensor_i2c_soil
            which contains: [ _init_, start_sensor,
                            measure_soil, measure_all,
                            ]


# References:
    https://learn.adafruit.com/adafruit-stemma-soil-sensor-i2c-capacitive-moisture-sensor/overview


# Notes:
     RED  = SDA (e.g., gp pin D18 from Arduino Nano RP2040 Connect); BLACK  = power (3v3);
     BLUE = SCL (e.g., gp pin D19 from Arduino Nano RP2040 Connect); YELLOW = Ground
        
    
# Examples:



"""

# Packages
import time
import board
import busio
from adafruit_seesaw.seesaw import Seesaw


# Class
class sensor_i2c_soil:
    """CLASS: Adafruit STEMMA Soil Sensor - I2C Capacitive Moisture Sensor
    
    
    """
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        # VARIABLES
        # Internal class variables
        
        # sensor
        self.sensor_name             = "Adafruit STEMMA Soil Sensor - I2C Capacitive Moisture Sensor"
        self.i2c_address             = 0x36
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        
        self.i2c_soil_sensor         = None     # sensors class
        
                
        # measured variables
        self.soil_moisture           = None
        self.temperature             = None
        
        # calculated variables
        self.soil_moisture_modified  = None
        
        # metadata
        self.variable_dictionary = {'Soil Moisture'          : {'variable name' : 'soil_moisture'         , 'units symbol' : ' ' , 'units' : ' '              , 'format' : '0.2f'   , 'data_type' : 'measured'  , 'method' : None, 'provenance' : 'I2C soil sensor', 'dependent_variable' : None},
                                    'Soil Moisture modified' : {'variable name' : 'soil_moisture_modified', 'units symbol' : ' ' , 'units' : ' '              , 'format' : '0.2f', 'data_type' : 'calculated', 'method' : None, 'provenance' : 'I2C soil sensor', 'dependent_variable' : 'soil_moisture'},
                                    'Temperature'            : {'variable name' : 'temperature'           , 'units symbol' : 'C' , 'units' : 'degrees Celcius', 'format' : '0.1f', 'data_type' : 'measured'  , 'method' : None, 'provenance' : 'I2C soil sensor', 'dependent_variable' : None},
                                     }
    
    
    ###### Generic Functions
    def modify_divide(self, value, modifier = None):
        """ FUNC - DIVIDE: modify value via divide
                
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output = value / modifier
        
        return output 
    
    
    def modify_add(self, value, modifier = None):
        """ FUNC - ADD: modify value via add
                
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output = value + modifier
        
        return output
    
    
    def modify_subtract(self, value, modifier = None):
        """ FUNC - SUBTRACT: modify value via subtract
                
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output =  value - modifier
        
        return output
    

    def modify_multiply(self, value, modifier = None):
        """ FUNC - MULTIPLY: modify value via multiply
        
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output =  value * modifier
        
        return output
    
    
    ###### Sensors
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None,
                     integration_time=100, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
        
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        Output:
        
        
        Package:
            from board import SCL, SDA
            from busio import I2C
            import adafruit_veml6075
            
        """
        
        # Variables
        # frequency of i2c
        if frequency is not None:
            
            self.i2c_frequency = frequency
            
        else:
            
            frequency = self.i2c_frequency
        
        
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            # Define I2C bus, for instance the default board.SCL and board.SDA 
            # this is then defined as i2c_address which is different from the
            # 0x36 referred to as i2c_I2C
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        # Create sensor object, communicating over the board's default I2C bus
        self.i2c_soil_sensor  = Seesaw(self.i2c_I2C,addr = self.i2c_address)
        
    

    def measure_temperature(self):
            """FUNC: measure temperature
            
            Input:
                None
            
            Output:
                temperature = sensor temperature value
            
            Packages:
                #import time
                import board
                import busio
                from adafruit_seesaw.seesaw import Seesaw
            
            """
            # functions
            # read temperature from the temperature sensor
            temperature      = self.i2c_soil_sensor.get_temp()
            
            # modify internal variables
            self.temperature = temperature
            
            return temperature
        
        
    def measure_moisture(self):
            """FUNC: measure moisture
            
            Input:
                None
            
            Output:
                temperature = sensor temperature value
            
            Packages:
                #import time
                import board
                import busio
                from adafruit_seesaw.seesaw import Seesaw
            
            """
            # functions
            # read moisture level through capacitive touch pad
            soil_moisture      = self.i2c_soil_sensor.moisture_read()
            
            # modify internal variables
            self.soil_moisture = soil_moisture
            
            return soil_moisture
    
    
    def measure_all(self):
        """ FUNC: measure all, returns (soil_moisture, temperature)
        
        Input:
            None
        
        Output:
            soil_moisture, temperature
        
        Packages:
            #import time
            import board
            import busio
            from adafruit_seesaw.seesaw import Seesaw
            
        """
        temperature     = self.measure_temperature()
        soil_moisture   = self.measure_moisture()
        
        return soil_moisture, temperature
     

    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            None, print values
        
        Packages:
            #import time
            import board
            import busio
            from adafruit_seesaw.seesaw import Seesaw
        
        Notes:
            print("\nTemperature: %0.1f C" % (temperature))
            print("Soil Moisture: %0.2f %%" % soil_moisture)
            
        
        """
        # functions
        
        temperature   = self.temperature
        soil_moisture = self.soil_moisture  
        
        # Print values
        # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
        
        temperature_text   =  "\nTemperature: %" + self.variable_dictionary['Temperature']['format']   + " " +  self.variable_dictionary['Temperature']['units symbol']
        soil_moisture_text = "Soil Moisture: %" + self.variable_dictionary['Soil Moisture']['format'] + " " +  self.variable_dictionary['Soil Moisture']['units symbol'] 
        print(temperature_text  % (temperature))
        print(soil_moisture_text % (soil_moisture))
        
    