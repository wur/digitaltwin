""" led_display.py

# Rationale:
    Display images and text onto a LED display

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
    

# Requirements:
    import board
    #import busio
    from busio import I2C
    import displayio
    import terminalio
        
    # add to lib
    import adafruit_display_text.bitmap_label as label # from adafruit_display_text
    import adafruit_displayio_sh1107
    from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297

# Contains:
    class : oled_1_12inch
            which contains: [_init_, refresh_display,
                            draw_rectangle, add_text_to_group,
                            add_text_to_text_group, add_text_group]

# References:
    https://github.com/adafruit/Adafruit_CircuitPython_DisplayIO_SH1107/blob/main/examples/displayio_sh1107_mono_128x128_test.py
    
"""

#Packages
import board
#import busio
from busio import I2C
import displayio
import terminalio
    
# add to lib
import adafruit_display_text.bitmap_label as label # from adafruit_display_text
import adafruit_displayio_sh1107
from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297


# Class
class oled_1_12inch:
    """
    NOTE
    
    REF
        https://github.com/adafruit/Adafruit_CircuitPython_DisplayIO_SH1107/blob/main/examples/displayio_sh1107_mono_128x128_test.py
    
    EXAMPLES
    
    EXAMPLE1:
        import board
        display1 = oled_1_12inch(SCL_pin = board.SCL,  SDA_pin = board.SDA)
        display1.draw_rectangle()
        display1.add_text("test", 2,4, 1)
        
    EXAMPLE2:
        # RUN BME680 sensor and DISPLAY
        from helper_functions import sensor_bme680,oled_1_12inch
        import board
        import time

        sensor1 = sensor_bme680()
        sensor1.start_sensor(SCL_pin = board.D11, SDA_pin = board.D13)

        display1 = oled_1_12inch(SCL_pin = board.SCL,  SDA_pin = board.SDA)
        display1.draw_rectangle()

        while True:
            sensor1.measure_all()
            sensor1.print_sensor_values()
            
            display1.draw_rectangle()
            display1.add_text("Temp.: %0.1f C" % sensor1.temperature, 4,20, 1)
            display1.add_text("Gas: %d ohm" % sensor1.gas, 4,40, 1)
            display1.add_text("Hum.: %0.1f %%" %  sensor1.rel_humidity, 4,60, 1)
            display1.add_text("Pres.: %0.3f hPa" % sensor1.pressure, 4,80, 1)
            display1.add_text("Alt. = %0.2f m" % sensor1.altitude, 4,100, 1)
            
            
            time.sleep(10)
    
    EXAMPLE 3
        from helper_functions import sensor_bme680,oled_1_12inch
        import board
        import time

        sensor1 = sensor_bme680()
        sensor1.start_sensor(SCL_pin = board.D11, SDA_pin = board.D13)

        display1 = oled_1_12inch(SCL_pin = board.SCL,  SDA_pin = board.SDA)
        display1.draw_rectangle()
        loading_txt = display1.add_text_to_group(" WUR Digital Twins \n Loading \n Please Wait ", 4,40, 1, group_to_add_to = display1.splash)
        time.sleep(10)

        while True:
            sensor1.measure_all()
            sensor1.print_sensor_values()
            
            #display1.refresh_display()
            #display1.draw_rectangle()
            display1.add_text_group([
                                     ["Temp.: %0.1f C" % sensor1.temperature  , 4,20, 1],
                                     ["Gas: %d ohm" % sensor1.gas             , 4,40, 1],
                                     ["Hum.: %0.1f %%" %  sensor1.rel_humidity, 4,60, 1],
                                     ["Pres.: %0.3f hPa" % sensor1.pressure   , 4,80, 1],
                                     ["Alt. = %0.2f m" % sensor1.altitude    , 4,100, 1],
                                     ]
                                    )    
            time.sleep(10)
    

    """


    def __init__(self, using_which ='busio',
                        SCL_pin          = None,
                        SDA_pin          = None,
                        display_width    = 128,
                        display_height   = 128,
                        display_border   = 2,
                        display_rotation = -90,
                 ):
        """ FUNC: Initialise class
        
        Input
            using_which = for an IF statement to change between busio or board library modules
            SCL_pin          = SCL pin default is None,
            SDA_pin          = SDA pin default is None,
            display_width    = display width default is 128,
            display_height   = display height default is 128,
            display_border   = display border default is 2,
            display_rotation = rotation of display default is -90,
            
        Output
            None
            
        Packages
            import board
            from busio import I2C
            import displayio
            import terminalio
            import adafruit_display_text.bitmap_label as label # from adafruit_display_text
            import adafruit_displayio_sh1107
            from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
        

            
        """
        # VARIABLES
        # Internal class variables
        self.WIDTH    = display_width
        self.HEIGHT   = display_height
        self.BORDER   = display_border
        self.ROTATION = display_rotation  # SH1107 is vertically oriented 64x128 so need to modify
        
        # FUNCTION
        displayio.release_displays()
        #oled_reset = board.D9
        
        if using_which == 'board':
            
            # packages
            # import board
            
            # Use for I2C
            i2c_address = board.I2C()
            
        
        elif using_which == 'busio':
            
            # packages
            # from busio import I2C
        
            # Create sensor object, communicating over the board's second I2C bus
            i2c_address = I2C(SCL_pin,SDA_pin)
            
        else:
            raise ValueError('using_which should be busio or board')
            
        self.display_bus = displayio.I2CDisplay(i2c_address, device_address=0x3c)
        
        # Start display
        self.display = adafruit_displayio_sh1107.SH1107(self.display_bus,
                                                       width=self.WIDTH, height=self.HEIGHT,
                                                       display_offset = DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297,
                                                       rotation       = self.ROTATION)

        # Make the display context
        self.splash = displayio.Group()
        self.display.show(self.splash)

        self.color_bitmap     = displayio.Bitmap(self.WIDTH, self.HEIGHT, 1)
        self.color_palette    = displayio.Palette(1)
        self.color_palette[0] = 0xFFFFFF  # White

        self.bg_sprite        = displayio.TileGrid(self.color_bitmap, pixel_shader=self.color_palette, x=0, y=0)
        self.splash.append(self.bg_sprite)
        
        
    
    def refresh_display(self):
        """ FUNC: refresh display
        
        Input:
            None
            
        Output:
            None
        
        """
        self.display.refresh()
    
    
    
    def draw_rectangle(self):
        """ FUNC: Draw rectangle
                
        Input:
            None
            
        Output:
            None
        
        Packages:
            import board
            from busio import I2C
            import displayio
            import terminalio
            import adafruit_display_text.bitmap_label as label # from adafruit_display_text
            import adafruit_displayio_sh1107
            from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
            
        """
        # Draw a smaller inner rectangle in black
        self.inner_bitmap     = displayio.Bitmap(self.WIDTH - self.BORDER * 2, self.HEIGHT - self.BORDER * 2, 1)
        self.inner_palette    = displayio.Palette(1)
        self.inner_palette[0] = 0x000000  # Black
        self.inner_sprite     = displayio.TileGrid(self.inner_bitmap, pixel_shader=self.inner_palette,
                                                   x=self.BORDER, y=self.BORDER)
        self.splash.append(self.inner_sprite)
        
    
    
    def add_text_to_group(self, 
                       text_to_display          = "test",
                       text_to_display_x        =  0,
                       text_to_display_y        =  0,
                       text_to_display_scale    =  1,
                       group_to_add_to          = None,
                       text_to_display_color    =  0xFFFFFF,
                 
                 ):
        """ FUNC: add text to a group
        
        Input
            text_to_display          = text to display, default = "test",
            text_to_display_x        = display x coordindate, default =  0,
            text_to_display_y        = display y coordinate, default =  0,
            text_to_display_scale    = display scale, default =  1,
            group_to_add_to          = group to add text to (e.g., self.splash), default = None,
            text_to_display_color    = display colour , default =  0xFFFFFF,
            
        Output:
            
        Packages:
            import board
            from busio import I2C
            import displayio
            import terminalio
            import adafruit_display_text.bitmap_label as label # from adafruit_display_text
            import adafruit_displayio_sh1107
            from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
        """
        # FUNCTION
        text_area = label.Label(terminalio.FONT,
                                text  =text_to_display,
                                scale = text_to_display_scale,
                                color =text_to_display_color,
                                x     =text_to_display_x,
                                y     =text_to_display_y)
        group_to_add_to.append(text_area)

    
    
    def add_text_to_text_group(self, text_to_display       = "test",
                       text_to_display_x     =  0,
                       text_to_display_y     =  0,
                       text_to_display_scale =  1,
                       text_to_display_color =  0xFFFFFF,
                 
                 ):
        """ FUNC: add text to a text group
        
        Input:
            text_to_display       = text to display, default = "test",
            text_to_display_x     = display x coordindate, default = 0,
            text_to_display_y     = display y coordinate, default = 0,
            text_to_display_scale = display scale, default = 1,
            text_to_display_color = display colour , default = 0xFFFFFF,
        
        Output:
            
        Packages:
            import board
            from busio import I2C
            import displayio
            import terminalio
            import adafruit_display_text.bitmap_label as label # from adafruit_display_text
            import adafruit_displayio_sh1107
            from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
        
        """
        
        # FUNCTION
        text_area = label.Label(terminalio.FONT,
                                text  =text_to_display,
                                scale = text_to_display_scale,
                                color =text_to_display_color,
                                x     =text_to_display_x,
                                y     =text_to_display_y)
        self.text_to_display.append(text_area)
    
    
    
    def add_text_group(self,list_of_lists):
        """ FUNC: add text group

            Input:
                list_of_lists = a list of lists to add text (see explained example below)
                
            Output:
                None
                
            Packages:
                import board
                import displayio
                import terminalio
                import adafruit_display_text.bitmap_label as label # from adafruit_display_text
                import adafruit_displayio_sh1107
                from adafruit_displayio_sh1107 import SH1107, DISPLAY_OFFSET_ADAFRUIT_128x128_OLED_5297
                
            Explained example:
                text_A = display1.add_text_to_group("Temp.: %0.1f C" % sensor1.temperature, 4,20, 1)
                text_B = display1.add_text_to_group("Gas: %d ohm" % sensor1.gas, 4,40, 1)
                text_C = display1.add_text_to_group("Hum.: %0.1f %%" %  sensor1.rel_humidity, 4,60, 1)
                text_D = display1.add_text_to_group("Pres.: %0.3f hPa" % sensor1.pressure, 4,80, 1)
                text_E = display1.add_text_to_group("Alt. = %0.2f m" % sensor1.altitude, 4,100, 1)
                
                converts to list of lists in the format [TEXT , x_value, y_value, scale] like so:
                
                text_to_add = [  ["Temp.: %0.1f C" % sensor1.temperature  , 4,20, 1],
                                 ["Gas: %d ohm" % sensor1.gas             , 4,40, 1],
                                 ["Hum.: %0.1f %%" %  sensor1.rel_humidity, 4,60, 1],
                                 ["Pres.: %0.3f hPa" % sensor1.pressure   , 4,80, 1],
                                 ["Alt. = %0.2f m" % sensor1.altitude    , 4,100, 1],
                                 ]
             

        """
        # FUNCTION
        # Make the display context
        self.text_to_display = displayio.Group()
        self.display.show(self.text_to_display)
        
        for item in list_of_lists:
            
            self.add_text_to_text_group(item[0],item[1], item[2], item[3])


# Functions
# None


# Dictionaries
# None