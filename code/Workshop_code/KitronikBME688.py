""" KitronikBME688.py

class KitronikBME688

MODIFIED FROM:
    Kitronik-Pico-Smart-Air-Quality-Board-MicroPython/PicoAirQuality.py 
    https://github.com/KitronikLtd/Kitronik-Pico-Smart-Air-Quality-Board-MicroPython

MODIFICATIONS:
    remove display; modified functions documentation

LICENSE:
    MIT License

    Copyright (c) 2021 Kitronik Ltd 

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.


"""
# PACKAGES
import math
import framebuf
import array
import os
from machine import Pin, PWM, ADC, time_pulse_us, I2C, RTC
from rp2 import PIO, StateMachine, asm_pio
from time import sleep, sleep_ms, sleep_us, ticks_ms, ticks_us
from micropython import const


class KitronikBME688:
    """
    
    RATIONALE:
        The KitronikBME688 class enables contro and use of the BME688 sensor on the board
        The following functions are for reading the registers on the BME688
        
    REQUIREMENTS:
        import math
        import framebuf
        import array
        import os
        from machine import Pin, PWM, ADC, time_pulse_us, I2C, RTC
        from rp2 import PIO, StateMachine, asm_pio
        from time import sleep, sleep_ms, sleep_us, ticks_ms, ticks_us
        from micropython import const
        
    REF:
        modified from
        https://github.com/KitronikLtd/Kitronik-Pico-Smart-Air-Quality-Board-MicroPython/blob/main/PicoAirQuality.py
    """
    
    def getUInt8(self, reg):
        """Function for reading register as signed 8 bit integer
        
        """
        return int.from_bytes(self.i2c.readfrom_mem(self.CHIP_ADDRESS, reg, 1), "big")
    
    
    def twosComp(self, value, bits):
        """ Function to convert unsigned ints to twos complement signed ints
        
        """
        if ((value & (1 << (bits - 1))) != 0):
            value = value - (1 << bits)
        return value

    
    def mapValues(self, value, frMin, frMax, toMin, toMax):
        """ Function for proportionally mapping a value to a different value range
        
        """
        toRange = toMax - toMin
        mappedVal = toMin + ((value - frMin) * ((toMax - toMin) / (frMax - frMin)))
        return mappedVal

    def __init__(self, i2cAddr=0x77, sda=8, scl=9):
        """ intialisation the class includings functions and register addresses
        
        INPUT:
            i2cAddr = i2c address of board, default 0x77
            sda     = sda Pin
            scl     = scl Pin
        
        """
        self.CHIP_ADDRESS = i2cAddr    # I2C address as determined by hardware configuration
        sda = Pin(sda)
        scl = Pin(scl)
        self.i2c = I2C(0,sda=sda, scl=scl, freq=100000)

        # Useful BME688 Register Addresses
        # Control
        self.CTRL_MEAS = 0x74       # Bit position <7:5>: Temperature oversampling   Bit position <4:2>: Pressure oversampling   Bit position <1:0>: Sensor power mode
        self.RESET = 0xE0           # Write 0xB6 to initiate soft-reset (same effect as power-on reset)
        self.CHIP_ID = 0xD0         # Read this to return the chip ID: 0x61 - good way to check communication is occurring
        self.CTRL_HUM = 0x72        # Bit position <2:0>: Humidity oversampling settings
        self.CONFIG = 0x75          # Bit position <4:2>: IIR filter settings
        self.CTRL_GAS_0 = 0x70      # Bit position <3>: Heater off (set to '1' to turn off current injection)
        self.CTRL_GAS_1 = 0x71      # Bit position <5> DATASHEET ERROR: Enable gas conversions to start when set to '1'   Bit position <3:0>: Heater step selection (0 to 9)

        # Pressure Data
        self.PRESS_MSB_0 = 0x1F     # Forced & Parallel: MSB [19:12]
        self.PRESS_LSB_0 = 0x20     # Forced & Parallel: LSB [11:4]
        self.PRESS_XLSB_0 = 0x21    # Forced & Parallel: XLSB [3:0]

        # Temperature Data
        self.TEMP_MSB_0 = 0x22      # Forced & Parallel: MSB [19:12]
        self.TEMP_LSB_0 = 0x23      # Forced & Parallel: LSB [11:4]
        self.TEMP_XLSB_0 = 0x24     # Forced & Parallel: XLSB [3:0]

        # Humidity Data
        self.HUMID_MSB_0 = 0x25     # Forced & Parallel: MSB [15:8]
        self.HUMID_LSB_0 = 0x26     # Forced & Parallel: LSB [7:0]

        # Gas Resistance Data
        self.GAS_RES_MSB_0 = 0x2C   # Forced & Parallel: MSB [9:2]
        self.GAS_RES_LSB_0 = 0x2D   # Forced & Parallel: Bit <7:6>: LSB [1:0]    Bit <5>: Gas valid    Bit <4>: Heater stability    Bit <3:0>: Gas resistance range

        # Status
        self.MEAS_STATUS_0 = 0x1D   # Forced & Parallel: Bit <7>: New data    Bit <6>: Gas measuring    Bit <5>: Measuring    Bit <3:0>: Gas measurement index

        # Calibration parameters for compensation calculations
        # Temperature
        self.PAR_T1 = self.twosComp((self.getUInt8(0xEA) << 8) | self.getUInt8(0xE9), 16)      # Signed 16-bit
        self.PAR_T2 = self.twosComp((self.getUInt8(0x8B) << 8) | self.getUInt8(0x8A), 16)      # Signed 16-bit
        self.PAR_T3 = self.twosComp(self.getUInt8(0x8C), 8)           # Signed 8-bit

        # Pressure
        self.PAR_P1 = (self.getUInt8(0x8F) << 8) | self.getUInt8(0x8E)   # Always a positive number, do not do twosComp() conversion!
        self.PAR_P2 = self.twosComp((self.getUInt8(0x91) << 8) | self.getUInt8(0x90), 16)      # Signed 16-bit
        self.PAR_P3 = self.twosComp(self.getUInt8(0x92), 8)                                 # Signed 8-bit
        self.PAR_P4 = self.twosComp((self.getUInt8(0x95) << 8) | self.getUInt8(0x94), 16)      # Signed 16-bit
        self.PAR_P5 = self.twosComp((self.getUInt8(0x97) << 8) | self.getUInt8(0x96), 16)      # Signed 16-bit
        self.PAR_P6 = self.twosComp(self.getUInt8(0x99), 8)                                 # Signed 8-bit
        self.PAR_P7 = self.twosComp(self.getUInt8(0x98), 8)                                 # Signed 8-bit
        self.PAR_P8 = self.twosComp((self.getUInt8(0x9D) << 8) | self.getUInt8(0x9C), 16)      # Signed 16-bit
        self.PAR_P9 = self.twosComp((self.getUInt8(0x9F) << 8) | self.getUInt8(0x9E), 16)      # Signed 16-bit
        self.PAR_P10 = self.twosComp(self.getUInt8(0xA0), 8)                                # Signed 8-bit

        # Humidity
        parH1_LSB_parH2_LSB = self.getUInt8(0xE2)
        self.PAR_H1 = (self.getUInt8(0xE3) << 4) | (parH1_LSB_parH2_LSB & 0x0F)
        self.PAR_H2 = (self.getUInt8(0xE1) << 4) | (parH1_LSB_parH2_LSB >> 4)
        self.PAR_H3 = self.twosComp(self.getUInt8(0xE4), 8)                                 # Signed 8-bit
        self.PAR_H4 = self.twosComp(self.getUInt8(0xE5), 8)                                 # Signed 8-bit
        self.PAR_H5 = self.twosComp(self.getUInt8(0xE6), 8)                                 # Signed 8-bit
        self.PAR_H6 = self.twosComp(self.getUInt8(0xE7), 8)                                 # Signed 8-bit
        self.PAR_H7 = self.twosComp(self.getUInt8(0xE8), 8)                                 # Signed 8-bit

        # Gas resistance
        self.PAR_G1 = self.twosComp(self.getUInt8(0xED), 8)                                 # Signed 8-bit
        self.PAR_G2 = self.twosComp((self.getUInt8(0xEB) << 8) | self.getUInt8(0xEC), 16)      # Signed 16-bit
        self.PAR_G3 = self.getUInt8(0xEE)                                # Unsigned 8-bit
        self.RES_HEAT_RANGE = (self.getUInt8(0x02) >> 4) & 0x03
        self.RES_HEAT_VAL = self.twosComp(self.getUInt8(0x00), 8)              # Signed 8-bit

        # Oversampling rate constants
        self.OSRS_1X = 0x01
        self.OSRS_2X = 0x02
        self.OSRS_4X = 0x03
        self.OSRS_8X = 0x04
        self.OSRS_16X = 0x05

        # IIR filter coefficient values
        self.IIR_0 = 0x00
        self.IIR_1 = 0x01
        self.IIR_3 = 0x02
        self.IIR_7 = 0x03
        self.IIR_15 = 0x04
        self.IIR_31 = 0x05
        self.IIR_63 = 0x06
        self.IIR_127 = 0x07

        #Global variables used for storing one copy of value, these are used in multiple locations for calculations
        self.bme688InitFlag = False
        self.gasInit = False

        self.tRead = 0       # calculated readings of sensor parameters from raw adc readings
        self.pRead = 0
        self.hRead = 0
        self.gRes = 0
        self.iaqPercent = 0
        self.iaqScore = 0
        self.airQualityRating = ""
        self.eCO2Value = 0

        self.gBase = 0
        self.hBase = 40        # Between 30% & 50% is a widely recognised optimal indoor humidity, 40% is a good middle ground
        self.hWeight = 0.25     # Humidity contributes 25% to the IAQ score, gas resistance is 75%
        self.hPrev = 0
        self.measTime = 0
        self.measTimePrev = 0

        self.tRaw = 0    # adc reading of raw temperature
        self.pRaw = 0       # adc reading of raw pressure
        self.hRaw = 0       # adc reading of raw humidity
        self.gResRaw = 0  # adc reading of raw gas resistance
        self.gasRange = 0

        self.t_fine = 0                          # Intermediate temperature value used for pressure calculation
        self.newAmbTemp = 0
        self.tAmbient = 0       # Intermediate temperature value used for heater calculation
        self.ambTempFlag = False
        

        # Begin the hardware inititialisation for the BME688 sensor
        self.bme688Init()

    
    def calcTemperature(self, tempADC):
        """Temperature compensation calculation: rawADC to degrees C (integer)
        
        """
        var1 = (tempADC >> 3) - (self.PAR_T1 << 1)
        var2 = (var1 * self.PAR_T2) >> 11
        var3 = ((((var1 >> 1) * (var1 >> 1)) >> 12) * (self.PAR_T3 << 4)) >> 14
        self.t_fine = var2 + var3
        self.newAmbTemp = ((self.t_fine * 5) + 128) >> 8
        self.tRead = self.newAmbTemp / 100     # Convert to floating point with 2 dp
        if (self.ambTempFlag == False):
            self.tAmbient = self.newAmbTemp
    
    
     
    def intCalcPressure(self, pressureADC):
        """ Pressure compensation calculation: rawADC to Pascals (integer)
        
        """
        var1 = (self.t_fine >> 1) - 64000
        var2 = ((((var1 >> 2) * (var1 >> 2)) >> 11) * self.PAR_P6) >> 2
        var2 = var2 + ((var1 * self.PAR_P5) << 1)
        var2 = (var2 >> 2) + (self.PAR_P4 << 16)
        var1 = (((((var1 >> 2) * (var1 >> 2)) >> 13) * (self.PAR_P3 << 5)) >> 3) + ((self.PAR_P2 * var1) >> 1)
        var1 = var1 >> 18
        var1 = ((32768 + var1) * self.PAR_P1) >> 15
        self.pRead = 1048576 - pressureADC
        self.pRead = ((self.pRead - (var2 >> 12)) * 3125)

        if (self.pRead >= (1 << 30)):
            self.pRead = (self.pRead // var1) << 1
        else:
            self.pRead = ((self.pRead << 1) // var1)

        var1 = (self.PAR_P9 * (((self.pRead >> 3) * (self.pRead >> 3)) >> 13)) >> 12
        var2 = ((self.pRead >> 2) * self.PAR_P8) >> 13
        var3 = ((self.pRead >> 8) * (self.pRead >> 8) * (self.pRead >> 8) * self.PAR_P10) >> 17
        self.pRead = self.pRead + ((var1 + var2 + var3 + (self.PAR_P7 << 7)) >> 4)

    
    def intCalcHumidity(self, humidADC, tempScaled):
        """ Humidity compensation calculation: rawADC to % (integer)
        
        INPUT:
            humidADC   =
            tempScaled = is the current reading from the Temperature sensor
        
        """
        
        self.hPrev = self.hRead
        tempScaled = math.trunc(tempScaled)
        
        var1 = humidADC - (self.PAR_H1 << 4) - (((tempScaled * self.PAR_H3) // 100) >> 1)
        var2 = (self.PAR_H2 * (((tempScaled * self.PAR_H4) // 100) + (((tempScaled * ((tempScaled * self.PAR_H5) // 100)) >> 6) // 100) + (1 << 14))) >> 10
        var3 = var1 * var2
        var4 = ((self.PAR_H6 << 7) + ((tempScaled * self.PAR_H7) // 100)) >> 4
        var5 = ((var3 >> 14) * (var3 >> 14)) >> 10
        var6 = (var4 * var5) >> 1
        self.hRead = (var3 + var6) >> 12
        self.hRead = (((var3 + var6) >> 10) * (1000)) >> 12
        self.hRead = self.hRead // 1000

    
    def intConvertGasTargetTemp(self, ambientTemp, targetTemp):
        """ Gas sensor heater target temperature to target resistance calculation
        
        INPUT:
            'ambientTemp' = reading from Temperature sensor in degC (could be averaged over a day when there is enough data?)
            'targetTemp'  = desired temperature of the hot plate in degC (in range 200 to 400)
        
        NOTE:
            Heating duration also needs to be specified for each heating step in 'gas_wait' registers
    
        """
        
        var1 = ((ambientTemp * self.PAR_G3) // 1000) << 8    # Divide by 1000 as we have ambientTemp in pre-degC format (i.e. 2500 rather than 25.00 degC)
        var2 = (self.PAR_G1 + 784) * (((((self.PAR_G2 + 154009) * targetTemp * 5) // 100) + 3276800) // 10)
        var3 = var1 + (var2 >> 1)
        var4 = (var3 // (self.RES_HEAT_RANGE + 4))
        var5 = (131 * self.RES_HEAT_VAL) + 65536                 # Target heater resistance in Ohms
        resHeatX100 = (((var4 // var5) - 250) * 34)
        resHeat = ((resHeatX100 + 50) // 100)

        return resHeat

    
    def intCalcgRes(self, gasADC, gasRange):
        """  Gas resistance compensation calculation: rawADC & range to Ohms (integer)

        """
        var1 = 262144 >> gasRange
        var2 = gasADC - 512
        var2 = var2 * 3
        var2 = 4096 + var2
        calcGasRes = ((10000 * var1) // var2)
        self.gRes = calcGasRes * 100

     
    def bme688Init(self):
        """Initialise the BME688.

            Initialise, establishing communication, entering initial T, P & H oversampling rates,
            setup filter and do a first data reading (won't return gas)
        
        """
        
        # Establish communication with BME688
        chipID = self.i2c.readfrom_mem(self.CHIP_ADDRESS, self.CHIP_ID, 1)
        chipID = int.from_bytes(chipID, "big")
        while (chipID != 97):
            chipID = self.i2c.readfrom_mem(self.CHIP_ADDRESS, self.CHIP_ID, 1)
        # Do a soft reset
        self.i2c.writeto_mem(self.CHIP_ADDRESS, self.RESET, "\xB6")
        sleep_ms(1000)
        # Set mode to SLEEP MODE: CTRL_MEAS reg <1:0>
        self.i2c.writeto_mem(self.CHIP_ADDRESS, self.CTRL_MEAS, "\x00")
        # Set the oversampling rates for Temperature, Pressure and Humidity
        # Humidity: CTRL_HUM bits <2:0>
        self.i2c.writeto_mem(self.CHIP_ADDRESS, self.CTRL_HUM, str(self.OSRS_2X))
        # Temperature: CTRL_MEAS bits <7:5>     Pressure: CTRL_MEAS bits <4:2>    (Combine and write both in one command)
        self.i2c.writeto_mem(self.CHIP_ADDRESS, self.CTRL_MEAS, str(((self.OSRS_2X << 5) | (self.OSRS_16X << 2))))

        # IIR Filter: CONFIG bits <4:2>
        self.i2c.writeto_mem(self.CHIP_ADDRESS, self.CONFIG, str(self.IIR_3 << 2))

        # Enable gas conversion: CTRL_GAS_1 bit <5>    (although datasheet says <4> - not sure what's going on here...)
        self.i2c.writeto_mem(self.CHIP_ADDRESS, self.CTRL_GAS_1, "\x20")
        self.bme688InitFlag = True

        # Do an initial data read (will only return temperature, pressure and humidity as no gas sensor parameters have been set)
        self.measureData()

    
    def setupGasSensor(self, targetTemp=300, heatDuration=180):
        """ Setup the gas sensor (defaults are 300°C and 180ms).
            
            INPUT:
                targetTemp = the target temperature for the gas sensor plate to reach (200 - 400°C), eg: 300
                heatDuration = the length of time for the heater to be turned on (0 - 4032ms), eg: 180
                
            WARNING:
                The temperature and duration values can be changed but
                this is not recommended unless the user is familiar with gas sensor
                setup. The default values have been chosen as they provide a good
                all-round sensor response for air quality purposes
        
        """
        if (self.bme688InitFlag == False):
            self.bme688Init()

        # Limit targetTemp between 200°C & 400°C
        if (targetTemp < 200):
            targetTemp = 200
        elif (targetTemp > 400):
            targetTemp = 400

        # Limit heatDuration between 0ms and 4032ms
        if (heatDuration < 0):
            heatDuration = 0
        elif (heatDuration > 4032):
            heatDuration = 4032

        # Define the target heater resistance from temperature
        self.i2c.writeto_mem(self.CHIP_ADDRESS, 0x5A, self.intConvertGasTargetTemp(self.tAmbient, targetTemp).to_bytes(1, 'big'))   # res_wait_0 register - heater step 0

        # Define the heater on time, converting ms to register code (Heater Step 0) - cannot be greater than 4032ms
        # Bits <7:6> are a multiplier (1, 4, 16 or 64 times)    Bits <5:0> are 1ms steps (0 to 63ms)
        codedDuration = 0
        if (heatDuration < 4032):
            factor = 0
            while (heatDuration > 63):
                heatDuration = (heatDuration // 4)
                factor = factor + 1

            codedDuration = heatDuration + (factor * 64)
        else:
            codedDuration = 255

        self.i2c.writeto_mem(self.CHIP_ADDRESS, 0x64, codedDuration.to_bytes(1, 'big'))     # gas_wait_0 register - heater step 0

        # Select index of heater step (0 to 9): CTRL_GAS_1 reg <3:0>    (Make sure to combine with gas enable setting already there)
        gasEnable = self.getUInt8(self.CTRL_GAS_1) & 0x20
        self.i2c.writeto_mem(self.CHIP_ADDRESS, self.CTRL_GAS_1, (0x00 | gasEnable).to_bytes(1, 'big'))   # Select heater step 0

        self.gasInit = True

     
    def measureData(self):
        """Run all measurements on the BME688: Temperature, Pressure, Humidity & Gas Resistance.
        
        """
        if (self.bme688InitFlag == False):
            self.bme688Init()

        self.measTimePrev = self.measTime       # Store previous measurement time (ms since micro:bit powered on)

        # Set mode to FORCED MODE to begin single read cycle: CTRL_MEAS reg <1:0>    (Make sure to combine with temp/pressure oversampling settings already there)
        oSampleTP = self.getUInt8(self.CTRL_MEAS)
        self.i2c.writeto_mem(self.CHIP_ADDRESS, self.CTRL_MEAS, str((0x01 | oSampleTP)))

        # Check New Data bit to see if values have been measured: MEAS_STATUS_0 bit <7>
        newData = (self.getUInt8(self.MEAS_STATUS_0) & 0x80) >> 7
        while (newData != 1):
            newData = (self.getUInt8(self.MEAS_STATUS_0) & 0x80) >> 7

        # Check Heater Stability Status bit to see if gas values have been measured: <4> (heater stability)
        heaterStable = (self.getUInt8(self.GAS_RES_LSB_0) & 0x10) >> 4

        # If there is new data, read temperature ADC registers(this is required for all other calculations)        
        self.tRaw = (self.getUInt8(self.TEMP_MSB_0) << 12) | (self.getUInt8(self.TEMP_LSB_0) << 4) | (self.getUInt8(self.TEMP_XLSB_0) >> 4)

        # Read pressure ADC registers
        self.pRaw = (self.getUInt8(self.PRESS_MSB_0) << 12) | (self.getUInt8(self.PRESS_LSB_0) << 4) | (self.getUInt8(self.PRESS_XLSB_0) >> 4)

        # Read humidity ADC registers
        self.hRaw = (self.getUInt8(self.HUMID_MSB_0) << 8) | (self.getUInt8(self.HUMID_LSB_0) >> 4)
        
        # Read gas resistance ADC registers
        self.gResRaw = (self.getUInt8(self.GAS_RES_MSB_0) << 2) | self.getUInt8(self.GAS_RES_LSB_0) >> 6           # Shift bits <7:6> right to get LSB for gas resistance

        gasRange = self.getUInt8(self.GAS_RES_LSB_0) & 0x0F

        self.measTime = ticks_ms()  # Capture latest measurement time (ms since Pico powered on)

        # Calculate the compensated reading values from the the raw ADC data
        self.calcTemperature(self.tRaw)
        self.intCalcPressure(self.pRaw)
        self.intCalcHumidity(self.hRaw, self.tRead)
        self.intCalcgRes(self.gResRaw, gasRange)


    def calcBaselines(self, forcedRun=False):
        """
            # A baseline gas resistance is required for the IAQ calculation - it should be taken in a well ventilated area without obvious air pollutants
            # Take 60 readings over a ~5min period and find the mean
            # Establish the baseline gas resistance reading and the ambient temperature.
            # These values are required for air quality calculations
            # When the baseline process is complete, values for gBase and tAmbient are stored in a file
            # On subsequent power cycles of the board, this function will look for that file and take the baseline values stored there
            # To force the baselines process to be run again, call the function like this: calcBaselines(True)
        
        """
        if (self.bme688InitFlag == False):
            self.bme688Init()
        if (self.gasInit == False):
            self.setupGasSensor()
            
        print("Setting Baseline")
        
        
        try: # Look for a 'baselines.txt' file existing - if it does, take the baseline values from there (unless 'forcedRun' is set to True)
            if not forcedRun:
                f = open("baselines.txt", "r")
                self.gBase = float(f.readline())
                self.tAmbient = float(f.readline())
            else:
                raise Exception("RUNNING BASELINE PROCESS")
        except: # If there is no file, an exception is raised, and the baseline process will be carried out (creating a new file at the end)
            self.ambTempFlag = False

            burnInReadings = 0
            burnInData = 0
            ambTotal = 0
            progress = 0
            while (burnInReadings < 60):               # Measure data and continue summing gas resistance until 60 readings have been taken
                progress = math.trunc((burnInReadings / 60) * 100)
                
                print(str(progress) + "%")
                print("Setting Baseline")
                
                self.measureData()
                burnInData = burnInData + self.gRes
                ambTotal = ambTotal + self.newAmbTemp
                sleep_ms(5000)
                burnInReadings = burnInReadings + 1

            self.gBase = (burnInData / 60)             # Find the mean gas resistance during the period to form the baseline
            self.tAmbient = (ambTotal / 60)            # Calculate the ambient temperature as the mean of the 60 initial readings

            # Save baseline values to a file
            f = open("baselines.txt", "w") #open in write - creates if not existing, will overwrite if it does
            f.write(str(self.gBase) + "\r\n")
            f.write(str(self.tAmbient) + "\r\n")
            f.close()
            
            self.ambTempFlag = True
        
        
        print("Setup Complete!")
        
        sleep_ms(2000)

    
    def readTemperature(self, temperature_unit="C"):
        """Read Temperature from sensor as a Number.
        
        INPUT:
            temperature_unit = Units for temperature are in °C (Celsius) or °F (Fahrenheit) according to selection.
        
        """
        temperature = self.tRead
        # Change temperature from °C to °F if user selection requires it
        if (temperature_unit == "F"):
            temperature = ((temperature * 18) + 320) / 10

        return temperature

    
    def readPressure(self, pressure_unit="Pa"):
        """ Read Pressure from sensor as a Number.
        
        INPUT:
            pressure_unit =  Units for pressure are in Pa (Pascals) or mBar (millibar) according to selection.
        
        """
        pressure = self.pRead
        #Change pressure from Pascals to millibar if user selection requires it
        if (pressure_unit == "mBar"):
            pressure = pressure / 100

        return pressure

    
    def readHumidity(self):
        """ Read Humidity from sensor as a Number.
       
        OUTPUT:
            Humidity is output as a percentage.
        
        """
        return self.hRead

    
    def readGasRes(self):
        """ Read Gas Resistance from sensor as a Number.
        
        OUTPUT:
            Units for gas resistance are in Ohms.
        
        """
        if (self.gasInit == False):
            
            print("ERROR")
            print("Setup Gas Sensor")
            
            return 0
        return self.gRes

    
    def readeCO2(self):
        """ Read eCO2 from sensor as a Number (250 - 40000+ppm).
        
        OUTPUT:
            Units for eCO2 are in ppm (parts per million).
        
        """
        if (self.gasInit == False):
            
            print("ERROR")
            print("Setup Gas Sensor")
            
            return 0
        self.calcAirQuality()

        return self.eCO2Value

    
    def getAirQualityPercent(self):
        """ Return the Air Quality rating as a percentage (0% = Bad, 100% = Excellent).
        
        """
        if (self.gasInit == False):
            
            print("ERROR")
            print("Setup Gas Sensor")
            
            return 0
        self.calcAirQuality()

        return self.iaqPercent

    
    def getAirQualityScore(self):
        """Return the Air Quality rating as an IAQ score

            Return the Air Quality rating as an IAQ score (500 = Bad, 0 = Excellent).
            These values are based on the BME688 datasheet, Page 11, Table 6.
            
        """
        if (self.gasInit == False):
            
            print("ERROR")
            print("Setup Gas Sensor")
            
            return 0
        self.calcAirQuality()

        return self.iaqScore
    
    
    def calcAirQuality(self):
        """Calculate the Index of Air Quality score from the current gas resistance and humidity readings
        
        # iaqPercent: 0 to 100% - higher value = better air quality
        # iaqScore: 25 should correspond to 'typically good' air, 250 to 'typically polluted' air
        # airQualityRating: Text output based on the iaqScore
        # Calculate the estimated CO2 value (eCO2)
    
        """
        humidityScore = 0
        gasScore = 0
        humidityOffset = self.hRead - self.hBase         # Calculate the humidity offset from the baseline setting
        ambTemp = (self.tAmbient / 100)
        temperatureOffset = self.tRead - ambTemp     # Calculate the temperature offset from the ambient temperature
        humidityRatio = ((humidityOffset / self.hBase) + 1)
        temperatureRatio = (temperatureOffset / ambTemp)

        # IAQ Calculations

        if (humidityOffset > 0):                                       # Different paths for calculating the humidity score depending on whether the offset is greater than 0
            humidityScore = (100 - self.hRead) / (100 - self.hBase)
        else:
            humidityScore = self.hRead / self.hBase
            
        humidityScore = humidityScore * self.hWeight * 100

        gasRatio = (self.gRes / self.gBase)

        if ((self.gBase - self.gRes) > 0):                                            # Different paths for calculating the gas score depending on whether the offset is greater than 0
            gasScore = gasRatio * (100 * (1 - self.hWeight))
        else:
            # Make sure that when the gas offset and humidityOffset are 0, iaqPercent is 95% - leaves room for cleaner air to be identified
            gasScore = math.floor(70 + (5 * (gasRatio - 1)))
            if (gasScore > 75):
                gasScore = 75

        self.iaqPercent = math.trunc(humidityScore + gasScore)               # Air quality percentage is the sum of the humidity (25% weighting) and gas (75% weighting) scores
        self.iaqScore = (100 - self.iaqPercent) * 5                               # Final air quality score is in range 0 - 500 (see BME688 datasheet page 11 for details)

        # eCO2 Calculations
        self.eCO2Value = 250 * math.pow(math.e, (0.012 * self.iaqScore))      # Exponential curve equation to calculate the eCO2 from an iaqScore input

        # Adjust eCO2Value for humidity and/or temperature greater than the baseline values
        if (humidityOffset > 0):
            if (temperatureOffset > 0):
                self.eCO2Value = self.eCO2Value * (humidityRatio + temperatureRatio)
            else:
                self.eCO2Value = self.eCO2Value * humidityRatio
        elif (temperatureOffset > 0):
            self.eCO2Value = self.eCO2Value * (temperatureRatio + 1)

        # If measurements are taking place rapidly, breath detection is possible due to the sudden increase in humidity (~7-10%)
        # If this increase happens within a 5s time window, 1200ppm is added to the eCO2 value
        # (These values were based on 'breath-testing' with another eCO2 sensor with algorithms built-in)
        if ((self.measTime - self.measTimePrev) <= 5000):
            if ((self.hRead - self.hPrev) >= 3):
                self.eCO2Value = self.eCO2Value + 1500

        self.eCO2Value = math.trunc(self.eCO2Value)
