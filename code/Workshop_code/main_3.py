""" main.py

    NOTE:
        Code to run the sensors upon start up
        using thread to save a file 

"""
# 0. PACKAGES
from breakout_bme68x import BreakoutBME68X
from breakout_sgp30 import BreakoutSGP30
from pimoroni_i2c import PimoroniI2C
from machine import Pin, I2C
import time
import sh1107
import _thread

#lock = _thread.allocate_lock()

# 1. USER SPECIFIED
# time between measurements in seconds
# 10 mins = 600; 1 hr = 3600
sampling_interval  = 600

# 2. FILE STORAGE
# 2.1 Find unused filename
running_filename_check = True
file_count = 0

while running_filename_check:
    
    try:
        filename = "sensor_data_" + str(file_count) + ".txt"
        
        f = open(filename,'r')
        f.close
        file_count += 1
        
    except:
        running_filename_check = False

filename = "sensor_data_" + str(file_count) + ".txt"

# 3. START THREAD - FILE SAVING
time_dif       = -99
count          = -99
temperature    = -99
pressure       = -99
humidity       = -99
gas_resistance = -99
status         = -99
gas_index      = -99
meas_index     = -99
eCO2           = -99
TVOC           = -99

def sensor_data_store(filename, sampling_interval):
    
    # Open file for writing       
    file = open(filename, 'w')
    
    # Make header string
    header_string = "time, count, temperature, pressure, humidity, gas, eCO2, TVOC \n"
    unit_string   = "seconds, number, C, hPa, percentRH, kOmega, ppm, count \n"
    file.write(header_string + unit_string)
    file.flush()
    
    
    while True:
        #lock.acquire()
        global time_dif
        global count
        global temperature
        global pressure
        global humidity
        global gas_resistance
        global status
        global gas_index
        global meas_index
        global eCO2
        global TVOC
        
        data_to_write = str(time_dif) + "," + str(count)  + "," + str(temperature)  + "," + str(pressure/100)  + "," + str(humidity)  + "," + str(gas_resistance/1000)  + "," + str(eCO2)  + "," + str(TVOC) + "\n"

        file.write(data_to_write)
        file.flush()
        #lock.release()
        time.sleep(sampling_interval)
        
_thread.start_new_thread(sensor_data_store,(filename, sampling_interval))


# DO SOMETHING ELSE

# 3. HARDWARE - set up the hardware
#PINS_BREAKOUT_GARDEN  = {"sda": 4, "scl": 5}
#PINS_PICO_EXPLORER    = {"sda": 20, "scl": 21}
PINS_PICO_W           = {"sda" : 8,  "scl" : 9}

i2c0 = I2C(1, scl=Pin(7), sda=Pin(6), freq=400000)
i2c1 = PimoroniI2C(**PINS_PICO_W)


# 4. INIT SENSORS
display = sh1107.SH1107_I2C(128, 128, i2c0, address=0x3c, rotate=-90)
display.sleep(False)
display.fill(0)

bme = BreakoutBME68X(i2c1)

sgp30 = BreakoutSGP30(i2c1)
sgp30.start_measurement(False)
id = sgp30.get_unique_id()
print("sgp30 intialised about to start measuring")


# 5. MEASUREMENTS
# 5.1 set up counters
count      = 0
timeInit   = time.time()
time_start = time.time()  

while True:
    
    display.fill(0)
    
    # 5.2 Get time
    time_end = time.time()
    time_dif = time_end - time_start


    # 5.3 measure bme688
    temperature, pressure, humidity, gas_resistance, status, gas_index, meas_index = bme.read()
    print(temperature, pressure, humidity, gas_resistance, status, gas_index, meas_index)
    

    # 5.4 measure sgp30
    count += 1
    air_quality = sgp30.get_air_quality()
    eCO2 = air_quality[BreakoutSGP30.ECO2]
    TVOC = air_quality[BreakoutSGP30.TVOC]
    print(eCO2, TVOC)
       

    # 5.6 CALIBRATION 
    # when count == 40 then start measuring sgp30 properly
    if count == 40:
        print("Resetting device")
        sgp30.soft_reset()
        time.sleep(0.5)
        print("restarting measurements waiting 15 seconds before returning")
        sgp30.start_measurement(True)
        print("measurement restarted")
    
    
    # 5.7 Display data
    #display.text('SH1107', 0, 0, 1)
    display.text('Temperature C', 0, 0, 1)
    display.large_text("{:.2f}".format(temperature), 0, 10,2, 1)
    
    display.text('Humidity %RH', 0, 30, 1)
    display.large_text("{:.2f}".format(humidity), 0, 40,2, 1)
    
    display.text('Gas kOmega', 0, 60, 1)
    display.large_text("{:.2f}".format(gas_resistance), 0, 70,2, 1)
    
    display.text('eCO2 in ppm', 0, 90, 1)
    display.large_text("{:.2f}".format(eCO2), 0, 100,2, 1)
    
    #display.text('TVOC', 0, 120, 1)
    display.text(" TVOC: {:.2f}".format(TVOC), 0, 120, 1)
    
    display.show()
    
    
    # 5.8 go to sleep 
    time_start = time.time()  
    time.sleep(1)


