import time
import board
from analogio import AnalogIn

sensor = AnalogIn(board.A2)

while True:
    print(sensor.value)
    time.sleep(1) 