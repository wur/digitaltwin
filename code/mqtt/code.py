import time
# Get data from a secrets.py file
from secrets import secrets

import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import board
from digitalio import DigitalInOut, Direction

# Turn the red led on during startup
led = DigitalInOut(board.LED)
led.direction = Direction.OUTPUT
led.value = True

####################################
###           WiFi               ###
####################################

from helper_functions import sensor_bme680, wifi_connect

get_connected = wifi_connect()
get_connected.connect_to_network(secrets["ssid"], secrets["password"])

####################################
###          Led                ###
####################################

def blink(times, led, status):
    for i in range(times):
        led.value = False
        time.sleep(0.1)
        led.value = True
        time.sleep(0.1)
    led.value = status

####################################
###          Sensor              ###
####################################
gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature = sensor_bme680.measure_all(pressure_sea_level = 1011, temperature_offset = -5)

####################################
###          MQTT                ###
####################################

# Define callback functions which will be called when certain events happen.
# pylint: disable=unused-argument
def connect(mqtt_client, userdata, flags, rc):
    # This function will be called when the mqtt_client is connected # successfully to the broker.
    print("Connected to MQTT Broker!")
    print("Flags: {0}\n RC: {1}".format(flags, rc))


def subscribe(client, userdata, topic, granted_qos):
    # This method is called when the client subscribes to a new feed.
    print("Subscribed to {0} with QOS level {1}".format(topic, granted_qos))


def unsubscribe(client, userdata, topic, pid):
    # This method is called when the client unsubscribes from a feed.
    print("Unsubscribed from {0} with PID {1}".format(topic, pid))


# pylint: disable=unused-argument
def disconnect(mqtt_client, userdata, rc):
    # Disconnected function will be called when the client disconnects.
    print("Disconnected from MQTT Broker!")


# pylint: disable=unused-argument
def message(client, feed_id, payload):
    # Message function will be called when a subscribed feed has a new value.
    # The feed_id parameter identifies the feed, and the payload parameter has
    # the new value.
    print("Feed {0} received new value: {1}".format(feed_id, payload))

def publish(mqtt_client, userdata, topic, pid):
    # This method is called when the mqtt_client publishes data to a feed.
    print("Published to {0} with PID {1}".format(topic, pid))

# Initialize MQTT interface with the esp interface
MQTT.set_socket(socket, get_connected.esp)

# Initialize a new MQTT Client object
mqtt_client = MQTT.MQTT(
    broker=secrets["mqtt_broker"],
    port=secrets["mqtt_port"],
    username=secrets["mqtt_username"],
    password=secrets["mqtt_password"],
    #    socket_pool=socket,
)

# Connect callback handlers to mqtt_client
mqtt_client.on_connect = connect
mqtt_client.on_disconnect = disconnect
mqtt_client.on_subscribe = subscribe
mqtt_client.on_unsubscribe = unsubscribe
mqtt_client.on_publish = publish
mqtt_client.on_message = message

mqtt_topic = secrets['mqtt_topic']

print("Attempting to connect to %s" % mqtt_client.broker)
mqtt_client.connect()


####################################
###          END STARTUP         ###
####################################

blink(6, led, False)

####################################
###          LOOP                ###
####################################

while True:
    # Enable the led
    led.value = True

    # Check for wifi connection
    if get_connected.get_status() == False:
        get_connected.connect_to_network(secrets["ssid"], secrets["password"])

    if mqtt_client.is_connected() == False:
        mqtt_client.connect()

    # Push the sensor data  
    mqtt_client.publish("sensors/garden", "garden corrected_temperature=" +str(corrected_temperature) + ",measured_temperature="+str(measured_temperature)+",pressure_value="+str(pressure_value)+",altitude_value="+str(altitude_value)+",rel_humidity_value="+str(rel_humidity_value)+",gas_value="+str(gas_value))
    
    # Blink
    blink(6, led, False)

    # Sleep for 30 seconds, rinse & repeat
    time.sleep(30)
