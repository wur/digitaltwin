import time
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import board
from digitalio import DigitalInOut, Direction

# Get data from a secrets.py file and import helper functions
from secrets import secrets
from helper_functions import sensor_bme680, wifi_connect, MQTT_comms, board_led


####################################
###          Led                ###
####################################
led = board_led()
led.turn_on_led()  # Turn on board LED


####################################
###           WiFi               ###
####################################
get_connected = wifi_connect()
get_connected.connect_to_network(secrets["ssid"], secrets["password"])


####################################
###          Sensor              ###
####################################
gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature = sensor_bme680.measure_all(pressure_sea_level = 1011, temperature_offset = -5)


####################################
###          MQTT                ###
####################################
do_some_mqtt = MQTT_comms()
do_some_mqtt.new_client_object(
                               broker_to_use    = secrets["mqtt_broker"],
                               port_to_use      = secrets["mqtt_port"],
                               username_to_use  = secrets["mqtt_username"],
                               password_to_use  = secrets["mqtt_password"],
                               topic_to_use     = secrets['mqtt_topic'],
                               )               

do_some_mqtt.connect()


####################################
###          END STARTUP         ###
####################################

led.blink(6, False)

####################################
###          LOOP                ###
####################################

while True:
    # Enable the led
    led.turn_on_led()

    # Check for wifi connection
    if get_connected.get_status() == False:
        get_connected.connect_to_network(secrets["ssid"], secrets["password"])

    if do_some_mqtt.connection_status() == False:
        do_some_mqtt.connect()

    # Push the sensor data
    feed    = "sensors/garden"
    value   = "garden corrected_temperature=" +str(corrected_temperature) + ",measured_temperature="+str(measured_temperature)+",pressure_value="+str(pressure_value)+",altitude_value="+str(altitude_value)+",rel_humidity_value="+str(rel_humidity_value)+",gas_value="+str(gas_value)
    do_some_mqtt.publish(feed,value)
    
    # Blink
    led.blink(6, False)

    # Sleep for 30 seconds, rinse & repeat
    time.sleep(30)
