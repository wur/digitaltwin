# Circuitpython 7.2.5, libraries needed: pimoroni_circuitpython_ltr559 adafruit_register
# pin 4 SDA, pin 5 SCL, 3.3V, GND

import time
from board import SCL, SDA
from busio import I2C
from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559

I2C_BUS = I2C(SCL, SDA)
LTR559 = Pimoroni_LTR559(I2C_BUS)

while True:
    print("Lux", LTR559.lux)  # Get Lux value from light sensor
    print("Proximity", LTR559.prox)  # Get Proximity value from proximity sensor
    time.sleep(1.0)