# -*- coding: utf-8 -*-
""" Planck Blackbody Figure Maker

# PACKAGES
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
    import matplotlib.gridspec as gridspec
    from matplotlib.ticker import (MultipleLocator) 

# DATA REQUIRED:
    
    The American Society for Testing and Materials (ASTM) G-173 spectra 
    represent terrestrial solar spectral irradiance on a surface of specified
    orientation under one and only one set of specified atmospheric conditions.
    https://www.nrel.gov/grid/solar-resource/spectra-am1.5.html

# USAGE NOTES
    data_directory = needs to be altered to the directory, where data_fname exists
    data_fname     = needs to download (see DATA REQUIRED) 

"""

## DATA
data_directory    = " "   # need to add data directory
data_fname        = "astmg173.xls"

data_sheetname    = "SMARTS2"
data_column_names = ['Wvlgth nm', 'Etr W*m-2*nm-1', 'Global tilt',  'W*m-2*nm-1', 'Direct+circumsolar W*m-2*nm-1']
data_metadata     = { 'method' : 'ASTM G173-03 Reference Spectra Derived from SMARTS v. 2.9.2',
                     'url'     : 'https://www.nrel.gov/grid/solar-resource/spectra-am1.5.html',
                     'url alt' : 'http://rredc.nrel.gov/solar/spectra/am1.5/ASTMG173/ASTMG173.html',
                     'source'  :  'Data from United States Department of Energy, National Renewable Energy Laboratory, Reference Solar Spectral Irradiance: ASTM G-173', 
                     }

## VARIABLES
temperature_of_sun = 5778    # in kelvin

# Define spectra
EM_light_spectra = {
                    # spectra                  : [start, end, abbreviation, units]
                   'Ultraviolet'               : [0.005,0.4   ,'UV'     ,'um'],
                   'Ultraviolet (UVC)'         : [0.1  ,0.28  ,'UVC'    ,'um'],
                   'Ultraviolet (UVB)'         : [0.28 ,0.315 ,'UVB'    ,'um'],
                   'Ultraviolet (UVA)'         : [0.315,0.4   ,'UVA'    ,'um'],
                   'Visible'                   : [0.4  ,0.7   ,'Visible','um'],
                   'Infrared (IR-A)'           : [0.7  ,1.4   , 'IR-A'  ,'um'],
                   'Near Infrared'             : [0.75 ,1.4   ,'NIR'    ,'um'],
                   'Short-wavelength Infrared' : [1.4  ,3     ,'SWIR'   ,'um'],
                   'Mid-wavelength Infrared'   : [3    ,8     ,'MWIR'   ,'um'],
                   'Long-wavelength Infrared'  : [8    ,15    ,'LWIR'   ,'um'],
                   'Far Infrared'              : [15   ,1000  ,'FIR'    ,'um'],
                    }


## PACKAGES
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.ticker import (MultipleLocator) 


## FUNCTIONS
def plancks_law(wavelength_lambda, Temperature):
    """
    # PURPOSE
    # Planck's law defines the distribution of the radiated flux E(lambda,T) as a function of the wavelength, lambda. 
    # Note, when the wavelength tends to either zero or to infinity then E(lambda,T) tends to zero. As temperature increases
    # the E(lambda,T) increases. As temperature increases the maximum wavelength tends to move toward shorter wavelengths.
    
    # REFERENCE:
    # Melieres and Marechal, 2015. Climate Change. Past, Present and Future Wiley & Sons Ltd, UK, pp 391
    # Dolman, 2019. Biogeochemical cycles and climate. Oxford University Press, pp 251
    # (note equation 4.2 of Dolman (2019), pg 45)
    # https://physics.stackexchange.com/questions/241741/black-body-spectrum-plot
    
    # INPUT:
    # Temperature       = temperature of a surface in Kelvin
    # wavelength_lambda = wavelength
    
    # OUTPUT:
    # the energy flux in (Wm-2um-1)
    """
    # PACKAGES
    import numpy as np
    # VARIABLES
    plancks_constant_h              = 6.626068*10**-34   # in Joule sec
    boltzmanns_constant_k           = 1.38066*10**-23    # in joule deg-1
    velocity_of_light_in_a_vacuum_c = 2.997925*10**8     # in m/s
    
    # CODE
    lhs_num  = 2 * np.pi * plancks_constant_h * (velocity_of_light_in_a_vacuum_c**2)
    lhs_den  = wavelength_lambda**5
    rhs_num  = 1
    expo     = (plancks_constant_h * velocity_of_light_in_a_vacuum_c) / (boltzmanns_constant_k * Temperature * wavelength_lambda)
    rhs_den  = np.exp(expo) -1
    output   = (lhs_num/lhs_den) * (rhs_num/rhs_den)
    
    return output


## LOAD
df = pd.read_excel(data_directory + data_fname, sheet_name = data_sheetname,
                   skiprows = 0, header = 1)


# MODEL
# make planck estimated constant
# for planck equation wavelength is in m, we want nm so multiply x_values by 10**-9:
x_values  = np.linspace(0*10**-9,4000*10**-9,4000)

# using a Sun temperature in kelvin of 5778
blackbody_Sun_planck      = [plancks_law(value, temperature_of_sun) for value in x_values]

# Then we need to correct the spectral radiance, first by multiplying by the 
# surface area of the sun (4*pi*r**2) and divide by the radius of a sphere 
# that covers the Earth Sun distance (4*pi*R**2), which
# leaves us with mulitplying by r2/R2 which has a value of:
r2_R2 = 2.17*(10**-5)

# multiply values by 10**-9 to get nm and then 
blackbody_Sun_planck_unit = [item * (10**-9)*r2_R2 for item in blackbody_Sun_planck]

# convert x_values back into nm
x_values_unit             = [item / (10**-9) for item in x_values]


## PLOT
# MAKE PLOT
fig = plt.figure(tight_layout=True, figsize=(14,8))
gs  = gridspec.GridSpec(1, 1)
ax1 = fig.add_subplot(gs[0, :])

# PLOT LINES
line_black_body_meas,= ax1.plot(df['Wvlgth nm'], df['Etr W*m-2*nm-1'], lw=0.4, color = 'darkorange', zorder = 12)
line_black_body_meas,= ax1.plot(df['Wvlgth nm'], df['Direct+circumsolar W*m-2*nm-1'], lw=0.4, color = 'darkred', zorder = 12)
line_black_body_est, = ax1.plot( x_values_unit,blackbody_Sun_planck_unit , lw=1.2, color = 'black', label = 'Blackbody ' + str(temperature_of_sun) + ' K', zorder = 12)

# PLOT VERTICAL LINES
for item in EM_light_spectra.keys():
    
    tmp       = np.ones((10,2))
    tmp[:,0]  = tmp[:,0] *(EM_light_spectra[item][1] * 1000)
    tmp[:,1]  = np.linspace(0,2.5, 10)
    
    line_EM,= ax1.plot(tmp[:,0], tmp[:,1], ls= '--', lw=0.8, label = EM_light_spectra[item][2],zorder = 10)

# PLOT AREA
area_meas   = ax1.fill_between(df['Wvlgth nm'],df['Etr W*m-2*nm-1'], y2=0,  color = 'white', alpha = 1, zorder = 11)
area_meas   = ax1.fill_between(df['Wvlgth nm'],df['Etr W*m-2*nm-1'], y2=0,  color = 'darkorange', alpha = 0.4, label = 'Top of Atmosphere', zorder = 12)
area_meas2  = ax1.fill_between(df['Wvlgth nm'],df['Direct+circumsolar W*m-2*nm-1'], y2=0,  color = 'darkred', alpha = 0.4, label = 'Surface', zorder = 12)

# # PLOT ANNOTATIONS
# # ANNOTATE Absorption paths
ax1.text(260 , 0.06, 'O$_{3}$' , rotation=0, color = 'black', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(710 , 0.08, 'O$_{2}$' , rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(860 , 0.08, 'H$_{2}$O', rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(1050, 0.08, 'H$_{2}$O', rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(1320, 0.08, 'H$_{2}$O', rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(1810, 0.06, 'H$_{2}$O', rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(1980, 0.14, 'CO$_{2}$', rotation=0, color = 'black', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(2600, 0.1 , 'H$_{2}$O', rotation=0, color = 'black', size = 16, fontweight = 'extra bold', zorder = 15);

# # ANNOTATE EM spectrum
ax1.text(220 , 2.30, 'UVC'     , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(265 , 2.15, 'UVB'     , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(340 , 2.3 , 'UVA'     , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(500 , 2.3 , 'Visible' , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(975 , 2.2 , 'IR-A \n NIR' , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(1950 , 2.2 , 'Short-wavelength Infrared' , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);

# # ANNOTATE Arrows  
TEXT              = "Blackbody at 5778 K "
xy_arrow_x        = 1050
xy_arrow_y        = 0.7
xy_text_x         = 1600
xy_text_y         = 1.2
connection_to_use = "arc3,rad=.2"
color_to_use      = "black"
    
ax1.annotate(TEXT,xy=(xy_arrow_x,xy_arrow_y),size =14,color = color_to_use, xycoords='data',xytext=(xy_text_x, xy_text_y), textcoords='data',bbox=dict(boxstyle="round", facecolor='white', edgecolor='white',),
            arrowprops=dict(arrowstyle="->", color=color_to_use,shrinkA=5, shrinkB=5, patchA=None, patchB=None,connectionstyle=connection_to_use,),)

text_formula = r'$B(\lambda, T) =\frac{2\pi hc^{2}}{\lambda^{5}} \cdot \frac{1}{e^{hc/k_{b}T\lambda} -1 } $'
ax1.text(1600 , 1.35, text_formula , rotation=0, color = 'black', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.annotate("With \natmospheric \nabsorption",xy=(1650,0.28), size = 12, color = "darkred", xycoords='data',xytext=(2100, 0.5), textcoords='data',bbox=dict(boxstyle="round", facecolor='white', edgecolor='white',),
            arrowprops=dict(arrowstyle="->", color="darkred",shrinkA=5, shrinkB=5, patchA=None, patchB=None,connectionstyle=connection_to_use,),)
   

ax1.annotate("Without \natmospheric \nabsorption",xy=(630,1.8), size = 12, color = "darkorange", xycoords='data',xytext=(800, 1.5), textcoords='data',bbox=dict(boxstyle="round", facecolor='white', edgecolor='white',),
            arrowprops=dict(arrowstyle="->", color="darkorange",shrinkA=5, shrinkB=5, patchA=None, patchB=None,connectionstyle=connection_to_use,),)

# AXIS
# Set axis and title labels
ax1.set_ylabel('Spectral Irradiance ($W/m^{2}/nm$)', size = 16, fontweight = 'bold')
ax1.set_xlabel('Wavelength (nm)', size = 16, fontweight = 'bold')
ax1.tick_params(direction='out', length=6, )
ax1.set_xlim(200, 4000)
ax1.set_ylim(0, 2.5)
ax1.xaxis.set_minor_locator(MultipleLocator(50))
ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
ax1.grid(True, ls = '--', c = 'lightgrey', alpha = 0.4)
ax1.legend()

# SAVE
plt.savefig('Planck_atmosphere.jpg', dpi = 650, format = 'jpg')
