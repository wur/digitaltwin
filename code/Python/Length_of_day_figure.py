# -*- coding: utf-8 -*-
""" Length of Day Figure Maker

# PACKAGES
    import math
    import numpy as np

    # For plotting
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    import matplotlib.dates as mdates
    import matplotlib.gridspec as gridspec
    import matplotlib.patches as mpatches
    from matplotlib.collections import PatchCollection

# DATA REQUIRED:
    None

# USAGE NOTES
    None

"""

#### PACKAGES
import math
import numpy as np

# For plotting
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

#### DEFINE FUNCTIONS
def solar_declination_approx(day_in_N_day):
    """ Approximate the solar declination using simplification of day in year

    # PURPOSE:
    # determine solar declination, delta, as approximated by day in year
    # where day_in_N_day starts at Jan 1 as 1, Jan 2 as 2 and so forth.
    
    # PACKAGES:
    # import math
    
    # INPUT:
    # day_in_N_day = day in year counting from 1 onwards, so Jan 1 = 1, Jan 2 = 2,...
    
    # EXAMPLES:
    
    # Example 1
    N_day             = np.linspace(0,365,365)
    # create output array
    declination_sun = []
    
    for item in N_day:    
        temp = solar_declination_approx(item)
        # convert output into degrees for plotting
        declination_sun.append(math.degrees(temp))
        
    # Example 2
    declination_sun = [math.degrees(solar_declination_approx(item)) for item in np.linspace(0,365,365)]
    
    """
    # PACKAGES
    import math
    
    # CODE:
    output = math.radians(23.45)*(math.sin((math.radians(360)*(day_in_N_day-80))/365))
    
    return output


def length_of_the_day_complex(lat_on_earth,solar_decl):
    """
    # PURPOSE:
    # compute length of the day (= LOD) using a slightly more complex argument
    
    # REFERENCE:
    # http://www.atmos.albany.edu/facstaff/brose/classes/ATM623_Spring2015/Notes/Lectures/Lecture11%20--%20Insolation.html
    
    # PACKAGES
    # import math
    
    # INPUT:
    # lat_on_earth = latitude on the earth, in radians
    # solar_decl   = solar declination, in radians
    
    # OUTPUT:
    # length of day in hours
    
    # EXAMPLE:
    # lat_on_earth = math.radians(90)
    # solar_decl   = math.radians(solar_decl)
    # length_of_the_day_complex(lat_on_earth,solar_decl)
    """
    # PACKAGES:
    import math
    
    # CODE:
    
    # Set latitudinal limits by which 0 and 24 hours of daylight will be defined
    lat_limits =  90-abs(math.degrees(solar_decl))
    
    # Set 24 hour of daylight (i.e., polar circle in 'Summer')
    if abs(math.degrees(lat_on_earth)) >= lat_limits  and np.sign(lat_on_earth) == np.sign(solar_decl):
        output = 24
        
    # Set 0 hour of daylight (i.e., polar circle in 'Winter')
    elif abs(math.degrees(lat_on_earth)) >= lat_limits  and np.sign(lat_on_earth) != np.sign(solar_decl):
        output = 0
    else:
        output = (24/math.pi)*math.acos((-math.tan(lat_on_earth)*math.tan(solar_decl)))
    
    return output


#### GENERATE DATA
# create output days
N_day             = np.linspace(0,365,365)
latitude_on_Earth = np.linspace(-90,90,180)

# create output array
declination_sun = []


for item in N_day:    
    
    temp = solar_declination_approx(item)
    
    # convert output into degrees for plotting
    declination_sun.append(math.degrees(temp))
    

lod_90 = []
lod_70 = []
lod_60 = []
lod_amsterdam = []
lod_50 = []
lod_30 = []
lod_0  = []

for item_sun_dec in declination_sun:

    lod_90.append(length_of_the_day_complex(math.radians(90),math.radians(item_sun_dec))) 
    lod_70.append(length_of_the_day_complex(math.radians(70),math.radians(item_sun_dec))) 
    lod_60.append(length_of_the_day_complex(math.radians(60),math.radians(item_sun_dec))) 
    lod_amsterdam.append(length_of_the_day_complex(math.radians(52.366),math.radians(item_sun_dec))) 
    lod_50.append(length_of_the_day_complex(math.radians(50),math.radians(item_sun_dec))) 
    lod_30.append(length_of_the_day_complex(math.radians(30),math.radians(item_sun_dec))) 
    lod_0.append(length_of_the_day_complex(math.radians(0),math.radians(item_sun_dec))) 

## PLOT DATA
# MAKE PLOT
fig = plt.figure(tight_layout=True, figsize=(8,6))
gs  = gridspec.GridSpec(1, 1)
ax1 = fig.add_subplot(gs[0, :])

# PLOT LINES
#line_lod_90, = ax1.plot(N_day, lod_90, lw=2, color = 'black')
line_lod_70, = ax1.plot(N_day, lod_70, lw=2, color = 'blue')
line_lod_60, = ax1.plot(N_day, lod_60, lw=2, color = 'green')
#line_lod_50, = ax1.plot(N_day, lod_50, lw=2, color = 'red')
line_lod_Amsterdam, = ax1.plot(N_day, lod_amsterdam, lw=2, color = 'red')
line_lod_30, = ax1.plot(N_day, lod_30, lw=2, color = 'orange')
line_lod_0,  = ax1.plot(N_day, lod_0 , lw=2, color = 'grey',dashes = [6,2])

# PLOT VERTICAL LINES
t = np.linspace(0,24,24)
s = []
[s.append(80) for item in t];
line_vernal_equinox = ax1.plot(s,t, lw=1, color = 'grey' , dashes = [6,2])
s = []
[s.append(265) for item in t];
line_autumn_equinox = ax1.plot(s,t, lw=1, color = 'grey' , dashes = [6,2])
s = []
[s.append(172) for item in t];
line_summer_solstice = ax1.plot(s,t, lw=1, color = 'grey' , dashes = [6,2])
s = []
[s.append(355) for item in t];
line_winter_solstice = ax1.plot(s,t, lw=1, color = 'grey' , dashes = [6,2])

# ANNOTATE
ax1.text(70 , 16 , 'Vernal Equinox'  , rotation=90);
ax1.text(250, 4, 'Autumnal Equinox', rotation=90);
ax1.text(160, 4, 'Summer Solstice' , rotation=90);
ax1.text(345, 16 , 'Winter Solstice' , rotation=90);
ax1.text(225, 20 , '70°N' , rotation=0, color = 'blue');
ax1.text(200, 18 , '60°N' , rotation=0, color = 'green');
#ax1.text(190, 16.25 , '50°N' , rotation=0, color = 'red');
ax1.text(180, 16.7 , '52.3°N' , rotation=0, color = 'red');
ax1.text(180, 14.5 , '30°N' , rotation=0, color = 'orange');
ax1.text(200, 12.5 , 'Equator' , rotation=0, color = 'grey');

# Set axis and title labels
ax1.set_ylabel('Length of day (in hours)')
ax1.set_xlabel('Day of Year')
ax1.set_xlim(0, 365)
ax1.set_ylim(0, 24)

# FIGURE CAPTION
# Set and plot caption titles
caption_text1 = "Figure 2:  "
caption_text2 = "Length of the day in hours for the Northern Hemisphere"
caption_text3 = "Red line is the length of day in hours for Amsterdam (52.3°N)."

fig.text(0.5, -0.02, caption_text1, ha='center')
fig.text(0.5, -0.08, caption_text2, ha='center')
fig.text(0.5, -0.14, caption_text3, ha='center');
