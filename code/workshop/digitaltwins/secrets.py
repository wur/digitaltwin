""" secrets.py: File for secret settings

    CONTAINS:
        secrets = dictionary with WIFI, Timezone, and Tokens

"""

secrets = {
            'board'         : {
                                'name' : 'board_1',
                                'sensor' : 'BME688',
                                },
            'ssid'          : 'WORKSHOP',
            'ssid_password' : 'workshop',
            'influxdb'        : {
                                'url'           : 'https://influx.systemsbiology.nl',
                                'token'         : "THdld0VNOYlxVD8J_5LEf3mj9ANajKv9-J3jwgQwcaIiLyX5ycSFSC863Hsc84AddDOb-rY_fj0BciRFb42_4Q==",
                                'bucket'        : 'WORKSHOP',
                                'org'           : 'WUR',
                            }
          }