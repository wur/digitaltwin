# 0. PACKAGES
from breakout_bme68x import BreakoutBME68X
import KitronikBME688
#from pimoroni_i2c import PimoroniI2C
from machine import Pin, I2C
import time
import sh1107
import network
from secrets import secrets
import urequests


##################
# 1. Dictionary
html_status_code_dictionary = {'influx_db' :    {204 : 'Success',
                                                 400 : 'Bad request',
                                                 401 : 'Unauthorized, check token',
                                                 404 : 'Not found, check url',
                                                 413 : 'Too large, requested payload is too large',
                                                 429 : 'Too many requests, check plan',
                                                 500 : 'Internal server error, the server has encountered an unexpected error',
                                                 503 : 'Service unavailable, series cardinality exceeeds plan',
                                                },
                               }

####################
# 2. NETWORK CONNECT
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(secrets['ssid'], secrets['ssid_password'])

print('Connected to WiFi')

payload_data    = ''
url_to_post_to  = secrets['influxdb']['url']+ '/api/v2/write?org='+secrets['influxdb']['org']+'&bucket='+ secrets['influxdb']['bucket']
headers_to_send = {'Authorization': 'Token ' + secrets['influxdb']['token'],
                    'Content-Type': 'application/x-www-form-urlencoded',
                  }

# DISPLAY INITIALISATION
i2c0 = I2C(1, scl=Pin(7), sda=Pin(6), freq=400000)
display = sh1107.SH1107_I2C(128, 128, i2c0, address=0x3c, rotate=-90)
display.sleep(False)
display.fill(0)
display.text('Hello Dave', 0, 0, 1)
display.large_text(secrets['board']['name'], 0, 100,2, 1)
display.show()
time.sleep(1)

# SENSOR INITIALISATION
# if i2c error change i2cAddr=0x76 to 0x77
bme688 = KitronikBME688.KitronikBME688(i2cAddr=0x76, sda=8, scl=9)

# SETUP/CALIBRATION GAS SENSOR
# if no baseline exists the pico will take 5 minutes of measurements
# best to do this initial set-up in a well-ventilated room.
display.fill(0)
display.text('Gas sensor init', 0, 0, 1)
display.show()
bme688.setupGasSensor()

time.sleep_ms(1000)
display.fill(0)
display.text('Calc. Baselines', 0, 0, 1)
display.show()
bme688.calcBaselines()

time.sleep_ms(1000)
display.fill(0)
display.text('All set up', 0, 0, 1)

# MEASURE VARIABLES
try:
    
    while True:
        time.sleep_ms(1000)
        display.fill(0)
        
        # MEASURE
        bme688.measureData()
        
        temperature = bme688.readTemperature()
        pressure    = bme688.readPressure()
        humidity    = bme688.readHumidity()
        gas_res     = bme688.readGasRes()
        IAQ         = bme688.getAirQualityScore()
        co2         = bme688.readeCO2()
        print(temperature, pressure, humidity, gas_res, IAQ, co2)
        
        
        # DISPLAY
        display.text('Temperature C', 0, 0, 1)
        display.large_text("{:.2f}".format(temperature), 0, 10,2, 1)
        
        display.text('Humidity %RH', 0, 30, 1)
        display.large_text("{:.2f}".format(humidity), 0, 40,2, 1)
        
        display.text('Air Quality', 0, 60, 1)
        display.large_text("{:.2f}".format(IAQ), 0, 70,2, 1)
        
        display.text('eCO2 in ppm', 0, 90, 1)
        display.large_text("{:.2f}".format(co2), 0, 100,2, 1)
        
        display.show()
        
        
        # POST
        payload_data = payload_data + secrets['board']['sensor'] + ',host=' + secrets['board']['name'] + ' ' +   'temperature=' + str(temperature)  + ',humidity=' + str(humidity) + ',pressure=' + str(pressure/100) + ',gas=' + str(gas_res)  + ',iaq=' + str(IAQ) + ',co2=' + str(co2)     
        print(payload_data)

        response = urequests.post(
                                             url     = url_to_post_to,
                                             data    = payload_data,
                                             headers = headers_to_send,
                                    )
        
        print("HTML status code : " + str(response.status_code) + " " + html_status_code_dictionary['influx_db'][response.status_code]) #(response.data())
        response.close()
        
        payload_data = ''
        time.sleep(1)


except:
        
    while True:
        time.sleep_ms(1000)

        # MEASURE
        bme688.measureData()
        
        temperature = bme688.readTemperature()
        pressure    = bme688.readPressure()
        humidity    = bme688.readHumidity()
        gas_res     = bme688.readGasRes()
        IAQ         = bme688.getAirQualityScore()
        co2         = bme688.readeCO2()
        print(temperature, pressure, humidity, gas_res, IAQ, co2)
        
        
        # DISPLAY
        display.text('Temperature C', 0, 0, 1)
        display.large_text("{:.2f}".format(temperature), 0, 10,2, 1)
        
        display.text('Humidity %RH', 0, 30, 1)
        display.large_text("{:.2f}".format(humidity), 0, 40,2, 1)
        
        display.text('Gas kOmega', 0, 60, 1)
        display.large_text("{:.2f}".format(IAQ), 0, 70,2, 1)
        
        display.text('eCO2 in ppm', 0, 90, 1)
        display.large_text("{:.2f}".format(co2), 0, 100,2, 1)
        
        display.show()
        
        time.sleep(1)




