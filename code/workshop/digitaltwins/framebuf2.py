""" framebuf2.py

# ORIGINAL HEADER:
    # this code is distributed under the MIT licence.
    # frambuf2 v008
    # (c) 2022 Peter Lumb (peter-l5) 

# LICENSE:
    MIT License

    Copyright (c) 2022 peter-l5

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

"""
import framebuf

# constants available in MicroPython 1.19.1
MONO_HMSB=framebuf.MONO_HMSB
MONO_VLSB=framebuf.MONO_VLSB
MONO_HMSB=framebuf.MONO_HMSB
RGB565=framebuf.RGB565
GS2_HMSB=framebuf.GS2_HMSB
GS4_HMSB=framebuf.GS4_HMSB
GS8=framebuf.GS8

class FrameBuffer(framebuf.FrameBuffer):

    def large_text(self, s, x, y, m, c=1):
        smallbuffer=bytearray(8)
        letter=framebuf.FrameBuffer(smallbuffer,8,8,framebuf.MONO_HMSB)
        for character in s:
            letter.fill(0)
            letter.text(character,0,0,1)
            for i in range (0, 8):
                for j in range (0, 8):
                    if letter.pixel(i, j) == 1:
                        for k in range (x+i*m, x+(i+1)*m):
                            for l in range (y+j*m, y+(j+1)*m):
                                self.pixel(k, l, c)
            x=x+8*m
