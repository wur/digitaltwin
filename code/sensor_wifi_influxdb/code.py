# SPDX-FileCopyrightText: 2021 Liz Clark for Adafruit Industries
#
# SPDX-License-Identifier: MIT

"""Adapted from the Adafruit_CircuitPython_ESP32SPI
library example esp32spi_simpletest.py:
https://github.com/adafruit/Adafruit_CircuitPython_ESP32SPI/
blob/master/examples/esp32spi_simpletest.py """

import board
import busio
from digitalio import DigitalInOut
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from adafruit_esp32spi import adafruit_esp32spi
import adafruit_requests as requests
from adafruit_io.adafruit_io import IO_HTTP, AdafruitIO_RequestError

# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

# Circuitpython 7.2.5, libraries needed: adafruit_bme680.mpy adafruit_bus_device
# only thing added to work is the "address=0x76" in the i2c declaration
# pin 4 SDA, pin 5 SCL, 3.3V, GND

import time
import board
import adafruit_bme680

# Create sensor object, communicating over the board's default I2C bus
i2c = board.I2C()  # uses board.SCL and board.SDA
bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, address=0x76, debug=False)

# change this to match the location's pressure (hPa) at sea level
bme680.sea_level_pressure = 1013.25

# You will usually have to add an offset to account for the temperature of
# the sensor. This is usually around 5 degrees but varies by use. Use a
# separate temperature sensor to calibrate this one.
temperature_offset = -5

# Get wifi details and more from a secrets.py file
try:
    from secrets import secrets
except ImportError:
    print("WiFi secrets are kept in secrets.py, please add them there!")
    raise

#  ESP32 pins
esp32_cs = DigitalInOut(board.CS1)
esp32_ready = DigitalInOut(board.ESP_BUSY)
esp32_reset = DigitalInOut(board.ESP_RESET)

#  uses the secondary SPI connected through the ESP32
spi = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)

print('-' * 40)
if esp.status == adafruit_esp32spi.WL_IDLE_STATUS:
    print("ESP32 found and in idle mode")

print('-' * 40)
print("Firmware vers.", esp.firmware_version)
print("MAC addr:", [hex(i) for i in esp.MAC_address])

while True:
    if not esp.is_connected:
        print("Connecting to AP...")

    while not esp.is_connected:
        try:
            esp.connect_AP(secrets["ssid"], secrets["ssid_password"])
            print("Connected to", str(esp.ssid, "utf-8"), "\tRSSI:", esp.rssi)
        except RuntimeError as e:
            print("could not connect to AP, retrying: ", e)
            continue

    # Initialize a requests object with a socket and esp32spi interface
    socket.set_interface(esp)
    requests.set_socket(socket, esp)

    headers = {
        'Authorization': 'Token ' + secrets['influx_token'],
        'Content-Type': 'application/x-www-form-urlencoded',
    }

    temperature = str(round(bme680.temperature + temperature_offset, 2))
    gas = str(round(bme680.gas, 2))
    humidity = str(round(bme680.relative_humidity, 2))
    pressure = str(round(bme680.pressure, 2))
    altitude = str(round(bme680.altitude, 2))
    
    data = 'measurement,host=host1 temperature='+temperature+',gas=' + gas + ',humidity=' + humidity + ',pressure=' + pressure + ',altitude=' + altitude

    response = requests.post(secrets['influx_host'] + '/api/v2/write?org='+secrets['influx_org']+'&bucket=' + secrets['influx_bucket'], headers=headers, data=data)
    print('-' * 40)
    print('Status code: %s' % response.__dict__['status_code'])
    print('-' * 40)
    print("Temperature: %0.1f C" % (bme680.temperature + temperature_offset))
    print("Gas: %d ohm" % bme680.gas)
    print("Humidity: %0.1f %%" % bme680.relative_humidity)
    print("Pressure: %0.3f hPa" % bme680.pressure)
    print("Altitude = %0.2f meters" % bme680.altitude)

    time.sleep(1000)
