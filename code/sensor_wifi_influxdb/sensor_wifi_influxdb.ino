#include <SPI.h>
#include <WiFiNINA.h>
#include <ArduinoHttpClient.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_BME680.h"

/***
  Read Our Complete Guide: https://RandomNerdTutorials.com/bme680-sensor-arduino-gas-temperature-humidity-pressure/
  
  Designed specifically to work with the Adafruit BME680 Breakout ----> http://www.adafruit.com/products/3660 These sensors use I2C or SPI to communicate, 2 or 4 pins are required to interface. Adafruit invests time and resources providing this open source code, please support Adafruit and open-source hardware by purchasing products from Adafruit! Written by Limor Fried & Kevin Townsend for Adafruit Industries. BSD license, all text above must be included in any redistribution
 ***/

/*#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10*/

#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME680 bme; // I2C
//Adafruit_BME680 bme(BME_CS); // hardware SPI
//Adafruit_BME680 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK);

///////please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = "SSID";     // your network SSID (name)
char pass[] = "PASS";   // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key index number (needed only for WEP)
// WIFI status
int status = WL_IDLE_STATUS;
// InfluxDB v2 server url, e.g. https://eu-central-1-1.aws.cloud2.influxdata.com (Use: InfluxDB UI -> Load Data -> Client Libraries)
char INFLUXDB_URL[] = "192.168.1.110";  // server address
// InfluxDB v2 server port
int INFLUXDB_PORT = 8086;
// InfluxDB v2 server or cloud API token (Use: InfluxDB UI -> Data -> API Tokens -> <select token>)
String INFLUXDB_TOKEN = "45dgOWQdNYRScvUvoG3On0mRG17XOtiGcHYzIeXHZBtYPV4KVSMYDHOPn6Liw0BzYF3e7r5QsUrcty9HPxsN4w==";
// InfluxDB v2 organization id (Use: InfluxDB UI -> User -> About -> Common Ids )
String INFLUXDB_ORG = "DigitalTwin";
// InfluxDB v2 bucket name (Use: InfluxDB UI ->  Data -> Buckets)
String INFLUXDB_BUCKET = "DigitalTwin";

WiFiClient wifi;
HttpClient client = HttpClient(wifi, INFLUXDB_URL, INFLUXDB_PORT);

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);

  while (!Serial);
  Serial.println(F("BME680 async test"));

  if (!bme.begin(0x76)) {
    Serial.println(F("Could not find a valid BME680 sensor, check wiring!"));
    while (1);
  }

  // Set up oversampling and filter initialization
  bme.setTemperatureOversampling(BME680_OS_8X);
  bme.setHumidityOversampling(BME680_OS_2X);
  bme.setPressureOversampling(BME680_OS_4X);
  bme.setIIRFilterSize(BME680_FILTER_SIZE_3);
  bme.setGasHeater(320, 150); // 320*C for 150 ms
  
  
  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }
  
  String fv = WiFi.firmwareVersion();
  Serial.println("Current firmware version: " + fv);
  
  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.println("Connected to wifi");
  printWifiStatus();

  Serial.println("making GET request");
  client.get("/");

  // read the status code and body of the response
  int statusCode = client.responseStatusCode();
  String response = client.responseBody();

  // read the status code and body of the response
  Serial.print("Status code: ");
  Serial.println(statusCode);
  Serial.print("Response: ");
  Serial.println(response);
}

void loop() {
  // Tell BME680 to begin measurement.
  unsigned long endTime = bme.beginReading();
  if (endTime == 0) {
    Serial.println(F("Failed to begin reading :("));
    return;
  }
  
  Serial.print(F("Reading started at "));
  Serial.print(millis());
  Serial.print(F(" and will finish at "));
  Serial.println(endTime);

  Serial.println(F("You can do other work during BME680 measurement."));
  delay(50); // This represents parallel work.
  
  // There's no need to delay() until millis() >= endTime: bme.endReading()
  // takes care of that. It's okay for parallel work to take longer than
  // BME680's measurement time.

  // Obtain measurement results from BME680. Note that this operation isn't
  // instantaneous even if milli() >= endTime due to I2C/SPI latency.
  if (!bme.endReading()) {
    Serial.println(F("Failed to complete reading :("));
    return;
  }
  
  Serial.print(F("Reading completed at "));
  Serial.println(millis());

  Serial.print(F("Temperature = "));
  Serial.print(bme.temperature);
  Serial.println(F(" *C"));

  Serial.print(F("Pressure = "));
  Serial.print(bme.pressure / 100.0);
  Serial.println(F(" hPa"));

  Serial.print(F("Humidity = "));
  Serial.print(bme.humidity);
  Serial.println(F(" %"));

  Serial.print(F("Gas = "));
  Serial.print(bme.gas_resistance / 1000.0);
  Serial.println(F(" KOhms"));

  Serial.print(F("Approx. Altitude = "));
  Serial.print(bme.readAltitude(SEALEVELPRESSURE_HPA));
  Serial.println(F(" m"));

  Serial.println();
  
  if (WiFi.status() == WL_CONNECTED) {
    String path = "/api/v2/write?org=" + INFLUXDB_ORG + "&bucket=" + INFLUXDB_BUCKET;
    String postData = "sensor,sensor_id=BME680 temperature="+String(bme.temperature)+",pressure="+String(bme.pressure)+",humidity="+String(bme.humidity)+",gas_resistance="+String(bme.gas_resistance);

    Serial.println("Begin POST request");    
    
    client.beginRequest();
    client.post(path);
    client.sendHeader("Authorization", "Token " + String(INFLUXDB_TOKEN));
    client.sendHeader("Content-Type", "text/plain; charset=utf-8");
    client.sendHeader("Content-Length", postData.length());
    client.sendHeader("Accept", "application/json");
    client.beginBody();
    client.print(postData);
    client.endRequest();
    
    Serial.println("End POST request");

    // read the status code and body of the response
    int statusCode2 = client.responseStatusCode();
    String response2 = client.responseBody();
  
    Serial.print("Status code2: ");
    Serial.println(statusCode2);
    Serial.print("Response2: ");
    Serial.println(response2);
  
    Serial.println("Wait some seconds");
    
    delay(1000);
  } else {
    Serial.println("No WIFI connection");
  }

  delay(10000);
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
