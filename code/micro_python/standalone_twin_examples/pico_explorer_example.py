""" pico_explorer_example.py

Example code for running BME280 and SGP30 breakouts using a Pimoroni Pico Explorer Base.

"""
# 0.  Import Packages
from pimoroni_i2c import PimoroniI2C
from breakout_sgp30 import BreakoutSGP30
from breakout_bme280 import BreakoutBME280
from picographics import PicoGraphics, DISPLAY_PICO_EXPLORER
import time

# 1.  Set-up Hardware
# 1.1 Define i2c pins
PINS_PICO_EXPLORER = {"sda": 20, "scl": 21} # Default i2c pins for Pico Exp.

# 1.2 Set-up display
display            = PicoGraphics(display=DISPLAY_PICO_EXPLORER)

# constants for drawing
WIDTH, HEIGHT      = display.get_bounds()

# define pen colors 
white              = display.create_pen(255, 255, 255)
black              = display.create_pen(0, 0, 0)
red                = display.create_pen(255, 0, 0)
grey               = display.create_pen(125, 125, 125)

# make a simple rectangle
display.set_pen(black)
display.rectangle(1, 1, 100, 25)

# 1.4 Start Sensor
# set i2c pins
i2c               = PimoroniI2C(**PINS_PICO_EXPLORER)
bme               = BreakoutBME280(i2c)
sgp30             = BreakoutSGP30(i2c)
print("SGP30 initialised - about to start measuring without waiting")


# 2.  Functions
# 2.1 Show data
# Positional values for each measurement in the format:
# variable_to_display  = [[position of var. name text],[position of var. value text],var_name,unit, decimals]

CO2_display         = [[10,  20, 240, 3], [10, 40, 240, 5], 'CO2'          , ' ppm', 0]
Temperature_display = [[10,  85, 240, 3], [10,105, 240, 5], 'Temperature'  , ' C'  , 2]
Humidity_display    = [[10, 150, 240, 3], [10,170, 240, 5], 'Rel. Humidity', ' %'  , 2]

def show_data(variable_name, variable, variable_unit, position, decimal_places = 0):
    """ Display data
    
    # Requirements:
        import picoexplorer as display
    
    """ 
    # 1. Display variable name
    display.text(variable_name,
                 position[0][0], position[0][1],
                 position[0][2], position[0][3])
    
    #2. Display variable with the ability to modify the number of decimal places
    display.text('{:.{dec}f}'.format(variable, dec = decimal_places) + variable_unit, 
                position[1][0], position[1][1],
                position[1][2], position[1][3])
     

# 3. Measure
sgp30.start_measurement(False)
id                = sgp30.get_unique_id()
print("Started measuring for id 0x", '{:04x}'.format(id[0]), '{:04x}'.format(id[1]), '{:04x}'.format(id[2]), sep="")

count = 0
while True:
    count += 1
   
    # get airquality sensor sgp30 data
    air_quality     = sgp30.get_air_quality()
    eCO2            = air_quality[BreakoutSGP30.ECO2]
    TVOC            = air_quality[BreakoutSGP30.TVOC]

    air_quality_raw = sgp30.get_air_quality_raw()
    H2              = air_quality_raw[BreakoutSGP30.H2]
    ETHANOL         = air_quality_raw[BreakoutSGP30.ETHANOL]

    # get temperature sensor bme280 data
    temperature, pressure, humidity = bme.read()
    
    # pressure comes in pascals convert to the more manageable hPa
    pressurehpa = pressure / 100
    
    if count == 30:
        print("Resetting device")
        sgp30.soft_reset()
        time.sleep(0.5)
        print("Restarting measurement, waiting 15 secs before returning")
        sgp30.start_measurement(True)
        print("Measurement restarted, now read every second")
    
    # change pen back to black, so when it clears it resets to black
    display.set_pen(black)
    display.clear()

    # drawing the LCD text
    display.set_pen(white)
    
    # add the temperature text
    show_data(Temperature_display[2],temperature, Temperature_display[3],Temperature_display,Temperature_display[4])
    
    # add the CO2 text
    show_data(CO2_display[2],eCO2, CO2_display[3],CO2_display,CO2_display[4])
    
    # add the humidity text
    show_data(Humidity_display[2],humidity, Humidity_display[3],Humidity_display,Humidity_display[4])

    # time to update the display
    display.update()
    
    # wait one second before restarting loop
    time.sleep(1.0)
