""" mqtt_functions.py


"""
# 0. PACKAGES
import time
import machine
from umqtt.simple import MQTTClient


# 1. FUNCTIONS
def mqtt_connect(client_id, mqtt_server,
                 keep_alive_seconds = 3600,
                 subscribe = False):
    """ Connect to MQTT broker
    
    # Input:
        client_id          = 
        mqtt_server        =
        keep_alive_seconds = option for keeping the connection live,
                             default 1 hr : 3600 (in seconds)
        subscribe          = option for adding subscription
        
    # Output:
        client = client object
        
    # Packages:
        from umqtt.simple import MQTTClient
        
    """    
    # 1. CODE
    # 1.1 create client object
    client = MQTTClient(client_id, mqtt_server, keepalive= keep_alive_seconds)
    
    # 1.2 add subscription
    if subscribe is True:
        client.set_callback(message_listener)
        
    # 1.3 connect to MQTT broker
    client.connect()
    
    # 1.3 Print conenction status
    print('Connected to %s MQTT Broker' %(mqtt_server))
    
    # 1.4 return object
    return client


def reconnect(seconds_to_wait = 10):
    """ reset Pico W
    
    # Input:
        seconds_to_wait = number of seconds to wait between print statement
                         and machine reset so as to be able to catch it,
                         default : 10 (in seconds)
                         
    # Output:
        None, reset machine
        
    # Packages:
        import time
        import machine
    
    """    
    # 1. CODE
    # 1.1 Message to REPL
    print(' Failed to connect to the MQTT Broker. Attempting to reconnect...')
    
    # 1.2 sleep - wait
    time.sleep(seconds_to_wait)
    
    # 1.3 Reset machine
    machine.reset()
    
    
def mqtt_message(client_to_message, topic_to_publish_to, topic_message,
                 seconds_to_wait = 10):
    """ Send MQTT Broker a message
    
    # Input:
          client_to_message    = client object to message
          topic_to_publish_to  = MQTT Topic, adding b to a string (eg., b'STRING') converts it to bytes
          topic_message        = MQTT Topic message, adding b to a string (eg., b'STRING') converts it to bytes
          seconds_to_wait      = number of seconds to wait between print statement
                                 and machine reset so as to be able to catch it,
                                 default : 10 (in seconds)
                         
    # Output:
        None, sends message
        
    # Packages:
        import time
        from umqtt.simple import MQTTClient
    
    """
    # 1. Code
    # 1.1 send client a message
    client_to_message.publish(topic_to_publish_to, topic_message)
    
    # 1.2  sleep - wait
    time.sleep(seconds_to_wait)


def mqtt_subscribe(client_to_listen_to, topic_subscribed_to,
                   seconds_to_wait = 10):
    """ Receive a MQTT Broker message
    
    # Input:
          client_to_listen_to  = client object to receive messages from
          topic_subscribed_to  = MQTT Topic to receive messages about
          seconds_to_wait      = number of seconds to wait between print statement
                                 and machine reset so as to be able to catch it,
                                 default : 10 (in seconds)
                         
    # Output:
        None
        
    # Packages:
        import time
        from umqtt.simple import MQTTClient
    
    """
    # 1. Code
    # 1.1Receive message status
    print("Subscribing to topic %s" %(topic_subscribed_to))
    
    # 1.2 Subscribe
    client_to_listen_to.subscribe(topic_subscribed_to)
    
    # 1.3  sleep - wait
    time.sleep(seconds_to_wait)
    
    # 1.4 Message
    print("Subscribed to topic %s" %(topic_subscribed_to))


def message_listener(topic_subscribed_to, message):
    """ Listen and react to MQTT Broker messages
    
    # Input:
          topic_subscribed_to  = MQTT Broker topic
          message              = message of the topic from the broker
                         
    # Output:
        topic_message          = MQTT Topic message
        
    # Packages:
        import time
        from umqtt.simple import MQTTClient
    
    """
    # 1. Code
    # 1.1 Note which topic the message is from
    print("New message received on topic {}".format(topic_subscribed_to.decode('utf-8')))
    
    # 1.2 Get message
    message = message.decode('utf-8')
    
    # 1.3 Give message
    print(message)
    
    # 1.4 React
    
    ###################
    #                 #
    #  ADD CODE HERE  #
    #      TO DO      #
    #    something    #
    #                 #
    ###################
    
    # 1.5  sleep - wait
    time.sleep(10)
