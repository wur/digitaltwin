""" test_MQTT_subscribe.py


"""
# 0. PACKAGES
import network
import time
from machine import Pin
from umqtt.simple import MQTTClient
from secrets import secrets
#from mqtt_functions import mqtt_connect, reconnect
import mqtt_functions

# 0.a Variables
client_id = 'test_picoW'

# 1.  NETWORK
# 1a. Connect
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(secrets['ssid'], secrets['ssid_password'])

# 1b. Check status
time.sleep(10)
print('Connection status: ' + str(wlan.isconnected()))

# 2. CONNECT TO BROKER
client = mqtt_functions.mqtt_connect(client_id, secrets['mqtt_broker'], subscribe = True)
mqtt_functions.mqtt_subscribe(client,'sensors/garden')

while True:
    client.check_msg()
    time.sleep(30)

