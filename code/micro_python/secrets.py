# secrets.py
""" secrets.py: File for secret settings

    CONTAINS:
        secrets = dictionary with WIFI, Timezone, and Tokens

"""

secrets = {
    
            # WIFI
            # SSID + Password
            'ssid'                 : 'ACCESS POINT NAME',
            'ssid_password'        : 'AP PASSWORD',

            },
