#include <SPI.h>
#include <WiFiNINA.h>
#include <ArduinoHttpClient.h>

///////please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = "WIFINAME";     // your network SSID (name)
char pass[] = "WIFIPASSWORD!";   // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key index number (needed only for WEP)
// WIFI status
int status = WL_IDLE_STATUS;
// InfluxDB v2 server url, e.g. https://eu-central-1-1.aws.cloud2.influxdata.com (Use: InfluxDB UI -> Load Data -> Client Libraries)
char INFLUXDB_URL[] = "192.168.1.110";  // server address
// InfluxDB v2 server port
int INFLUXDB_PORT = 8086;
// InfluxDB v2 server or cloud API token (Use: InfluxDB UI -> Data -> API Tokens -> <select token>)
String INFLUXDB_TOKEN = "45dgOWQdNYRScvUvoG3On0mRG17XOtiGcHYzIeXHZBtYPV4KVSMYDHOPn6Liw0BzYF3e7r5QsUrcty9HPxsN4w==";
// InfluxDB v2 organization id (Use: InfluxDB UI -> User -> About -> Common Ids )
String INFLUXDB_ORG = "DigitalTwin";
// InfluxDB v2 bucket name (Use: InfluxDB UI ->  Data -> Buckets)
String INFLUXDB_BUCKET = "DigitalTwin";

WiFiClient wifi;
HttpClient client = HttpClient(wifi, INFLUXDB_URL, INFLUXDB_PORT);

void setup() {
  
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  
  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }
  
  String fv = WiFi.firmwareVersion();
  Serial.println("Current firmware version: " + fv);
  
  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.println("Connected to wifi");
  printWifiStatus();

  Serial.println("making GET request");
  client.get("/");

  // read the status code and body of the response
  int statusCode = client.responseStatusCode();
  String response = client.responseBody();

  // read the status code and body of the response
  Serial.print("Status code: ");
  Serial.println(statusCode);
  Serial.print("Response: ");
  Serial.println(response);
}

void loop() {
  if (WiFi.status() == WL_CONNECTED) {
    String path = "/api/v2/write?org=" + INFLUXDB_ORG + "&bucket=" + INFLUXDB_BUCKET;
    String postData = "airSensors,sensor_id=TLM0201 temperature=73.97038159354763,humidity=35.23103248356096,co=0.48445310567793615";

    Serial.println("Begin POST request");    
    
    client.beginRequest();
    client.post(path);
    client.sendHeader("Authorization", "Token " + String(INFLUXDB_TOKEN));
    client.sendHeader("Content-Type", "text/plain; charset=utf-8");
    client.sendHeader("Content-Length", postData.length());
    client.sendHeader("Accept", "application/json");
    client.beginBody();
    client.print(postData);
    client.endRequest();
    
    Serial.println("End POST request");

    // read the status code and body of the response
    int statusCode2 = client.responseStatusCode();
    String response2 = client.responseBody();
  
    Serial.print("Status code2: ");
    Serial.println(statusCode2);
    Serial.print("Response2: ");
    Serial.println(response2);
  
    Serial.println("Wait some seconds");
    
    delay(1000);
  } else {
    Serial.println("No WIFI connection");
  }

  delay(10000);
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
