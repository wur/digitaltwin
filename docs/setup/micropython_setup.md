# MicroPython
## What is MicroPython?

### Python 
The Python programming language is described as a **high-level**, **interpreted**, **general-purpose** programming language. Let's break that down. High level refers to the degree of abstraction of a programming language from machine code, machine code being a low level programming language that may not be substantially different from the processor's commands. High level languages may utilise natural language, i.e., the language we as humans speak to one another, whilst hiding or automating the machine components (e.g., storage) allowing a user to focus on 'abstract' concepts (e.g., variables, loops). Interpreter's are those programmes that can directly execute the written instructions without converting, or compiling, into a machine language. Finally, a general-purpose programming language is one that can be used for multiple domains (= target, activity, or goal) as opposed to a domain-specific programming language. Python therefore allows a user to write commands in a natural language, these commmands are directly executed, and such commands can have a range of goals (e.g., a series of commands can sort a database for you and be programmed to email you the results). Python can be [downloaded directly](https://www.python.org/) or as part of a software distribution (e.g., [anaconda](https://www.anaconda.com/), [miniconda](https://docs.conda.io/en/latest/miniconda.html), etc.). <br>


### MicroPython 
Python is however too large for **microcontrollers** - small computers on integrated circuits that can be used in embedded systems to interact with other types of hardware - this is where [MicroPython](https://micropython.org/) comes in. [MicroPython](https://en.wikipedia.org/wiki/MicroPython) began life as a [crowd funding campaign](https://www.kickstarter.com/projects/214379695/micro-python-python-for-microcontrollers) in 2013 and was released in 2014. MicroPython is an implementation of Python 3 that has been designed and optimised to run on microcontrollers (the [official site](https://micropython.org/) references 256Kb of code space and 16Kb of RAM). MicroPython acting as the microcontrollers **firmware** that is flashed on to the writeable memory. Yet MicroPython will be still familiar to and contain many of the packages used by 'traditional' Python users. In fact, whilst there are some differences the implementation allows Python users and scripts written in Python to be transferred relatively easy (with moderate tweeks). It is for these reasons that MicroPython has been chosen as one of the programming languages of choice for this Project. <br>


## Installing MicroPython
An important note, first and foremost this is a general guide and it is therefore **always prudent to check the documentation of the board you are using**. For the boards below links are provided to the relevant installation guides and it is always worthwhile checking those in case there is any updates. Regardless of the how, installing MicroPython will require you to first download a firmware '.uf2' file either from [Micropython.org](https://micropython.org/download/) or if applicable the board's vendor. As some of our examples use several Pimoroni sensors installing Pimoroni's MicroPython release can be time saving especially as it incorporates various libraries. It is available via [Pimoroni's GitHub](https://github.com/pimoroni/pimoroni-pico/releases) repository. <br>


### Installing: Raspberry Pico
The steps to install Micropython onto a Raspberry Pico can be found [here](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html). However, the process is relatively straightforward. First ensure that the Pico is disconnected from your computer by removing either end of the USB. Press and hold ‘BOOTSEL’, and whilst continuing to hold ‘BOOTSEL’ connect the USB of the Pico with a computer. The Pico should appear as a new drive called ‘RPI-RP2’, download the MicroPython firmware image and copy the download to the ‘RPI-RP2’ drive. The Pico will now restart and the MicroPython version you dragged and dropped installed. 
<br><br>


```{admonition} Click here to learn how to install on an Arduino!
:class: tip, dropdown
**Installing: Arduino Nano RP2040 Connect**<br>

The steps to install [Micropython](https://micropython.org/download/ARDUINO_NANO_RP2040_CONNECT/) onto a Arduino Nano RP2040 Connect can be found [here](https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-python-api). However, the process is relatively straightforward. First connect the Arduino from your computer via a USB capable. Double tap the 'RESET' button.The Arduino should appear as a new mass storage drive called ‘RPI-RP2’, download the MicroPython firmware [image](https://micropython.org/download/ARDUINO_NANO_RP2040_CONNECT/) and copy the download to the ‘RPI-RP2’ drive. The Arduino will now restart and the MicroPython version you dragged and dropped installed. If double tapping the reset button doesn't open the mass storage an alternative method (outlined [here](https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-python-api)) involves placing a jumper cable between the REC and GND pins on the board, then press the 'RESET' button. Removing the jumper cable once the board show's up as a mass storage drive can sometimes help the writing process. 
```

## Programming
### IDE
Once the firmware is installed it is time to start writing code for your microcontroller to perform some actions, then deploy it to your microcontroller, and eventually debug your code when problems arise. It is possible to do these steps with different programmes however, for ease an integrated development environment (IDE) can be used to gather together all of the necessary tools you will need into one single environment. <br>


```{figure} ../../images/python_Micropython.jpg
---

name: python_Micropython-fig
---
The Thonny IDE: (A) Toolbar; File window for (B) the computer and (D) the microcontroller. On the microcontroller (C) it is possible to have folders and files. (E) Open file toolbar; (F) Scripting area; (G) Shell environment; (H) Microcontroller version; and, (I) the Interpreter.  
```

Thonny and Mu are open-source IDE's that can be installed on Windows, Linux, Mac OS, and Raspberry Pi OS that are useful for writing and uploading MicroPython (and CircuitPython) to single board microcontrollers (such as a micro:bit, Raspberry Pico, or Arduino Nano). The Thonny IDE can be downloaded at [thonny.org](https://thonny.org/) whilst the MU IDE can be downloaded at [codewith.mu](https://codewith.mu/). These IDEs have the customary programming interfaces. For instance a toolbar used for quick access to functions (A in {numref}`python_Micropython-fig`), icons include running, stopping, saving the current script, and (B and D in {numref}python_Micropython-fig) file directory views. 


### Hello World!
Opening the Thonny IDE and connecting the microcontroller to the computer allows us to write programmes. Let us begin by typing the following into the shell or read–eval–print loop (REPL) environment (G and H in {numref}python_Micropython-fig). The REPL accepts discrete inputs from the user (*read*), performs them (*eval* or *evaluate*), and then outputs the result (*print*). Enter the following `print`command:

```{code-block} python
---
lineno-start: 1
---
print("hello world!")
```

If everything is working then the message *hello world!* should appear below the executed command:

```{figure} ../../images/python_hello_world.jpg
---

name: python_hello_world-fig
---
The read–eval–print loop (REPL) environment in use. 
```

### Writing code
The main area is where programmes can be written, saved, and then executed (E and F in {numref}`python_Micropython-fig`). When writing such programmes consider that **maintainability**, **dependibility**, **efficiency**, and **usability** are key. Make your code concise, compact, simple, through modularity (i.e., a function does only one thing) to make your code re-useable, adaptible, and extenable. For maintainability give variables descriptive names rather than generic, ambigious names (e.g., a1, b2). Likewise, do not *hard code* - the process of embedding data into the code - *literal values* - exact alphanumeric values - as this reduces it's portability. Comments and documentation can help fellow developers and users understand the purpose and rationale behind specific lines of code which is useful for debugging and maintaining code. These can be written as single (`#`):

```{code-block} python
---
lineno-start: 1
---
# I am a single line comment

```

Or multi-line between two sets of (`"""`) (`"""`):

```{code-block} python
---
lineno-start: 1
---
""" Multi-line comment

    I am a multi-line comment

"""

```

The [*PEP-8*](https://peps.python.org/pep-0008/) style guide offers help with style and formatting, such as naming variables, whitespace, capitals, underscores, etc. .<br>


### Running the code
To write a programme to the Pico (Arduino Nano, or another microcontroller) and get it to run independently on your computer, connect the USB of the board to a computer (you do not need to hold down the BOOTSEL or RESET), write your programme, and save your programme as main.py to the board. Unplug from the computer and connect to an external power source and the programme should run. <br>


### Interpreter
```{figure} ../../images/python_thonny_configuration.jpg
---

name: python_thonny_configuration-fig
---
Configuration. The IDE allows you to configure the (A) interpreter and (B) the communication port.
```

At the bottom right of the current interpreter is displayed, clicking this opens a menu where modifications to the current interpreter and port can be made ({numref}python_thonny_configuration-fig). 


