# CircuitPython

## Python and MicroPython

### Python 

The Python programming language is described as a **high-level**, **interpreted**, **general-purpose** programming language. Let's break that down. High level refers to the degree of abstraction of a programming language from machine code, machine code being a low level programming language that may not be substantially different from the processor's commands. High level languages may utilise natural language, i.e., the language we as humans speak to one another, whilst hiding or automating the machine components (e.g., storage) allowing a user to focus on 'abstract' concepts (e.g., variables, loops). Interpreter's are those programmes that can directly execute the written instructions without converting, or compiling, into a machine language. Finally, a general-purpose programming language is one that can be used for multiple domains (= target, activity, or goal) as opposed to a domain-specific programming language. Python therefore allows a user to write commands in a natural language, these commmands are directly executed, and such commands can have a range of goals (e.g., a series of commands can sort a database for you and be programmed to email you the results). Python can be [downloaded directly](https://www.python.org/) or as part of a software distribution (e.g., [anaconda](https://www.anaconda.com/), [miniconda](https://docs.conda.io/en/latest/miniconda.html), etc.). 
<br><br>


### MicroPython 
Python is however too large for **microcontrollers** - small computers on integrated circuits that can be used in embedded systems to interact with other types of hardware - this is where [MicroPython](https://micropython.org/) comes in. [MicroPython](https://en.wikipedia.org/wiki/MicroPython) began life as a [crowd funding campaign](https://www.kickstarter.com/projects/214379695/micro-python-python-for-microcontrollers) in 2013 and was released in 2014. MicroPython is an implementation of Python 3 that has been designed and optimised to run on microcontrollers (the [official site](https://micropython.org/) references 256Kb of code space and 16Kb of RAM). MicroPython acting as the microcontrollers **firmware** that is flashed on to the writeable memory. Yet MicroPython will be still familiar to and contain many of the packages used by 'traditional' Python users. In fact, whilst there are some differences the implementation allows Python users and scripts written in Python to be transferred relatively easy (with moderate tweeks). <br><br>


## What is CircuitPython?

[CircuitPython](https://circuitpython.org/) is a code **fork** - the independent development of a copy of a source code - of [MicroPython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html) that is kept up-to-date with the latest release of the original [forked code](https://micropython.org/download/)). CircuitPython, like MicroPython, is designed for Microcontrollers fitting the useability and readability of programming language like Python onto a tiny, resource-lite, chip. However, Circuitpython expands Micropython by [focussing](https://www.youtube.com/watch?v=KoBJG3Taq_Y&list=PLjF7R1fz_OOXrI15wuXeESA0aA4VzcWSi) on providing simplicity for the user, being 'beginner friendly', alongside producing a uniform API (Application Programming Interface) so that the code that you write is 'board agnostic'. Not only does Circuitpython have a wealth of [libraries](https://circuitpython.org/libraries) and modules for different sensors and peripherials built into the firmware but the 'installation' of new libraries is as simple as dragging and dropping them into the *lib* folder on the *CIRCUITPY drive*.  
<br><br>


## Installing CircuitPython
An important note, first and foremost this is a general guide and it is therefore **always prudent to check the documentation of the board you are using**. For the boards below links are provided to the relevant installation guides and it is always worthwhile checking those in case there is any updates. Regardless of the how, installing CircuitPython will require you to first download a firmware '.uf2' file  from [Circuitpython.org](https://circuitpython.org/). 
<br><br>

### Installing: Arduino Nano RP2040 Connect
The steps to install [CircuitPython](https://circuitpython.org/board/arduino_nano_rp2040_connect/) onto a Arduino Nano RP2040 Connect can be found [here](https://learn.adafruit.com/circuitpython-on-the-arduino-nano-rp2040-connect). However, the process is relatively straightforward. First connect the Arduino from your computer via a USB capable. Double tap the 'RESET' button. The Arduino should appear as a new mass storage drive called ‘RPI-RP2’, download the CircuitPython firmware [image](https://circuitpython.org/board/arduino_nano_rp2040_connect/) and copy the download to the ‘RPI-RP2’ drive. The Arduino will now restart and a new drive called CIRCUITPY will appear with the CircuitPython version you dragged and dropped installed. If double tapping the reset button doesn't open the mass storage an alternative method (outlined [here](https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-python-api)) involves placing a jumper cable between the REC and GND pins on the board, then press the 'RESET' button. Removing the jumper cable once the board show's up as a mass storage drive can sometimes help the writing process. 
<br><br>

```{admonition} Click here to learn how to install on an Raspberry Pico!
:class: tip, dropdown
**Installing: Raspberry Pico**<br>

The steps to install CircuitPython onto a Raspberry Pico can be found [here](https://learn.adafruit.com/getting-started-with-raspberry-pi-pico-circuitpython). However, the process is relatively straightforward. First ensure that the Pico is disconnected from your computer by removing either end of the USB. Press and hold ‘BOOTSEL’, and whilst continuing to hold ‘BOOTSEL’ connect the USB of the Pico with a computer. The Pico should appear as a new drive called ‘RPI-RP2’, download the CircuitPython firmware image and copy the download to the ‘RPI-RP2’ drive. The Pico will now restart and a new drive called CIRCUITPY will appear with the CircuitPython version you dragged and dropped installed. 
```

## Differences between MicroPython and CircuitPython
As a fork CircuitPython has similarities to MicroPython however, it differs in [various aspects](https://docs.circuitpython.org/en/latest/README.html#differences-from-micropython) including a loss of `threading`.

### CIRCUITPY
```{figure} /images/circuitpython_files.png
---
:name: circuitpy_drive-fig
:alt: File structure
:width: 50%
:align: center
---
**Screenshot of the CIRCUITPY drive.**
``` 
Following installation the drive has a new folder setup ({numref}`circuitpy_drive-fig`). Various peripherials and sensors have pre-written libraries including the drivers allowing a user to simply download the `circuitpython` [library](https://circuitpython.org/libraries) and directly copy the needed module into the `libs` folder of the board.
