# Portainer

## What is Portainer?
[Portainer](https://www.portainer.io/) is a graphical user interface (GUI) based universal management system for containers (among others) with four key aims: application deployment; platform management; observability; and, governance and security. As such it is a useful application for managing our Docker and allow us to view and manage the docker containers. 

## Installing Portainer

Having verified that Docker is installed, the next thing is to install [Portainer](https://www.portainer.io/) a  Management System for Docker. With the Raspberry Pi still connected via ssh, type into the terminal:

```{code-block} console
 docker run --restart=always -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```

The addition of `--restart=always` ensures that the portainer will automatically start on reboot and restart on failure.

```{figure} /images/portainer_install.jpg
---

name: portainer_install-fig1
---
**Install Portainer.**

```

## Connect

Once installed and with the Raspberry Pi still connected open an internet browser on your computer and navigate to:

```{code-block} console
 http://raspberrypi:9000/
 ```

First time you will be prompted to create an account, create the account and select a password. 

```{figure} /images/portainer_user.jpg
---

name: portainer_install-fig3
---
**Create account and set password.**

```


 Then select the Docker container environment:


```{figure} /images/portainer_docker.jpg
---

name: portainer_install-fig4
---
**Select Docker.**

```

## Portainer GUI

Once the account is created, the environment selected, the GUI will be displayed. 

```{figure} /images/portainer.jpg
---

name: portainer_install-fig5
---
**Portainer Environment.**

```
