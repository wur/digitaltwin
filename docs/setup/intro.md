# Physical Computing

## Software and Hardware

As Digital Twins are rooted in both the virtual and physical worlds an important first step will be in understanding physical computing. <br><br>

Talk about programming and it is likely one's thoughts go to writing scripts and algorithms or developing programmes, applications, or models. All things that exist in the virtual world that are essentially a set of instructions to make a computer perform a task. These 'instructions' are **software**, a term used to differentiate the physical components which are **hardware** . Software however, is not the only thing we can programme, we can also write programmes for hardware so that we take inputs from *sensors* which can lead to outputs from *actuators* that affect things in the real-world. This is the basis of **Physical Computing** which we can define as programmes that interact with the physical or real-world .
<br><br>

Look around your home, learning environment, or workplace and you will undoubtly encounter electronics that are using a form of physical computing. For instance, toys, thermostats, dishwashers and washing machines all have sensors that convert a physical action (e.g., pressing a button) or phenomenon (e.g., temperature) into an electronic signal and actuators that convert an electronic signal into a physical action. Between them sits a **Microcontroller** that acts as the 'brains' of the device, interpreting the signal coming from the sensors and responding with the actuators. Microcontrollers are tiny, resource-lite, computers where you store the programmes you write that giving meaning to the sensor data and define appropriate action for the actuators.
<br><br>

In the following section a description of the Hardware (e.g., Microcontrollers) and Software (e.g., Circuitpython) used in the various projects will be outlined:

- [Microcontollers](https://wur.gitlab.io/digitaltwin/docs/setup/microcontrollers.html);
- [Raspberry Pi](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html);
- [Computer Vision](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_camera.html);
- [Micropython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html);
- [Circuitpython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html);
- [Docker](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation.html);

<br><br>
