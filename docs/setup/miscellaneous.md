# Miscellaneous

## Docker

### What is Docker?


## Setting up Docker

To install Docker on your Raspberry Pi, you will need to go through the following steps:

- Update your system
- Install docker
- Allow non-root users to execute docker commands
- Verify installation
- Run the hello-world container

### Update your system
After login you can start updating and upgrading the system to the latest security patch. The update command will synchronise the package repositories so that the system can check if any new packages are available. The upgrade command will update any packages of which a newer version is available.

```
sudo apt-get update && sudo apt-get upgrade
```

### Install docker
Conveniently a script has been developed that makes the process of installing docker considerably easy. The first step is to download the install file:

```
curl -fsSL https://get.docker.com -o get-docker.sh
```

Then enter the following command to execute the installation file:

```
sudo sh get-docker.sh
```

This command may request the root password and will then install the required packages for your Raspbian Linux distribution.

```{figure} /images/docker_to_install.jpg
---

name: docker_to_install-fig
---

```

### Allow non-root users to execute docker commands
By default only admin users are allowed to run containers. If you are not logged in as root you can use `sudo` to execute any of the docker commands. Another option is to add your user to the Docker group which will allow them to execute docker commands without `sudo`.

The syntax for adding users to the Docker group is:

```
sudo usermod -aG docker [user_name]
```

To add the Pi user (the default user in Raspbian), use the command:
```
sudo usermod -aG docker Pi
```
For the changes to take place, you need to log out and then back in.

### Verify installation
To verify if docker was installed correctly and you managed to add your user to the docker group execute the following command:

```
docker version
```
If all was correct you should be able to see the Docker version along with some additional information.

### Run the hello-world container
A final test is to actually start a docker container. To do so we will use the hello-world container made by docker.

```
docker run hello-world
```

This should return a welcome message.

```
Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```


## Portainer

Having verified that Docker is installed, the next thing is to install [Portainer](https://www.portainer.io/) a Universal Management System for Docker (among others). This will allow you to view and manage the docker containers. With the Raspberry Pi still connected via ssh, type into the terminal:

```{code-block} console
 sudo docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```

Once installed and with the Raspberry Pi still connected open an internet browser on your computer and navigate to:

```{code-block} console
 http://raspberrypi:9000/
 ```

 First time you will be prompted to create an account, create the account and select a password. 
