# Raspberry Pi

## What is a Raspberry pi?

The Raspberry Pi is a **single board computer** (SBC), aptly named because it is a computer constructed on a single printed circuit board. The Raspberry Pico and Arduino Nano that we are using are **microcontrollers**. The difference between them is that microcontrollers are small computers on integrated circuits that can be used in embedded systems to interact with other types of hardware. Microcontrollers have low resources in terms of both CPU or RAM, and do not have a operating system (OS). Despite this Microcontrollers can perform real-time, fine grained processing for specific tasks. In comparison, despite their small size SBC's are what we would generally associate with a computer. Single board computers have an operating system which allows for them to have a much more general purpose.


Depending upon the model of Raspberry Pi the board has the following components: GPIO Pins; USB, HDMI, Display, and Camera ports; SD Card Reader; as well as CPU, GPU, and RAM. For our purposes we will be using a Raspberry Pi with wireless internet (e.g., Raspberry Pi Zero W; Raspberry Pi Zero 2 W; Raspberry Pi 400; Raspberry Pi 4 Model B; etc.).


## Preparing the Raspberry Pi

To get started with the Raspberry Pi you will need a power supply (e.g., a USB and USB-power adaptor), a separate computer, and an SD card. The SD card is necessary as it will be both where the Raspberry Pi's operating system (OS) is installed and act as a file storage. 


## Raspberry Pi OS

The first thing to do is to install the Raspberry Pi's Operating System onto the SD card. This is where the other computer becomes necessary whilst you can purchase SD cards with pre-installed OS the fastest and easiest approach is to use the [Raspberry Pi Imager](https://www.raspberrypi.com/software/). 

Connect the SD card to the computer. Download, install, and open the Raspberry Pi Imager (from [here](https://www.raspberrypi.com/software/)). The Imager gives you two mandatory choices and the option to specify more advanced settings: 



```{figure} /images/Imager.jpg
---

name: rasp_pi_Imager_figure-fig
---
**Raspberry PI Imager.**
```

First select the choose operating system tab, the main menu has the default Raspberry Pi 32 bit OS however, our application requires the 64 bit operating system install. Select Raspberry PI OS (Operating System) Other from the menu: 

```{figure} /images/raspi_operating_system.jpg
---

name: rasp_pi_Imager_choose_OS_figure-fig
---
**Select the 'Raspberry PI OS Other' Operating System**
```

Then scroll down to the 64-bit OS image:

```{figure} /images/raspi_operating_system2.jpg
---

name: rasp_pi_Imager_choose_OS_figure-fig1
---
**Select the 'Raspberry PI OS (64-bit)' Operating System**
```

Once clicked you will return to the main menu, next select the SD card that you wish to write the OS to:

```{figure} /images/Imager_choose_Storage.jpg
---

name: rasp_pi_Imager_choose_Storage_figure-fig2
---
**Click the SD card**
```

Next click the cog wheel to access the advanced settings. Here we will define the host name, enable Secure Shell Protocol (SSH), set up username and password, and enter the connection information for the WiFi that the Raspberry Pi will connect to. 



```{figure} /images/Imager_choose_Advanced_settings.jpg
---

name: rasp_pi_Imager_Advanced_settings_figure-fig
---
**Click the Cog wheel**
```

## Advanced options: Enabling SSH

Rather than connecting a screen, keyboard, and mouse to our Raspberry Pi we will remotely access the Raspberry Pi. To do so we need to provide some further information to the Imager. Note the following code uses the default hostname `raspberrypi` ({numref}`rasp_pi_Imager_Advanced_settings_figure1-fig` ) you can however, alter the hostname. Altering the hostname requires you to change the hostname here ({numref}`rasp_pi_Imager_Advanced_settings_figure1-fig`).


```{figure} /images/Imager_choose_Advanced_settings1.jpg
---

name: rasp_pi_Imager_Advanced_settings_figure1-fig
---
**Advanced settings.** 
```

As the Raspberry Pi will be set up without a monitor, keyboard, or mouse attached click enable SSH and define both the username and password:


```{figure} /images/Imager_choose_Advanced_settings2.jpg
---

name: rasp_pi_Imager_Advanced_settings_figure2-fig
---
**Enable SSH. Set username and password.** 
```

To ensure that SSH works, i.e., that you can connect, requires giving the imager both the WiFI network name (*SSID*) and password (*key*). Remember to set the wireless LAN country to the correct option:


```{figure} /images/Imager_choose_Advanced_settings3.jpg
---

name: rasp_pi_Imager_Advanced_settings_figure3-fig
---
**Setup WiFi network.**
```

Finally, click save. This should return you to the main screen, with everything done it is time to write to the SD card. Click write.


```{figure} /images/Imager_choose_write.jpg
---

name: rasp_pi_Imager_choose_write_figure-fig
---
**Write in progress.**
```

Once the SD card has been written with the OS Image you need to add an empty file (e.g., a text file) without any extension called *ssh*  to the boot folder. This is the folder that your computer sees when you plug the SD card into it, if the card ejected after writing just plug it back in. Having added this file, eject the SD card from your computer and insert it into the Raspberry Pi's SD card reader and plug in the power supply. If the Raspberry Pi is working the LED should blink when both SD card and power supply are connected.


## Remote access
 To continue setting up the Raspberry Pi requires us to enter a series of commands, check if those commands have worked, and debug if necessary. However, you should notice we have not connected any periperhials (e.g., display, mouse, or keyboard). Raspberry Pi's have the capacity for remote access, as it can be useful when your Raspberry Pi is in an inaccessible location or in our case not connected to any peripherials. However, to [access the Raspberry Pi remotely](https://www.raspberrypi.com/documentation/computers/remote-access.html#introduction-to-remote-access) requires knowing that Raspberry Pi's IP address. Having connected the Raspberry Pi to your WiFi network it will have been assigned an IP addresss, to obtain it you can try a number of different approaches. For instance, connect a display or navigate to your router's IP address (usually printed on them) and search the device list.  Here we will use **multicast DNS** (mDNS). 
 
 Open a terminal on your computer (e.g., Windows Powershell) and ensuring that your Raspberry Pi is turned on, type into your terminal:
 
 ```{code-block} console
ping raspberrypi.local

```
If you altered the host name ({numref}`rasp_pi_Imager_Advanced_settings_figure1-fig`) earlier then replace `raspberrypi` here with that. *It can take up to a few mintues before the Raspberry Pi has finished booting. If ping is unresponsive try giving it a bit longer. After booting you should be able to ping / login to your system.*. If it works you should see 'reply' followed by the IP address and a series of times in ms:

```{figure} /images/ping_Raspberry_pi_connected_modified.jpg
---

name: ping_Raspberry_pi_connected_modified_figure-fig
---
**Ping the Raspberry Pi.**
```

Adding `-4` to `ping -4` will give the IP address in a different format if required. Once the IP address is obtained to connect with the Raspberry Pi enter the following into the terminal, replacing `ip` with the IP address obtained. 

 ```{code-block} console
ssh pi@[ip]

```
You will then be asked whether you want to connect and prompted to give the password ({numref}`rasp_pi_Imager_Advanced_settings_figure2-fig`) you set up for the Raspberry Pi:

```{figure} /images/connected_modified.jpg
---

name: connected_modified_figure-fig
---
**Connect to the Raspberry Pi.**
```
Congratulations your Raspberry Pi is now connected to your computer.

