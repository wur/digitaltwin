# Docker

## What is Docker?
Let us imagine for an instant that two instances of an application have competing needs, *programme instance A* needs certain packages and libraries to be installed but the packages and libraries required conflict with those required by *programme instance B* (e.g., the programmes need different versions of the same package). One solution is to install the separate instances of the programme on two separate machines. A second solution is to perform **hardware virtualisation**, the creation of two virtual machines (VM) on the same machine. **Virtualisation** is the act of creating a virtual version of something.  In this second solution each virtual machine has it's own operating system (OS). Both solutions are either costly (multiple machines) or computationally expensive (multiple OS). A more cost effective solution would be to share the same OS but to isolate each instance from one another this is what [Docker](https://www.docker.com/) does in a nutshell.

[Docker](https://www.docker.com/) is a software platform that [aims](https://www.docker.com/resources/what-container/) to simplify developing, building, managing, running, distributing, and sharing applications through virtulization. Docker virtualisation is virtualisation at the operating-system-level or application-level which allows for multiple but isolated user-spaces to be created, commonly referred to as *Containerization*. Containerization has been [described](https://www.portainer.io/) as offering improved scalability whilst reducing time to market and the *total cost of ownership*. The Docker Host, or Host, is the machine that Docker is installed and running upon, a Docker Container or Container shares the Host's OS kernel but is isolated from the other Containers present on the Host.   
<br><br>

## Setting up Docker

To install Docker on your Raspberry Pi, you will need to go through the following steps:

- Update your system
- Install docker
- Allow non-root users to execute docker commands
- Verify installation
- Run the hello-world container

### Update your system
After login you can start updating and upgrading the system to the latest security patch. The update command will synchronise the package repositories so that the system can check if any new packages are available. The upgrade command will update any packages of which a newer version is available.

```
sudo apt-get update && sudo apt-get upgrade
```

### Install docker
Conveniently a [script](https://get.docker.com/) has been [developed](https://docs.docker.com/engine/install/debian/#install-using-the-convenience-script) that makes the process of installing docker considerably easy on Raspbian OS. The first step is to download the install file:

```
curl -fsSL https://get.docker.com -o get-docker.sh
```

Then enter the following command to execute the installation file:

```
sudo sh get-docker.sh
```

Running the `sudo sh get-docker.sh` command may request the root password and will then install the required packages (e.g., docker *engine*, *compose*) for your Raspbian Linux distribution ( {numref}`docker_to_install-fig` ). Alternatively, adding `Dry_RUN=1` to the [command](https://docs.docker.com/engine/install/debian/#install-using-the-convenience-script) will state what steps the script will execute during installation. 

```
sudo Dry_RUN=1 sh get-docker.sh
```

```{figure} /images/docker_to_install.jpg
---

name: docker_to_install-fig
---

```

### Allow non-root users to execute docker commands

By default only admin users are allowed to run containers. If you are not logged in as root you can use `sudo` to execute any of the docker commands. Another option is to add your user to the Docker group which will allow them to execute docker commands without `sudo`.

The syntax for adding users to the Docker group is:

```{code} terminal
---

---

sudo usermod -aG docker [user_name]
```

To add the `pi` user (the default user in Raspbian), use the command:

```{code} terminal
---

---
sudo usermod -aG docker pi
```

### Reboot
For the changes to take place, you need to log out and then back in via reboot:

```{code} terminal
---

---
sudo reboot
```

### Verify installation
To verify if docker was installed correctly and you managed to add your user to the docker group execute the following command:

```
docker version
```
If all was correct you should be able to see the Docker version along with some additional information.

```{figure} /images/docker_version.jpg
---

name: docker_version-fig
---

```


### Run the hello-world container
A final test is to actually start a docker container. To do so we will use the hello-world container made by docker.

```
docker run hello-world
```

This should return a welcome message.

```
Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```


```{figure} /images/docker_hello_world.jpg
---

name: docker_hello_world-fig
---

```
