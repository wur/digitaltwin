# Computer Vision

## Sensing 
Getting our computer to sense the world involves attaching peripherials to a computer to mimic or exceed human senses. Humans can, to a lesser or greater degree, sense the world with five senses: sight, sound, smell, taste, and touch. These senses allow us to construct both a literal and metaphoric 'picture' of the world. Of these senses mimicing vision, referred to as computer vision, has importance for AI in general as a way to perform object classification, detection, and recognition. For instance, self-driving cars need to recognise the road, detect fellow 'objects' on and around the road, and react to changes in their environment. Applications of computer vision is not just limited to self-driving cars, with applications in healthcare (e.g., cancer detection); agriculture (e.g., plant/animal health); and, society (e.g., idenification via facial recognition) among others. In the following, we will explain how computer's deal with images, define important terminology, and explain how to get started with computer vision using a Raspberry Pi.
<br><br>

## Computer vision
### Images
In computing a digital image can be considered as essentially made up of square cells, pixels, representing the smallest element of a picture on a screen. Each pixel is arranged along rows and columns in a 2D table (array or matrix) with each pixel (or cell of an array) containing numbers. These numbers represent the intensity of colour that can be represented in a pixel. The colour the image displays will depend upon the **bit depth** and any colour **channels**, an attribute of colour space.

The bit depth informs us of how many unique colours or shades an image can have. A bit is actually a portmanteau - blending of two words - of binary digit, a bit is the most basic unit of information having only two values: 0 or 1 (or True or False; Yes or No; etc.). If you have an 1 bit image then it can only have one of two colours, generally considered monochrome the 'colour' information will be represented as 0 or 1 or only black or white. An 8 bit image on the other hand will have one of 256 colours. *How are we calculating this?* If a bit can have two values (0 or 1) then a one bit image can have $ 2^{1} $ which equals 2 colours and an 8 bit image can have $ 2^{8} $ or 256 colours. It is worth noting that as 8 bits equal 1 byte sometimes the amount of information contained within an 8 bit image may be reffered to as a byte. 


In images, colour channels represent the primary colour information of a pixel. For instance, a grayscale image will have one channel with the intensity value being defined by the *bits per channel*. An 8 bit grayscale image will have intensity values ranging from 0 that represents black or the 'darkest gray' to 255 that represents white or the 'lightest gray' with shades of grade between these two end members. A RGB colour image will have three channels with each colour being produced by mixing the different primary colours Red, Green, and Blue (RGB). For an 8 bit RGB image varying the intensity of the different R,G,B channels will give distinct colours: 
<br><br>

$$
\begin{matrix}
    & Red  & Green & Blue  & Black & White \\
    &      &       &       &       &       \\
R   & 255  &   0   & 0     & 0     &  255  \\
G   & 0    &   255 & 0     & 0     &  255  \\
B   & 0    &   0   & 255   & 0     &  255  \\
    &      &       &       &       &
\end{matrix}
$$

 A CYMK digital image, a standard for printing as it is comprised of the colours in an printer ink cartridge is made up of four colour channels representing intensity information of cyan, magenta, yellow, and a key channel typically black. Summing the intensity values of pixel along the different colour channels gives the *bits per pixel*.
<br><br>


### Classification, Localization, Detection, Segmentation
The previous section described how computer's 'see' images, however ideally we don't just want the computer to see interesting images, we want the computer to be able derive meaningful [pieces of information](https://www.ibm.com/topics/computer-vision#:~:text=Computer%20vision%20is%20a%20field,recommendations%20based%20on%20that%20information.) from them. With progress in both machine learning and deep learning computer vision can be used to perform one or more tasks that can be applied to different problems. These tasks involve:

- **Image Classification**. What is, or does, the image contain? A broad description of the dominant 'object' in an image.
- **Image Localization**. Where in an image is a single object located?
- **Image Detection**. Where are the locations of multiple objects in an image?
- **Image Segmentation**. Which pixels belong to which object in an image?
<br><br>


## Rationale
The Raspberry Pi Foundation has released a number of camera modules to give their single board computers computer vision capabilities, starting in 2013 with the first Camera Module that was replaced with an updated second high-spec version in 2016 and a 'high-end' version in 2020. There are a number of tutorials that explain how to deploy the packages necessary to get a Raspberry Pi to perform object classification and detection (for instance, see Tensorflow's official [YouTube channel](https://www.youtube.com/watch?v=mNjXEybFn98)). However, almost exclusively these tutorials run code that displays the output of the script in a window, requiring you to see what the code sees, which can be problematic with a headless setup. Alternatives, such as opening a virtual desktop (e.g., VNC viewer) don't really solve this problem. In the following tutorial the aims are to:
- set-up the Camera headless
- install the packages necessary for object detection
- stream the video 
<br><br>


## Installing
Power up your [set-up Raspberry Pi](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html) and using another machine on the same network as was used in the Pi's set-up (see [here](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html#advanced-options-enabling-ssh)), open a terminal. Enter the following command, replacing *hostname* with the hostname you [assigned](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html#advanced-options-enabling-ssh) the Raspberry Pi (the default being raspberrypi):

```{code-block} python


ping <hostname>.local


```

If you are on the same network you should receive a response that starts with *Pinging hostname.local **IP** with 32 bytes of data* that gives you the Raspberry Pi's IP address. If you already have the IP address you can skip that step. Next enter the following replacing *username* with the username you [assigned](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html#advanced-options-enabling-ssh) the Raspberry Pi (the default being pi):

```{code-block} python


ssh <username>@[ip]


```

If this is the first time connecting you will receive a message about the authenticity of the host and ECDSA key fingerprint. Confirm this. You will then be prompted to give the password [assigned](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html#advanced-options-enabling-ssh) to the username. 

````{margin}
```{note}
**Linux commands** <br>
cd = change directory to home <br>
cd *dir* = change directory to directory indicated by *dir* <br>
echo = displays lines of text or string which are passed as arguments on the command line <br>
ls = list all files and directories in working directory <br>
nano = simple terminal text editor <br>
nano *filename* = opens *filename* in simple terminal text editor if the file exists, if not the file is created in the current working directory <br>
pip3 = pip3 is the package manager of python 3 <br>
sh = invokes the default shell and uses its syntax and flags <br>
sudo = *substitute user do* or *super user do* gives the current user permission to utilise specifici system command at the root level (i.e., root privileges)  <br>
tee = <br>
wget *file* = get *file* <br>

```
````


### Install packages
To begin with let us first update and upgrade the Raspberry Pi OS:

```{code-block} python
---

---

sudo apt update
sudo apt upgrade -y
```


`````{margin}
````{note}
*Debian package* <br>
The tflite can be installed by following the instructions outlined [here](https://github.com/tensorflow/tensorflow/blob/v2.5.0/tensorflow/lite/g3doc/guide/python.md#install-tensorflow-lite-for-python)

```{code-block} python

echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get install python3-tflite-runtime

```
or using the instructions [here](https://pimylifeup.com/raspberry-pi-tensorflow-lite/):

```{code-block} python

echo "deb [signed-by=/usr/share/keyrings/coral-edgetpu-archive-keyring.gpg] https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo tee /usr/share/keyrings/coral-edgetpu-archive-keyring.gpg >/dev/null

```


````
`````

Next install tensorflow lite (see Debian Package Note)

```{code-block} python

python3 -m pip install tflite-runtime
```
Verify the package has been installed by opening Python 3 in the terminal by entering the following command:


```{code-block} python
---

---

python

```

A simple way to test if a package has been installed is to try and import a module or the package. Within the Python environment try and import a package from the tflite_runtime module that was just installed:

```{code-block} python
---

---

from tflite_runtime.interpreter import Interpreter

```

If there is no error then typing the command *exit()* will allow the terminal to leave the python environment. Continuing the installation, the following packages need to be installed by entering the commands:

```{code-block} python
sudo apt-get install libatlas-base-dev
sudo apt-get install libportaudio2
sudo apt-get install libopenjp2-7
sudo apt-get install -y ffmpeg
sudo apt-get install -y libgtk-3-0


sudo apt install git
sudo apt-get install python3-distutils
pip3 install opencv-python
sudo apt-get install openexr
```


`````{margin}
````{note}
Suggested by [Adafruit](https://learn.adafruit.com/running-tensorflow-lite-on-the-raspberry-pi-4/tensorflow-lite-2-setup)

```{code-block} python

sudo apt-get install libhdf5-dev
sudo apt-get install libc-ares-dev
sudo apt-get install libeigen3-dev
sudo apt-get install build-essential
sudo apt-get install libsdl-ttf2.0-0
sudo apt-get install python3-pygame
sudo apt-get install festival
sudo apt-get install python3-h5py

```
````
`````




### Camera  packages
The move from the *Buster* to *Bullseye* release of Raspberry Pi OS comes with signficant [changes](https://www.raspberrypi.com/news/new-old-functionality-with-raspberry-pi-os-legacy/), one of which is the 'loss' of the *Picamera* Python Library and its [replacement](https://www.raspberrypi.com/news/bullseye-camera-system/) by the open source but **work-in-progress** *libcamera*. More importantly (for this project) there is no Python interface (yet). Therefore, we will [re-enable](https://www.youtube.com/watch?v=E7KPSc_Xr24) the Python Picamera library, open a terminal and type:

```{code-block} python

sudo apt-get update
```

```{figure} /images/Camera_raspi_config_1.jpg
---

name: rasp_pi_config_1-fig
---
Update the Raspberry Pi

```

Then:
```{code-block} python

sudo raspi-config
```

```{figure} /images/Camera_raspi_config_2.jpg
---

name: rasp_pi_config_2-fig
---
Type this command then press enter

```

Entering the configuration menu, select the interface options:

```{figure} /images/Camera_raspi_config_3.jpg
---

name: rasp_pi_config_3-fig
---
Go down to *3. Interface Option* and press enter

```

Then select the option for Legacy Camera support:

```{figure} /images/Camera_raspi_config_4.jpg
---

name: rasp_pi_config_4-fig
---
Select option *I1 Legacy Camera Enable/Disable*

```

The configuration menu will then ask for you to confirm whether you wish to enable (or disable) legacy camera support, confirm, click Finish, and then choose to reboot. Legacy Camera support will now be enabled on your Raspberry Pi.
<br><br>

### Connect the Camera
Lets now connect a Camera Module to the Raspberry Pi Board, for this example we are using the official Raspberry Pi Camera Module version 2 (can also be referred to as Camera Module 2 or camera module v2). First, **make sure that the board is powered off and unplugged**. Then on the Raspberry Pi 4B board locate the Camera Serial interface (CSI) which is nestled between one of the hdmi connectors and the 3.5 mm AV jack. The CSI looks like a straight line with a plastic flap that is perpendicular to the board edge. Pull up and outwards the flap using the overlapping edges until it is part-way out. The camera module should have a flat ribbon cable attached to it, this ribbon should have, at the ends, a blue plastic side and on the reverse a silver edged side. With the blue plastic side facing upwards insert the ribbon cable into the CSI. Then carefully push the flap back in. If it is installed correctly the blue side of the ribbon cable should be facing towards the Raspberry Pi 4B board's USB B and Ethernet ports and the silver side towards the micro hdmi ports. The ribbon cable should sit straight and not come out if gently tugged, if it isn't or comes out try and re-connect it. 
<br><br>

## Webstreaming
To get around having no monitor connected or using a remote desktop we will stream the camera's output using Flask a lightweight Python based micro-web framework for web applications, outlined elsewhere ([Rosebrock, 2021](https://pyimagesearch.com/2019/09/02/opencv-stream-video-to-web-browser-html-page/)). 

### Webstreaming set-up
Let us first construct the directory structure for our Webstreaming application. In the terminal of the Raspberry Pi type:

```{code-block} python
---

---
cd ~/Documents

sudo mkdir webstreaming

cd webstreaming

sudo mkdir templates
sudo mkdir code

cd templates

```

This should produce a series of folders (e.g, *templates*, *code*) within a overarching *webstreaming* folder that is located in the Documents directory of your Raspberry Pi and leave you within the templates folder.

### html page

 The next task is to make an index.html file populated by the Flask application so that your web browser can render the webpage. In the terminal type:

```{code-block} python
sudo nano index.html
```
This will create a file called index.html and open it in a simple text editor in the terminal, enter the following into it:

```{code-block} html
---

---
<html>
  <head>
    <title>WUR Raspberry Pi Video Streaming</title>
  </head>
  <body>
    <h1>WUR Raspberry Pi Video</h1>
    <img src="{{ url_for('video_feed') }}">
  </body>
</html>

```

### Webstreaming code

Close the file (ctrl + X) and it will ask you to save the changes. Next go up one directory into the webstreaming folder:
```{code-block} python
cd webstreaming
```

Create a python script file:

```{code-block} python
sudo nano webstreaming.py
```

Enter the following:

```{code-block} python
---

---
# -*- coding: utf-8 -*-
"""
WUR: Raspberry Pi Webstreaming

Rationale:
    Stream camera video to web browser

Metadata:
    @created: Fri Jul  8 15:46:21 2022
    @author: JK, BM
    @modified_from: 'OpenCV - Stream video to web browser/HTML page'
    @original_author: Adrian Rosebrock
    @original_creation_data: Sept 9 2019
    @original_last_modified: July 9 2021
    @ref: https://pyimagesearch.com/2019/09/02/opencv-stream-video-to-web-browser-html-page/

Requirements
    import cv2
    from imutils.video import VideoStream
    from flask import Response
    from flask import Flask
    from flask import render_template
    from flask import request
    import threading
    import argparse
    import datetime
    import imutils
    import time

"""

# PACKAGEs
#import numpy as np
#from tflite_support.task import processor
import cv2
from imutils.video import VideoStream
from flask import Response
from flask import Flask
from flask import render_template
from flask import request
import threading
import argparse
import datetime
import imutils
import time



# SET (GLOBAL) VARIABLES
COORDS = [0, 0, 0, 0]

_ROW_SIZE                = 50  # pixels
_LEFT_MARGIN             = 50  # pixels
_TEXT_COLOR              = (0, 0, 255)  # red
_FONT_SIZE               = 1
_FONT_THICKNESS          = 2
_FPS_AVERAGE_FRAME_COUNT = 10


outputFrame = None
lock        = threading.Lock()
app         = Flask(__name__)


vs          = VideoStream(src=0,usePiCamera=True,resolution=(1920,1440)).start()
time.sleep(2.0)


# FUNCTIONS
@app.route("/", methods=['GET', 'POST'])
def index():
	global COORDS
	if request.method == 'POST':
		text = request.form['text']
		text = text.split(",")
		print("TEXT: ", text)
		for index, element in enumerate(text):
			COORDS[index] = int(element.strip())
			print(COORDS)
	elif request.method == 'GET':
		return render_template('index.html')
	# return the rendered template
	return render_template("index.html")



def generate():
    """
    

    Yields
    ------
    None.

    """
	# grab global references to the output frame and lock variables
    global outputFrame, lock
	# loop over frames from the output stream
    while True:
		# wait until the lock is acquired
        with lock:
			# check if the output frame is available, otherwise skip
			# the iteration of the loop
            if outputFrame is None:
                continue
			# encode the frame in JPEG format
            (flag, encodedImage) = cv2.imencode(".jpg", outputFrame)
			# ensure the frame was successfully encoded
            if not flag:
                continue
		# yield the output frame in the byte format
        yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
			bytearray(encodedImage) + b'\r\n')



@app.route("/video_feed")
def video_feed():
    """
    

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
	# return the response generated along with the specific media
	# type (mime type)
    return Response(generate(),
		mimetype = "multipart/x-mixed-replace; boundary=frame")



def update_frame(frameCount, 
                 rotation      = 180,
                 box_colour    = (255,0,0),
                 box_thickness = 2,             ):
    """
    INPUT:
        frameCount    = 
        rotation      = degress to rotate frame
        box_colour    = colour of box in BGR, default is blue (255,0,0)
        box_thickness = 2
    
    
    
    """
    # PACKAGES
    import imutils
    import cv2
    import datetime
    
    
    # VARIABLES
    # grab global references to the video stream, output frame, and
	# lock variables
    global vs, outputFrame, lock
	
    
    # FUNCTION
	# loop over frames from the video stream
    while True:
		
        # GET FRAME
        # read the next frame from the video stream
        frame = vs.read()
        
        # rotate it (default is 180 degrees)
        frame = imutils.rotate(frame, rotation)
        
		
        # Box
		# represents the top left corner of rectangle
        start_point = (COORDS[0], COORDS[1])
		
		# Ending coordinate, here (220, 220)
		# represents the bottom right corner of rectangle
        end_point = (COORDS[2], COORDS[3])

        frame = cv2.rectangle(frame, start_point, end_point, box_colour, box_thickness)

		# grab the current timestamp and draw it on the frame 
        cv2.putText(frame, get_current_time().strftime("%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10),
			        cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
        
		# acquire the lock, set the output frame, and release the lock
        with lock:
            outputFrame = frame.copy()
            


def get_current_time():
    """
    
    PACKAGE
        import datetime
        
    EXAMPLE:
        timestamp = get_current_time()
        timestamp.strftime("%A %d %B %Y %I:%M:%S%p")
    
    INPUT:
        None
        
    OUTPUT:
        timestamp = timestamp now

    """
    # PACKAGES
    import datetime
    
    # FUNCTIONS
    timestamp = datetime.datetime.now()
    
    return timestamp
    

# check to see if this is the main thread of execution
if __name__ == '__main__':
     
    # ARGUMENT PARSER
	# construct the argument parser and parse command line arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--ip", type=str, required=True,
		help="ip address of the device")
    ap.add_argument("-o", "--port", type=int, required=True,
		help="ephemeral port number of the server (1024 to 65535)")
    ap.add_argument("-f", "--frame-count", type=int, default=32,
		help="# of frames used to construct the background model")
    args = vars(ap.parse_args())
    
    # THREAD
	# start a thread that will perform motion detection
    #t = threading.Thread(target=detect_objects, args=(args["frame_count"],))
    t = threading.Thread(target=update_frame, args=(args["frame_count"],))
    t.daemon = True
    t.start()
	
    # FLASK
	# start the flask app
    app.run(host=args["ip"], port=args["port"], debug=True,
		threaded=True, use_reloader=False)

# release the video stream pointer
vs.stop()
```

### Running the code

Close the file (ctrl + X) and it will ask you to save the changes. Now let us run the code, in the terminal type:

```{code-block} python
python3 webstreaming.py -i <ip> -o <port>
```
Replacing *ip* with the IP address of the Raspberry Pi and *port* with a port number, in a web browser go to $http://ip:port/$ and the video should be displayed


```{figure} /images/Webstream_example.jpg
---

name: webstream_1-fig
---
The video should be streaming in real-time

```
