# Microcontrollers

## Physical Computing: Software and Hardware

As Digital Twins are rooted in both the virtual and physical worlds an important first step will be in understanding physical computing. <br><br>

Talk about programming and it is likely one's thoughts go to writing scripts and algorithms or developing programmes, applications, or models. All things that exist in the virtual world that are essentially a set of instructions to make a computer perform a task. These 'instructions' are **software**, a term used to differentiate the physical components which are **hardware** . Software however, is not the only thing we can programme, we can also write programmes for hardware so that we take inputs from *sensors* which can lead to outputs from *actuators* that affect things in the real-world. This is the basis of **Physical Computing** which we can define as programmes that interact with the physical or real-world .
<br><br>

Look around your home, learning environment, or workplace and you will undoubtly encounter electronics that are using a form of physical computing. For instance, toys, thermostats, dishwashers and washing machines all have sensors that convert a physical action (e.g., pressing a button) or phenomenon (e.g., temperature) into an electronic signal and actuators that convert an electronic signal into a physical action. Between them sits a **Microcontroller** that acts as the 'brains' of the device, interpreting the signal coming from the sensors and responding with the actuators. Microcontrollers are tiny, resource-lite, computers where you store the programmes you write that giving meaning to the sensor data and define appropriate action for the actuators.


## What are Microcontrollers?
Microcontrollers are small, compact, **integrated circuits (IC)** - circuits that are on a small flat piece of semiconductor material - which are used as the brains of an **embedded system** - a computer hardware system with a dedicated function. Data is received by the microcontroller's **input/output (I/O)** peripherial(s) where it is stored in its volatile data memory, assessed by the processor (CPU), and subject to the instructions in the non-volatile programme memory. An action, or lack of action, can then occur using the I/O peripherial(s). Thus, the microcontroller [governs](https://www.techtarget.com/iotagenda/definition/microcontroller) a specific operation. For our Digital Twin  microcontrollers will be used for sensing (e.g., measuring temperature, light, or air quality) and where needed governing actions (e.g., turning on/off a fan, pump, etc.) in response to what has or hasn't been sensed. 


## Microcontroller boards 

```{figure} /images/microcontroller_2_fig.jpg
---

name: microcontroller_2-fig
---
**Images of various microcontrollers.** *Including the Raspberry Pi [Pico](https://www.raspberrypi.com/products/raspberry-pi-pico/) and the [Pico W](https://www.raspberrypi.com/products/raspberry-pi-pico/), the Adafruit QT Py [ESP32-C3](https://www.adafruit.com/product/5405) and [ESP32-S3](https://www.adafruit.com/product/5325), the Pimoroni [Tiny 2040](https://shop.pimoroni.com/products/tiny-2040?variant=39560012234835), Arduino [Uno](https://store.arduino.cc/products/arduino-uno-rev3), and the [Arduino Nano](https://store.arduino.cc/products/arduino-nano-rp2040-connect).*

```

There are a great many microcontroller boards that are available commercially and inexpensively ({numref}`microcontroller_2-fig`). These microcontrollers range in price from five to 20 euros and vary in the size of the board (e.g., from postage stamp to credit card sized), it's components (e.g., USB B or C), the peripherial connectors (e.g., stemma QT), whether pins are pre-soldered (i.e., *with headers*), and what type of connectivity there is mounted on the board (e.g., Bluetooth, WiFi). 


## Microcontrollers in this Project
Several Microcontroller boards have been used in this Project including the Raspberry Pi Pico ({numref}`pinout_raspberry_pico-fig`), Raspberry Pi Pico Wireless ({numref}`pinout_raspberry_pico_W-fig`), and the Arduino Nano RP2040 Connect ({numref}`pinout_arduino_connect-fig`). Outlined below are some details of these boards.


### Raspberry Pi Pico 
```{figure} /images/pinout_raspberry_pico_including_logo.jpg
---

name: pinout_raspberry_pico-fig
---
**Pinout of the Raspberry Pi Pico Board.** *The [Pinout](https://datasheets.raspberrypi.com/pico/Pico-R3-A4-Pinout.pdf) is copyright © 2020-2021 Raspberry Pi (Trading) Ltd licensed under a Creative Commons [Attribution-NoDerivatives 4.0](https://creativecommons.org/licenses/by-nd/4.0/) International (CC BY-ND) licence.* 

```
The Raspberry Pi Pico is a microcontroller development board. The board is both powered by a micro-usb connector as well as being the interface to *talk* to your Pico via a computer Along the edges, the Pico has 40 Input/Output (I/O) connections that can either be attached to via the castellations - the bumpy outer edge - to other boards or by soldering 2.54 mm male pin headers to the holes that are slightly inwards from these castellations. Along with I/O for Ground (GND), 3.3V (3V3) there are 26 General Purpose (GP) Pins and two I2C buses. There is an onboard LED and the RP2040 microcontroller chip decorated with the Raspberry Pi logo.  The Raspberry Pi Pico comes with the [MicroPython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html#installing-micropython) programming language as *software*, however it is relatively easy to replace this with [CircuitPython](https://wur.gitlab.io/digitaltwin/docs/setup/circuitpython_setup.html#installing-circuitpython). 

### Raspberry Pi Pico Wireless
```{figure} /images/pinout_raspberry_pico_W_including_logo.jpg
---

name: pinout_raspberry_pico_W-fig
---
**Pinout of the Raspberry Pi Pico W Board.** *The [Pinout](https://datasheets.raspberrypi.com/picow/PicoW-A4-Pinout.pdf) is copyright © 2020-2021 Raspberry Pi (Trading) Ltd licensed under a Creative Commons [Attribution-NoDerivatives 4.0](https://creativecommons.org/licenses/by-nd/4.0/) International (CC BY-ND) licence.* 

```
The Raspberry Pi Pico Wireless or Pico W adds wireless communications to the Raspberry Pi Pico microcontroller development board. Like, the Raspberry Pi Pico the Raspberry Pi Pico Wireless comes 40 Input/Output (I/O) connections that can provide Ground (GND), 3.3V (3V3), as well as 26 General Purpose (GP) Pins, and two I2C buses. But differs in adding an on-board Infineon CYW4343 giving access to single-band 2.4GHz wireless interfaces (802.11n). Like the Pico it comes with the [MicroPython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html#installing-micropython) programming language as *software*. However, MicroPython can be replaced with [CircuitPython](https://wur.gitlab.io/digitaltwin/docs/setup/circuitpython_setup.html#installing-circuitpython).


### Arduino Nano RP2040 Connect
```{figure} /images/pinout_arduino_nano_connect_including_logo.jpg
---

name: pinout_arduino_connect-fig
---
**Pinout of the Arduino Nano RP2040 Connect Board.** *The [Pinout](https://content.arduino.cc/assets/Pinout_NanoRP2040_latest%20%281%29.png) is provided by Arduino licensed under a Creative Commons [Attribution-ShareAlike 4.0](https://creativecommons.org/licenses/by-sa/4.0/) International (CC BY-SA 4.0) licence.* 

```
Arduino produces a variety of different open source microcontrollers whose designs are distributed under a Creative Commons Attribution Share-Alike 2.5 license. The [Arduino Nano RP2040 Connect](https://store.arduino.cc/products/arduino-nano-rp2040-connect) incorporates a Raspberry Pi RP2040 microcontroller in a similar form factor as a Raspberry Pico but at a reduced size. The Arduino Nano RP2040 Connect was amongst the first RP2040 microcontroller chip bearing boards to contain Bluetooth and WiFi connectivity using a U-blox® Nina W102.

#### Python
The Arduino Nano RP2040 Connect intially comes with the [Arduino](https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-01-technical-reference) programming language, however it can also be [replaced](https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-python-api#hardware-requirements) with either [MicroPython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html#installing-micropython) or [CircuitPython](https://wur.gitlab.io/digitaltwin/docs/setup/circuitpython_setup.html#installing-circuitpython). If CircuitPython or MicroPython is being installed it may necessitate upgrading the U-blox® Nina W102 firmware to ensure that the Bluetooth and WiFi connectivity works smoothly. 


````{admonition} Click here for Upgrading Nina Firmware!
:class: tip, dropdown
**Upgrading Nina Firmware** <br>

The firmware associated with the U-blox® Nina W102 found on the Arduino Nano RP2040 Connect can be [upgraded](https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-upgrading-nina-firmware) with the following steps. Note that these steps have been tested with a Raspberry Pi running the Debian version of the [Raspberry Pi OS](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html):

- Download the Arduino Firmware Updater using the following [link](https://github.com/arduino/arduino-fwuploader/releases) for your OS. Note that the Linux ARM version can be used for computers running the Raspberry Pi OS.<br>
- Extract the folder from it's Gzip (.gz) Tar (.tar) or Zip (.zip) folder.<br>
- Either [download](https://github.com/arduino/ArduinoCore-mbed/blob/master/post_install.sh) the post_install shell script. Or create a new text file (e.g., sudo nano post_install.sh) and copy and paste the following into it:

```{code} terminal
---
---
#!/usr/bin/env bash

rp2040rules () {
    echo ""
    echo "# Raspberry Pi RP2040 bootloader mode UDEV rules"
    echo ""
cat <<EOF
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2e8a", MODE:="0666"
EOF
}

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

rp2040rules > /etc/udev/rules.d/60-rp2040.rules

# reload udev rules
echo "Reload rules..."
udevadm trigger
udevadm control --reload-rules

```

- Open the terminal and run the shell script post_install.sh, e.g., via *sudo sh post_install.sh* this should allow flashing, necessary for upgrading the firmware of the board, as a normal user.<br>
- Next obtain the Port for the Board. For Unix system this can be done by first ensuring that the Board is unplugged from the computer or before you connect the board. Type *sudo dmesg --clear* this will clear the message buffer of the Kernel including those associated with device drivers. By first clearing the message buffer it should make it easy to identify which port is associated with your device.<br>
- Plug the board in.<br> 
- Type *dmesg* which should give you the message buffer, having cleared it earlier the message should be only those associated with the board's USB. The port should be in the message list (e.g., COM7, /dev/ttyACM0, /dev/ttyAMA0)<br>
- In the terminal navigate to the folder containing the extracted Arduino Firmware Updater folder, e.g., *cd Downloads* if the folder was extracted to the Downloads folder.<br>
- In the terminal type *./arduino-fwuploader firmware list* this will give you a list of the firmware versions to obtain the latest version of Nina for the Arduino Nano RP2040 Connect, for example '1.5.0'.<br>
- Once more in the terminal type *./arduino-fwuploader firmware flash -b arduino:mbed_nano:nanorp2040connect -a port -m NINA@version* replacing port and version with the values obtained from the steps above.<br>

Once this command has been entered the firmware uploader should first load, then flash the firmware to the board.  

````

