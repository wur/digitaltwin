# Loops

## Recap 

We've been writing a program to collect and collate three environmental variables (Temperature, Humidity, and Carbon Dioxide) and display the real-time data to an observer. The observer should be able to read off the values, so a display is required, and these values should be ‘real-time’ or as close to as possible. So far our program looks like this:

```{code-block} python
---
lineno-start: 1
---
# 0.  Import Packages
from pimoroni_i2c import PimoroniI2C
from breakout_sgp30 import BreakoutSGP30
from breakout_bme280 import BreakoutBME280
from picographics import PicoGraphics, DISPLAY_PICO_EXPLORER
import time

# 1.  Set-up Hardware
# 1.1 Define i2c pins
PINS_PICO_EXPLORER = {"sda": 20, "scl": 21} # Default i2c pins for Pico Exp.

# 1.2 Set-up display
display            = PicoGraphics(display=DISPLAY_PICO_EXPLORER)

# constants for drawing
WIDTH, HEIGHT      = display.get_bounds()

# define pen colors 
white              = display.create_pen(255, 255, 255)
black              = display.create_pen(0, 0, 0)
red                = display.create_pen(255, 0, 0)
grey               = display.create_pen(125, 125, 125)

# make a simple rectangle
display.set_pen(black)
display.rectangle(1, 1, 100, 25)

# 1.4 Start Sensor
# set i2c pins
i2c               = PimoroniI2C(**PINS_PICO_EXPLORER)
bme               = BreakoutBME280(i2c)
sgp30             = BreakoutSGP30(i2c)
print("SGP30 initialised - about to start measuring without waiting")

sgp30.start_measurement(False)
id                = sgp30.get_unique_id()
print("Started measuring for id 0x", '{:04x}'.format(id[0]), '{:04x}'.format(id[1]), '{:04x}'.format(id[2]), sep="")


# 2. GET DATA
# read the sensors
air_quality                     = sgp30.get_air_quality()         # get measurements
eCO2                            = air_quality[BreakoutSGP30.ECO2] # get variable
temperature, pressure, humidity = bme.read()                      # get variable
pressurehpa                     = pressure / 100                  # convert pressure to hPa


# 3. SHOW DATA
# Positional values for each measurement in the format:
# variable_to_display  = [[position of var. name text],[position of var. value text],var_name,unit, decimals]

CO2_display         = [[10,  20, 240, 3], [10, 40, 240, 5], 'CO2'          , ' ppm', 0]
Temperature_display = [[10,  85, 240, 3], [10,105, 240, 5], 'Temperature'  , ' C'  , 2]
Humidity_display    = [[10, 150, 240, 3], [10,170, 240, 5], 'Rel. Humidity', ' %'  , 2]

def show_data(variable_name, variable, variable_unit, position, decimal_places = 0):
    """ Display data
    
    # Requirements:
        import picoexplorer as display
    
    """ 
    # 1. Display variable name
    display.text(variable_name,
                 position[0][0], position[0][1],
                 position[0][2], position[0][3])
    
    #2. Display variable with the ability to modify the number of decimal places
    display.text('{:.{dec}f}'.format(variable, dec = decimal_places) + variable_unit, 
                position[1][0], position[1][1],
                position[1][2], position[1][3])
    
# set the text to white
display.set_pen(white)
    
# add the temperature text
show_data(Temperature_display[2],temperature, Temperature_display[3],Temperature_display,Temperature_display[4])
    
# add the CO2 text
show_data(CO2_display[2],eCO2, CO2_display[3],CO2_display,CO2_display[4])
    
# add the humidity text
show_data(Humidity_display[2],humidity, Humidity_display[3],Humidity_display,Humidity_display[4])

# update the display
display.update()
```

From line 40 onwards our device is now programmed to measure the three parameters of interest once and display their unique values. Not exactly useful if you are a sat in an office for a couple of hours. You want up-to-date real-time information. So we need to instruct the program to keep measuring continuously, for that we need to re-write our program to incorporate a loop. 

<br/>

## Loops

There are a couple types of loop: a **definite loop** which repeats a set of instructions a defined number of times and an **indefinite loop** which repeats without end whilst a condition is not met. Indefinite loops can veer off either intentionally or through error into an **infinite loop** when a condition can never be met. If we are making a time series we can instruct the program to keep measuring until some condition is met or forever (as long as power and storage space are not an issue). 

### Definite loop

An example of a *definite loop* is as follows: 

```{code-block} python
---

---
for i in range(5):
    print('i is ',i)

print('done')

```
The variable *i* is defined by the program with the *range* function as a value in a series of numbers beginning at 0 up to, but not including, 5. It begins with zero because python is a **zero-indexed language** which is fancy way of saying it starts counting from 0 rather than 1. In Python the first line ends with a colon (:) instructing that the loop begins in the following. The instructions for the actions performed in the loop are **indented**, i.e., four empty spaces from the left, to allow Python to differentiate code belonging outside and inside this loop. If you put a loop within a loop, **nesting**, each new loop is indented four empty spaces further. Removing the indentation tells Python that this instruction is outside of a loop. 

### Indefinite and infinite loops

Modifying the code to the following sets up an infinite *indefinite loop*:

```{code-block} python
---

---
i = 0                 # set up a 'counter'
while True:
    i += 1            # each time the loop iterates add 1
    print('i is ',i)

print('done')

```

Replacing the *for* with a *while* sets up a condition. As long as it is 'True' the loop will continue and as nothing within the loop changes it to 'False' the loop will run indefinitely. You might be asking yourself what variable we are referring to here that is being checked to see if it is True, it is essentially saying whilst True is True. To see how many times we have repeated we can set up a counter, during every loop we can add one to this counter (in python you can shorten i = i + 1 to i +=1). The statement outside of this loop will never be run. We can set a condition to be met that terminates our loop like so:

```{code-block} python
---

---
i = 0                 # set up a 'counter'
while True:
    i += 1            # each time the loop iterates add 1
    print('i is ',i)
    if i == 30:       # check if condition is met
        break         # terminate the loop using break

print('done')

```

<br>

## Loop in our Program

To meet our goal our program needs to produce a continuous set of measurements and continuously update the display with them. Therefore, we need to incorporate an *indefinite loop*. By doing so we can also include a **calibration** step which is an act of configuration to produce a result within an acceptable range. The SGP30 sensor during its initilisation state returns fixed values of 400 ppm CO2eq and 0 ppb TVOC. Therefore, we should incorporate this into our loop:

```{code-block} python
---
lineno-start: 35

---

# 2.  Functions
# 2.1 Show data
# Positional values for each measurement in the format:
# variable_to_display  = [[position of var. name text],[position of var. value text],var_name,unit, decimals]

CO2_display         = [[10,  20, 240, 3], [10, 40, 240, 5], 'CO2'          , ' ppm', 0]
Temperature_display = [[10,  85, 240, 3], [10,105, 240, 5], 'Temperature'  , ' C'  , 2]
Humidity_display    = [[10, 150, 240, 3], [10,170, 240, 5], 'Rel. Humidity', ' %'  , 2]

def show_data(variable_name, variable, variable_unit, position, decimal_places = 0):
    """ Display data
    
    # Requirements:
        import picoexplorer as display
    
    """ 
    # 1. Display variable name
    display.text(variable_name,
                 position[0][0], position[0][1],
                 position[0][2], position[0][3])
    
    #2. Display variable with the ability to modify the number of decimal places
    display.text('{:.{dec}f}'.format(variable, dec = decimal_places) + variable_unit, 
                position[1][0], position[1][1],
                position[1][2], position[1][3])
     

# 3. Measure
sgp30.start_measurement(False)
id                = sgp30.get_unique_id()
print("Started measuring for id 0x", '{:04x}'.format(id[0]), '{:04x}'.format(id[1]), '{:04x}'.format(id[2]), sep="")

count = 0
while True:
    count += 1
   
    # get airquality sensor sgp30 data
    air_quality     = sgp30.get_air_quality()
    eCO2            = air_quality[BreakoutSGP30.ECO2]
    TVOC            = air_quality[BreakoutSGP30.TVOC]

    air_quality_raw = sgp30.get_air_quality_raw()
    H2              = air_quality_raw[BreakoutSGP30.H2]
    ETHANOL         = air_quality_raw[BreakoutSGP30.ETHANOL]

    # get temperature sensor bme280 data
    temperature, pressure, humidity = bme.read()
    
    # pressure comes in pascals convert to the more manageable hPa
    pressurehpa = pressure / 100
    
    if count == 30:
        print("Resetting device")
        sgp30.soft_reset()
        time.sleep(0.5)
        print("Restarting measurement, waiting 15 secs before returning")
        sgp30.start_measurement(True)
        print("Measurement restarted, now read every second")
    
    # change pen back to black, so when it clears it resets to black
    display.set_pen(black)
    display.clear()

    # drawing the LCD text
    display.set_pen(white)
    
    # add the temperature text
    show_data(Temperature_display[2],temperature, Temperature_display[3],Temperature_display,Temperature_display[4])
    
    # add the CO2 text
    show_data(CO2_display[2],eCO2, CO2_display[3],CO2_display,CO2_display[4])
    
    # add the humidity text
    show_data(Humidity_display[2],humidity, Humidity_display[3],Humidity_display,Humidity_display[4])

    # time to update the display
    display.update()
    
    # wait one second before restarting loop
    time.sleep(1.0)

```


## Full code:
Our program should now meet our goal of providing continuous real-time observations of temperature, humidity, and CO2 levels in our office. In order to understand the code, including what inputs and outputs are necessary, it is a good practice to fully annotate your code. The full code is as follows:

<br/>

```{literalinclude} /code/micro_python/standalone_twin_examples/pico_explorer_example.py

```

