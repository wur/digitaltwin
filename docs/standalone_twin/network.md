# Networking

At the moment the data from our sensor is stored *locally* on the non-volatile storage of the Raspberry Pico. This approach whilst useful, can in the long run lead to impracticalities. For instance, what if we don't want to be in close proximity to our sensors? The sensor might be in a remote location or some inconvenient distance from the analyst. Furthermore, what if we want to select the room with the best air quality, must we enter each room to pull the sensor's data prior to analysing the room's air quality? One can imagine that having to physically *download* the data from each sensor will not only be time-consuming and impractical but also counter to the purpose of our sensor exposing the data collector to the poor air quality.

## IoT: Internet of things 
The solution is to connect each sensor to either an internal or external network that can allow for the sensor data to be relayed from the sensor to a device where investigation can be carried out. Such networking capabilities turns our sensor into an Internet of Things (IoT) device, although the term 'internet' here is a misnomer as any networking approach (e.g., Bluetooth, LAN, RFID, LORA) can be used as a means of communication. 

## MQTT

```{figure} /images/standalone_network.jpg
---

name: standalone_network-fig
---
**How MQTT works.** *MQTT uses a publish and subscribe model. Based upon [InfluxDB MQTT](https://www.influxdata.com/mqtt/).*  
```

MQTT 

In a later module the focus will be on how to [set up your own](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/cloud_storage.html) MQTT and cloud storage. But for now this module will focus on primarily sending (*publishing*) data to the cloud via a MQTT Broker that already exists, and receiving (*subscribing*) data.

## Raspberry Pico W

[Download](https://micropython.org/download/rp2-pico-w/rp2-pico-w-latest.uf2) and [install](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html) MicroPython as per the Raspberry Pi [Documentation](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html). Once installed, open the IDE that you will be using and create a new file. Create a a dictionary called `secrets` and save the file to the Raspberry Pico W as `secrets.py` that contains the Wireless Network's SSID and password:

```{literalinclude} /code/micro_python/secrets.py

```

We will be adding more to this dictionary later however, for now close this file. 

## Install MQTT

Next install the packages necessary for MQTT using the package `upip` which is similar to Python's package manager `pip`. First connect the Pico W to your Wireless network:

```{code-block} python
---
lineno-start: 1
---
""" install_packages.py
   

"""

# PACKAGES
import network
from secrets import secrets
import upip

# 1. NETWORK CONNECT
wlan   = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(secrets['ssid'],secrets['ssid_password'])

```
Then install either the [umqtt.simple](https://github.com/micropython/micropython-lib/tree/master/micropython/umqtt.simple) or [umqtt.robust](https://github.com/micropython/micropython-lib/tree/master/micropython/umqtt.simple) package with the latter adding auto-reconnect features:

```{code-block} python
---
lineno-start: 12
---
# 2. INSTALL package
upip.install('umqtt.simple')
```

## MQTT: Functions

Having installed the `umqtt.simple` package let us create a series of functions

```{code-block} python
---
lineno-start: 1
---
from umqtt.simple import MQTTClient

client           = MQTTClient(ID, mqtt_server, keepalive= 10)
client.set_callback(message_listener)
client.connect()

client.publish(topic, message)

client.subscribe(topic)


```


new file and  

```{literalinclude} /code/micro_python/MQTT/mqtt_functions.py
---

---

```

## MQTT: Publish


```{code-block} python
---
lineno-start: 1
---

```

## MQTT: Subscribe

```{figure} /images/standalone_MQTT_Subscribe.jpg
---

name: standalone_MQTT_Subscribe-fig
---
**Example of MQTT subscribe in action.** *As data is published to the topic /sensors/garden the subscription to the topic is triggered relaying the message here.*

```

The 

```{literalinclude} /code/micro_python/MQTT/test_MQTT_subscribe.py

```
