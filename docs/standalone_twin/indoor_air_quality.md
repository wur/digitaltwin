# Indoor air quality

```{figure} /images/air_quality.jpg
---

name: air_quality-fig
---
**Cartoon representation of carbon dioxide concentration.** *Perceived levels of health impacts at various carbon dioxide concentrations by parties interested in indoor air quality.*
```

Poor air quality has been identified as the cause of severe respiratory health problems and illnesses (e.g., lung cancer, asthma, allergies and respiratory infections, chronic obstructive pulmonary disease COPD, etc.). Leading to studies, programmes, and legislation meant to limit or curb the variety of environmental sources of reduced air quality. However, much of the focus has been on outdoor rather than indoor air quality. Yet in our daily lives we constantly enter public and private indoor spaces; environments that represent potential vectors of exposure to environmental pollutants. Such pollutants include emissions from cooking, smoking tobacco products, heating, cleaning (e.g., [Yeoman et al., 2021](https://doi.org/10.1111/ina.12811)), and building materials. Likewise, limitations in ventilation, aspects of thermal insulation, and microbiological contaminants (e.g., mould, pests, pets) can impact or further exacerbate poor air quality. That is not to discuss the mixing of indoor and outdoor air; outdoor pollutants can enter such spaces contributing both to a reducing air quality and the complexity of the issue.<br><br>

## Context: COVID and cost-of-living crises
Monitoring indoor air quality has gained new impetus since 2020 as the World experiences the ongoing COVID 19 pandemic caused by the severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2). Many studies, projects, and products have sprung up to monitor the ambient air quality of our homes and offices with a focus on ventilation. Yet, poor ventilation is not the only potential pollutant to worry about.<br> 

The current economic downturn and cost-of-living crisis, including increases in energy costs, may have a detrimental impact on indoor air quality in 2022. For example, the economic crisis of 2008 saw an increase in solid fuel use, given that it was cheaper than other fuels ([Sigsgaard et al., 2015](https://doi.org/10.1183/13993003.01865-2014)). In the UK rises in particulate matter with a diameter <2.5 $\mu m$ (PM2.5) has occured due to increases in residential (17% of primary emissions) and manufacturing or construction biomass burning (14.5% of emissions) ([National statistics, 2020](https://www.gov.uk/government/statistics/emissions-of-air-pollutants/emissions-of-air-pollutants-in-the-uk-summary)). Residential stoves can release a considerable amount of particulate matter ([Chakraborty et al., 2020](https://doi.org/10.3390/atmos11121326)) which can include polycylic aromatic hydrocarbons (PAHs) increasing cancer risk ([Tsiodra et al., 2021](https://doi.org/10.5194/acp-21-17865-2021)). A lower estimate suggest 40,000 early deaths per year in Europe are due to residential wood burning ([Sigsgaard et al., 2015](https://doi.org/10.1183/13993003.01865-2014)).<br>

High energy costs can also contribute to adverse conditions, as decisions between heating and ventilating indoor spaces can have unintended consequences. For example, the lack of or limiting the amount of ventilation in homes can have an impact on humidity levels. Too low humidity can cause discomfort and skin iteration. Whereas, too high humidity caused by for instance, not letting excess moisture from cooking or cleaning escape, can promote the growth of pests and microbes, moulds and funghi.<br><br> 

## Pollutants
The following have been identified as major contributors to air quality: 

- Chemicals for intended use (e.g., *paints, home and personal cleaning products, furniture and electrical goods*, etc.);
- Chemical from (unintentional) emissions from various sources;
- Environmental factors (e.g., *naturally occuring Radon; Lead pipes and paint*) and conditions (e.g., *Temperature, Humidity, Ventilation*);
- Particles
- Biologicals (e.g., *microbes, mould, funghi, pets and pests*)

Despite the seriousness of health problems the associated risks to health from indoor (or outdoor) air pollutants are complex given that there are a wide range in pollution types, characteristics, toxicity, and causes. Associating certain pollutants with adverse health (i.e., dose-response) becomes increasingly complex when one considers that it is not just the transmission (inhalation, ingestion, etc), longevity, and amount of exposure to a single pollutant but individuals in indoor environments may be exposed to multiple substances.<br>

Likewise, people's vulnerabilities and responses to various conditions and pollutants is not identical. Vulnerable groups, such as children, elderly (> 65 years of age), pregnant women, and individuals from respiratory illnesses (e.g., asthma) or cardiovascular diseases may have age-dependent differences in physiology and toxicokinetics that lead to a different response ([SCHER, 2007](https://ec.europa.eu/health/ph_risk/committees/04_scher/docs/scher_o_055.pdf)).<br><br>


## Digital Twin

```{figure} /images/DT_maturity_levels_modified.jpg
---

name: DT_maturity_levels_modified-fig
---
**Digital Twin Maturity levels.** *The maturity level (ML), a measure of its ability, of Digital Twins from top to bottom: Data, Information, Knowledge, Understanding, Insight, Wisdom. The boxes represents whether the (dark blue) user or (green) twin performs a specific task.*  
```

Our knowledge of indoor air quality is further hampereed by the variability in indoor spaces. As such it represents an interesting test case for a Digital Twin project. Our knowledge of the concentration, exposure, and risk associated with the pollutants is limited by a lack of data upon their presence, concentration, and temporal distributions which a Digital Twin can provide us with. In future with more data it could be possible to adapt and expand our twin. But for now we will limit ourselves to providing the user with the status of various variables through the "*collection of values that represent the properties or state of an asset*" ({numref}`DT_maturity_levels_modified-fig`).<br>

## Sensors and Measurements
Of interest to us in this Indoor air quality study are the following parameters:

- **Temperature.** Air temperature.<br>
- **Humidity.** The humidity of the air.<br>
- **Carbon dioxide.** The concentration of carbon dioxide ($[CO_{2}]$).<br>
- **VOCs.** The concentration of volatile organics compounds in the air. 
- **Particles.** Particles of various sizes found in air (e.g., PM2.5, PM10).<br>
- **Light.** The ambient light level.<br>

<br>

```{admonition} Further reading
:class: tip, dropdown

Scientific Committee on Health and Environmental Risks (SCHER), 2007. Opinion on risk assessment on indoor air quality, European Commission, Brussels, Belgium, pp. 33, https://ec.europa.eu/health/ph_risk/committees/04_scher/docs/scher_o_055.pdf


```
