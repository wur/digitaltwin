# Circuits

A circuit is a closed loop in which (electrical) energy moves along a fixed path from higher potential energy (+, pwr, or power) to lower potential energy (-, GND, or ground) in one direction if it is direct current (DC) or in alternating directions if it is alternating current (AC). Circuits are composed of a current source (power supply), a conductor (wires), and a load or things that use the current. The load can be **tranducers** converts one form of energy into another form of energy, it can be the input or the output. A **sensor** is a type tranducer that is used as input converting one form of energy into electrical energy, whereas an **actuator** is used as output converting electrical energy into another form of energy. 

Electricity is the movement of electrons, to understand this movement three characteristics need to be known: 

- **Current.** Current (symbol *I*) is the amount of 'flow' of electrical charge. It is measured in amps or amperes (A), where an ampere is the amount of Coulombs (C), defined as $6.241\cdot10^{18}$ electrons per second, that flows past a point in a circuit. 

- **Resistance.** The resistance (symbol *R*) is the force that counteracts, or resists, the 'flow' of electrical charge.  It is measured in ohms ($\Omega$) where 1 Ohm is equal to a current of one ampere ($6.241\cdot10^{18}$ electrons) when subjected to a potential difference of one volt.

- **Voltage.** Voltage is pressure or the difference in electric potential between one point and another point along a circuit. This electric potential is measured in volts (V) or  joules (work) per coulomb (charge) (J/C).

The relationship between these three characteristics can be expressed by Ohm's law:

$$ V = I \cdot R $$

where *V* is Voltage in volts, *I* is Current in amps, and *R* is resistance in ohms. When connecting components Ohm's law can be used to compute the resistors need for a specific component. 

## Breadboards

```{figure} /images/standalone_circuit_breadboard.jpg
---

name: standalone_circuit_breadboard-fig
---
**Breadboard schematic.** *(A) Internal schematic, inside view of a breadboard which is a series of conductive metal tracks arranged horizontally (blue) or vertically (green). (B) External schematic showing the arrangement of holes.*  
```

A solderless **breadboard**, or breadboard for short, is used for simple prototyping as it allows you to build circuits and prototype by inserting components as the project requires into the holes and then connecting them with jumper wires. Once a project is finished components can be removed and replaced. Inside the breadboard are a series of conductive metal strips that connect a series of holes either vertially or horizontally ({numref}`standalone_circuit_breadboard-fig`). Arranging components in a specific order can therefore allow for connections to be made. On some sizes of breadboard the outer long edges have two rows of that can be used for power distribution and ground when connected with the central prototyping area. 

## Resistor

Resistors are electrical components that resist the flow of electricity. Therefore, by placing them in the circuit they will reduce the current and supply the parts with the correct or a sufficient amount of energy rather than an excessive amount which may damage the component. Plated through-hole (PTH) axial lead resistors are resistors commonly used for 'prototyping' these have long 'legs' that can be inserted into a breadboard. On the axial package there are coloured bands that denote the size of the resistor. These can have either 4 or 5 bands. In the types with 4 bands the first and second bands represent the first and second digits, whereas the third and fourth bands represent the multiplier and tolerance. For example, if the colours of the bands represent 1 0 3 5 then this should be read as $10 \cdot 10^{3}$ $\pm$ 5 or 10,000 $\Omega$ $\pm$ 5 %. In comparison, those resistor types with 5 bands have the first three bands representing the first, second, and third significant digits and the fourth and fifth representing a multiplier and tolerance. For example, if the colours of the bands represent 1 0 0 2 5 then this should be read as $100 \cdot 10^{2}$ $\pm$ 5 or 10,000 $\Omega$ $\pm$ 5 %. 

## Motor






