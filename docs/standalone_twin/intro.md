# Standalone Digital Twin

## Rationale

Reading this it is likely that you are sat indoors, either at home, university, or in an office. When we think about air quality it is likely that our thoughts go to traffic and industry related air pollution. However, indoor air quality can be far worse than outdoor air quality due to poor ventilation and conditions within the place of work (e.g., too low or high humidity, allergens, mould, etc.). The COVID pandemic has significantly altered what 'place of work' means especially those who have jobs that can be done remotely leading to many of us spending more time working from home. Our homes may not have undergone the same planning and regulatory requirements of an office building. For example, ventilation may not have been considered (e.g., older homes) or a lack of upkeep (e.g., unscrupulous landlords) may ultimately result in a range of health issues. Likewise, the return to working in the office is not without its problems either, for instance there is ongoing discussion regarding the ventilation of lecture halls, offices, etc. and the amount of people who can be present in one room.
<br/><br/>

Therefore, let’s start our journey with Digital Twins with an example of using a simple set of sensors in an environment that is both easily (1) definable and (2) controllable: our indoor office space. This example is not going to be a full Digital Twin instead we will focus on a primitive sensor device before setting up an internet of things (IoT) device. The aim of this section is to present to you the reader examples of:
 <br/><br/>
-	**How to collect data from sensors and relay that information to the user.**<br/><br/>
-	**How to use the data gathered to add meaning, or knowledge, to the collected data to help inform the user’s decision-making process.**<br/><br/>

Let us first define who are users and what are our goals.
<br/><br/>

## Who are the users? 
 Employees, employers, and building services are all interested in creating a working environment that is suitable for use. 
 <br/><br/>

 ## What is our goal?
The Digital Twin should provide actionable knowledge regarding:
- adverse conditions  <br/><br/>
- building control (e.g., ventilation usage)  <br/><br/>
- potential cost-saving initiatives (e.g., when a room’s air conditioning could be reduced or not required) and so forth. <br/><br/>
