# Storing measurements

## Data logging

### Reading and writing data

The Raspberry Pico has **non-volatile storage** that allows it to store data for later access (essentially a **data logger**). Non-volatile storage even keeps this data when the Pico is without power just like your own computer's hard drive. The Pico's 1.375MiB file system can be used to set up a recording of the measurements. By setting this up and combining with a battery pack our device can be used to allow sensors to become untethered and portable. Adding a form of communication to our device (e.g., Bluetooth, LoRA, or WiFi) allows us to collect and collate data without the need for our physical presence (i.e., to collect and collate the data remotely). More about that later. For now lets talk about how to read, write, and store data locally.

<br/>

In python you can open a text file to *write* or store your data to with the following:

```{code-block} python
---

---
file = open("sensor_data.txt", "w")
```
<br/>

Python creates a '.txt' file called *sensor_data*. The **"w"** puts the file into a mode in which data can be written to it, like so:

```{code-block} python
---

---
file.write("400, 22.5, 27.35")
```
<br/>

To ensure that the data is written to the file system you need to close it:

```{code-block} python
---

---
file.close()
```
<br/>

The data will now have been written, to check you can open the file in read mode by replacing the **"w"** with an **"r"**. Or by simply removing the **"w"** altogether as python's default is to open a file in read mode. Once open you can read the data stored and then close the file again, like so:

```{code-block} python
---

---
file = open("sensor_data.txt", "r")
# or
#file = open("sensor_data.txt")

file.read()
file.close()
```

There is a reason why the default for the 'open' instruction is read mode. If the file exists and you open it in write mode ("w") then python overwrites the file deleting all the information stored within. For a data logger this can be problematic because our aim is to write newly acquired data to an existing file. However, doing so we will overwrite the previous data.
````{margin}
```{note}
One solution is to first open a file in read mode and copy its existing data into a variable before re-opening it in write mode. The old data would be written back to the now blank file before appending the new data. This is somewhat inefficient.
```
````

Luckily python has a solution for this: 

```{code-block} python
---

---
file = open("sensor_data.txt", "w")

file.write("400, 22.5, 27.35")

file.flush()

```
<br/>

The instruction **flush** performs the action of saving the file similar to **close** but instead of closing the file it keeps it open. That is the data is stored in a temporary storage area (a **buffer**) and when the file is closed it is **flushing** from the buffer to the file. By keeping the file open it becomes possible to continuously write new data as it is collected.

<br/>

### Code for writing our sensor data

Let's modify our code so that each time our sensor powers up the first thing our program should do is create a file for output. For simplicity our sensor will write data to a file called 'data_{count}' where count is just add 1 to the last file name (i.e., data_1, data_2, data_3 ... data_ith). This is to ensure that old data is not wiped as new data is collected. 

To check what file name to use an indefinite loop is used with the condition that if a file cannot be opened it doesn't exist and can therefore be used as our filename. To do this each iteration of the loop adds one to counter which is used in the filename. This is then checked to see if it exists with a **try** statement. Once we find a file that doesnt exist the **except** statement sets our conditional variable (*running_file_check*) value to False.


```{code-block} python
---


---
# CHECK FILENAME

# set counter and define variable to break loop 
running_filename_check = True
file_count         = 0

while running_filename_check:

    try:
        filename = "sensor_data_" + str(file_count) + ".txt"
        
        # if the file does not exist the following will raise an exception
        f = open(filename, 'r') 
        f.close()
        file_count += 1
     
    except:
        running_filename_check = False


# OPEN FILE IN WRITE MODE

filename = "sensor_data_" + str(file_count) + ".txt"
file     = open(filename, "w")
```

You might ask: *technically could we not just open up an existing data file, read it into memory, close it, and then reopen it in write mode, write the existing data, and then just append new data to that?* Indeed it is possible to do that, but the Raspberry Pico does not have a real time clock. A real time clock is a component that keeps an accurate knowledge of the time even when on low or no power. Always appending to the same data file works if the relative temporal spacing between data is (near) constant or there is some indication of the time between data points. Hence, for this example it is easier to just create a new file for every initialisation. Having learned how to record, or log, values the next question is what should we record? This leads us onto data and metadata.

<br/>
