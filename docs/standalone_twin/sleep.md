# Sleep

```{figure} /images/standalone_loops_sleep_power.jpg
---

name: standalone_loops_sleep_power-fig
---
**Loops, Sleep, and Power.** *For power (orange/blue line) sensitive projects the down time between measurements (green circles) can be wasteful, especially if the (1) consumption remains the same. Having the ability to (2) reduce consumption can prolong the battery life of the device.*
```

Loops allow the programmer to make the microcontroller perform a periodic action and then wait for an alotted time to have passed before performing it, or some other action, again. This feature is done via the `time.sleep` command. For the code below the loop will add one to the variable `count` and then sleep for 1 second before continuing the loop, adding 1 at each iteration every second.

```{code-block} python
---

---
# initiate counter
count = 0         

# start loop
while True:

    # perform action
    count += 1

    # wait one second before restarting/continuing loop
    time.sleep(1.0)

```

If the **frequency** of measurements, i.e., the time interval between measurements, is longer (*e.g.*, minutes or hours) it can be useful to reduce the power consumption of the microcontroller to a fraction of its regular usage. Placing the microcontroller into a dormant, low powered, state between measurements will ensure that battery life is maximised. 

<br>

 ## Light and deep sleep

There are two modes of sleep:

<br>

- **Lightsleep**. For this mode the microcontroller has full RAM and (system) state retention. Upon waking the programming running on the microcontroller (either **main.py** or **code.py**) is resumed from the point in the code at which sleep was requested with all subsystems being operational.

<br>

- **Deepsleep**. For this mode neither RAM nor (system) state are retained by the microcontroller, this includes the state of any peripherials (i.e., the I2C sensors) or network interfaces. Essentially, upon waking the microcontrollers acts as if the reset button were pushed and starts the board's **main.py** or **code.py** from the beginning. Because of the similarity of this feature to a *reset* in MicroPython calling `machine.reset_cause()` will return `machine.DEEPSLEEP_RESET` to allow a user to distinguish this action from other reset causes. 

<br>

The commands for entering light and deep sleep differ for [C++](https://ghubcoder.github.io/posts/awaking-the-pico/), [MicroPython](http://docs.micropython.org/en/latest/library/machine.html?highlight=machine), and [CircuitPython](https://learn.adafruit.com/deep-sleep-with-circuitpython/alarms-and-sleep). 

## Micropython

### Machine module

The `machine` module in MicroPython contains a series of functions aimed at interacting with the hardware components (e.g., CPU, timers, buses, etc.) of the microcontroller. Of these, three functions will be dealt with here: `machine.lightsleep()`, `machine.deepsleep()`, and, `machine.idle()` using the following example to print a temperature reading from the RP2040: 

```{code-block} python
---
lineno-start: 1
---
""" main.py

Based upon:
    > Pimoroni's thermometer.py example
        https://github.com/pimoroni/pimoroni-pico/blob/main/micropython/examples/pico_explorer/thermometer.py
    > "Getting Started with MicroPython on the Raspberry Pi Pico" 
    

"""

# PACKAGES
import machine
import time


###########
# HARDWARE
# board temperature sensor called via analog-digital converter with ID 4
# https://docs.micropython.org/en/latest/library/machine.ADC.html
sensor_temp   = machine.ADC(4)


###########
# VARIABLES
# measurements, 
number_of_measurements = 5  # number of measurements per interval
measurement_interval   = 5  # in seconds


###########
# FUNCTIONS
def board_temperature(sensor_temp):
    """ Microcontroller board temperature
    
    # INPUT:
        sensor_temp     = temperature sensor
        
    # EXAMPLE:
        import machine
        sensor_temp   = machine.ADC(4)
        board_temperature(sensor)
    
    # REQUIREMENTS:
        import machine
        
    # REF:
        https://github.com/pimoroni/pimoroni-pico/blob/main/micropython/examples/pico_explorer/thermometer.py
        Halfacree & Everard, "Getting Started with MicroPython on the Raspberry Pi Pico", page 98
        
    """
    # conversion factor necessary to convert raw
    # reading into a temperature value
    conversion_factor = 3.3 / (65535)  
    
    # the following two lines do some maths to
    # convert the number from the temp sensor into celsius
    reading           = sensor_temp.read_u16() * conversion_factor
    temperature       = 27 - (reading - 0.706) / 0.001721
    
    return temperature


###########
# MAIN

while True:
    for measurement_number in range(number_of_measurements):
        
        # get measurement
        temperature = board_temperature(sensor_temp)
        print(temperature)
        
        # wait between measurements
        time.sleep(measurement_interval)
    
    #### SLEEP 
    time.sleep(10)

```

At every iteration the code makes five temperature measurements with a break of five seconds between them before going to sleep for 10 seconds. *For convenience, the code is condensed in the subsequent examples.*

<br>

### Light Sleep

The function `machine.lightsleep()` command with a single argument `time_ms` puts the microcontroller into a light sleep mode that reduces the supply voltage, and clock-gates any digital peripherals, CPUs, and (part of) the RAM. In digital circuits the [clock signal](https://en.wikipedia.org/wiki/Clock_signal) oscillates between high and low states allowing for actions to become coordinated, [clock-gating](https://en.wikipedia.org/wiki/Clock_gating) is the removal of this clock signal when it is not needed reducing the dynamic power of the circuit through the reduction of the switching frequency. Once light sleep is exited these various components resume their operation at the point in the code where light sleep was entered having preserved their various internal states. Hence, to make a series of measurements at regular time intervals the example code can be modified to include an indefinite loop (i.e., a `while True:` statement). The amount of time to light sleep is defined by a value in milliseconds (1 millisecond = 0.001 seconds) given by the `time_ms` argument, here the microcontroller sleeps for only 10000 milliseconds (or 10 seconds):

```{code-block} python
---
lineno-start: 1
---
""" main.py - LIGHTSLEEP example

"""
# PACKAGES
import machine
import time

###########
# HARDWARE
sensor_temp   = machine.ADC(4)

###########
# VARIABLES
number_of_measurements = 5  # number of measurements per interval
measurement_interval   = 5  # in seconds

###########
# FUNCTIONS
def board_temperature(sensor_temp):
    """ Microcontroller board temperature
    
    """
    conversion_factor = 3.3 / (65535)  
    
    reading           = sensor_temp.read_u16() * conversion_factor
    temperature       = 27 - (reading - 0.706) / 0.001721
    
    return temperature

###########
# MAIN

while True:   
    for measurement_number in range(number_of_measurements):
            
        # get measurement
        temperature = board_temperature(sensor_temp)
        print(temperature)
            
        # wait between measurements
        time.sleep(measurement_interval)

    #### SLEEP MSG
    #sleep for 10 seconds (10000 milliseconds)
    machine.lightsleep(10000)

```

<br>

### Deep Sleep

In contrast, the function `machine.deepsleep()` command also with a single argument `time_ms` puts the microcontroller into a deep sleep mode that terminates the CPU and any peripherals. Unlike the light sleep once deep sleep is exited these various components resume their operation 
as if the reset button had been pressed, i.e., at the beginning of the code. As such there is no need to include the indefinite loop (i.e., the `while True:` statement in the lightsleep example). The code will make five temperature measurements with a break of five seconds go into a deep sleep before restarting, making five temperature measurements with a break of five seconds, ..., and so on. The amount of time to deep sleep is defined by a value in milliseconds (1 millisecond = 0.001 seconds) given by the `time_ms` argument, here the microcontroller sleeps for only 10000 milliseconds (or 10 seconds):

```{code-block} python
---
lineno-start: 1
---
""" main.py - DEEPSLEEP example

"""
# PACKAGES
import machine
import time

###########
# HARDWARE
sensor_temp   = machine.ADC(4)

###########
# VARIABLES
number_of_measurements = 5  # number of measurements per interval
measurement_interval   = 5  # in seconds

###########
# FUNCTIONS
def board_temperature(sensor_temp):
    """ Microcontroller board temperature
    
    """
    conversion_factor = 3.3 / (65535)  
    
    reading           = sensor_temp.read_u16() * conversion_factor
    temperature       = 27 - (reading - 0.706) / 0.001721
    
    return temperature

###########
# MAIN
   
for measurement_number in range(number_of_measurements):
        
    # get measurement
    temperature = board_temperature(sensor_temp)
    print(temperature)
        
    # wait between measurements
    time.sleep(measurement_interval)

#### SLEEP MSG
#sleep for 10 seconds (10000 milliseconds)
machine.deepsleep(10000)

```


````{admonition} Example: deep sleep with a Pico Explorer!
:class: tip, dropdown
``` {code-block} python
---

---

""" main.py

Based upon:
    > Pimoroni's thermometer.py example
        https://github.com/pimoroni/pimoroni-pico/blob/main/micropython/examples/pico_explorer/thermometer.py
    > "Getting Started with MicroPython on the Raspberry Pi Pico" 
    

"""

# PACKAGES
import machine
import time
from picographics import PicoGraphics, DISPLAY_PICO_EXPLORER


###########
# HARDWARE
display       = PicoGraphics(display=DISPLAY_PICO_EXPLORER)

# board temperature sensor called via analog-digital converter with ID 4
# https://docs.micropython.org/en/latest/library/machine.ADC.html
sensor_temp   = machine.ADC(4)


###########
# VARIABLES
# Display variables, set up constants for drawing
WIDTH, HEIGHT          = display.get_bounds()
BLACK                  = display.create_pen(0, 0, 0)
WHITE                  = display.create_pen(255, 255, 255)

# measurements, 
number_of_measurements = 5  # number of measurements per interval
measurement_interval   = 5  # in seconds


###########
# FUNCTIONS
def board_temperature(sensor_temp):
    """ Microcontroller board temperature
    
    # INPUT:
        sensor_temp     = temperature sensor
        
    # EXAMPLE:
        import machine
        sensor_temp   = machine.ADC(4)
        board_temperature(sensor)
    
    # REQUIREMENTS:
        import machine
        
    # REF:
        https://github.com/pimoroni/pimoroni-pico/blob/main/micropython/examples/pico_explorer/thermometer.py
        Halfacree & Everard, "Getting Started with MicroPython on the Raspberry Pi Pico", page 98
        
    """
    # conversion factor necessary to convert raw
    # reading into a temperature value
    conversion_factor = 3.3 / (65535)  
    
    # the following two lines do some maths to
    # convert the number from the temp sensor into celsius
    reading           = sensor_temp.read_u16() * conversion_factor
    temperature       = 27 - (reading - 0.706) / 0.001721
    
    return temperature

    
def display_boxed_message(text, display_to_alter, text_color, background_color, resting_time = 10 ):
    """ Display a message within a box
    
    # INPUT:
        text             = message that you wish to display
        display_to_alter = display to alter, e.g., display = PicoGraphics(display=DISPLAY_PICO_EXPLORER)
        text_color       = colour of the text, e.g., BLACK = display.create_pen(0, 0, 0)
        background_color = colour of the background, e.g., WHITE = display.create_pen(255, 255, 255)
        resting_time     = time in seconds to give time for the message to be displayed 
    
    # EXAMPLE:
        import time
        from picographics import PicoGraphics, DISPLAY_PICO_EXPLORER
        display = PicoGraphics(display=DISPLAY_PICO_EXPLORER)
        BLACK   = display.create_pen(0, 0, 0)
        WHITE   = display.create_pen(255, 255, 255)
        display_boxed_message("Start sensor...", display, text_color = BLACK, background_color = WHITE)
    
    # REQUIREMENTS:
        import time
        from picographics import PicoGraphics, DISPLAY_PICO_EXPLORER
        
    # Notes:
        > display.text(text, x, y, wordwrap, scale, angle, spacing)
            # text     = string to draw
            # x, y     = X and Y coordinates
            # wordwrap = number of pixels width before trying to break text into multiple lines
            # scale    = size
            # angle    = rotation angle (Vector only!)
            # spacing  = letter spacing
    
    """

    # clear display
    display.clear()
    
    # make a background
    display.set_pen(background_color)
    display.rectangle(3, 3, 100, 25)
    
    # send message
    display.set_pen(WHITE)
    display.text("Start sensor", 3, 3, 0, 3)
    display.update()
    
    # Rest period
    time.sleep(10)
    


###########
# MAIN

display_boxed_message("Start sensor...", display, text_color = BLACK, background_color = WHITE)

for measurement_number in range(number_of_measurements):
    
    # fills the screen with black
    display.set_pen(BLACK)
    display.clear()

    # get measurement
    temperature = board_temperature(sensor_temp)
    
    # draws a white background for the text
    display.set_pen(WHITE)
    display.rectangle(1, 1, 100, 25)

    # writes the reading as text in the white rectangle
    display.set_pen(BLACK)
    display.text("{:.2f}".format(temperature) + "c", 3, 3, 0, 3)

    # time to update the display
    display.update()

    # wait between measurements
    time.sleep(measurement_interval)


#### SLEEP MSG
#sleep for 10 seconds (10000 milliseconds)
display_boxed_message("Sensor going to sleep...", display, text_color = BLACK, background_color = WHITE)
machine.deepsleep(10000)

```

````

<br>

### Idle

The function `machine.idle()` reduces the power consumption by gating the clock to the CPU. 

<br>

### Interruption

There is also the discussion regarding the possibilty to create an interupt request ([IRQ](https://en.wikipedia.org/wiki/Interrupt_request_(PC_architecture) )) from either a [Pin](https://github.com/micropython/micropython/blob/master/docs/library/machine.Pin.rst) using [Pin.irq](https://docs.micropython.org/en/latest/library/machine.Pin.html?highlight=wake) or a real time clock (RTC) using [RTC.irq](hhttps://docs.micropython.org/en/latest/library/machine.RTC.html?highlight=wake). However, at present it is still a work in progress for RP2040 microcontrollers. 


```{admonition} Further reading!
:class: tip, dropdown

Note that there is ongoing discussion with respect to the amount power consumed with Micropython during deep sleep for the Raspberry Pico as `machine.deepsleep` still pulls 25.6ma (see [here](https://ghubcoder.github.io/posts/deep-sleeping-the-pico-micropython/) ). Discussions and workarounds can be found in the following: <br> 


- Low power support involves copying `lowpower.py` from the following [github repository](https://github.com/tomjorquera/pico-micropython-lowpower-workaround).

- Discussion regarding [waking](https://ghubcoder.github.io/posts/awaking-the-pico/) and [sleeping](https://ghubcoder.github.io/posts/deep-sleeping-the-pico-micropython/)

- Tutorials for MicroPython [ESP](https://randomnerdtutorials.com/micropython-esp32-deep-sleep-wake-up-sources/)

```

<br>
<br>

````{admonition} Alternative: sleep with CircuitPython!
:class: tip, dropdown

**Alternative: CircuitPython**<br>

In contrast with [MicroPython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html), [CircuitPython](https://wur.gitlab.io/digitaltwin/docs/setup/circuitpython_setup.html) places functions to allow the microcontroller to enter deep and light [sleep](https://learn.adafruit.com/deep-sleep-with-circuitpython/alarms-and-sleep) into the `alarm` module.<br>

**Alarm module**<br>

Just like MicroPython there is a light (`alarm.light_sleep_until_alarms`) and deep sleep (`alarm.exit_and_deep_sleep_until_alarms`) as well as the option to sleep and wait for an interupt from a Pin. There is also the ability to utilise a 'backup RAM' (`alarm.sleep_memory`) that can store a finite amount of information encoded as bytes as long as power is connected. The loss of power will wipe this memory.<br> 

**Light sleep**<br>

The difference between MicroPython and CircuitPython is that an alarm must be constructed prior to entering the sleep mode using `alarm.time.TimeAlarm(monotonic_time=time.monotonic() + 10)` where `10` represents 10 seconds added to the current time `time.monotonic()`, i.e., the alarm is set for 10 seconds from now. Light sleep begins with the following command `alarm.light_sleep_until_alarms(time_alarm)`. Modifying the previous examples into CircuitPython gives:

``` {code-block} python
---
lineno-start: 1
---
""" code.py - LIGHTSLEEP example

"""
# PACKAGES
import alarm
import microcontroller
import time

###########
# HARDWARE

###########
# VARIABLES
number_of_measurements = 5  # number of measurements per interval
measurement_interval   = 5  # in seconds

###########
# FUNCTIONS

###########
# MAIN

while True:   
    for measurement_number in range(number_of_measurements):
            
        # get measurement
        temperature = microcontroller.cpu.temperature
        print(temperature)
            
        # wait between measurements
        time.sleep(measurement_interval)

    #### SLEEP 
    # Create alarm with a 10 seconda trigger (from now).
    time_alarm = alarm.time.TimeAlarm(monotonic_time=time.monotonic() + 10)

    # Begin alarm
    alarm.light_sleep_until_alarms(time_alarm)
```

**Deep sleep**<br>

Likewise, the deep sleep mode also requires the alarm to be constructed (`alarm.time.TimeAlarm(monotonic_time=time.monotonic() + 10)`) prior to the variable it is assigned to being called. Once more modifying the previous examples into CircuitPython gives:

``` {code-block} python
---
lineno-start: 1
---
""" code.py - DEEPSLEEP example

"""
# PACKAGES
import alarm
import microcontroller
import time

###########
# HARDWARE

###########
# VARIABLES
number_of_measurements = 5  # number of measurements per interval
measurement_interval   = 5  # in seconds

###########
# FUNCTIONS

###########
# MAIN
 
for measurement_number in range(number_of_measurements):
            
    # get measurement
    temperature = microcontroller.cpu.temperature
    print(temperature)
            
    # wait between measurements
    time.sleep(measurement_interval)

#### SLEEP 
# Create alarm with a 10 seconda trigger (from now).
time_alarm = alarm.time.TimeAlarm(monotonic_time=time.monotonic() + 10)

# Begin alarm
alarm.exit_and_deep_sleep_until_alarms(time_alarm)
```
<br>

**Interrupt**<br>

CircuitPython also allows for a Pin alarm to be called, by replacing the `time_alarm` with `pin_alarm` like so:

``` {code-block} python
---
lineno-start: 32
---

#### SLEEP 
# Create alarm that occurs when button D11 is pressed 
pin_alarm = alarm.pin.PinAlarm(pin=board.D11, value=False, pull=True)

# Begin alarm
alarm.exit_and_deep_sleep_until_alarms(pin_alarm)

```

The microcontroller can be awoken from the associated sleep mode. 

````
