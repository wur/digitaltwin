# Metadata

```{figure} /images/metadata.jpg
---

name: metadata_figure-fig
---
**The relationships between data and Metadata.**
```

## What is Metadata?

In the previous module storing data was discussed, whilst storing the data is important it is equally important to ensure that the stored data has meaning. Consider for the moment the following:

```{code-block} python
---

---
current_data = ["400", "22.5", "27.35"]

```
<br/>

Can you derive any meaning of substance from this line of code? Or do you need more information? What sort of information do you need? Maybe you need answers to a series of questions, for instance: *What exactly do these three numbers mean? How should we interpret them? Where, are they from (e.g., geographically, temporally, provenance, etc.)? When were they collected? How were they collected? Who collected them? For what purpose? Who is responsible for this data?*

Essentially you need some data about this data otherwise you have a collection of numbers ({numref}`metadata_figure-fig`). That is both the definition and purpose of **Metadata**. Metadata's role is **to giving meaning through documenting objects (e.g., data, models, processes, etc.) so as to preserve understanding, convey pertinent information, facilitate usage, and to record the history of an object (e.g., change, modification, deletion, etc.)**. 

<br/>

## Why Metadata?
Done well metadata should provide the essentials from which an object can be understood, interpreted, and used through adding contextual information. Metadata must also describe the data, where it can be found, who made it, and for what purpose. 

As such metadata is one step on the road to transparancy and trust, can help to reduce and remove data silos by making data findable, conveying how and by who it can be accessed, and increase both scalability and flexibility. 

```{admonition} FAIR and Open data!
:class: tip, dropdown

Metadata feeds into both **Open data** - data that can be 'freely used' and made available in accessible format (i.e., machine-readable format) - and FAIR data standards. FAIR being an approach that aims (ultimately) to make data more reuseable: <br>

- **Findable**. The Data must be discoverable, being stored in a (public) repository, have an associated identifier (e.g., **doi**), have rich, clear, indexed metadata.<br>
- **Accessible**. The knowledge of how to access the resource is known (including authentication and authorisation) or the metadata is at minimum available.<br>
- **Interoperable**. The data is operable for analysis, storage, and processing using a variety of applications.<br>
- **Reusable**. The data is well described in such a way as to replicable.<br>

See Go-FAIR for further [information](https://www.go-fair.org/fair-principles/)
```

<br/>

## Types of Metadata

Note that metadata can be used to describe an individual datum, a part of a single dataset, the whole dataset, or a collection of datasets. However, there are generally speaking three 'higher level' types of Metadata that reflects the purpose of the metadata. These types of Metadata are: Metadata that is descriptive, Metadata that is administrative, and Metadata that references structure:

- **Descriptive metadata**. Descriptive metadata are metadata that facilitates the discovery, identification, and selection of data.

- **Administrative metadata**. Administrative metadata are metadata that ensures the (correct) management of resources. Administrative metadata can be further subdivided into: *Technical metadata* that is metadata required to open or operate data (e.g., file format); *Rights metadata* which are metadata necessary for compliance with data sharing, interlectual property, licensing, and user access; and *Preservation metadata* which are metadata required to ensure data remains accessible, viable, and useable.

- **Structural Metadata**. Metadata that facilitates bringing together components.

<br/>

## Metadata Elements

When collecting metadata about an object you may want a particular set of information. For instance, the name of a dataset or when it was created. Instead of long winded questions we may use specific words or phrases ('Title', 'Creation date') that can be collected together to give greater meanings. These are referred to as **terms**, or **metadata elements** and relate to concepts and relationships for particular areas of interest. These terms are collected into **vocabularies** when simple or informal. Or **ontologies** when complex or formal. From a (controlled) vocabularly or ontology, inference, the basic building block of *semantics* (i.e., knowledge), can be made. Such an approach makes metadata a form of structured data.  

<br/>

## Metadata Standards

 Consider that the usefulness of any 'question' and 'answer' is only apparent if it is understandable to all users (e.g., software and people), therefore many organisations find it prudent to predefine and publish metadata sets for particular needs {cite}`Riley2017`. Defining what a metadata element or term means, when it should be used, how the answer should appear, and what potential values it may hold is important for consistency between answers. These are referred to as **metadata standards**, of which there are four types:

<br/>

- **Structural standards**. What metadata elements or terms are required for a particular purpose. 

- **Content standards**. What input is needed for a particular metadata element or term. This can be further refined by *value standards*.

- **Value standards**. The limits of the potential values such metadata elements or terms may have.

- **Format standards**. The technical specifications.

<br>

It is important to consider the fact that any (collected) metadata will be a reflection of the, or a, community (i.e., users, developers, etc.). Which means that including existing metadata approaches and standards will also include any biases associated with that community. For instance, a focus on the English language and pecularities of its semantics or the focus on acquiring metadata only for a paticular resource giving rise to questions of suitability of such standards to other resources (i.e., a lack of generalisation). 

<br/>



## Storing and sharing Metadata

```{figure} /images/metadata_tree_graph_datamodel.jpg
---

name: metadata_tree_graph_datamodel-fig
---
**Tree vs Graph data models.** *Both Tree and Graph data models are abstract representatives of non-linear data structures. (1) Tree data models are hierarchical structures where a single path links to a node. The basic structural components of the tree are a **root** (initial item), **node** (item in the tree), and **edge** (connection between two nodes, a tree has n-1 edges). These can be further distinguished as a leaf (terminal, childless, node), parent (a node's predecessor), and child (a node's descendant). (2) Graph data model are less hierarchical being a collection of nodes or vertices connected via edges. Depending on whether the edge for both data models has direction or not confers on the data model the term directed or undirected.*  

```


Once metadata elements and standards are chosen or devised the metadata is collected which means it needs to be stored. Metadata can be stored in a variety of formats, however it is often stored in a database or alongside/inside the file itself as an embedding (e.g., [JPEG2000](https://en.wikipedia.org/wiki/JPEG_2000#Metadata) using [XML](https://en.wikipedia.org/wiki/XML) ). Or, a mixture of the two. There are Pro's and Con's of both approaches, storing metadata alongside the file ensures that metadata is stored alongside the resource, whereas in a database the metadata becomes queryable. 

Sharing metadata requires a format that can be imported and exported. The two most common formats for sharing metadata are Extensible Markup Language ([XML](https://en.wikipedia.org/wiki/XML) ) and Resource Description Framework ([RDF](https://en.wikipedia.org/wiki/Resource_Description_Framework) ). The difference between the two being how the information is simulated. Either as a tree (XML), where information is represented as a hierarchical tree diverging from a root node with subtrees branching off along a single path from parent nodes to form children. To interograte such a tree requires knowledge of the hierarchy. Or as a graph (RDF) where data is connected to other pieces of information, where no vertice has primary importance ( {numref}`metadata_tree_graph_datamodel-fig` ). 

<br>

### Metadata: XML

Extensible [Markup](https://en.wikipedia.org/wiki/Markup_language) Language ([XML](https://en.wikipedia.org/wiki/XML) ) has similarity to HyperText Markup Language ([HTML](https://en.wikipedia.org/wiki/HTML) ) with both having *tags* (start tag: `<start>`, end tag: `</start >`, ) to denote *elements* and between those element's start and end tag the *content* (or data). Using the same terms as the Tree data structure example ( {numref}`metadata_tree_graph_datamodel-fig` ), metadata encoded within the leaf node labelled as E could be represented as: 

```{code} html
---
---

<metadata_A>
    <metadata_B>
    <metadata_E>data</metadata_E>
    </metadata_B>
</metadata_A>

```
Where each tag represents a specific metadata category (e.g., `<format>JPEG</format>`, `<creator>WUR</creator>`, etc.).

<br>

### Metadata: RDF

On the other hand the Resource Description Framework ([RDF](https://en.wikipedia.org/wiki/Resource_Description_Framework) ) is a framework for describing a *resource* - an object that can be identified with a unique sequence of characters or Universal Resource Indicator (URI) - that can incorporate XML. Within RDF resources are expressed by a *triple*, three components containing a subject–predicate–object. These components can also be referred to as resource-property-literal. 


```{figure} /images/metadata_RDF.jpg
---

name: metadata_RDF-fig
---
**Resource Description Framework (RDF).** *(A) Node and directed-arc diagram. In a subject–predicate–object triple the subject and objects are denoted by nodes whilst the arc, or edge, represents the predicate. The direction is always from the **subject** node to **object** node. (B) Natural language statements, in English, regarding the index page of this document using specific metadata terms defined by the [Dublin Core](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) schema. Note that https://wur.gitlab.io/digitaltwin/docs/index.html  has been shortened to ...index.html. (C) An RDF Graph Describing the index page of this document and representing each statement in (B) as a graph. Nodes that are URIrefs are shown as ellipses, while nodes that are literals (constant values) are shown as boxes. Note that predicates (or properties) have been also been assigned URIrefs based upon the [Dublin Core](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) schema. Figure based upon the [W3C RDF Primer](https://www.w3.org/TR/rdf-primer/).*  

```


The **subject** denotes the resource, i.e., what it is about, which has a relationship, the **predicate**, to an **object**, i.e., the thing it is related to. The predicate can denote 
an aspect or trait of resource that can be used to express a relationship between the subject and the object. For instance, a book can have a creation date, a creator, a language all of which provide linkage between the resource (the book) and the objects (date, author, published language). In plain English a series of RDF statements regarding this JupyterBook would be ({numref}`metadata_RDF-fig` ):

```{code} 
---

---

https://wur.gitlab.io/digitaltwin/docs/index.html has a creation-date whose value is April 15, 2022
https://wur.gitlab.io/digitaltwin/docs/index.html has a language whose value is English
https://wur.gitlab.io/digitaltwin/docs/index.html has a creator whose value is Jasper Koehorst
https://wur.gitlab.io/digitaltwin/docs/index.html has a contributor whose value is Brett Metcalfe

```
For the first line, the RDF statement has a **subject** (`https://wur.gitlab.io/digitaltwin/docs/index.html`), **predicate** (`creation-date`), and an **object** (April 15, 2022). Each line seperates out a simple statement regarding the same resource (`https://wur.gitlab.io/digitaltwin/docs/index.html`) that follows the structure "**subject** has a **predicate** whose value is **object**". 

Objects that are [literals](https://www.w3.org/TR/2004/REC-rdf-concepts-20040210/#section-Literals), i.e., *constant values*, can be **plain literals**, being a string with an optional language tag, or **typed literals**, a string with a datatype URI. A **Uniform Resource Identifier** (URI) is a global identifier that can be used in machine-processable statements usable for [Linked data](https://www.w3.org/standards/semanticweb/data). URI's can be things but they can also represent abstract concepts (e.g., creator) allowing for each metadata term to be identifiable with a URL. For instance, it is possible to replace the Objects creator and contributor with URIrefs like so:


```{code} 
---

---

...index.html has a creation-date whose value is April 15, 2022
...index.html has a language whose value is English
...index.html has a creator whose value is https://orcid.org/0000-0001-8172-8981
...index.html has a contributor whose value is https://orcid.org/0000-0002-5873-9815

```

These statements can be written down into a condensed form also called a triple containing the `<subject> <predicate> <object>`. Continuing our example, our example in triple notation would be:

```{code-block} xml 
---

---
<https://wur.gitlab.io/digitaltwin/docs/index.html> <http://purl.org/dc/terms/creation-date> "April 15, 2022"
<https://wur.gitlab.io/digitaltwin/docs/index.html> <http://purl.org/dc/elements/1.1/language> "en"
<https://wur.gitlab.io/digitaltwin/docs/index.html> <http://purl.org/dc/elements/1.1/creator> <https://orcid.org/0000-0001-8172-8981>
<https://wur.gitlab.io/digitaltwin/docs/index.html> <http://purl.org/dc/elements/1.1/contributor> <https://orcid.org/0000-0002-5873-9815>

```

This is somewhat lengthy especially as there is much repetition between these statements and it is possible to shorten these statements using a shorthand which substitues it for a **QName** or **XML qualified name**. These QNames are an abbreviation made up of a prefix that is mapped to a namespace URI, followed by a colon, and a value that represents the local name. For instance, the prefix `dc:` can be assigned the namespace URI http://purl.org/dc/elements/1.1/ making `dc:creator` shorthand for the longer, fuller, URI reference http://purl.org/dc/elements/1.1/creator. Using this shorthand and following the convention that QNames appear without the `< >` our triples can be represented as:

```{code} 
---

---

wur:index.html  dcterms:creation-date  "April 15, 2022"
wur:index.html  dc:language            "en"
wur:index.html  dc:creator             orcid:0000-0001-8172-8981
wur:index.html  dc:contributor         orcid:0000-0002-5873-9815
    
prefix wur: represents namespace URI https://wur.gitlab.io/digitaltwin/docs/
prefix dc: represents namespace URI http://purl.org/dc/elements/1.1/
prefix dcterms: represents namespace URI http://purl.org/dc/terms/
prefix orcid: represents namespace URI https://orcid.org/

```

These URIrefs act as a vocabularly and can be conveniently arranged into a common prefix. For instance, within this book it is possible to add an additional page https://wur.gitlab.io/digitaltwin/terms/ that could act as our common URIref that is represented by our prefix `wur:` and subsequently appending local names that terms utilised by this project. From which metadata can be encoded about our project. Although mixing and matching vocabularies, as our example highlights, is allowed. 

More information on RDF, including expanding the objects to incorporate the data type (typed literals), can be found in the World Wide Web Consortium's (W3C) [Primer on RDF](https://www.w3.org/TR/rdf-primer/).  


```{admonition} Linked data!
:class: tip, dropdown

**Linked Data** is **structured data** - data that neatly fits within fixed fields - that is interlinked with other data, the purpose is for such data to allow for the drawing of inference through queries making the data useful.

See the World Wide Web Consortium (W3C) for further [information](https://www.w3.org/DesignIssues/Linkeddata.html) on [Linked data](https://www.w3.org/standards/semanticweb/data).

```
<br>


### Metadata: RDF/XML

RDF also provides a XML syntax for both the communication, storage, and exchange of RDF graphs, referred to as RDF/XML. Putting our previous example into this format it becomes: 

```{code-block} xml
---
lineno-start: 1
---
<?xml version="1.0"?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
             xmlns:dc="http://purl.org/dc/elements/1.1/"
             xmlns:dcterms="http://purl.org/dc/terms/">

    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dcterms:created>2022-04-15</dcterms:created>
    </rdf:Description>

    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dc:language>en</dc:language>
    </rdf:Description>

    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dc:creator rdf:resource="https://orcid.org/0000-0001-8172-8981"/>
    </rdf:Description>

    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dc:contributor rdf:resource="https://orcid.org/0000-0002-5873-9815"/>
    </rdf:Description>

</rdf:RDF>
```

Let us break this down. The code begins (line's 1 to 3) by indicating the following is RDF/XML content and the defined namespaces being utilised within the content. This begins on line number 1 with `<?xml version="1.0"?>` the XML declaration, informing that the following lines are in XML, as well as the version of XML used. On line 2 an XML element that represents RDF is begun with a beginning (`<rdf:RDF>`) tag, its ending (`</rdf:RDF>`) sits on line 22. The beginning tag (`<rdf:RDF`) has a series of name-value pairs (or *attributes*), these are [XML namespace](https://en.wikipedia.org/wiki/XML_namespace) declarations in the format `xlmns:prefix=value`. Meaning that any name beginning with the prefix is mapped to the prefixes namespace. A sidepoint, it is possible to declare a default namespace, `xlmns=value`, meaning that any element without a namespace prefix is by default considered to be in the values namespace. 

In this example, line 2 has an `xmlns` attribute of the `rdf:RDF` start-tag that declares all tags in the following content that have been prefixed with `rdf:` are part of the namespace identified by the URIref http://www.w3.org/1999/02/22-rdf-syntax-ns#. The `rdf:` vocabularly and `rdfs:` vocubarly are vocubarlies of terms that have special significance to RDF, these prefixes representing http://www.w3.org/1999/02/22-rdf-syntax-ns# and http://www.w3.org/2000/01/rdf-schema# respectively. Likewise, in line 3 all tags prefixed with `dc` are part of the [Dublin Core DCMI terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/) belonging to the `/elements/1.1/` [namespace](http://purl.org/dc/elements/1.1/). An additional DCMI namespace is called on line 4, those prefixed with `dcterms` reflect new Dublin Core terms belonging to the `/terms/` [namespace](http://purl.org/dc/terms/). Alongside this, on line 4 the `rdf:RDF` start-tag is closed with the addition of `>` at the end.

Lines 6 to 20 represent the RDF/XML for the four statements about our resource, each statement is presented separately beginning with the `rdf:Description` tag. A statement is a *description* (`rdf:Description`), and this description is *about* (`rdf:about`) something, therefore our subject is encoded as `<rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">`. Nested between the beginning (`<rdf:Description>`) and ending (`</rdf:Description>`) tags is the property element with it's content generally being the object. For example, line 7 is a property element with the QName `dcterms:created` being its associated tag, the content of this element is the object,a plain literal `2022-04-15`. Converting Line 8 into a typed literal necessitates adding an attribute `rdf:datatype` to the `dcterms:created` tag: 

```{code-block} xml
---
lineno-start: 6
---
    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dcterms:created rdf:datatype=
         "http://www.w3.org/2001/XMLSchema#date">2022-04-15
        </dcterms:created>
    </rdf:Description>
```


````{admonition} Further abbreviations: XML entity
:class: tip, dropdown

Using URIrefs as attribute values within RDF/XML requires them to be written out in full, for instance, adding the datatype to the `dcterms:created` in RDF/XML would be done in the following fashion `<dcterms:created rdf:datatype="http://www.w3.org/2001/XMLSchema#date">2022-04-15</dcterms:created>`. However, this is not very readable. Abbreviations can be used by making a XML entity declaration that maps a term to a string and replaces any references to it with that string. The `ENTITY` declaration allows for abbreviation to the URIref namespace for XML Schema datatypes:

```{code-block} xml
<!DOCTYPE rdf:RDF [<!ENTITY xsd "http://www.w3.org/2001/XMLSchema#">]>
```
Thereafter, the entity reference `&xsd;` can be used to abbreviate "http://www.w3.org/2001/XMLSchema#" just like a RDF/XML prefix:

```{code-block} xml
---
lineno-start: 1
---
<?xml version="1.0"?>
<!DOCTYPE rdf:RDF [<!ENTITY xsd "http://www.w3.org/2001/XMLSchema#">]>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
             xmlns:dc="http://purl.org/dc/elements/1.1/"
             xmlns:dcterms="http://purl.org/dc/terms/">

    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dcterms:created rdf:datatype="&xsd;date">2022-04-15
        </dcterms:created>
    </rdf:Description>

    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dc:language>en</dc:language>
    </rdf:Description>

    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dc:creator rdf:resource="https://orcid.org/0000-0001-8172-8981"/>
    </rdf:Description>

    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dc:contributor rdf:resource="https://orcid.org/0000-0002-5873-9815"/>
    </rdf:Description>

</rdf:RDF>
```
````

For some of our statements (`creator` and `contributor`) the object can be represented as its own resource (`rdf:resource`) given that [ORCID](https://orcid.org/) provides a persistent digital identifier for researchers called an ORCID iD that [supports](https://info.orcid.org/faq/orcid-schema-org-linked-open-data-and-json-ld/) RDF/XML. However, a resource necessitates being written out in full, i.e., without prefixes, and uses a different XML tag called an *empty-element* tag that lacks a separate end tag:

```{code-block} xml
---
lineno-start: 14
---
    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dc:creator rdf:resource="https://orcid.org/0000-0001-8172-8981"/>
    </rdf:Description>

```

The statements property elements on line's 6 to 8 (`dcterms:created`), 10 to 12 (`dc:language`), 14 to 16 (`dc:creator`), 18 to 20 (`dc:contributor`) can be nested within a single element, reducing the previous examples into the following:

```{code-block} xml
---
lineno-start: 1
---
<?xml version="1.0"?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
             xmlns:dc="http://purl.org/dc/elements/1.1/"
             xmlns:dcterms="http://purl.org/dc/terms/">

    <rdf:Description rdf:about="https://wur.gitlab.io/digitaltwin/docs/index.html">
        <dcterms:created>2022-04-15</dcterms:created>
        <dc:language>en</dc:language>
        <dc:creator rdf:resource="https://orcid.org/0000-0001-8172-8981"/>
        <dc:contributor rdf:resource="https://orcid.org/0000-0002-5873-9815"/>
    </rdf:Description>

</rdf:RDF>
```

<br>

## Metadata for our sensor


*So what does this mean for our program?* Well let us imagine that the data collection is done by ourselves, in a *lab book* alongside the data you would need to note down pertinent details that are essential for writing up the data. You would need information about what equipment was used, how the measurements were made, and what units, precision, etc. For our sensors (as well as models, digital twin, etc) it is crucial to give information that will be of use of operators and applications, i.e., what the sensor does, where it is, who owns it and it's data


So for our sensor the (meta)data that we collect could include (i.e., {cite}`Milenkovic2020`):

- **The measurements**. Information regarding the data collected. For instance, variable names, type of measurement, range in values, resolution and sampling rate, frequency of reporting, units of measurement, timestamp,  measurement device used, etc.

- **The sensor**. Information regarding the sensor used for data collection. For instance, a unique identifier, type, model, manufacturer, etc.

- **Sensor characteristics**. Information regarding the operating conditions, differs from sensor information in that this information relates to the sensors operation. For instance,precision, accuracy, range, measurement frequency/sampling rate, correction or alteration to raw measurement, etc.

- **Asset Management**. Information regarding the asset itself. For instance, the firmware, (configuration) settings, calibration, status (e.g., measuring, not measuring), type, model, manufacturer, access rights, ownership, etc.

- **The Location**. Information regarding the location of the sensor, for instance physical location, timezone, etc. 

<br/>




```{admonition} Further reading!
:class: tip, dropdown

- RDF metadata primer. Accesible [here](https://www.w3.org/TR/rdf-primer/).

- Schema.org a collaboration to create, maintain, and promote schemas for structured data on the Internet and beyond.  Accessible [here](https://schema.org/).

- RDA Metadata Standards Catalog, a collborative, open directory of metadata standards for research data. Accessible [here](https://rdamsc.bath.ac.uk/).

- Metadata Guide. Accessible [here](https://www.jisc.ac.uk/full-guide/metadata)

- FAIR data standards. Accessible [here](https://www.go-fair.org/fair-principles/)

```
