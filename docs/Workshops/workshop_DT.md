# Sensor Workshop

Welcome to the online repository for the 14th December 2022 **Build your own digital twin with sensors** Masterclass Workshop of the [Digital Twins WUR](https://www.wur.nl/en/research-results/research-programmes/research-investment-programmes/digital-twins/conference-digital-twins-at-wur.htm): Bringing Digital Twins to Life Conference! For the workshop the Raspberry Pi Pico W microcontrollers have been pre-loaded with the code to make them work and you **just need to follow the build instructions**. However, to allow you to repeat what the content of the workshop this page has the code and instructions to do just that. You can find various [MicroPython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html) scripts that can make your [microcontroller](https://wur.gitlab.io/digitaltwin/docs/setup/microcontrollers.html) work, utilise the various components and sensors, as well as make it talk to various other devices. Let us start by discussing what the goal of this project will be.
<br><br>

## Goal

Discussing air quality, especially poor quality air quality, one may assume that the discussion reflects the outdoor urban environment and various associated risks (e.g., traffic, factories, ...). Yet indoor environments such as your home or office can also have poor air quality due to various exacerbating factors (e.g., age, layout, ventilation,... ). The goal of the workshop is to build a sensor for air quality ($CO_{2}$, VOC), temperature, and relative humidity using a Raspberry Pico W. Determining Indoor Air Quality has implications for our short- and long-term health. 
<br><br>

## What do you have?

```{figure} /images/Workshops_Sensor_components.jpg
---

name: workshop_sensor_components-fig
---
**Components in your sensor box.** 
```

In your sensor box you will find some of the following components:

- A **Raspberry Pico W** with headers
- A MicroUSB power cable (5.1V/2.5A)
- 1.12" Mono **OLED** display
- **BME688** 4 in 1 Air Quality 'breakout'
- An 830 point **breadboard** 
- various cables (e.g., male-female and male-male jumper wires and STEMMA QT/QWIC JST SH-4-pin)

In addition there are:
<br>
- A **LoRa** module (868 MHz)
- A **PIR motion** sensor
- **SGP30** Air quality 'breakout'
<br><br>

## Wiring things up
### Raspberry Pico

```{figure} /images/pinout_raspberry_pico_W_including_logo.jpg
---

name: workshop_sensor_pico-fig
---
**Pinout of Raspberry Pico W.** The [Pinout](https://datasheets.raspberrypi.com/picow/PicoW-A4-Pinout.pdf) is copyright © 2020-2021 Raspberry Pi (Trading) Ltd licensed under a Creative Commons [Attribution-NoDerivatives 4.0](https://creativecommons.org/licenses/by-nd/4.0/) International (CC BY-ND) licence.
```
The 'brains' and 'mouth' of our sensor is a Raspberry Pico W microcontroller. The *brain* is a RP2040 chip that has two ARM Cortex-M0+ cores clocked at 133MHz with 256KB RAM combined with 2 MB of QSPI Flash memory. The *mouth* is an Infineon CYW43439 wireless chip that enables the board to communicate via wireless (IEEE 802.11 b/g/n wireless LAN) or bluetooth (v5.2). To connect to peripherials there are 40 pins/castellated edges divided between: 26 general purpose input/output (GPIO) pins; 8 ground (GND) pins; and 4 pins related to power.  

The sensor will communicate with the microcontroller using I2C buses. On the pinout (light blue in {numref}`workshop_sensor_pico-fig`) it is apparent that with the Pico W that you can connect a sensor using I2C with the majority of the GP pins. The pins are in pairs with a repeating pattern of SDA and SCL pins - which stand for serial data and serial clock - next to one another. Notice also that there is a 0 and 1 after I2C (I2C0 and I2C1) that is because on the Pico W there are two I2C buses. *What does this mean?* You can only connect I2C devices to two places on the board, although you can chain the sensors together in a series so that you can have more than two sensors at any one time. If you connect a sensor or a series of sensors to pins allocated to I2C1 then when you wish to connect directly to the Pico another I2C device then you must use pins allocated to I2C0 (and vice versa).

### I2C Sensors

```{figure} /images/Workshop_i2c_breakout.jpg
---

name: workshop_i2c_sensor_breakout-fig
---
**I2C Breakout extender** (A-C) Some I2C sensors can be inserted into a breakout extender. (D) Additional sensors can then be connected (E) via a QT/ST port. Or via connecting an additional breakout extender in a series (not shown here).
```

```{figure} /images/Workshop_i2c_stemma.jpg
---

name: workshop_i2c_sensor_stemma-fig
---
**I2C Stemma QT** (A) Other  I2C sensors can be connected using STEMMA QT/QWIC JST SH-4-pin wires that either have (B) a male DuPont cable or (D-E) have the same connector on either end. These sensors need to have a special QT/ST port to make a connection.
```


### Breadboard 
The microcontroller and the various sensors and components can be arranged and wired up onto the (830 point) breadboard without requiring you to solder the components together. A breadboard has four sets of holes: two outer sets each comprising two rows that are called rails and two inner sets of 5 rows and 63 columns. The rails run horizontally and all the holes in a row of a rail are connected. These can be used for power (+) and ground (-) and can distribute that across the breadboard allowing for components to be spaced out. 

```{figure} /images/Workshop_Components_part_1.jpg
---

name: workshop_sensor_fritz_1-fig
---
**Step 1: Breadboard.** The first step is to connect the ground and power rails of the breadboard to power and ground pins of the Raspberry Pico W.
```

In contrast, the two inner sets of holes are not connected along the row but along the column. Within a set anything plugged into the same column will be connected so if you connect one hole in a column to ground the remaining 4 holes can be used to connect to ground. The two sets are divided by a deep groove and are not connected without first bridging the gap with a jumper cable. 
<br><br>


### Build Instructions
The various components can be connected to each other and the microcontroller via jumper cables, a STEMMA QT cable via the attached plugs, and/or by slotting the component into a breakout extender. 
<br><br>

```{figure} /images/Workshop_Components_part_2.jpg
---

name: workshop_sensor_fritz_2-fig
---
**Step 2: I2C Bus 0.** Next step is to wire up the I2C bus 0 of the Raspberry Pico W. It needs power (red), ground (black), SDA for serial data (blue) and SCL for serial clock (green). The SDA and SCL in our example is the 9th and 10th pins also referred to as GP6 and GP7.
```
<br>

```{figure} /images/Workshop_Components_part_3.jpg
---

name: workshop_sensor_fritz_3-fig
---
**Step 3: Connect the first breakout extender.** Next, the first breakout extender needs to be slotted into the breadboard. The first i2c device is our **OLED display**, slot it into the extender ensuring that the it goes in the correct way by looking at the side with the pin labels.
```
<br>

```{figure} /images/Workshop_Components_part_4.jpg
---

name: workshop_sensor_fritz_4-fig
---
**Step 4: I2C Bus 1.** Having completed the first i2c bus the next steps involve setting up the second bus. Once again it needs power (red), ground (black), SDA for serial data (blue) and SCL for serial clock (green). The SDA and SCL in our example is the 11th and 12th pins also referred to as GP8 and GP9.
```

<br>
You can connect the sensors in one of two ways via another breakout extender ( {numref}`workshop_i2c_sensor_breakout-fig` ) or with a different type of cables ( {numref}`workshop_i2c_sensor_stemma-fig` ). If the second breakout extender will be used follow step 5a. Alternatively, if the DuPont to STEMMA QT/JST male-to-female connectors will be used then use step 5b .

```{figure} /images/Workshop_Components_part_5.jpg
---

name: workshop_sensor_fritz_5a-fig
---
**Step 5a: Connect the second breakout extender.** Just like step 3 the second breakout extender needs to be slotted into the breadboard. The second i2c devices are our sensors, slot it into the extender ensuring that the it goes in the correct way by looking at the side with the pin labels. Slot in the BME688 sensor and then connect the SGP30 as per {numref}`workshop_i2c_sensor_breakout-fig`. 
```

<br>

```{figure} /images/Workshop_Components_part_5c.jpg
---

name: workshop_sensor_fritz_5b-fig
---
**Step 5b: Connect the second I2C using a Stemma QT cable.** Using the DuPont male to male Stemma QT connector connect the BME688 sensor to the board. The wires are connected as follows, Red is power, Blue is SDA, Yellow is SCL, and Black is Ground.  
```

### Final Product

```{figure} /images/Workshops_Sensor_final.jpg
---

name: workshop_sensor_final-fig
---
**Final Product**: The final product should look like the following. 
```

## Not working?

### Wiring
Make sure that the board is wired up as per the instructions, i.e., the position of the display and sensor.

### Packages and main
Make sure that the code is saved as `main.py` including the `.py` extension. Next check that the additional packages `sh1107.py` and `framebuf2.py` have been added to the board by copying and pasting the code into a new file and saving as `sh1107.py` and/or `framebuf2.py`.

### I2C error
If there is an error with the I2C first make sure that the SDA and SCL wires are arranged correctly. Next check that the OLED is in the I2C-0 and the sensors in I2C-1 the code above has told the microcontroller that the OLED can be found on I2C-0 and the sensors on I2C-1. If it still doesn't work make sure that the wires in the breadboard match the positions of the sensor, i.e., line up the sensor's 2-5v with the power pin. If it still doesn't work then change the I2C address variable (`i2cAddr`) on the following line bme688 = KitronikBME688.KitronikBME688(i2cAddr=0x76, sda=8, scl=9) from 0x76 to 0x77. The address of the sensor and its alternative is written on the sensor, its 0x.. followed by two numbers. 

### OS error
If there is an OS error 12 or 28 then there is no space on the drive, remove large data files.

### Constant CO2 / TVOC values

First breathe close to the BME688 sensor, if its still not recording anything then unplug the STEMMA QT cable and try inserting it into the other port and check again. 

<br><br>

**For the workshop you DO NOT need to perform the next steps. The following allows you to repeat the Workshop whenever you wish to.**

<br><br>

## MicroPython
Once wired together the sensors and components can be controlled using the programming language [MicroPython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html). For instance, an unstable release of MicroPython that contains the drivers for wireless communication (version: `rp2-pico-w-20221201-unstable-v1.19.1-724-gfb7d211153.uf2`) can be downloaded as a `.uf2` file from the Raspberry Pico's [documentation](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html). To install MicroPython perform the following. Hold down the Bootsel button of a disconnected Pico board and whilst continuing to hold it down plug it into a computer via the Pico's micro-USB port. Download the `.uf2` file before dragging and dropping it onto the RPI-RP2 drive that appears. The Pico should reboot and MicroPython will have been installed.

Like 'regular' Python additional packages can be downloaded using a package manager. Depending on its version, for MicroPython, the `mip` or `upip` packages act as a package manager. As long as the board is connected to WiFi (i.e., via the `network` package) a new package (e.g., `umqtt.simple`) can be installed like so:

```{code}
import network
import mip      # or upip
from secrets import secrets

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(secrets['ssid'], secrets['ssid_password'])

mip.install('umqtt.simple')  # or upip.install('umqtt.simple')

```

Where, `secrets` is a file written by the user containing a WiFi SSID and SSID Password using the following dictionary format:

```{code}
""" secrets.py: File for secret settings

    CONTAINS:
        secrets = dictionary with WIFI
"""
secrets = {
            'ssid'          : 'wifi name',
            'ssid_password' : 'wifi password',
            }
```

As packages are installed a 'lib' directory will appear, so for the example above a sub-directory, 'umqtt', will appear once the `mip.install('umqtt.simple')` command is run. To save time the boards used in this course have been pre-loaded with the drivers for various breakouts using a version of Micropython (`pimoroni-picow-v1.19.10-micropython.uf2`) that can be downloaded from Pimoroni's [GitHub](https://github.com/pimoroni/pimoroni-pico/releases/tag/v1.19.10).

Code can be written either in a text editor (e.g., notepad) or via an interactive developer environment (IDE) such as [Thonny](https://thonny.org/) or [Mu](https://codewith.mu/). To have the code run once the board is powered save the file as `main.py`. For convenience, the code snippets found in the [appendix](https://wur.gitlab.io/digitaltwin/docs/Workshops/workshop_biotech.html#appendix) can be used to control the various sensors independent of the others. 
<br><br>


## Code

Begin by modifying the `secrets` file by adding a dictionary (`board`) within the dictionary (`secrets`) that contains a unique ID (`name`) for the microcontroller and the attached `sensor`. Note that if you want to add multiple sensors make this into a list (`sensor : ['sensor 1', 'sensor 2', 'so forth']`) and then add a line that unpack them into a single string. Following that make another dictionary (`influxdb`) within the dictionary (`secrets`) that contains the information required to send data to [InfluxDB](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/cloud_storage.html) via [HTTP](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/storage_alternative.html) (`url`, `token`, `bucket`, and `org`). The `secrets` file should look like the following:


```{code}
""" secrets.py: File for secret settings

    CONTAINS:
        secrets = dictionary with WIFI
"""
secrets = {
            'board'         : {
                                'name'   : ' UNIQUE MICROCONTROLLER NAME ',
                                'sensor' : 'BME688',
                                },
            'ssid'          : ' WiFI SSID ',
            'ssid_password' : ' WIFI PASSWORD ',
            'influxdb'        : {
                                'url'           : ' URL ',
                                'token'         : ' TOKEN ',
                                'bucket'        : ' BUCKET ',
                                'org'           : ' ORGANISATION ',
                            }
          }
```

Next add the [KitronikBME688](https://wur.gitlab.io/digitaltwin/docs/Workshops/workshop_DT.html#sensor-bme688) class code by copying and pasting it into a new file and saving as `KitronikBME688.py`. After which start a new file and copy the following into it before saving as `main.py`:

```{literalinclude} /code/Workshop_code/main_air_quality.py

```

## Code Explanation

```{code-block}
---
lineno-start: 1
---
# 0. PACKAGES
#from breakout_bme68x import BreakoutBME68X
#from pimoroni_i2c import PimoroniI2C
import KitronikBME688
from machine import Pin, I2C
import time
import sh1107
import network
from secrets import secrets
import urequests


```

The first ten lines load the packages, you will need to copy and paste the `sh1107.py`, `framebuf2.py`, and `KitronikBME688` scripts either from the [appendix](https://wur.gitlab.io/digitaltwin/docs/Workshops/workshop_DT.html#display) or their GitHub repositories( [sh1107](https://github.com/peter-l5/SH1107) and [framebuf2](https://github.com/peter-l5/framebuf2) ).

```{code-block}
---
lineno-start: 13
---
##################
# 1. Dictionary
html_status_code_dictionary = {'influx_db' :    {204 : 'Success',
                                                 400 : 'Bad request',
                                                 401 : 'Unauthorized, check token',
                                                 404 : 'Not found, check url',
                                                 413 : 'Too large, requested payload is too large',
                                                 429 : 'Too many requests, check plan',
                                                 500 : 'Internal server error, the server has encountered an unexpected error',
                                                 503 : 'Service unavailable, series cardinality exceeeds plan',
                                                },
                               }


```

The following lines (13-25) construct a HTML status code dictionary that allows you to translate the status_code of urequests.  

```{code-block}
---
lineno-start: 26
---
# 2. NETWORK CONNECT
wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(secrets['ssid'], secrets['ssid_password'])
if wlan.isconnected() is True:
    print('Connected to WiFi')

```
Connects the board to the WiFi network defined in the `secrets.py` file using the `ssid` and `ssid_password`. Lines 34-39 define the data, or payload's, header and destination. 


```{code-block}
---
lineno-start: 40
---
# DISPLAY INITIALISATION
i2c0 = I2C(1, scl=Pin(7), sda=Pin(6), freq=400000)
display = sh1107.SH1107_I2C(128, 128, i2c0, address=0x3c, rotate=-90)
display.sleep(False)
display.fill(0)


# SENSOR INITIALISATION
# if i2c error change i2cAddr=0x76 to 0x77
bme688 = KitronikBME688.KitronikBME688(i2cAddr=0x76, sda=8, scl=9)

```

Lines 40-49 initialise both the display and sensor.

```{code-block}
---
lineno-start: 51
---
# SETUP/CALIBRATION GAS SENSOR
# if no baseline exists the pico will take 5 minutes of measurements
# best to do this initial set-up in a well-ventilated room. 
bme688.setupGasSensor()
bme688.calcBaselines()


```
After the initialisation the Gas sensor needs to be set-up and a calibration step needs to be performed (lines 51-56). If there is no baseline file then the sensor makes one using calcBaselines. 


```{code-block}
---
lineno-start: 57
---
# MEASURE VARIABLES
try:

```

The next section, lines 57-141, perform the measurements. However, the measurement starts with a try statement (lines 57-59). If the network can be connected to and the data sent without interuption then the try statement (lines 57-108) is performed. For any exception (i.e., a problem) then the except statement (lines 109-141) is performed.

```{code-block}
---
lineno-start: 60
---
    while True:
        time.sleep_ms(1000)
        display.fill(0)
        
```

Lines 60-63 starts a loop and fills the display black (0).

```{code-block}
---
lineno-start: 64
---
        # MEASURE
        bme688.measureData()
        
        temperature = bme688.readTemperature()
        pressure    = bme688.readPressure()
        humidity    = bme688.readHumidity()
        gas_res     = bme688.readGasRes()
        IAQ         = bme688.getAirQualityScore()
        co2         = bme688.readeCO2()
        print(temperature, pressure, humidity, gas_res, IAQ, co2)


```

Lines 64-75 measure then reads the sensor's values (Temperature, Pressure, Humdity, Gas, eCO2 and Air Quality).

```{code-block}
---
lineno-start: 76
---     
        # DISPLAY
        display.text('Temperature C', 0, 0, 1)
        display.large_text("{:.2f}".format(temperature), 0, 10,2, 1)
        
        display.text('Humidity %RH', 0, 30, 1)
        display.large_text("{:.2f}".format(humidity), 0, 40,2, 1)
        
        display.text('Air Quality', 0, 60, 1)
        display.large_text("{:.2f}".format(IAQ), 0, 70,2, 1)
        
        display.text('eCO2 in ppm', 0, 90, 1)
        display.large_text("{:.2f}".format(co2), 0, 100,2, 1)
        
        display.show()
        

```

Lines 76-91 displays the measurements on the OLED display.

```{code-block}
---
lineno-start: 92
---
# POST
        payload_data = payload_data + secrets['board']['sensor'] + ',host=' + secrets['board']['name'] + ' ' +   'temperature=' + str(temperature)  + ',humidity=' + str(humidity) + ',pressure=' + str(pressure/100) + ',gas=' + str(gas_res)  + ',iaq=' + str(IAQ) + ',co2=' + str(co2)     
        print(payload_data)

        response = urequests.post(
                                             url     = url_to_post_to,
                                             data    = payload_data,
                                             headers = headers_to_send,
                                    )
        
        print("HTML status code : " + str(response.status_code) + " " + html_status_code_dictionary['influx_db'][response.status_code]) #(response.data())
        response.close()
        
        payload_data = ''
        time.sleep(1)


    
```

Lines 92-108 posts the data to the cloud. Line 93 converts the sensor data into a string (str) and then posts the data by constructing a post with urequests (line 96). The status of the post will then be printed (line 102) and then it is closed (line 103). Closing is important otherwise the Raspberry Pi Pico W will quickly run out of memory. The payload string is reset (line 105) and then the microcontroller goes to sleep for 1 second (line 106) until the next iteration.

```{code-block}
---
lineno-start: 109
---
except:
        
    while True:
        time.sleep_ms(1000)

        # MEASURE
        bme688.measureData()
        
        temperature = bme688.readTemperature()
        pressure    = bme688.readPressure()
        humidity    = bme688.readHumidity()
        gas_res     = bme688.readGasRes()
        IAQ         = bme688.getAirQualityScore()
        co2         = bme688.readeCO2()
        print(temperature, pressure, humidity, gas_res, IAQ, co2)
        
        
        # DISPLAY
        display.text('Temperature C', 0, 0, 1)
        display.large_text("{:.2f}".format(temperature), 0, 10,2, 1)
        
        display.text('Humidity %RH', 0, 30, 1)
        display.large_text("{:.2f}".format(humidity), 0, 40,2, 1)
        
        display.text('Gas kOmega', 0, 60, 1)
        display.large_text("{:.2f}".format(IAQ), 0, 70,2, 1)
        
        display.text('eCO2 in ppm', 0, 90, 1)
        display.large_text("{:.2f}".format(co2), 0, 100,2, 1)
        
        display.show()
        
        time.sleep(1)
    
```

Lines 109-141 is a repeat of the same code without the functions to perform the data posting. 


## Example 

Whilst the Raspberry Pico is **unplugged** from any power source or your computer wire up the board as per the instructions above and insert the display and sensors. Download and open Thonny. Plug in the Raspberry Pico into your computer. In the bottom right of Thonny click the Python version, click configure interpreter. Two drop down menus appear, for the first select MicroPython (Raspberry Pi Pico) or MicroPython (Generic) and for the second dropdown menu select the port that is accessible. Click OK. You may have to click the Red stop button. If it works `>>>` should appear in the shell environment, at the bottom of Thonny. Next, click View and then click Files this will open a panel on the left hand side of Thonny that will give you a view of what is on your computer and on the Raspberry Pico. This files tab will allow you to download or delete the data. 

Click new and copy and paste the code above. Click save and then click Raspberry Pico/Board. Make sure to save the code as `main.py`, you have to add the extension `.py` yourself Thonny does not do this automatically. Next open another new file and copy and paste the `sh1107.py` from [below](https://wur.gitlab.io/digitaltwin/docs/Workshops/workshop_biotech.html#display) and save as `sh1107.py`. Repeat for both `framebuf2.py` and `KitronikBME688.py`. This will ensure that the display and sensor works. 

Press stop and disconnect the Raspberry Pico from your computer. The first time the sensor begins it will perform a baseline calculation for 5 minutes after which it will begin to measure, it is best that this calibration step is done in a well ventilated room. Alternatively, save the following to the board as `baselines.txt`:

```{code}
177356.7
2359.933

```

Insert a power cable and if all has worked out the display should start showing data. If the board is connected to WiFi and InfluxDB has been set up then it should begin receving data, if no data is shown try modifying the time range on InfluxDB as sometimes the data will be 5 minutes in the future.



## Twinning: Going beyond the sensor

The above code makes a sensor that collects various data on a number of Air Quality variables (e.g., TVOC, Temperature, Humidity, ...). The next step would be to incorporate this into a decision process, where the data is used to produce actionable knowledge. For instance:

- Best time to ventilate the room for the current class...
- ...and / or to ensure that the intial CO2 returns to a baseline prior to subsequent classes
- How to ventilate the room whilst saving on costs?
- What lecture theatre to use for different activities, class sizes, etc.?



<br><br>

## Appendix

### Sensors

#### Sensor BME688
Sensor BME688 is an air quality sensor that measures temperature, pressure, humidty, and gas resistance that can be used to calculate an Index of Air Quality (IAQ) and Estimated CO2 (eCO2) produced from gas resistance, temperature and humidity measurements. Kitronik Ltd. have [produced](https://github.com/KitronikLtd/Kitronik-Pico-Smart-Air-Quality-Board-MicroPython) a class that allows for easy calculation of IAQ and eCO2 modified here:

```{literalinclude} /code/Workshop_code/KitronikBME688.py

```

Save the above code as a file called `KitronikBME688.py` and it can then be called with the following `import` statement:

```{code}
import KitronikBME688
```

As an example, the following gives the commands to get the sensor to return a series of measurements (`.measureData()`) every second after initialisation steps have been performed (`.setupGasSensor()` and `.calcBaselines()`):

```{code}
import KitronikBME688
import time

# CLASS INITIALISATION
# if i2c error change i2cAddr=0x76 to 0x77
bme688 = KitronikBME688.KitronikBME688(i2cAddr=0x76, sda=8, scl=9)

# SETUP/CALIBRATION GAS SENSOR
# if no baseline exists the pico will take 5 minutes of measurements
# best to do this initial set-up in a well-ventilated room. 
bme688.setupGasSensor()
bme688.calcBaselines()

# MEASURE VARIABLES
while True:
    time.sleep_ms(1000)

    # MEASURE
    bme688.measureData()
    
    
    temperature = bme688.readTemperature()
    pressure    = bme688.readPressure()
    humidity    = bme688.readHumidity()
    gas_res     = bme688.readGasRes()
    IAQ         = bme688.getAirQualityScore()
    co2         = bme688.readeCO2()
    print(temperature, pressure, humidity, gas_res, IAQ, co2)

```


#### Sensor SGP30
Sensor SGP30 is an air quality sensor that measures Hydrogen and Ethanol content that can be used to produce an equivalent Carbon dioxide (eCO2) and Total Volatile Organic Compound (TVOC) value. The SGP30 sensor requires an initial >30 second calibration phase prior to measuring during which the sensor will report 400 ppm and 0 for eCO2 and TVOC, after which it is possible to measure.

```{code}
# 0. PACKAGES
from pimoroni_i2c import PimoroniI2C
from breakout_sgp30 import BreakoutSGP30
import time

# 1. HARDWARE
PINS_PICO_W   = {"sda" : 8,  "scl" : 9}
i2c1          = PimoroniI2C(**PINS_PICO_W)

# 2. INIT SENSORS
sgp30 = BreakoutSGP30(i2c1)
sgp30.start_measurement(False)
id = sgp30.get_unique_id()
print("SGP30: initialised. About to start measuring")

# 3. COUNTERS
counter = 0
timer   = 1

# 4. LOOP
while True:

    count += 1

    # 4.1. READ SENSOR
    air_quality = sgp30.get_air_quality()
    eCO2        = air_quality[BreakoutSGP30.ECO2]
    TVOC        = air_quality[BreakoutSGP30.TVOC]
    

    # 4.2. PRINT SENSOR READING
    # uncomment either basic or complex
    # print(eCO2, TVOC)
    print("Carbon dioxide: " + str(eCO2) + " ppm, TVOC: " + str(TVOC) + " "

    
    # 4.3 SENSOR CALIBRATION 
    if count == 40:

        print("SGP30: Resetting device.")
        sgp30.soft_reset()

        time.sleep(0.5)
        print("SGP30: Restarting measurements. Waiting 15 seconds before returning.")
        
        sgp30.start_measurement(True)
        timer = 10
        print("SGP30: Measurement restarted at " + str(timer) + " second intervals.")

    
    # 4.4. SLEEP
    time.sleep(timer)

```

#### Sensor PIR
Passive infrared (PIR) sensor is a sensor that can be used to detect motion through receiving infrared radiation (the passive part) that can be interpreted as the motion of an animal, bird, or human being. The 'disco ball' is a plastic lens that provides the sensor with a wider field of vision from which it can receive infrared radiation. 

```{code}
# 0. PACKAGE
import machine
import time

# 1. HARDWARE
pir = machine.Pin(16, machine.Pin.IN, machine.Pin.PULL_UP)

2. LOOP
while True:

    pir_value = pir.value()
    print(pir_value)

    if pir_value == 0:

        print(" Movement detected ")
        time.sleep(5)

    else:

        print(" Waiting for movement ")
        time.sleep(0.2)

```

<br><br>

### Communication
#### Display
The mono OLED display is a simple 1.12" black and white display that can be run by downloading the driver for an OLED, `sh1107.py`, from [GitHub](https://github.com/peter-l5/SH1107) and the [extension](https://github.com/peter-l5/framebuf2) to the Frame Buffer MicroPython package `framebuf2.py`

````{dropdown} sh1107 and framebuf2 packages
```{literalinclude} /code/Workshop_code/sh1107.py

```

```{literalinclude} /code/Workshop_code/framebuf2.py

```
````

```{code}
# 0. PACKAGES
from machine import Pin, I2C
import sh1107
import time

# 1. HARDWARE
i2c0 = I2C(1, scl = Pin(7), sda = Pin(6), freq = 400000)

# 2. INIT DISPLAY 
# initialise (frame size, frame size, i2c address, device address, rotation)
display = sh110.SH1107_I2C(128,128, i2c0, address = 0x3c, rotate = -90)

display.sleep(False)

# fill display with black (0) or white (1)
display.fill(0)

while True:
    
    # fill display with black (0) or white (1) at start of each iteration
    display.fill(0)

    # display normal text with string, x axis, y axis, color (black - 0; white - 1)
    display.text('normal text', 0, 0, 1)

    # display large text requires framebuf2 to be on device 
    # string, x axis, y axis, size in integers, color (black - 0; white - 1)
    display.large_text('large text', 0, 0, 2, 1)

    display.show()

    time.sleep(10)

```

#### Wireless
The Raspberry Pico W has the ability to wirelessly communicate. To connect to a WiFi network create a script called `secrets.py` and save the following information.

```{code}

secrets =   {
            'ssid'          : 'your wifi ssid/name',
            'ssid_password' : 'your wifi password',
            }
```

Then run the following to connect.

```{code}
# 0. PACKAGES
import network
from secrets import secrets

# 1. NETWORK
wlan    = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(secrets['ssid'], secrets['ssid_password'])


```



