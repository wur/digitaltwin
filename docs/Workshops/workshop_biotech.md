# BioNano Technology; Sensors and Devices

Welcome to the online repository for the 5th - 9th December 2022 'Free lab' component of the BioNano Technology; [Sensors and Devices Course](https://www.nature.com/articles/s41570-021-00285-2)! On this page you can find various [MicroPython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html) scripts that can make your [microcontroller](https://wur.gitlab.io/digitaltwin/docs/setup/microcontrollers.html) work, utilise the various components and sensors, as well as make it talk to various other devices. Let us start by discussing what the goal of this project will be.
<br><br>

## Goal
Fleshy fruits can be divided into those with (e.g., apples, avocados, bananas, peaches, …) and without (e.g., blueberries, citrus, cherries, grapes, strawberries, …) a respiratory presence at the onset of fruit ripening. Those with a respiratory presence undergo so-called **climacteric fruit ripening** which is a process that converts an unready fruit into a ready-to-eat fruit even after harvesting the fruit from the parent plant. The goal of this part of the course is to build a sensor based upon a Raspberry Pico W for the products of climacteric fruit ripening (ethylene: $C_{2}H_{4}$) - the final stage of fruit maturation prior to the fruit's **senescence** - as well as the associated increase in cellular respiration (carbon dioxide: $CO_{2}$). Determining when a plant enters this phase can be useful for logistics, longevity, storage, and display. 
<br><br>

## What do you have?

```{figure} /images/Workshops_Sensor_components.jpg
---

name: workshop_sensor_components-fig
---
**Components in your sensor box.** 
```

In your 4.6L (29 x 20 x 12 cm) sensor box you will find:

- A **Raspberry Pico W** with headers<br>
- A MicroUSB power cable (5.1V/2.5A)
- A **LoRa** module (868 MHz)
- 1.12" Mono **OLED** display
- A **PIR motion** sensor<br>
- **BME688** 4 in 1 Air Quality 'breakout'
- **SGP30** Air quality 'breakout'
- An 830 point **breadboard** 
- various cables (e.g., male-female and male-male jumper wires and STEMMA QT/QWIC JST SH-4-pin)
<br><br>

## Wiring things up
### Raspberry Pico

```{figure} /images/pinout_raspberry_pico_W_including_logo.jpg
---

name: workshop_sensor_pico-fig
---
**Pinout of Raspberry Pico W.** The [Pinout](https://datasheets.raspberrypi.com/picow/PicoW-A4-Pinout.pdf) is copyright © 2020-2021 Raspberry Pi (Trading) Ltd licensed under a Creative Commons [Attribution-NoDerivatives 4.0](https://creativecommons.org/licenses/by-nd/4.0/) International (CC BY-ND) licence.
```
The 'brains' and 'mouth' of our sensor is a Raspberry Pico W microcontroller. The *brain* is a RP2040 chip that has two ARM Cortex-M0+ cores clocked at 133MHz with 256KB RAM combined with 2 MB of QSPI Flash memory. The *mouth* is an Infineon CYW43439 wireless chip that enables the board to communicate via wireless (IEEE 802.11 b/g/n wireless LAN) or bluetooth (v5.2). To connect to peripherials there are 40 pins/castellated edges divided between: 26 general purpose input/output (GPIO) pins; 8 ground (GND) pins; and 4 pins related to power.  

The sensor will communicate with the microcontroller using I2C buses. On the pinout (light blue in {numref}`workshop_sensor_pico-fig`) it is apparent that with the Pico W that you can connect a sensor using I2C with the majority of the GP pins. The pins are in pairs with a repeating pattern of SDA and SCL pins - which stand for serial data and serial clock - next to one another. Notice also that there is a 0 and 1 after I2C (I2C0 and I2C1) that is because on the Pico W there are two I2C buses. *What does this mean?* You can only connect I2C devices to two places on the board, although you can chain the sensors together in a series so that you can have more than two sensors at any one time. If you connect a sensor or a series of sensors to pins allocated to I2C1 then when you wish to connect directly to the Pico another I2C device then you must use pins allocated to I2C0 (and vice versa).

### I2C Sensors

```{figure} /images/Workshop_i2c_breakout.jpg
---

name: workshop_i2c_sensor_breakout-fig
---
**I2C Breakout extender** (A-C) Some I2C sensors can be inserted into a breakout extender. (D) Additional sensors can then be connected (E) via a QT/ST port. Or via connecting an additional breakout extender in a series (not shown here).
```

```{figure} /images/Workshop_i2c_stemma.jpg
---

name: workshop_i2c_sensor_stemma-fig
---
**I2C Stemma QT** (A) Other  I2C sensors can be connected using STEMMA QT/QWIC JST SH-4-pin wires that either have (B) a male DuPont cable or (D-E) have the same connector on either end. These sensors need to have a special QT/ST port to make a connection.
```


### Breadboard 
The microcontroller and the various sensors and components can be arranged and wired up onto the (830 point) breadboard without requiring you to solder the components together. A breadboard has four sets of holes: two outer sets each comprising two rows that are called rails and two inner sets of 5 rows and 63 columns. The rails run horizontally and all the holes in a row of a rail are connected. These can be used for power (+) and ground (-) and can distribute that across the breadboard allowing for components to be spaced out. 

```{figure} /images/Workshop_Components_part_1.jpg
---

name: workshop_sensor_fritz_1-fig
---
**Step 1: Breadboard.** The first step is to connect the ground and power rails of the breadboard to power and ground pins of the Raspberry Pico W.
```

In contrast, the two inner sets of holes are not connected along the row but along the column. Within a set anything plugged into the same column will be connected so if you connect one hole in a column to ground the remaining 4 holes can be used to connect to ground. The two sets are divided by a deep groove and are not connected without first bridging the gap with a jumper cable. 
<br><br>


### Components
The various components can be connected to each other and the microcontroller via jumper cables, a STEMMA QT cable via the attached plugs, and/or by slotting the component into a breakout extender. 
<br><br>

```{figure} /images/Workshop_Components_part_2.jpg
---

name: workshop_sensor_fritz_2-fig
---
**Step 2: I2C Bus 0.** Next step is to wire up the I2C bus 0 of the Raspberry Pico W. It needs power (red), ground (black), SDA for serial data (blue) and SCL for serial clock (green). The SDA and SCL in our example is the 9th and 10th pins also referred to as GP6 and GP7.
```
<br>

```{figure} /images/Workshop_Components_part_3.jpg
---

name: workshop_sensor_fritz_3-fig
---
**Step 3: Connect the first breakout extender.** Next, the first breakout extender needs to be slotted into the breadboard. The first i2c device is our **OLED display**, slot it into the extender ensuring that the it goes in the correct way by looking at the side with the pin labels.
```
<br>

```{figure} /images/Workshop_Components_part_4.jpg
---

name: workshop_sensor_fritz_4-fig
---
**Step 4: I2C Bus 1.** Having completed the first i2c bus the next steps involve setting up the second bus. Once again it needs power (red), ground (black), SDA for serial data (blue) and SCL for serial clock (green). The SDA and SCL in our example is the 11th and 12th pins also referred to as GP8 and GP9.
```

<br>
You can connect the sensors in one of two ways via another breakout extender or with a different type of cables. If the second breakout extender will be used follow step 5a ( {numref}`workshop_i2c_sensor_breakout-fig` ). Alternatively, if the DuPont to STEMMA QT/JST male-to-female connectors will be used then use step 5b ( {numref}`workshop_i2c_sensor_stemma-fig` ).

```{figure} /images/Workshop_Components_part_5.jpg
---

name: workshop_sensor_fritz_5a-fig
---
**Step 5a: Connect the second breakout extender.** Just like step 3 the second breakout extender needs to be slotted into the breadboard. The second i2c devices are our sensors, slot it into the extender ensuring that the it goes in the correct way by looking at the side with the pin labels. Slot in the BME688 sensor and then connect the SGP30 as per {numref}`workshop_i2c_sensor_breakout-fig`. 
```

<br>

```{figure} /images/Workshop_Components_part_5b.jpg
---

name: workshop_sensor_fritz_5b-fig
---
**Step 5b: Connect the second I2C using a Stemma QT cable.** Using the DuPont male to male Stemma QT connector connect the SGP30 sensor to the board. The wires are connected as follows, Red is power, Blue is SDA, Yellow is SCL, and Black is Ground. Connect the BME688 sensor to the SGP30 as per {numref}`workshop_i2c_sensor_stemma-fig`. 
```

## MicroPython
Once wired together the sensors and components can be controlled using the programming language [MicroPython](https://wur.gitlab.io/digitaltwin/docs/setup/micropython_setup.html). For instance, an unstable release of MicroPython that contains the drivers for wireless communication (version: `rp2-pico-w-20221201-unstable-v1.19.1-724-gfb7d211153.uf2`) can be downloaded as a `.uf2` file from the Raspberry Pico's [documentation](https://www.raspberrypi.com/documentation/microcontrollers/micropython.html). 

Don't worry about installing MicroPython. The programming language has been pre-loaded on to the Raspberry Pi Pico W although to do so (if for instance anything goes wrong) is relatively simple. Disconnect the Pico from a computer or power supply. Hold down the Bootsel button of a disconnected Pico board and whilst continuing to hold it down plug it into a computer via the Pico's micro-USB port. Download the `.uf2` file before dragging and dropping it onto the RPI-RP2 drive that appears. The Pico should reboot and MicroPython will have been installed.

Like 'regular' Python additional packages can be downloaded using a package manager. Depending on its version, for MicroPython, the `mip` or `upip` packages act as a package manager. As long as the board is connected to WiFi (i.e., via the `network` package) a new package (e.g., `umqtt.simple`) can be installed like so:

```{code}
import network
import mip      # or upip
from secrets import secrets

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(secrets['ssid'], secrets['ssid_password'])

mip.install('umqtt.simple')  # or upip.install('umqtt.simple')

```

Where, `secrets` is a file written by the user containing a WiFi SSID and SSID Password using the following dictionary format:

```{code}
""" secrets.py: File for secret settings

    CONTAINS:
        secrets = dictionary with WIFI
"""
secrets = {
            'ssid'          : 'wifi name',
            'ssid_password' : 'wifi password',
            }
```

As packages are installed a 'lib' directory will appear, so for the example above a sub-directory, 'umqtt', will appear once the `mip.install('umqtt.simple')` command is run. To save time the boards used in this course have been pre-loaded with the drivers for various breakouts using a version of Micropython (`pimoroni-picow-v1.19.10-micropython.uf2`) that can be downloaded from Pimoroni's [GitHub](https://github.com/pimoroni/pimoroni-pico/releases/tag/v1.19.10).

Code can be written either in a text editor (e.g., notepad) or via an interactive developer environment (IDE) such as [Thonny](https://thonny.org/) or [Mu](https://codewith.mu/). To have the code run once the board is powered save the file as `main.py`. For convenience, the code snippets found in the [appendix](https://wur.gitlab.io/digitaltwin/docs/Workshops/workshop_biotech.html#appendix) can be used to control the various sensors independent of the others. 
<br><br>


## Code


```{literalinclude} /code/Workshop_code/main_1.py

```

## Code Explanation
```{code-block}
---
lineno-start: 1
---
# 0. PACKAGES
from breakout_bme68x import BreakoutBME68X
from breakout_sgp30 import BreakoutSGP30
from pimoroni_i2c import PimoroniI2C
from machine import Pin, I2C
import time
import sh1107


```

The first nine lines load the packages, you will need to copy and paste the sh1107.py and framebuf2.py scripts either from the [appendix](https://wur.gitlab.io/digitaltwin/docs/Workshops/workshop_biotech.html#display) or their GitHub repositories( [sh1107](https://github.com/peter-l5/SH1107) and [framebuf2](https://github.com/peter-l5/framebuf2) ).

```{code-block}
---
lineno-start: 10
---
# 1. USER SPECIFIED
# time between measurements in seconds
# 10 mins = 600; 1 hr = 3600
sampling_interval  = 600

```

The following lines (10-14) set the sampling interval. Whilst the sensor and it's display will refresh the measurement every second the data will only be recorded by the specified value in seconds defined by `sampling_interval`.

```{code-block}
---
lineno-start: 15
---
# 2. FILE STORAGE
# 2.1 Find unused filename
running_filename_check = True
file_count = 0

while running_filename_check:
    
    try:
        filename = "sensor_data_" + str(file_count) + ".txt"
        
        f = open(filename,'r')
        f.close
        file_count += 1
        
    except:
        running_filename_check = False

# 2.2 Open file for writing       
filename = "sensor_data_" + str(file_count) + ".txt"
file = open(filename, 'w')

# 2.3 Make header string
header_string = "time, count, temperature, pressure, humidity, gas, eCO2, TVOC \n"
unit_string   = "seconds, number, C, hPa, percentRH, kOmega, ppm, count \n"
file.write(header_string + unit_string)
file.flush()


```

Lines 15-41 establish the data file. If power or the sensor is disrupted or reset the running_filename_check `while` loop will ensure that the data is not overwritten by finding a filename that is not in use. Line's 35-39 write's a header including units to the open data file. The command `file,flush()` writes the data without closing the file. 


```{code-block}
---
lineno-start: 42
---
# 3. HARDWARE - set up the hardware
#PINS_BREAKOUT_GARDEN  = {"sda": 4, "scl": 5}
#PINS_PICO_EXPLORER    = {"sda": 20, "scl": 21}
PINS_PICO_W           = {"sda" : 8,  "scl" : 9}

i2c0 = I2C(1, scl=Pin(7), sda=Pin(6), freq=400000)
i2c1 = PimoroniI2C(**PINS_PICO_W)


```

Lines 42-50 set up the hardware by defining the pins used by the two I2C buses, here GP8 and GP9 are used for I2C1 and GP7 and GP6 for I2C0.

```{code-block}
---
lineno-start: 51
---
# 4. INIT SENSORS
display = sh1107.SH1107_I2C(128, 128, i2c0, address=0x3c, rotate=-90)
display.sleep(False)
display.fill(0)

bme = BreakoutBME68X(i2c1)

sgp30 = BreakoutSGP30(i2c1)
sgp30.start_measurement(False)
id = sgp30.get_unique_id()
print("sgp30 intialised about to start measuring")


```

The next section on lines 51-63 initialise both sensors (BME688 and SGP30).

```{code-block}
---
lineno-start: 64
---
# 5. MEASUREMENTS
# 5.1 set up counters
count      = 0
timer      = 0
timeInit   = time.time()
time_start = time.time()  
```
Lines 64-69 sets up a series of counters which will be used in `if` statements.

```{code-block}
---
lineno-start: 70
---
while True:
    
    display.fill(0)
    
    # 5.2 Get time
    time_end = time.time()
    time_dif = time_end - time_start


    # 5.3 measure bme688
    temperature, pressure, humidity, gas_resistance, status, gas_index, meas_index = bme.read()
    print(temperature, pressure, humidity, gas_resistance, status, gas_index, meas_index)
    

    # 5.4 measure sgp30
    count += 1
    timer += 1
    air_quality = sgp30.get_air_quality()
    eCO2 = air_quality[BreakoutSGP30.ECO2]
    TVOC = air_quality[BreakoutSGP30.TVOC]
    print(eCO2, TVOC)
```
Lines 70-87 Get the time and read both sensor's values (Temperature, Pressure, Humdity, Gas, eCO2 and TVOC) and update the counters by adding one (`variable += 1`) to its current value.

```{code-block}
---
lineno-start: 89
---

    # 5.5 WRITE DATA
    if timer == sampling_interval:
        # Complex, uses a lot of memory:
        # data_to_write = "temperature: " + str(temperature) + " C, pressure: " + str(pressure/100) + " hPa, humidity: " + str(humidity) + " %rH, gas: " + str(gas_resistance/1000) + " kOmega, eCO2: " + str(eCO2) + " ppm, TVOC: " + str(TVOC) 

        # Simple, uses less memory:
        data_to_write = str(time_dif) + "," + str(count)  + "," + str(temperature)  + "," + str(pressure/100)  + "," + str(humidity)  + "," + str(gas_resistance/1000)  + "," + str(eCO2)  + "," + str(TVOC) + "\n"

        file.write(data_to_write)
        file.flush()

        # reset timer
        timer = 0
```
Lines 89-102 check if the variable `timer` has reached the `sampling_interval` if so the data is written to the open data file and the variable `timer` reset.

```{code-block}
---
lineno-start: 103
---


    # 5.6 CALIBRATION 
    # when count == 40 then start measuring sgp30 properly
    if count == 40:
        print("Resetting device")
        sgp30.soft_reset()
        time.sleep(0.5)
        print("restarting measurements waiting 15 seconds before returning")
        sgp30.start_measurement(True)
        print("measurement restarted")
    
```
Lines 103-114 check if the counter has reached 40 and if so the SGP30 it is calibrated and the 'real' measurements begin. In this instance the calibration will be performed once at the start it is possible to get it to recalibrate every X number of measurements by adding the following `count = X` at line 114. Making X negative if a large interval is desired, e.g.,  count = -160 would make the recalibration every 200th iteration of the while loop.

```{code-block}
---
lineno-start: 115
---
    # 5.7 Display data
    #display.text('SH1107', 0, 0, 1)
    display.text('Temperature C', 0, 0, 1)
    display.large_text("{:.2f}".format(temperature), 0, 10,2, 1)
    
    display.text('Humidity %RH', 0, 30, 1)
    display.large_text("{:.2f}".format(humidity), 0, 40,2, 1)
    
    display.text('Gas kOmega', 0, 60, 1)
    display.large_text("{:.2f}".format(gas_resistance), 0, 70,2, 1)
    
    display.text('eCO2 in ppm', 0, 90, 1)
    display.large_text("{:.2f}".format(eCO2), 0, 100,2, 1)
    
    #display.text('TVOC', 0, 120, 1)
    display.text(" TVOC: {:.2f}".format(TVOC), 0, 120, 1)
    
    display.show()
    
```
Lines 115-131 displays the measurements on the OLED display.

```{code-block}
---
lineno-start: 132
---
    # 5.8 go to sleep 
    time_start = time.time()  
    time.sleep(1)


```
Finally, lines 132-134 makes the microcontroller sleep for 1 second until the next iteration.

## Example 

```{figure} /images/Workshops_Example_Banana_in_box.jpg
---

name: workshop_example_banana-fig
---
**Example of measuring a Banana.** 
```
Whilst the Raspberry Pico is **unplugged** from any power source or your computer wire up the board as per the instructions above and insert the display and sensors. Download and open Thonny. Plug in the Raspberry Pico into your computer. In the bottom right of Thonny click the Python version, click configure interpreter. Two drop down menus appear, for the first select MicroPython (Raspberry Pi Pico) or MicroPython (Generic) and for the second dropdown menu select the port that is accessible. Click OK. You may have to click the Red stop button. If it works `>>>` should appear in the shell environment, at the bottom of Thonny. Next, click View and then click Files this will open a panel on the left hand side of Thonny that will give you a view of what is on your computer and on the Raspberry Pico. This files tab will allow you to download or delete the data. 

Click new and copy and paste the code above. Click save and then click Raspberry Pico/Board. Make sure to save the code as `main.py`, you have to add the extension `.py` yourself Thonny does not do this automatically. Next open another new file and copy and paste the `sh1107.py` from [below](https://wur.gitlab.io/digitaltwin/docs/Workshops/workshop_biotech.html#display) and save as `sh1107.py`. Repeat for `framebuf2.py`. This will ensure that the display works. 

Press stop and disconnect the Raspberry Pico from your computer. Insert a power cable and if all has worked out the display should start giving data. The first 40 measurements of CO2 and TVOC will be 400 and 0 so give it a minute or two. Check if the SGP30 sensor is working by breathing onto it, the value of CO2 and TVOC should change. 

Perform your experiment. The display will refresh every second but the code will only save the data once every 600 seconds (10 minutes). You might want to check how large the file is before running the experiment for a while. Once it has been completed plug the Raspberry Pi Pico back into your computer, open Thonny and go through the configure steps as above. In the files tab you can right click the file and select download or delete. Note that the file will download to whatever directory is visible in the 'This computer' sub-panel. 


## Not working?

### Packages and main
Make sure that the code is saved as `main.py` including the `.py` extension. Next check that the additional packages `sh1107.py` and `framebuf2.py` have been added to the board by copying and pasting the code into a new file and saving as `sh1107.py` and/or `framebuf2.py`.

### I2C error
If there is an error with the I2C first make sure that the SDA and SCL wires are arranged correctly. Next check that the OLED is in the I2C-0 and the sensors in I2C-1 the code above has told the microcontroller that the OLED can be found on I2C-0 and the sensors on I2C-1. If it still doesn't work make sure that the wires in the breadboard match the positions of the sensor, i.e., line up the sensor's 2-5v with the power pin. If it still doesn't work then change the I2C address on line 52, 56, and 58 (i.e., bme = BreakoutBME68X(i2c1, 0x77) ). The address of the sensor is on the sensor, its 0x.. followed by two numbers. 

### OS error
If there is an OS error 12 or 28 then there is no space on the drive, remove large data files.

### Constant CO2 / TVOC values

First breathe close to the SGP30 CO2 sensor, if its still not recording anything then unplug the STEMMA QT cable and try inserting it into the other port and check again. If its still not working then try the following alternatives for the `main.py` code:

````{dropdown} Alternative BME688 

Add the [KitronikBME688](https://wur.gitlab.io/digitaltwin/docs/Workshops/workshop_biotech.html#sensor-bme688) class code by copying and pasting it into a new file and saving as `KitronikBME688.py`. Then copy the following into a new file and saving it as `main.py`: 

```{literalinclude} /code/Workshop_code/main_2.py

```
````

````{dropdown} Threading 

The following code uses the Raspberry Pi Pico's dual cores to divide the tasks up into two threads. Copy the following into a new file and saving it as `main.py`: 

```{literalinclude} /code/Workshop_code/main_3.py

```
````

## Twinning: Going beyond the sensor

The above code makes a sensor that collects various data on a number of variables (e.g., TVOC, Temperature, Humidity, ...) of a Banana contained within a box. The next step would be to incorporate this into a decision process, where the data is used to produce actionable knowledge. For instance:

- Determining when the Bananas are ready to eat
- Delaying ripening by triggering a feedback


<br><br>

## Appendix

### Sensors

#### Sensor BME688
Sensor BME688 is an air quality sensor that measures temperature, pressure, humidty, and gas resistance that can be used to produce an air quality index.  

```{code}
# 0. PACKAGES
from pimoroni_i2c import PimoroniI2C
from breakout_bme68x import BreakoutBME68X
import time

# 1. HARDWARE
PINS_PICO_W   = {"sda" : 8,  "scl" : 9}
i2c1          = PimoroniI2C(**PINS_PICO_W)

# 2. INIT SENSORS
bme = BreakoutBME68X(i2c1)

# 3. LOOP
while True:

    # 3.1. READ SENSOR
    temperature, pressure, humidity, gas_resistance, status, gas_index, meas_index = bme.read()

    # 3.2. PRINT SENSOR READING
    # uncomment either basic or complex
    # print(temperature, pressure, humidity, gas_resistance, status, gas_index, meas_index)
    # print("temperature: " + str(temperature) + " C, pressure: " + str(pressure/100) + " hPa, humidity: " + str(humidity) + " %rH, gas: " + str(gas_resistance/1000) + " kOmega, eCO2: " + str(eCO2) + " ppm, TVOC: " + str(TVOC))

    # 3.3. SLEEP
    time.sleep(10)

```

````{dropdown} More advanced BME688 class

The BME688 can measure:

- Temperature
- Pressure
- Humidity
- Gas Resistance

These can be used to calculate:

- Index of Air Quality (IAQ)
- Estimated CO2 (eCO2) produced from gas resistance, temperature and humidity measurements

Kitronik Ltd. have [produced](https://github.com/KitronikLtd/Kitronik-Pico-Smart-Air-Quality-Board-MicroPython) a class that allows for easy calculation of IAQ and eCO2 modified here:


```{literalinclude} /code/Workshop_code/KitronikBME688.py

```

Save the above code to `KitronikBME688.py` and it can then be called with the following:

```{code}
import KitronikBME688
import time

# CLASS INITIALISATION
# if i2c error change i2cAddr=0x76 to 0x77
bme688 = KitronikBME688.KitronikBME688(i2cAddr=0x76, sda=8, scl=9)

# SETUP/CALIBRATION GAS SENSOR
# if no baseline exists the pico will take 5 minutes of measurements
# best to do this initial set-up in a well-ventilated room. 
bme688.setupGasSensor()
bme688.calcBaselines()

# MEASURE VARIABLES
while True:
    time.sleep_ms(1000)

    # MEASURE
    bme688.measureData()
    
    
    temperature = bme688.readTemperature()
    pressure    = bme688.readPressure()
    humidity    = bme688.readHumidity()
    gas_res     = bme688.readGasRes()
    IAQ         = bme688.getAirQualityScore()
    co2         = bme688.readeCO2()
    print(temperature, pressure, humidity, gas_res, IAQ, co2)

```


````



#### Sensor SGP30
Sensor SGP30 is an air quality sensor that measures Hydrogen and Ethanol content that can be used to produce an equivalent Carbon dioxide (eCO2) and Total Volatile Organic Compound (TVOC) value. The SGP30 sensor requires an initial >30 second calibration phase prior to measuring during which the sensor will report 400 ppm and 0 for eCO2 and TVOC, after which it is possible to measure.

```{code}
# 0. PACKAGES
from pimoroni_i2c import PimoroniI2C
from breakout_sgp30 import BreakoutSGP30
import time

# 1. HARDWARE
PINS_PICO_W   = {"sda" : 8,  "scl" : 9}
i2c1          = PimoroniI2C(**PINS_PICO_W)

# 2. INIT SENSORS
sgp30 = BreakoutSGP30(i2c1)
sgp30.start_measurement(False)
id = sgp30.get_unique_id()
print("SGP30: initialised. About to start measuring")

# 3. COUNTERS
counter = 0
timer   = 1

# 4. LOOP
while True:

    count += 1

    # 4.1. READ SENSOR
    air_quality = sgp30.get_air_quality()
    eCO2        = air_quality[BreakoutSGP30.ECO2]
    TVOC        = air_quality[BreakoutSGP30.TVOC]
    

    # 4.2. PRINT SENSOR READING
    # uncomment either basic or complex
    # print(eCO2, TVOC)
    print("Carbon dioxide: " + str(eCO2) + " ppm, TVOC: " + str(TVOC) + " "

    
    # 4.3 SENSOR CALIBRATION 
    if count == 40:

        print("SGP30: Resetting device.")
        sgp30.soft_reset()

        time.sleep(0.5)
        print("SGP30: Restarting measurements. Waiting 15 seconds before returning.")
        
        sgp30.start_measurement(True)
        timer = 10
        print("SGP30: Measurement restarted at " + str(timer) + " second intervals.")

    
    # 4.4. SLEEP
    time.sleep(timer)

```

#### Sensor PIR
Passive infrared (PIR) sensor is a sensor that can be used to detect motion through receiving infrared radiation (the passive part) that can be interpreted as the motion of an animal, bird, or human being. The 'disco ball' is a plastic lens that provides the sensor with a wider field of vision from which it can receive infrared radiation. 

```{code}
# 0. PACKAGE
import machine
import time

# 1. HARDWARE
pir = machine.Pin(16, machine.Pin.IN, machine.Pin.PULL_UP)

2. LOOP
while True:

    pir_value = pir.value()
    print(pir_value)

    if pir_value == 0:

        print(" Movement detected ")
        time.sleep(5)

    else:

        print(" Waiting for movement ")
        time.sleep(0.2)

```

<br><br>

### Communication
#### Display
The mono OLED display is a simple 1.12" black and white display that can be run by downloading the driver for an OLED, `sh1107.py`, from [GitHub](https://github.com/peter-l5/SH1107) and the [extension](https://github.com/peter-l5/framebuf2) to the Frame Buffer MicroPython package `framebuf2.py`

````{dropdown} sh1107 and framebuf2 packages
```{literalinclude} /code/Workshop_code/sh1107.py

```

```{literalinclude} /code/Workshop_code/framebuf2.py

```
````

```{code}
# 0. PACKAGES
from machine import Pin, I2C
import sh1107
import time

# 1. HARDWARE
i2c0 = I2C(1, scl = Pin(7), sda = Pin(6), freq = 400000)

# 2. INIT DISPLAY 
# initialise (frame size, frame size, i2c address, device address, rotation)
display = sh110.SH1107_I2C(128,128, i2c0, address = 0x3c, rotate = -90)

display.sleep(False)

# fill display with black (0) or white (1)
display.fill(0)

while True:
    
    # fill display with black (0) or white (1) at start of each iteration
    display.fill(0)

    # display normal text with string, x axis, y axis, color (black - 0; white - 1)
    display.text('normal text', 0, 0, 1)

    # display large text requires framebuf2 to be on device 
    # string, x axis, y axis, size in integers, color (black - 0; white - 1)
    display.large_text('large text', 0, 0, 2, 1)

    display.show()

    time.sleep(10)

```

#### Wireless
The Raspberry Pico W has the ability to wirelessly communicate. To connect to a WiFi network create a script called `secrets.py` and save the following information.

```{code}

secrets =   {
            'ssid'          : 'your wifi ssid/name',
            'ssid_password' : 'your wifi password',
            }
```

Then run the following to connect.

```{code}
# 0. PACKAGES
import network
from secrets import secrets

# 1. NETWORK
wlan    = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(secrets['ssid'], secrets['ssid_password'])


```



