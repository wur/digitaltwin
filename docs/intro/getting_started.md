# Getting started
In the following sections we will outline how you can begin developing three Digital Twin instances. Each example will lead you through the steps alongside providing an explanation of the thought processes that have gone into them and the various components and code required. The examples are aimed at users with different skill levels, if you are new to programming and physical computing or need a refresh then begin with *Beginner: Indoor Digital Twin*. If not then proceed with the other sections by following the links below:

- [Beginner: Indoor Digital Twin](https://wur.gitlab.io/digitaltwin/docs/standalone_twin/intro.html)
- [Intermediate: Twinning a plant](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/intro.html)
- [Advanced: Outdoor Twin](https://wur.gitlab.io/digitaltwin/docs/campus_health/intro.html)

The next section, [Set-up](https://wur.gitlab.io/digitaltwin/docs/setup/intro.html), will outline what you need to have in order to follow along with the examples in this book. A specific list of components and software required is given at the start of each example as well as how to build these devices.

- [Set-Up](https://wur.gitlab.io/digitaltwin/docs/setup/intro.html)

<br/><br/>
