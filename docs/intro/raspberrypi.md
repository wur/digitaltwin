# Raspberry Pi Setup

## What is a Raspberry pi?

The Raspberry Pi is a **single board computer** (SBC), aptly named because it is a computer constructed on a single printed circuit board. The Raspberry Pico and Arduino Nano that we are using are **microcontrollers**. The difference between them is that microcontrollers are small computers on integrated circuits that can be used in embedded systems to interact with other types of hardware. Microcontrollers have low resources in terms of both CPU or RAM, and do not have a operating system (OS). Despite this Microcontrollers can perform real-time, fine grained processing for specific tasks. In comparison, despite their small size SBC's are what we would generally associate with a computer. Single board computers have an operating system which allows for them to have a much more general purpose.


Depending upon the model of Raspberry Pi the board has the following components: GPIO Pins; USB, HDMI, Display, and Camera ports; SD Card Reader; as well as CPU, GPU, and RAM. For our purposes we will be using a Raspberry Pi with wireless internet (e.g., Raspberry Pi Zero W; Raspberry Pi Zero 2 W; Raspberry Pi 400; Raspberry Pi 4 Model B; etc.).


## Preparing the Raspberry Pi

To get started with the Raspberry Pi you will need a power supply (e.g., a USB and USB-power adaptor), a separate computer, and an SD card. The SD card is necessary as it will be both where the Raspberry Pi's operating system (OS) is installed and act as a file storage. 


## Raspberry Pi OS

The first thing to do is to install the Raspberry Pi's Operating System onto the SD card. This is where the other computer becomes necessary whilst you can purchase SD cards with pre-installed OS the fastest and easiest approach is to use the [Raspberry Pi Imager](https://www.raspberrypi.com/software/). 

Connect the SD card to the computer. Download, install, and open the Raspberry Pi Imager (from [here](https://www.raspberrypi.com/software/)). The Imager gives you two mandatory choices and the option to specify more advanced settings: 



```{figure} /images/Imager.jpg
---

name: rasp_pi_Imager_figure-fig
---
Raspberry PI Imager
```

First select the OS: 

```{figure} /images/Imager_choose_OS.jpg
---

name: rasp_pi_Imager_choose_OS_figure-fig
---
Select 'Raspberry PI OS 32-bit' Operating System
```

Then select the SD card that you wish to write the OS to:

```{figure} /images/Imager_choose_Storage.jpg
---

name: rasp_pi_Imager_choose_Storage_figure-fig
---
Click the SD card
```

Next click the cog wheel to access the advanced settings. Here we will define the host name, enable Secure Shell Protocol (SSH), set up username and password, and enter the connection information for the WiFi that the Raspberry Pi will connect to. 



```{figure} /images/Imager_choose_Advanced_settings.jpg
---

name: rasp_pi_Imager_Advanced_settings_figure-fig
---
Click the Cog wheel
```

## Advanced options: Enabling SSH

Rather than connecting a screen, keyboard, and mouse to our Raspberry Pi we will remotely access the Raspberry Pi. To do so we need to provide some further information to the Imager:

```{figure} /images/Imager_choose_Advanced_settings1.jpg
---

name: rasp_pi_Imager_Advanced_settings_figure1-fig
---
Note: the following code uses the default hostname *raspberrypi* you can alter it here, but doing so requires you to change the hostname to your preferred choice in the following
```

Click enable SSH and set username and password:


```{figure} /images/Imager_choose_Advanced_settings2.jpg
---

name: rasp_pi_Imager_Advanced_settings_figure2-fig
---

```

Enter the WiFI SSID name and key:


```{figure} /images/Imager_choose_Advanced_settings3.jpg
---

name: rasp_pi_Imager_Advanced_settings_figure3-fig
---
Remember to set the wireless LAN country to the correct option
```


Finally, click save. This should return you to the main screen, with everything done it is time to write to the SD card. Click write.


```{figure} /images/Imager_choose_write.jpg
---

name: rasp_pi_Imager_choose_write_figure-fig
---

```

Once the SD card has been written with the OS Image you need to add an empty file (e.g., a text file) without any extension called *ssh*  to the boot folder. This is the folder that your computer sees when you plug the SD card into it, if the card ejected after writing just plug it back in. Having added this file, eject the SD card from your computer and insert it into the Raspberry Pi's SD card reader and plug in the power supply. If the Raspberry Pi is working the LED should blink when both SD card and power supply are connected.


## Remote access
 To continue setting up the Raspberry Pi requires us to enter a series of commands, check if those commands have worked, and debug if necessary. However, you should notice we have not connected any periperhials (e.g., display, mouse, or keyboard). Raspberry Pi's have the capacity for remote access, as it can be useful when your Raspberry Pi is in an inaccessible location or in our case not connected to any peripherials. However, to [access the Raspberry Pi remotely](https://www.raspberrypi.com/documentation/computers/remote-access.html#introduction-to-remote-access) requires knowing that Raspberry Pi's IP address. Having connected the Raspberry Pi to your WiFi network it will have been assigned an IP addresss, to obtain it you can try a number of different approaches. For instance, connect a display or navigate to your router's IP address (usually printed on them) and search the device list.  Here we will use **multicast DNS** (mDNS). 
 
 Open a terminal on your computer (e.g., Windows Powershell) and ensuring that your Raspberry Pi is turned on, type into your terminal:
 
 ```{code-block} console
ping raspberrypi.local

```
If you altered the host name ({numref}`rasp_pi_Imager_Advanced_settings_figure1-fig`) earlier then replace `raspberrypi` here with that. *It can take up to a few mintues before the Raspberry Pi has finished booting. If ping is unresponsive try giving it a bit longer. After booting you should be able to ping / login to your system.*. If it works you should see 'reply' followed by the IP address and a series of times in ms:

```{figure} /images/ping_Raspberry_pi_connected_modified.jpg
---

name: ping_Raspberry_pi_connected_modified_figure-fig
---
`ping -4` will give the IP address in a different format if required
```

Once the IP address is obtained to connect with the Raspberry Pi enter the following into the terminal, replacing `ip` with the IP address obtained:

 ```{code-block} console
ssh pi@[ip]

```
You will then be asked whether you want to connect and prompted to give the password ({numref}`rasp_pi_Imager_Advanced_settings_figure2-fig`) you set up for the Raspberry Pi:

```{figure} /images/connected_modified.jpg
---

name: connected_modified_figure-fig
---

```
Congratulations your Raspberry Pi is now connected to your computer.

## Setting up Docker

To install Docker on your Raspberry Pi, you will need to go through the following steps:

- Update your system
- Install docker
- Allow non-root users to execute docker commands
- Verify installation
- Run the hello-world container

### Update your system
After login you can start updating and upgrading the system to the latest security patch. The update command will synchronise the package repositories so that the system can check if any new packages are available. The upgrade command will update any packages of which a newer version is available.

```
sudo apt-get update && sudo apt-get upgrade
```

### Install docker
Conveniently a script has been developed that makes the process of installing docker considerably easy. The first step is to download the install file:

```
curl -fsSL https://get.docker.com -o get-docker.sh
```

Then enter the following command to execute the installation file:

```
sudo sh get-docker.sh
```

This command may request the root password and will then install the required packages for your Raspbian Linux distribution.

```{figure} /images/docker_to_install.jpg
---

name: docker_to_install-fig
---

```

### Allow non-root users to execute docker commands
By default only admin users are allowed to run containers. If you are not logged in as root you can use `sudo` to execute any of the docker commands. Another option is to add your user to the Docker group which will allow them to execute docker commands without `sudo`.

The syntax for adding users to the Docker group is:

```
sudo usermod -aG docker [user_name]
```

To add the Pi user (the default user in Raspbian), use the command:
```
sudo usermod -aG docker Pi
```
For the changes to take place, you need to log out and then back in.

### Verify installation
To verify if docker was installed correctly and you managed to add your user to the docker group execute the following command:

```
docker version
```
If all was correct you should be able to see the Docker version along with some additional information.

### Run the hello-world container
A final test is to actually start a docker container. To do so we will use the hello-world container made by docker.

```
docker run hello-world
```

This should return a welcome message.

```
Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```


## Portainer

Having verified that Docker is installed, the next thing is to install [Portainer](https://www.portainer.io/) a Universal Management System for Docker (among others). This will allow you to view and manage the docker containers. With the Raspberry Pi still connected via ssh, type into the terminal:

```{code-block} console
 sudo docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```

Once installed and with the Raspberry Pi still connected open an internet browser on your computer and navigate to:

```{code-block} console
 http://raspberrypi:9000/
 ```

 First time you will be prompted to create an account, create the account and fill in a password. After your account is created you are prompted with a multi option screen. Since portainer runs on the device of which you want to view the docker containers of you can select the `Local` instance and click on `Connect`. After that you should be able to enter the portainer instance and view the images and containers running on your instance.

 ```{figure} /images/portainer.jpg
---

name: portainer
---

```
