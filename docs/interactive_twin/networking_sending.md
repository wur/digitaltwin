# MQTT Sending data

After setting up your Raspberry Pi, or an alternative local machine, with [Docker](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation.html), [Portainer](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation_portainer.html), an [MQTT broker](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/cloud_storage.html), [Telegraf](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/cloud_storage.html), and [InfluxDB](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/cloud_storage.html) instances it is time to transfer data from the sensors connected to the internet enabled microcontroller, e.g., the Arduino Nano RP2040 Connect, to our MQTT broker.

## Submitting information 
Sends the data of a single, air quality, sensor to an [InfluxDB](https://www.influxdata.com/products/influxdb/) relies on sending information in the correct format. This [format](https://docs.influxdata.com/influxdb/cloud/reference/syntax/line-protocol/#timestamp) is as follows: 

```{code} python

# including optional timestamp
measurementName,tagKey=tagValue fieldKey="fieldValue" time

# excluding timestamp, timestamp provided by host machine
measurementName,tagKey=tagValue fieldKey="fieldValue"

```
Where:

- **Measurement**. Is the container for tags, field, time and that desribes the stored data associated with it. Consider that it is a data table's name.<br>
- **Tag**. Is additional metadata that is indexable. For instance, the name of the collection scientist, etc. <br>
- **Field**. Is the data (*field values*) associated with a column header (*field key*). For instance, oranges = 8 would be equivalent to a field key of oranges and field value of 8. <br>
- **Time** (*optional*). Is a Unix timestamp where time is in nanoseconds starting at January 1st 1970 at 00:00:00 UTC.<br>

There are two sections of whitespace in this format. The first unescaped space ( `tagValue fieldKey` ) delimits the metadata (measurement and the tag set) from the data (field set). Whereas, the second unescaped space delimits the field set from the timestamp if present. This first variant, without an optional timestamp, relies on the InfluxDB to provide the time stamp information per datapoint based upon the system time (UTC) of its host machine. When no timestamp is provided the time for the datapoint reflects the time the data was received and not when it was actually acquired, or measured.

This timestamp should be time is in nanoseconds starting at January 1st 1970 at 00:00:00 UTC. However, whilst InfluxDB expects timestamps to be in nanoseconds the Arduino `time` module does not give this level of precision. Therefore, the timestamp will be modified first by defining the timestamp (`time_now = time.time()`) and then adding the additional precision *000* to match the required precision (`str(time_now)  + '000' `). The timestamp precision is then incorporated into the URL (`&precision=ms`).


## Components

The components used for this module are as followed:

- Arduino Nano RP2040 Connect (€21,00)
- BME680 Air Quality Sensor (€20,00)
- Jumper Wire 4 pins ( < €1,00)
- Pimoroni breakout (optional)


## Code

### Secrets

The following information sensitive information should be incorporated into a dictionary assigned to the variable name `secrets` and saved as `secrets.py`

The code requires the secrets dictionary in secrets.py to be modified with the addition of several key-value pairs associated with MQTT. Note the the MQTT set-up described previously didn't include a username or password, so set these to the default:

```{code-block} python
""" secrets.py

    Contains:
        secrets = dictionary, containing sensitive information

"""
secrets = {
            'ssid'           : 'YOUR_WIFI_NAME',
            'password'       : 'WIFI_PASSWORD' ,

            'mqtt_broker'    : 'MQTT Broker address',   # MQTT broker address, e.g., 192.168.1.108
            'mqtt_port'      :  1883,                   # MQTT broker assinged Port
            'mqtt_username'  : 'mosquitto_username',    # MQTT broker username *UNASSIGNED* in set-up for now leave as is
            'mqtt_password'  : 'mosquitto_password',    # MQTT broker password *UNASSIGNED* in set-up for now  leave as is
            'mqtt_topic'     : 'sensors/garden'         # MQTT topic used for publishing to and subscribing to

            }
```

It can be called in scripts as:

```{code-block} python

from secrets import secrets

```

Although it is prudent to call it using a `try` and `except` statement:

```{code-block} python
try:
    from secrets import secrets
except ImportError:
    print("To run, add a dictionary containing keys ssid and password keys to dictionary called secrets in a file called secrets.py ")
    raise

```

## Script

```{code-block} python
import time
# Get data from a secrets.py file
from secrets import secrets

import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import board
from digitalio import DigitalInOut, Direction

from wur_circuitpython_functions import sensor_bme680, board_module, wifi_module, MQTT_comms

####################################
###          Led                ###
####################################

led = board_module.board_led()
led.turn_on_led()


####################################
###          Pins                ###
####################################

# i2c pins
sensor_1_scl = board.SCL
sensor_1_sda = board.SDA

# wifi pins
sensor_wifi_esp32_cs    = board.CS1
sensor_wifi_esp32_ready = board.ESP_BUSY
sensor_wifi_esp32_reset = board.ESP_RESET
sensor_wifi_SCK         = board.SCK1
sensor_wifi_SDO         = board.MOSI1
sensor_wifi_SDI         = board.MISO1


####################################
###          Sensor              ###
####################################

sensor1 = sensor_bme680.sensor_bme680()
sensor1.start_sensor(SCL_pin = sensor_1_scl, SDA_pin = sensor_1_sda)

####################################
###           WiFi               ###
####################################

get_connected = wifi_module.connect(pin_esp32_cs = sensor_wifi_esp32_cs,
                                    pin_esp32_ready = sensor_wifi_esp32_ready, pin_esp32_reset = sensor_wifi_esp32_reset,
                                    pin_SCK = sensor_wifi_SCK, pin_SDO = sensor_wifi_SDO, pin_SDI = sensor_wifi_SDI)
get_connected.connect_to_network(secrets["ssid"], secrets["password"])

####################################
###          MQTT                ###
####################################

sensor_mqtt_comms = MQTT_comms.MQTT_comms(get_connected.esp)
sensor_mqtt_comms.start_mqqt_interface(socket, get_connected.esp)
sensor_mqtt_comms.new_client_object(secrets["mqtt_broker"], secrets["mqtt_port"], secrets["mqtt_username"], secrets["mqtt_password"], secrets['mqtt_topic'])
sensor_mqtt_comms.connect()

####################################
###          END STARTUP         ###
####################################


led.blink(6, False)

####################################
###          LOOP                ###
####################################

# subscribe to topic: 
sensor_mqtt_comms.subscribe(secrets['mqtt_topic'])

while True:
    # Enable the led
    led.turn_on_led()

    # Check for wifi connection
    if get_connected.get_status() == False:
        get_connected.connect_to_network(secrets["ssid"], secrets["password"])

    # Check MQTT client connection
    if mqtt_comms.connection_status() == False:
        sensor_mqtt_comms.connect()

    # Perform measurements
    sensor1.measure_all()
    
    # Obtain measurements
    gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature = sensor1.measure_all(pressure_sea_level = 1011, temperature_offset = -5)

    # Print measurements
    sensor1.print_sensor_values()

    # Submit measurements
    sensor_mqtt_comms.publish(secrets["mqtt_topic"], "arduino,location=garden,arduino=RP2040,sensor=BME680 corrected_temperature=" +str(corrected_temperature) + ",measured_temperature="+str(measured_temperature)+",pressure_value="+str(pressure_value)+",altitude_value="+str(altitude_value)+",rel_humidity_value="+str(rel_humidity_value)+",gas_value="+str(gas_value))

    # Blink the LED
    led.blink(6, False)

    # Pull messages...
    rc = sensor_mqtt_comms.mqtt_client.loop(60)
    print("...")
    
    # ... and print output
    if sensor_mqtt_comms.payload != None:
        
        print("Message: " + sensor_mqtt_comms.payload)
        
        # Reset payload
        sensor_mqtt_comms.payload = None
    

    # Sleep for 30 seconds, rinse & repeat
    time.sleep(30)



```


## Example

````{admonition} Click here for the Sensor + WiFi + MQTT code!
:class: tip, dropdown

The following example shows how to publish, subscribe, and receive messages using MQTT.

```{code-block} python

########################
##      PACKAGES      ##
########################
import time
import board
import busio
from busio import I2C
import microcontroller
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import adafruit_requests as requests

# download wur_circuitpython_functions from https://gitlab.com/wur/digitaltwin/-/tree/main/code/lib/wur_circuitpython_functions
# remember to copy the full /lib https://gitlab.com/wur/digitaltwin/-/tree/main/code/lib as it contains the addtional Adafruit
# Circuitpython packages 
from wur_circuitpython_functions import sensor_bme680, sensor_ltr559, sensor_capacitive_soil_moisture, wifi_module, MQTT_comms
import params

try:
    from secrets import secrets
except ImportError:
    print("To run, add a dictionary containing keys ssid and password keys to dictionary called secrets in a file called secrets.py ")
    raise


########################
##   USER specified   ##
########################

# Specificiations for the board

# i2c pins
sensor_0_scl = board.SCL     # bme680 scl pin
sensor_0_sda = board.SDA     # bme680 sda pin
sensor_1_scl = board.D11     # ltr559 scl pin
sensor_1_sda = board.D13     # ltr559 scl pin

# digital/analog pins
sensor_2_power  = board.D2   # soil sensor power pin
sensor_2_analog = board.A0   # soil sensor analog pin

# wifi pins for an ESP32
sensor_wifi_esp32_cs    = board.CS1
sensor_wifi_esp32_ready = board.ESP_BUSY
sensor_wifi_esp32_reset = board.ESP_RESET
sensor_wifi_SCK         = board.SCK1
sensor_wifi_SDO         = board.MOSI1
sensor_wifi_SDI         = board.MISO1

# variables
location_sea_level_pressure  = 1013.25   # for BME680 location's pressure (hPa) at sea level, default is 1013.25
temperature_offset_from_true = -5        # for BME680 temperature sensor needs to be calibrated, default is -5
soil_sensor_modifier         = 100       # for the Capacitive Soil Moisture Sensor, default is 100

# number of times to retry 'try' statements
retry_count  = 10

# Time interval of measurements in seconds
measurement_time_interval = 10


########################
##   MESSAGE FORMAT   ##
########################

# mqtt payload with format: measurementName,tagKey=tagValue fieldKey="fieldValue"
iot_measurement_name  = "arduino"           # measurementName
iot_measurement_tags  = {     
        
                        "location" : "Brett_Office",                  # tagKey=tagValue  location tag, location of measurements, e.g., Brett_Office
                        "board"    : "arduino_nano_rp2040_connect",   # tagKey=tagValue  board tag, microcontroller,             e.g., arduino_nano_rp2040_connect
                        "iot_UID"  : "BM_000001",                     # tagKey=tagValue  iot_ID tag, IoT UID unique identifier,  e.g., BM_000001
                        "sensor"   : "bme680_ltr559_soil",            # tagKey=tagValue  sensor tag, the sensor tags,            e.g., bme680_ltr559_soil
    
                        }

iot_measurement_delimiters = {
    
                              'whitespace' : " ",
                              'comma'      : ",",
                              'equals'     : "=",

                              }


iot_measurement_delimiters['whitespace']
iot_measurement_delimiters['comma']
iot_measurement_delimiters['equals']



iot_tag_string = [iot_measurement_delimiters['comma'] + key + iot_measurement_delimiters['equals'] + iot_measurement_tags[key] for key in iot_measurement_tags]
iot_tag_string = ''.join(iot_tag_string)   # joins list together i.e., ',location=Brett_Office,iot_UID=BM_000001,sensor=bme680_ltr559_soil,board=arduino_nano_rp2040_connect'

# mqtt payload header:  "measurementName,tagKey=tagValue "
iot_header     = iot_measurement_name + iot_tag_string + iot_measurement_delimiters['whitespace']


########################
##      SENSORS       ##
########################

# start up all sensor objects
sensor_0 = sensor_bme680.sensor_bme680()
sensor_0.start_sensor(SCL_pin = sensor_0_scl, SDA_pin = sensor_0_sda)

sensor_1 = sensor_ltr559.sensor_ltr559()
sensor_1.start_sensor(SCL_pin = sensor_1_scl, SDA_pin = sensor_1_sda)

sensor_2 = sensor_capacitive_soil_moisture.soil_sensor()
sensor_2.start_sensor(power_pin = sensor_2_power, analog_pin = sensor_2_analog)


########################
##      WiFi          ##
########################

# connect to SSID using secrets["ssid"] and secrets["password"]
get_connected = wifi_module.connect(pin_esp32_cs = sensor_wifi_esp32_cs,
                                    pin_esp32_ready = sensor_wifi_esp32_ready, pin_esp32_reset = sensor_wifi_esp32_reset,
                                    pin_SCK = sensor_wifi_SCK, pin_SDO = sensor_wifi_SDO, pin_SDI = sensor_wifi_SDI)

get_connected.connect_to_network(secrets["ssid"], secrets["password"])

while not get_connected.get_connection_status():
    
    try:
        
        get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
    except RuntimeError as e:
        
        print("could not connect to AP, retrying: ", e)
        continue
    
    
########################
##      MQTT          ##
########################

# Set up and connect to MQTT
sensor_mqtt_comms = MQTT_comms.MQTT_comms(get_connected.esp)
sensor_mqtt_comms.start_mqqt_interface(socket, get_connected.esp)
sensor_mqtt_comms.new_client_object(secrets["mqtt_broker"], secrets["mqtt_port"], secrets["mqtt_username"], secrets["mqtt_password"], secrets['mqtt_topic'])

#sensor_mqtt_comms.connect()  # do try statement
count_MQTT = 0
check      = False
while not check:
    try:
        sensor_mqtt_comms.connect()
        check = True
        
    except RuntimeError as e:
        count_MQTT += 1
        
        print("CONNECTION - Could not connect to target AP, retrying: ", e)
        
        if count_MQTT == retry_count:
            break
        
        continue


########################
##      MAIN          ##
########################

# Step 0. Subscribe to topic
sensor_mqtt_comms.subscribe(secrets['mqtt_topic'])

while True:
    
    # Step 1. Check WiFi connection
    while get_connected.get_connection_status() == False:
        count_AP = 0
        
        try:
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
        
        except:
            
            count_AP +=1
            print('retrying')
            
            if count_AP == retry_count:
                
                break
            
            continue
    
    # Step 2. Check MQTT client connection
    if sensor_mqtt_comms.connection_status() == False:
        
        sensor_mqtt_comms.connect()
       
    # step 3. Get sensor measurements
    gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature = sensor_0.measure_all()
    lux_value, prox_value                    = sensor_1.measure_all()
    soil_sensor_value, soil_sensor_value_mod = sensor_2.measure_all(modifier = soil_sensor_modifier)
    
    # step 4. Print sensor measurements
    sensor_0.print_sensor_values()
    sensor_1.print_sensor_values()
    sensor_2.print_sensor_values()
    
    # step 5. Make message payload
    # Payload version: fieldKey="fieldValue" 
    message_payload      = "corrected_temperature=" +str(corrected_temperature) + ",measured_temperature="+str(measured_temperature)+",pressure_value="+str(pressure_value)+",altitude_value="+str(altitude_value)+",rel_humidity_value="+str(rel_humidity_value)+",gas_value="+str(gas_value) + ",soil_sensor=" + str(soil_sensor_value_mod) + ",lux=" + str(lux_value) 
        
    # Payload version: fieldKey="fieldValue" time
    # time_now = time.time()
    # message_payload_time =  str(time_now)  + '000'
    
    # step 6. Submit measurements:
    sensor_mqtt_comms.publish(secrets["mqtt_topic"], iot_header + message_payload)
    
    # step 7. Get subscriptions/ Poll the message queue
    print("Subscription:")
    rc = sensor_mqtt_comms.mqtt_client.loop(60)
    print("...")
    
    if sensor_mqtt_comms.payload != None:
        
        print("Message: " + sensor_mqtt_comms.payload)
        
        # Reset payload
        sensor_mqtt_comms.payload = None
    
    time.sleep(measurement_time_interval)

```
````

