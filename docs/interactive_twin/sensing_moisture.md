# Sensing moisture

```{figure} /images/Soil_sensors_V2.jpg
---

name: Soil_sensors_v2-fig
alt: Image of different soil sensors in a row
class: bg-primary mb-1
width: 100%
align: center
---

**Images of various Soil Moisture Sensors.** *Including, [Adafruit STEMMA - I2C Soil](https://www.adafruit.com/product/4026) Capacitive Moisture Sensor with ambient temperature; [Pimoroni Grow](https://shop.pimoroni.com/products/grow-moisture-sensor-pack-of-3?variant=32271401123923) capacitive moisture sensors using Pulse-Frequency Modulation; Analog Capacitive Soil Moisture Sensor V1.2; [DFRobot](https://wiki.dfrobot.com/Capacitive_Soil_Moisture_Sensor_SKU_SEN0193) [Analog](https://www.dfrobot.com/product-1385.html) Capacitive Soil Moisture Sensor V1.0; [Grove](https://wiki.seeedstudio.com/Grove-Moisture_Sensor/) - Moisture Sensor V1.4; and [Kitronik Prong Soil](https://kitronik.co.uk/products/5647-prong-soil-moisture-sensor-for-bbc-microbit?pr_prod_strat=description&pr_rec_id=6e3c87f5d&pr_rec_pid=4492257984575&pr_ref_pid=4854737469503&pr_seq=uniform) sensor.*

```
Having already obtained the ambient [gas](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html#version-a-bme680) level, the [temperature](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html), [humidity](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html), [pressure](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html), and ambient [light](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_light.html#components) levels it is now time to acquire the soil moisture ({numref}`Soil_sensors_v2-fig`). 

## Water

Alongside [other variables](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/plant_photosynthesis.html) such as carbon dioxide and nutrients Plants require water to be able to photosynthesise:

$$ 6 CO_{2} + 6 H_{2}O \: \: \overrightarrow{light} \: \: C_{6}H_{12}O_{6} + 6 O_{2} $$

*Most* plants acquire the water necessary for photosynthesis by absorbing the majority of it through their roots, although there will be a bit of water absorbed through their leaves. Water is transported from the site of absorption (e.g., roots) to the chloroplasts in their leaves where photosynthesis occurs. Water also plays a role in transporting nutrients from the soil into the plant ensuring cellular processes can continue.

Plants have visual cues when there is both a lack (*water limitation*) and excess of water. Although the cause can be difficult distinguish, some plant will visibly wilt or droop with both a lack or excess of water. Although with an excess of water, the plant's leaves may turn yellow and/or brown, no new growth may occur, and the soil around the plant may turn green as algae, finding the damp environment beneficial, grow.  

Therefore, it is important to have an accurate estimate of water presence, water use, and water-use efficiency by the plant that we intend to monitor. However, we will be using a **proxy**, an indirect measure of a variable of interest, of soil moisture rather than water usage inside the plant for two reasons. Firstly, soil sensors are cheap, if somewhat unreliable. Secondly, it captures a direct measurement of our proposed feedback (automated watering) ensuring that our system does not add too much or too little water.

## Components
- Arduino Nano RP2040 Connect (€21.00) with headers (€26.95)
- 3-6 x Jumper Wire male-to-female ( < €1.00)
- Analog Soil sensor ({numref}`Soil_sensors_v2-fig`) (~ €8.00)
- (*optional*) 300 pt (half) Breadboard
- (*optional*) I2C Soil sensor ({numref}`Soil_sensors_v2-fig`) (~ €8.00)

## Measurements

In this module we will show you how to monitor soil moisture with two variants on the soil sensor, I2C and an Analog setup:

- [Version A](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_moisture.html#variant-a-analog-soil-sensor) using an Analog Capacitive Soil Moisture Sensor. 

- [Version B](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_moisture.html#variant-b-i2c-soil-sensor) using a (Adafruit STEMMA) I2C Soil Capacitive Moisture Sensor.


## Variant A: Analog Soil Sensor
### Layout

```{figure} /images/Fritz_soil_sensor_analog.jpg
---
name: Fritz_Soil_sensor_schematic-fig
alt: Fritzing, breadboard schematic, 
class: bg-primary mb-1
width: 100%
align: center
---
**Set-up of the soil sensor.** *Fritzing diagram showing how to set-up the electronic components.*
```

### Breadboard

```{figure} /images/Breadboard.jpg
---
name: breadboard_half-fig4
alt: Picture of a Half (300 point) Breadboard 
class: bg-primary mb-1
width: 100%
align: center
---
**Half (300 point) Breadboard.** *Explanation of a Breadboard.*
```

First, let us arrange the wiring on the breadboard. With a half breadboard there are four sets of holes ({numref}`breadboard_half-fig4`), two outer sets of two rows each that are called **rails** ({numref}`breadboard_half-fig4`) and two inner sets of 5 rows and 30 columns ({numref}`breadboard_half-fig4`). The rails run horizontally and all holes in a row are connected, these can be used for power and ground. The two inner sets of holes are not connected. Within a set anything plugged into the same column will be connected so if you connect one hole in a column to ground the remaining 4 holes can be used to connect to ground.

###  Wire up components
Placing the breadboard on a table landscape ({numref}`Fritz_Soil_sensor_schematic-fig`) let us first place the Arduino Nano RP2040 Connect into the breadboard. If the breadboard is numbered, the microcontroller slots into the holes between d1 to d16 and h1 to h16, ensuring that the board's usb faces outwards. By placing the board into the breadboard each column in the breadboard will be associated with the individual pins. Place a jumper wire between the columns associated with a 3v3 pin and the power rail (denoted by *+*). Next do the same action but for a ground pin, connecting it to the ground rail (denoted by *-*). 

We will be using holes 19F, 20F and 21F as the location by which the breadboard and the sensor will be connected. If the breadboard you are using is not labelled then {numref}`Fritz_Soil_sensor_schematic-fig` gives you the holes we are referring to. Columns 19, 20, and 21 will be associated with an Analog I/O pin; a Digital I/O pin; and Ground respectively. For the *power* connect hole 20J with pin D2 (or GPIO25), the ground of the board to 21J, and the analog pin A0 (D14 or GPIO26) to 19J. Connect the sensor's blue cable, the analog, to hole 19F; the sensor's red cable, power, to hole 20F; and, the sensor's black cable, ground, to hole 21F.


### Soil sensor setup

```{figure} /images/Soil_sensor_schematic.jpg
---
name: Soil_sensor_schematic-fig
alt: Soil sensor schematic 
class: bg-primary mb-1
width: 100%
align: center
---
**Schematic of Analog Capacitive Soil Moisture Sensor V1.0.** *(A) Schematic of the soil sensor and (B) the [recommended depth](https://wiki.dfrobot.com/Capacitive_Soil_Moisture_Sensor_SKU_SEN0193) in the soil.*
```

There is an inverse relationship between the sensor's output value and (soil) moisture, at low moisture levels the value will be high whereas at high mositure levels the value will be low. However, the relative value should be determined prior to inserting the sensor into soil by using two end members: the sensor value in air and the sensor value in water. Run the following [example](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_moisture.html#example-1) whilst keeping the sensor in air and again after placing it in a glass of water. It should give a value of ~500 for the dry test and a value of ~400 for the wet test. 

### Code
#### Example 1

```{code} python
---

---


""" Example 1: Capacitive Soil Moisture Sensor with CircuitPython

# Notes:
        > Capacitive_Soil_Moisture_Sensor_SKU_SEN0193
        
        > RED = power; BLACK = Ground; BLUE = Analog pin
        > Typical ranges:
        > Dry   = [520, 430]
        > Wet   = [430, 350]
        > Water = [350, 260]  
        
        connect red power wire to pin D2, connect black
        ground wire to GND pin, connect blue wire to pin
        A0
        
# Modified from:
        > wiki:
        > https://wiki.dfrobot.com/Capacitive_Soil_Moisture_Sensor_SKU_SEN0193
        > concept:
        > https://andywarburton.co.uk/raspberry-pi-pico-soil-moisture-sensor/

"""

# packages
import board
import time
from analogio import AnalogIn
from digitalio import DigitalInOut, Direction, Pull


# SET-UP SOIL SENSOR
# setup the moisture sensor power pin and turn it off by default
soil_sensor_power           = DigitalInOut(board.D2)
soil_sensor_power.direction = Direction.OUTPUT
soil_sensor_power.value     = False

# set the analog read pin for the moisture sensor
soil_sensor_analog_in       = AnalogIn(board.A0)

# loop through
while True:
    
    soil_sensor_power.value = True
    
    sensor_value  = soil_sensor_analog_in.value
    print("Soil :  " + str(sensor_value/100))
    
    time.sleep(50)

```

Which will result in:

```{figure} /images/test_soil_sensor_example1a.jpg
---
name: output_ltr559-fig3
alt: Picture of the output of Analog Soil sensor using Circuitpython 
class: bg-primary mb-1
width: 100%
align: center
---
**Output for Analog Soil sensor.** 
```

<br></br>

## Variant B: I2C Soil Sensor
### Layout

```{figure} /images/Fritz_soil_sensor_i2c.jpg
---
name: Fritz_Soil_sensor_schematic_I2C-fig
alt: Fritzing, breadboard schematic, 
class: bg-primary mb-1
width: 100%
align: center
---
**Set-up of the soil sensor.** *Fritzing diagram showing how to set-up the electronic components.*
```


### Breadboard
```{figure} /images/Breadboard.jpg
---
name: breadboard_half-fig5
alt: Picture of a Half (300 point) Breadboard 
class: bg-primary mb-1
width: 100%
align: center
---
**Half (300 point) Breadboard.** *Explanation of a Breadboard.*
```

First, let us arrange the wiring on the breadboard. With a half breadboard there are four sets of holes ({numref}`breadboard_half-fig5`), two outer sets of two rows each that are called **rails** ({numref}`breadboard_half-fig5`) and two inner sets of 5 rows and 30 columns ({numref}`breadboard_half-fig5`). The rails run horizontally and all holes in a row are connected, these can be used for power and ground. The two inner sets of holes are not connected. Within a set anything plugged into the same column will be connected so if you connect one hole in a column to ground the remaining 4 holes can be used to connect to ground.

### Wire up components
Placing the breadboard on a table landscape ({numref}`Fritz_Soil_sensor_schematic_I2C-fig`) let us first place the Arduino Nano RP2040 Connect into the breadboard. If the breadboard is numbered, the microcontroller slots into the holes between d1 to d16 and h1 to h16, ensuring that the board’s usb faces outwards. By placing the board into the breadboard each column in the breadboard will be associated with the individual pins. Place a jumper wire between the columns associated with a 3v3 pin and the power rail (denoted by *+*). Next do the same action but for a ground pin, connecting it to the ground rail (denoted by *-*).

For the I2C soil sensor we will be using holes d26 to d30 as the location from which the sensor will be connected. If the breadboard you are using is not labelled then {numref}`Fritz_Soil_sensor_schematic_I2C-fig` gives you the holes we are referring to. Connecting holes A26 to A30 to the board to associate the columns with: 2-5v; SDA; SCL; no pin; and, ground. For the 2-5v connect the column (A26) with the power rain, likewise connect A30 to the ground rain. The default data (SDA) and clock (SCL) pins for the default I2C bus require you to connect the 8th (pin A4) and 9th (pin A5) pins of the Arduino (in the breadboard B8 and B9) with A27 and A28, respectively. The column associated with A29 remains unconnected.

Either slot in the breakout extender into the e26 to e30 or (Adafruit) connect the male ends of a JST-PH-to-male wire into e26 (2-5v = red wire), e27 (SDA = blue wire), e28 (SCL = yellow wire), and e30 (ground = black wire).


### Code
#### Example 1

```{code} python
---

---
"""  Example 1: I2C Soil Moisture Sensor with CircuitPython

# Notes:
        > Adafruit STEMMA Soil Sensor - I2C Capacitive Moisture Sensor
        
        > RED = D18 - SDA; BLACK = power (3v3); BLUE = D19 - SCL; YELLOW = Ground
        > Typical ranges:
        > Dry   = [ ]
        > Wet   = [ ]
        > Water = [ ]  
        
        connect to stemma QT using 
        
# Modified from:
        > https://learn.adafruit.com/adafruit-stemma-soil-sensor-i2c-capacitive-moisture-sensor/overview

"""

# packages
import time
import board
import busio
from adafruit_seesaw.seesaw import Seesaw

# variables

# Define I2C bus, for this example the default board.SCL and board.SDA are used
i2c = busio.I2C(board.SCL, board.SDA)

# Create sensor object, communicating over the board's default I2C bus
soil_sensor = Seesaw(i2c, addr=0x36)


while True:
    
    # read moisture level through capacitive touch pad
    moisture    = soil_sensor.moisture_read()
    
    # read temperature from the temperature sensor
    temperature = soil_sensor.get_temp()
    
    print("Soil: " + str(moisture) + "  , Temperature: " + str(temperature))    
    
    time.sleep(5)

```

Which will result in:

```{figure} /images/test_soil_sensor_example2.jpg
---
name: Soil_sensor_I2C-fig
alt: Picture of the output of I2C Soil sensor using Circuitpython 
class: bg-primary mb-1
width: 100%
align: center
---
**Output for I2C Soil sensor.** 
```
