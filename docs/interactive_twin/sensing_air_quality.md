# Sensing Temperature and Air quality
## Rationale
### Gases
Sunlight provides the energy from which plants chemically combine carbon dioxide and water ([Caffarri et al., 2014](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4030627/)). The carbon dioxide molecules gain electrons (become *reduced*) whilst the water loses electrons (become *oxidised*) transforming water into oxygen and carbon dioxide into glucose. The produced glucose molecules act as the energy stores of the plant. 

Like any chemical reaction the concentration of the reactants can have an impact on the rate of reaction. Therefore, the concentration of carbon dioxide in the air is a potential explanatory variable for changing photosynthetic reaction rates. Studies have shown that doubling CO2 leads to increases in reproductive yield of plants, although the amount of this increase will differ between C3 (~30% increase) and C4 (<10% increase) species ([Hatfield et al., 2011](https://acsess.onlinelibrary.wiley.com/doi/epdf/10.2134/agronj2010.0303)).

The presence of other gases and compounds in the air can also have consequence for our plants. These include particles that modify the stomata (= *holes in the leaves*), aerosols that impact air or light exchange, and air pollutants such as ozone (O3), nitrogen oxides (NOx), ammonia (NH3) and sulphur dioxide (SO2) that can cause damage ([Bender and Weigel, 2011](https://link.springer.com/article/10.1051/agro/2010013); [Cieslik et al., 2009](https://onlinelibrary.wiley.com/doi/10.1111/j.1438-8677.2009.00262.x)). Other gases present in the air may in fact be a byproduct of the plant's metabolism. 
<br>

### Temperature
The concentration and presence of gases is not the only thing that can impact plant growth, health, and yield. Although plants have evolved to cover much of Planet Earth an individual species will have a defined range in minimum and maximum temperatures where growth can occur. Along this temperature range the rate of growth will vary, decreasing before eventually ceasing towards the extremes but also having an optimum temperature where growth rate is fastest ([Hatfield et al., 2011](https://acsess.onlinelibrary.wiley.com/doi/epdf/10.2134/agronj2010.0303)). Transitory or constant elevated temperatures, as predicted for the coming century, can lead to physiological, biochemical and molecular changes in plants reducing photosynthesis (e.g., Photosystem II) and negatively impacting growth and productivity ([Song et al., 2014](https://bmcplantbiol.biomedcentral.com/articles/10.1186/1471-2229-14-111)). Temperature also has an impact upon complex life processes, including enzyme reactions, membrane transport, transpiration, pollination, and plant development ([Hatfield and Prueger, 2015](https://doi.org/10.1016/j.wace.2015.08.001); [Yu et al., 2015](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0135452)). Air Temperature also impacts the physical characteristics around the plant, such as soil moisture. 

However, It is prudent to note that plant temperature and air temperature can differ both as a function of external  (e.g., light levels, relative humidity, presence and amount of air movement) and internal variables ([Hatfield et al., 2011](https://acsess.onlinelibrary.wiley.com/doi/epdf/10.2134/agronj2010.0303); [Yu et al., 2015](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0135452)). For instance, plant temperature maybe higher than surrounding air temperature under water stress but lower when soil water conditions are adequate.  
<br>

## Measurements
In this module we will show you how to monitor temperature and air quality sensors with two variants: 
- [Version A](## Version A: BME680) using solely the BME680 Air Quality sensor that has temperature, humidity, gas, and pressure as output
- [Version B](## Version B: SGP30 and Temperature sensor) using both the SGP30 Air Quality Sensor and an independent temperature sensor. 
<br></br>

## Version A: BME680
In this version we will show you how to connect the [Bosch BME680](https://www.bosch-sensortec.com/products/environmental-sensors/gas-sensors/bme680/) Temperature, Pressure, Humidity, Air Quality Sensor onto a Arduino Nano RP2040 Connect board using [I2C](https://wur.gitlab.io/digitaltwin/docs/setup/communication.html).

### Components

The components used for this module are as followed:
- Arduino Nano RP2040 Connect (€21.00) with headers (€26.95)
- BME680 Air Quality Sensor Pimoroni breakout (€20.00) or Adafruit Stemma QT (€20.00)
- Jumper Wire 4 pins (< €1.00)
- (*optional*) 300 pt (half) Breadboard
- (*optional*) [Pimoroni I2C Breakout Extender](https://shop.pimoroni.com/products/breakout-extender?variant=13601226588243) (€7.31 for 3) 
- (*optional*) 2x [4-pin 1 mm JST-PH Stemma QT cable](https://learn.adafruit.com/introducing-adafruit-stemma-qt/what-is-stemma-qt) including male or female to JST-PH versions (< €2.00)


### Layout

```{figure} /images/Fritz_air_v2.jpg
---
name: airquality_sensor_fritz-fig
alt: Airquality wiring layout
class: bg-primary mb-1
width: 100%
align: center
---
**Set-up of the air quality sensor.** *Schematic and Fritzing diagram showing how to set-up the electronic components. The pins used on the Arduino Nano RP2040 Connect are red wire: 3v3, black wire: Ground, yellow wire: SDA (A4), and orange wire: SCL (A5). The 3v3 and Ground are connected to the Breaboard's rails to allow for multiple wires to attached, whereas the SDA and SCL are connected directly. The colour of the holes in the breadboard denote *connections* with green highlighting those holes associated with a wire or header.*
```

#### Breadboard

```{figure} /images/Breadboard.jpg
---
name: breadboard_half-fig1
alt: Picture of a Half (300 point) Breadboard 
class: bg-primary mb-1
width: 100%
align: center
---
**Half (300 point) Breadboard.** *Explanation of a Breadboard.*
```

First, let us arrange the wiring on the breadboard. With a half breadboard there are four sets of holes ({numref}`breadboard_half-fig1`), two outer sets of two rows each that are called **rails** ({numref}`breadboard_half-fig1`) and two inner sets of 5 rows and 30 columns ({numref}`breadboard_half-fig1`). The rails run horizontally and all holes in a row are connected, these can be used for power and ground. The two inner sets of holes are not connected. Within a set anything plugged into the same column will be connected so if you connect one hole in a column to ground the remaining 4 holes can be used to connect to ground.

####  Wire up components
Placing the breadboard on a table landscape ({numref}`breadboard_half-fig1`) let us first place the Arduino Nano RP2040 Connect into the breadboard. If the breadboard is numbered, the microcontroller slots into the holes between d1 to d16 and h1 to h16, ensuring that the board's usb faces outwards. By placing the board into the breadboard each column in the breadboard will be associated with the individual pins. Place a jumper wire between the columns associated with a 3v3 pin and the power rail (denoted by *+*). Next do the same action but for a ground pin, connecting it to the ground rail (denoted by *-*). 

Irrespective of the sensor (i.e., Pimoroni or Adafruit) we will be using holes d26 to d30 as the location by which the sensor will be connected. If the breadboard you are using is not labelled then {numref}`breadboard_half-fig1` gives you the holes we are referring to. From d26 to d30 the columns should be associated with: 2-5v; SDA; SCL; no pin; and, ground. For the 2-5v connect the column (d26) with the power rain, likewise connect d30 to the ground rain. The default data (SDA) and clock (SCL) pins for the default I2C bus require you to connect the 8th (pin A4) and 9th (pin A5) pins of the Arduino (in the breadboard b8 and b9) with d27 and d28, respectively. The column associated with d29 remains unconnected. 

Depending upon the purchased sensor (i.e., Pimoroni or Adafruit) either (Pimoroni) slot in the breakout extender into the e26 to e30 or (Adafruit) connect the male ends of a JST-PH-to-male wire into e26 (*2-5v = red wire*), e27 (*SDA = blue wire*), e28 (*SCL = yellow wire*), and e30 *(ground = black wire*).
 

### Software libraries

- BME680 Air Quality Sensor (adafruit_bme680)

To be able to read the `BME680 Air Quality Sensor` the library `adafruit_bme680` is used. This library is part of the `circuitpython` library and can be directly copied into the `libs` folder of the arduino board.


### Code
#### Example 1A

```{code} python
---

---
""" Example 1: BME680 with CircuitPython
# Modified from:
# https://learn.adafruit.com/adafruit-bme680-humidity-temperature-barometic-pressure-voc-gas/python-circuitpython
# Original: SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries; SPDX-License-Identifier: MIT
"""

# packages
import time
import board
import adafruit_bme680

# variables 
location_sea_level_pressure  = 1013.25   # location's pressure (hPa) at sea level
temperature_offset_from_true = -5        # sensor needs to be calibrated

# Define I2C bus, for this example the default board.SCL and board.SDA are used
i2c = board.I2C()  

# Create sensor object, communicating over the board's default I2C bus
bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, address=0x76, debug=False)
# or use alternate address, by uncommenting:
#bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, address=0x77, debug=False)

# modified output
bme680.sea_level_pressure = location_sea_level_pressure

# initiate loop and print out sensor variables
while True:

    print("\nTemperature: %0.1f C"  % (bme680.temperature + temperature_offset_from_true))
    print("Gas: %d ohm"             % bme680.gas)
    print("Humidity: %0.1f %%"      % bme680.relative_humidity)
    print("Pressure: %0.3f hPa"     % bme680.pressure)
    print("Altitude = %0.2f meters" % bme680.altitude)

    time.sleep(1)

```
Which will result in:

```{figure} /images/test_check_bme680.jpg
---
name: output_bme680-fig
alt: Picture of the output of BME680 sensor using Circuitpython 
class: bg-primary mb-1
width: 100%
align: center
---
**Output for BME680.** 
```


<br></br>

## Version B: SGP30 and Temperature sensor
In this version we will show you how to connect the [Sensiron SGP30](https://sensirion.com/us/products/catalog/SGP30/) Multi-Pixel fully integrated MOX gas Air Quality Sensor onto a Arduino Nano RP2040 Connect board along with a temperature sensor (e.g., SHT40) using [I2C](https://wur.gitlab.io/digitaltwin/docs/setup/communication.html). 

### Components

The components used for this module are as followed:
- Arduino Nano RP2040 Connect (€21.00) with headers (€26.95)
- SGP30 Air Quality Sensor Pimoroni breakout (€21.95) or Adafruit Stemma QT (€23.95)
- Temperature and Humidity Sensor, e.g., SHT40 Adafruit Stemma QT (€23.95) or BME280
- Jumper Wire 4 to 8 pins (< €1.00)
- (*optional*) 300 pt (half) Breadboard
- (*optional*) [Pimoroni I2C Breakout Extender](https://shop.pimoroni.com/products/breakout-extender?variant=13601226588243) (€7.31 for 3) 
- (*optional*) 2x [4-pin 1 mm JST-PH Stemma QT cable](https://learn.adafruit.com/introducing-adafruit-stemma-qt/what-is-stemma-qt) including male or female to JST-PH versions (< €2.00)

### Layout

```{figure} /images/Fritz_air_v2_generic_double_i2c.jpg
---
name: airquality_sensor_fritz-fig
alt: Airquality wiring layout
class: bg-primary mb-1
width: 100%
align: center
---
**Set-up of the air quality sensor.** *Schematic and Fritzing diagram showing how to set-up the electronic components. The pins used on the Arduino Nano RP2040 Connect are red wire: 3v3, black wire: Ground, yellow wire: SDA (A4), and orange wire: SCL (A5). The 3v3 and Ground are connected to the Breaboard's rails to allow for multiple wires to attached, whereas the SDA and SCL are connected directly. The second I2C device is connected in a series so the wires from the first I2C device are connected to the upper half of the breadboard (alternative approaches outlined here {numref}`breadboard_half_two_i2c-fig` ). The colour of the holes in the breadboard denote *connections* with green highlighting those holes associated with a wire or header.*
```
<br>

#### Breadboard

```{figure} /images/Breadboard.jpg
---
name: breadboard_half-fig2
alt: Picture of a Half (300 point) Breadboard 
class: bg-primary mb-1
width: 100%
align: center
---
**Half (300 point) Breadboard.** *Explanation of a Breadboard.*
```

First, let us arrange the wiring on the breadboard. With a half breadboard there are four sets of holes ({numref}`breadboard_half-fig2`), two outer sets of two rows each that are called **rails** ({numref}`breadboard_half-fig2`) and two inner sets of 5 rows and 30 columns ({numref}`breadboard_half-fig2`). The rails run horizontally and all holes in a row are connected, these can be used for power and ground. The two inner sets of holes are not connected. Within a set anything plugged into the same column will be connected so if you connect one hole in a column to ground the remaining 4 holes can be used to connect to ground.

####  Wire up components
Placing the breadboard on a table landscape ({numref}`breadboard_half-fig2`) let us first place the Arduino Nano RP2040 Connect into the breadboard. If the breadboard is numbered, the microcontroller slots into the holes between d1 to d16 and h1 to h16, ensuring that the board's usb faces outwards. By placing the board into the breadboard each column in the breadboard will be associated with the individual pins. Place a jumper wire between the columns associated with a 3v3 pin and the power rail (denoted by *+*). Next do the same action but for a ground pin, connecting it to the ground rail (denoted by *-*). 

Irrespective of the sensor (i.e., Pimoroni or Adafruit) we will be using holes d26 to d30 as the location by which the sensor will be connected and holes a26 to a30 where the wires will be connected. If the breadboard you are using is not labelled then {numref}`breadboard_half-fig2` gives you the holes we are referring to. From d26 to d30 the columns should be associated with: 2-5v; SDA; SCL; no pin; and, ground. For the 2-5v connect hole a26 with the power rain, likewise connect a30 to the ground rain. The default data (SDA) and clock (SCL) pins for the default I2C bus require you to connect the microcontroller's left handside's 8th (pin A4) and 9th (pin A5) pins of the Arduino (in the breadboard b8 and b9) with holes a27 and a28, respectively. The column associated with a29 remains unconnected. 

Depending upon the purchased sensor (i.e., Pimoroni or Adafruit) either (Pimoroni sensor) slot in the breakout extender into holes d26 to d30 or (Adafruit sensor) connect the male ends of a JST-PH-to-male wire into d26 (*2-5v = red wire*), d27 (*SDA = blue wire*), d28 (*SCL = yellow wire*), and d30 *(ground = black wire*). For the second sensor, either wire the next set of holes by placing jumper cables from the columns (e26 to e30) to the next (f26 to f30) set of columns (*left* in {numref}`breadboard_half_two_i2c-fig`). Or connect the second i2c bus by connecting the left handside's 1st (pin D13) pin to the SDA and the right handsides 2nd (pin D11) pin to the SCL alongside a 3v3 and ground pins (*right* in {numref}`breadboard_half_two_i2c-fig`). Like so:

```{figure} /images/double_i2c_differences.jpg
---
name: breadboard_half_two_i2c-fig
alt: Picture of a Half (300 point) Breadboard 
class: bg-primary mb-1
width: 100%
align: center
---
**Wire two sensors to a single microcontroller via I2C (*Left*) in a series or (*Right*) the second i2c bus.** *(Left) The pins used on the Arduino Nano RP2040 Connect are red wire: 3v3, black wire: Ground, yellow wire: SDA (A4), and orange wire: SCL (A5). The 3v3 and Ground are connected to the Breadboard's rails to allow for multiple wires to attached, whereas the SDA and SCL are connected directly. The second I2C device is connected in a series so the wires from the first I2C device are connected to the upper half. (Right) The pins used on the Arduino Nano RP2040 Connect are red wire: 3v3, black wire: Ground, yellow wire: SDA (pin A4), and orange wire: SCL (pin A5). The 3v3 and Ground are connected to the Breadboard's rails to allow for multiple wires to attached, whereas the SDA and SCL are connected directly. For the second I2C power and ground come from the rails whilst the white wire leads from the second SDA (pin D13) and the blue wire from the second SCL (pin D11).*  
```

### Software libraries

- SGP30 Air Quality Sensor (adafruit_sgp30)
- SHT40 Temperature and Humidity Sensor (adafruit_sht4x)

To be able to read the `SGP30 Air Quality Sensor` the library `adafruit_sgp30` is used, likewise the `SHT40 Temperature and Humidity Sensor` requires the `adafruit_sht4x` library. Both of these libraries are part of the `circuitpython` library and can be directly copied into the `libs` folder of the arduino board.


### Code
#### Example 1B

```{code} python
---

---
""" Example 1: SGP30 with CircuitPython
# Modified from:
# https://learn.adafruit.com/adafruit-sgp30-gas-tvoc-eco2-mox-sensor/circuitpython-wiring-test
# Original: SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries; SPDX-License-Identifier: MIT
"""

# packages
import time
import board
import busio
import adafruit_sgp30

# variables
baseline_eCO2         =  0x8973 #in hex
baseline_TVOC         =  0x8AAE #in hex
baseline_temperature  =  22.1   #in celcius
baseline_rel_humidity =  44     #in percent
calibration_time      =  15     #in seconds, time for calibration step
print_baseline_count  =  10     #in seconds, time for printing baseline var.

# Define I2C bus, for this example the default board.SCL and board.SDA are used
i2c = busio.I2C(board.SCL, board.SDA)

# Create sensor object, communicating over the board's default I2C bus
sgp30 = adafruit_sgp30.Adafruit_SGP30(i2c, address=0x58)

# Get serial hex code of the sensor
print("SGP30 serial #", [hex(i) for i in sgp30.serial])   

# modify output:
# IAQ baseline for TVOC and eCO2 
sgp30.set_iaq_baseline(baseline_TVOC, baseline_eCO2)
# Set baseline temperature and rel. humidity
sgp30.set_iaq_relative_humidity(baseline_temperature,
                                baseline_rel_humidity)


# initiate loop and print out sensor variables
elapsed_second_count = 0
print_count          = 0
measurement          = False

while True:
    
    print("t = %i \t eCO2 = %d ppm \t TVOC = %d ppb \t measurement = %s" % (elapsed_second_count,
                                                                           sgp30.eCO2, sgp30.TVOC,
                                                                           measurement))
    print_count           += 1
    
    time.sleep(1)
    
    elapsed_second_count += 1
    
    if elapsed_second_count == calibration_time:
        
        print()
        print('Calibration step finished, starting measurements') 
        measurement = True
        
    if print_count > print_baseline_count:
        
        # reset counter
        print_count = 0
        
        print()
        print(
                "**** Baseline values: eCO2 = 0x%x, TVOC = 0x%x"
                % (sgp30.baseline_eCO2, sgp30.baseline_TVOC)
                )
        print()


```
Which will result in:

```{figure} /images/test_check_sgp30.jpg
---
name: output_sgp30-fig
alt: Picture of the output of SGP30 sensor using Circuitpython 
class: bg-primary mb-1
width: 100%
align: center
---
**Output for SGP30.** 
```

#### Example 2B

```{code} python
---

---
""" Example 2: SGP30 and BME280 with CircuitPython
# Modified from:
# https://learn.adafruit.com/adafruit-sgp30-gas-tvoc-eco2-mox-sensor/circuitpython-wiring-test
# Original: SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries; SPDX-License-Identifier: MIT
"""

# packages
import time
import board
import busio
import adafruit_sgp30
from adafruit_bme280 import basic as adafruit_bme280

# variables
baseline_eCO2         =  0x8973 #in hex
baseline_TVOC         =  0x8AAE #in hex
baseline_temperature  =  22.1   #in celcius
baseline_rel_humidity =  44     #in percent
calibration_time      =  15     #in seconds, time for calibration step
print_baseline_count  =  10     #in seconds, time for printing baseline var.

# Define I2C bus, for this example the default board.SCL and board.SDA are used
i2c = busio.I2C(board.SCL, board.SDA)

# Create sensor object, communicating over the board's default I2C bus
sgp30 = adafruit_sgp30.Adafruit_SGP30(i2c, address=0x58)

# Get serial hex code of the sensor
print("SGP30 serial #", [hex(i) for i in sgp30.serial])   

# Create second object, communicating using the same i2c, give address
# alternate address is 0x77
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c, address=0x76)

# modify output:
# IAQ baseline for TVOC and eCO2 
sgp30.set_iaq_baseline(baseline_TVOC, baseline_eCO2)
# Set baseline temperature and rel. humidity
sgp30.set_iaq_relative_humidity(baseline_temperature,
                                baseline_rel_humidity)


# initiate loop and print out sensor variables
elapsed_second_count = 0
print_count          = 0
measurement          = False

print("Start measuring...")
print("\nTemperature: %0.1f C" % bme280.temperature)
print("Humidity: %0.1f %%"     % bme280.humidity)
print("Pressure: %0.1f hPa"    % bme280.pressure)


while True:
    
    print("t = %i \t eCO2 = %d ppm \t TVOC = %d ppb \t measurement = %s \t Temp =  %0.1f C \t Humid =  %0.1f " % (elapsed_second_count,
                                                                           sgp30.eCO2, sgp30.TVOC, measurement, bme280.temperature,
                                                                            bme280.humidity))
    print_count           += 1
    
    time.sleep(1)
    
    elapsed_second_count += 1
    
    if elapsed_second_count == calibration_time:
        
        print()
        print('Calibration step finished, starting measurements') 
        measurement = True
        
    if print_count > print_baseline_count:
        
        # reset counter
        print_count = 0
        
        print()
        print(
                "**** Baseline values: eCO2 = 0x%x, TVOC = 0x%x"
                % (sgp30.baseline_eCO2, sgp30.baseline_TVOC)
                )
        print()

```
Which will result in:

```{figure} /images/test_check_sgp30_bme280.jpg
---
name: output_sgp30_bme280-fig
alt: Picture of the output of SGP30 with BME280 sensor using Circuitpython 
class: bg-primary mb-1
width: 100%
align: center
---
**Output for SGP30 with BME280.** 
```


