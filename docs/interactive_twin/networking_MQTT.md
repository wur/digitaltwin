# MQTT
After setting up your raspberry pi or local machine with a MQTT broker, Telegraf and InfluxDB instance it is time to use the sensors connected to the Arduino to submit the information to our MQTT broker and initiate another listener to subscribe to a topic and to anticipate a message.

## Components
The components used for this module are as followed:

- Arduino Nano RP2040 Connect (€21,00)
- BME680 Air Quality Sensor (€20,00)
- Jumper Wire 4 pins ( < €1,00)
- (*optional*) I2C sensor, e.g., Adafruit or Pimoroni breakout

## Code


### MQTT Class


```{code-block} python
---

---
""" MQTT_comms.py

# Rationale:
    MQTT Machine-to-Machine Communcication protocol

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/networking_MQTT.html

# Requirements:
    import adafruit_esp32spi.adafruit_esp32spi_socket as socket
    import adafruit_minimqtt.adafruit_minimqtt as MQTT

# Contains:
    class : MQTT_comms
            which contains: [_init_, start_mqqt_interface,
                            new_client_object, connect,
                            connection_status, publish,
                            print_connect, print_subscribe,
                            print_unsubscribe, print_disconnect,
                            print_message, print_publish]

# References:
    https://learn.adafruit.com/mqtt-in-circuitpython/circuitpython-wifi-usage
    
"""

import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import adafruit_minimqtt.adafruit_minimqtt as MQTT


class MQTT_comms:
        
    def __init__(self, esp):
        """ initialise class
        
        Input:
            esp = esp connection
        
        Packages:
            import adafruit_esp32spi.adafruit_esp32spi_socket as socket
            import adafruit_minimqtt.adafruit_minimqtt as MQTT
        
        Reference:
            https://learn.adafruit.com/mqtt-in-circuitpython/circuitpython-wifi-usage
        
        """
        # VARIABLES
        # Internal class variables
        self.broker   = None
        self.port     = None
        self.username = None
        self.password = None
        self.topic    = None
        
        # socket and MQTT from module package call
        self.socket   = socket
        self.MQTT     = MQTT
        
        # user defined
        self.esp      = esp
        
        # FUNCTION
        # Start interface
        self.start_mqqt_interface(self.socket, self.esp)   
    
    
    def start_mqqt_interface(self, socket, esp):
        """ FUNC: start MQTT interface
        
        Initialize MQTT interface with the esp interface
        
        # Input:
            socket = web socket
            esp    = esp interface
        
        # Packages
            import adafruit_minimqtt.adafruit_minimqtt as MQTT
        """
        
        # functions            
        self.MQTT.set_socket(socket, esp)  
        
        
    def new_client_object(self, broker_to_use = None, port_to_use = None,
                                username_to_use = None, password_to_use = None,
                                topic_to_use = None):
        """FUNC: Initialize a new MQTT Client object

        Initialize a new MQTT Client object
        
        """     
        
        # functions
        self.mqtt_client = self.MQTT.MQTT(
                                            broker     = broker_to_use,
                                            port       = port_to_use,
                                            username   = username_to_use,
                                            password   = password_to_use,
                                           #socket_pool = socket,
                                        )

        # Connect callback handlers to mqtt_client
        self.mqtt_client.on_connect      = self.print_connect
        self.mqtt_client.on_disconnect   = self.print_disconnect
        self.mqtt_client.on_subscribe    = self.print_subscribe
        self.mqtt_client.on_unsubscribe  = self.print_unsubscribe
        self.mqtt_client.on_publish      = self.print_publish
        self.mqtt_client.on_message      = self.print_message

        self.mqtt_topic = topic_to_use
        
        # Class internal variables
        # set variables
        self.broker   = broker_to_use
        self.port     = port_to_use
        self.username = username_to_use
        self.password = password_to_use
        self.topic    = topic_to_use
    
    
    # FUNCTIONS: Connect
    def connect(self):
        """CONNECT: connect to broker

        """
        print("Attempting to connect to %s" % self.mqtt_client.broker)
        self.mqtt_client.connect()
    
    
    def connection_status(self):
        """CONNECT: check connection status
        
        """
        return self.mqtt_client.is_connected()
    
    
    # FUNCTIONS: Publish
    def publish(self, feed, value):
        """PUBLISH: publish value to feed

        """
        self.mqtt_client.publish(feed,value)
    
    
    # FUNCTIONS: Print
    def print_connect(self, mqtt_client, userdata, flags, rc):
        """EVENT: Connect
        This function will be called when the mqtt_client is connected # successfully to the broker.
        
        """
        print("Connected to MQTT Broker!")
        print("Flags: {0}\n RC: {1}".format(flags, rc))


    def print_subscribe(self, client, userdata, topic, granted_qos):
        """EVENT: Subscribe
        This method is called when the client subscribes to a new feed.

        """
        print("Subscribed to {0} with QOS level {1}".format(topic, granted_qos))

    
    def print_unsubscribe(self, client, userdata, topic, pid):
        """EVENT: unsubscribe
        This method is called when the client unsubscribes from a feed.

        """
        print("Unsubscribed from {0} with PID {1}".format(topic, pid))


    def print_disconnect(self, mqtt_client, userdata, rc):
        """EVENT: disconnect
        Disconnected function will be called when the client disconnects.
        
        """
        print("Disconnected from MQTT Broker!")

    
    def print_message(self, client, feed_id, payload):
        """ FUNC: message
        # Message function will be called when a subscribed feed has a new value.
        # The feed_id parameter identifies the feed, and the payload parameter has
        # the new value.
        """
        print("Feed {0} received new value: {1}".format(feed_id, payload))

    
    def print_publish(self, mqtt_client, userdata, topic, pid):
        """ FUNC: publish
        # This method is called when the mqtt_client publishes data to a feed.
        """
        print("Published to {0} with PID {1}".format(topic, pid))




```

