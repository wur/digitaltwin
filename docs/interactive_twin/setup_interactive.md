# Set-up

```{figure} /images/Plant_requirements_sensors.jpg
---

name: Plant_requirements_sensors-fig
---
 **Sensor information required to manage our Plants that will feed into our Digital Twin**
```

To begin our twin needs sensors to collect pertinent variables, the code to collect, store, and perform actions, and the actuators to perform those actions. However, for this Digital Twin instance multiple devices will be acting as either our sensors or actuators. Therefore, we will need a way to communicate between the different devices and to relay information between them. In this example multiple Arduino Nano RP2040 Connect microcontrollers will act as our Internet of Things (IoT) devices alongside a Raspberry Pi single board computer that will act as a central hub for data, visualisation, and predictions. In the following the necessary components and the steps required to get them to a state ready for the next sections are outlined.  

## Software and Hardware requirements

### Hardware
#### Required components:

- Raspberry Pi Zero 2 W (€19.95) or Raspberry Pi 4 Model B - 2GB (€57.95)
- Micro SD card, e.g., 32 Gb  (€11.95)
- Micro SD card reader, e.g., [Transcend RDF5 USB 3.0 SD/microSD Card Reader](https://www.kiwi-electronics.com/en/transcend-rdf5-usb-3-0-sd-microsd-card-reader-wit-897)  (€9.95)
- USB-C Power Supply, e.g., official Raspberry Pi USB-C Power Supply (€9.95)
- 3x Arduino Nano RP2040 Connect (€26.95 each)
- 3x MicroUSB to USB-A to Micro-B cable (~€1.25)
- Pimoroni BME680 Air Quality Sensor (€22.95) or Adafruit BME680 Air Quality Sensor (€22.95)
- Pimoroni LTR-559 Light and proximity sensor (€10.95)
- Soil sensor (> €3)
- Mini, 170 point, Breadboard (€2.50 each)
- 10K Ω resistor
- Mini submersible pump 2.5V-6V
- IRL540 Power mosfet with logic level gate drive 
- a PCB Terminal Block Connector
- Silicon tubing
- Various male-to-male and female-to-male jumper wires
- (*optional*) if using Adafruit sensors STEMMA QT / Qwiic JST SH 4-pin Cable with Premium Female Sockets - 150mm Long (€1.15) and STEMMA QT / Qwiic JST SH 4-Pin Cable - 50mm Long (€1.15)
- As well as a plant pot, soil, seeds or a plant
<br/><br/>


### Software
#### Raspberry Pi OS
For the Raspberry Pi you will need to first install an operating system (OS) on to a micro SD card and set up the device headless (i.e., without monitor, keyboard or mouse). The steps for this can be found [here](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html).

#### Thonny 
Thonny is an open-source integrated development environment (IDE) for Windows, Linux, and Mac OS that is useful for writing and uploading CircuitPython to single board microcontrollers (such as a micro:bit, Raspberry Pico, or Arduino Nano RP2040 Connect). This IDE can be downloaded [here](https://thonny.org/) alongside the installation instructions. 

#### CircuitPython 
On each of the Arduino Nano RP2040 Connects CircuitPython, a fork of MicroPython and a much leaner implementation of Python 3 optimised for use on microcontrollers, needs to be installed. To [install CircuitPython](https://wur.gitlab.io/digitaltwin/docs/setup/circuitpython_setup.html) to a Arduino Nano RP2040 Connect attach the USB to the Arduino, connect the Arduino to a computer, and double tap the ‘RESET’ button on the board. The Arduino should appear as a new drive called ‘RPI-RP2’, download the [CircuitPython firmware image](https://circuitpython.org/board/arduino_nano_rp2040_connect/). Copy the downloaded .uf2 file to the ‘RPI-RP2’ drive. The Arduino will now restart becoming a drive labelled CIRCUITPY. 

To write a programme to the Arduino, connect the USB of the Arduino with a computer (you do not need to hold down the BOOTSEL). Open Thonny or any other IDE that allows for connection and flashing your CircuitPython programmes. Write your programme, or copy and paste the code from the following examples, and **save your programme as code.py to the top directory of the Arduino**. Additional libraries can be installed in the libs folder. Unplug from the computer and connect to an external power source and, as long as the intended programme is named code.py (or main.py), the programme should run independently of your computer.

<br/>




