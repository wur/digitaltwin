# Cloud Storage: HTTP

## Alternative approach
In the following HTTP GET and POST are used as an alternative to MQTT communication for sending data to an [InfluxDB](https://www.influxdata.com/products/influxdb/) database. 

## Software requirements

To use the code the WiFi network information ('*SSID*', '*password*') alongside database information (e.g., '*url*', '*token*') must be provided by a user defined dictionaries in `secrets.py` (see [here](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/wifi.html#secrets) ). Likewise, messaging and sensor information are provided by a user defined dictionary in `params.py` (see [here](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/wifi.html#sensor-metadata) ). Additional  classes (`adafruit_esp32spi`, `adafruit_esp32spi_wifimanager`) are from the circuitpython library `adafruit_esp32spi` package which must be directly copied into the libs folder of the arduino board. The following code also uses [classes](https://docs.python.org/3/tutorial/classes.html#) within `helper_functions.py`: `sensor_sht40`, `sensor_SGP30`, `sensor_bme280`, `sensor_pmsa003i`, and `board_led`. 

## Code

### Example 1: Data
The first example sends the data of a single, air quality, sensor to an [InfluxDB](https://www.influxdata.com/products/influxdb/) timeseries database relying on the InfluxDB to provide the time stamp information per datapoint based upon the system time (UTC) of its host machine. When no timestamp is provided the time for the datapoint reflects the time the data was received and not when it was actually acquired, or measured. The **payload data** is formatted like the [following](https://docs.influxdata.com/influxdb/cloud/reference/syntax/line-protocol/#timestamp): 

```{code} python

# including optional timestamp
measurementName,tagKey=tagValue fieldKey="fieldValue" time

# excluding timestamp, timestamp provided by host machine
measurementName,tagKey=tagValue fieldKey="fieldValue"

```

Where:

- **Measurement**. Is the container for tags, field, time and that desribes the stored data associated with it. Consider that it is a data table's name.<br>
- **Tag**. Is additional metadata that is indexable. For instance, the name of the collection scientist, etc. <br>
- **Field**. Is the data (*field values*) associated with a column header (*field key*). For instance, oranges = 8 would be equivalent to a field key of oranges and field value of 8. <br>
- **Time** (*optional*). Is a Unix timestamp where time is in nanoseconds starting at January 1st 1970 at 00:00:00 UTC.<br>

There are two sections of whitespace in this format. The first unescaped space ( `tagValue fieldKey` ) delimits the metadata (measurement and the tag set) from the data (field set). Whereas, the second unescaped space delimits the field set from the timestamp if present. 

```{code} python
---

---
####################
# PACKAGES:
import time
import board
import busio
from digitalio import DigitalInOut
from adafruit_esp32spi import adafruit_esp32spi
from adafruit_esp32spi import adafruit_esp32spi_wifimanager

# Get additional packages
from helper_functions import sensor_sht40, sensor_SGP30, sensor_bme280, sensor_pmsa003i, board_led

# Get board and user specific variables
from params import board_params

# SSID and PASSWORD
# using adafruit style, where it is a dictionary with
# 'ssid' and 'password' as keys
try:
    from secrets import secrets_adafruit as secrets
except ImportError:
    print("WiFi SSID and Password required, please add them to secrets.py")
    raise

#############
# Dictionary

html_status_code_dictionary = {'influx_db' :    {204 : 'Success',
                                                 400 : 'Bad request',
                                                 401 : 'Unauthorized, check token',
                                                 404 : 'Not found, check url',
                                                 413 : 'Too large, requested payload is too large',
                                                 429 : 'Too many requests, check plan',
                                                 500 : 'Internal server error, the server has encountered an unexpected error',
                                                 503 : 'Service unavailable, series cardinality exceeeds plan',
                                                },
                               }


#############
# IoT

print("Start IoT device : " + board_params['board name'])
host_name  = board_params['board name']


#############
# WIFI
print("Initiate Wireless networking via ESP32")

#If you are using a board with pre-defined ESP32 Pins:
esp32_cs      = DigitalInOut(board.CS1)
esp32_ready   = DigitalInOut(board.ESP_BUSY)
esp32_reset   = DigitalInOut(board.ESP_RESET)

# uses the secondary SPI connected through the ESP32
#spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
spi           = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp           = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)
wifi          = adafruit_esp32spi_wifimanager.ESPSPI_WiFiManager(esp, secrets)
wifi.connect()
print('Connected to ' + secrets['ssid'])


#############
# SENSOR
# Create sensor object, communicating over the board's default I2C bus
print("Initiating board sensors")

co2_sensor         = sensor_SGP30()
co2_sensor.start_sensor(SCL_pin = board.SCL, SDA_pin = board.SDA,
                        eCO2_baseline        = 0x8e0b, TVOC_baseline = 0x94ce,
                        )

temperature_sensor = sensor_bme280()
temperature_sensor.start_sensor(SCL_pin = board.SCL, SDA_pin = board.SDA,
                                chain = True, i2c_chainaddress = co2_sensor.i2c_I2C)


#############
# DATA 
# Set up url, headers, and initialise data message
payload_data = ''
url_to_post_to  = secrets['influxdb']['url']+ '/api/v2/write?org='+secrets['influxdb']['org']+'&bucket='+ secrets['influxdb']['bucket']
headers_to_send = {'Authorization': 'Token ' + secrets['influxdb']['token'],
                    'Content-Type': 'application/x-www-form-urlencoded',
                   }

#############
# Main

# Loop: Measure, count, and send
while True:
    
    print("")
    print("--------------------------")
    print("Acquiring data...")
        
    ## GET SENSOR MEASUREMENTS
    ## carbon dioxide air quality
    eCO2_value, TVOC_value, Ethanol_value, H2_value                       = co2_sensor.measure_all()
    temperature_value, rel_humidity_value, pressure_value, altitude_value = temperature_sensor.measure_all()    
      
    # add co2 data to payload
    payload_data = payload_data + co2_sensor.sensor_name +         ',host=' + host_name + ' ' + 'co2='+str(eCO2_value)+',gas=' + str(TVOC_value) + ',ethanol=' + str(Ethanol_value) + ',H2=' + str(H2_value)  
        
    # alternative, uncomment to change the co2 data payload for a temperature data payload
    #payload_data = payload_data + temperature_sensor.sensor_name + ',host=' + host_name + ' ' + 'temperature=' + str(temperature_value)  + ',humidity=' + str(rel_humidity_value) + ',pressure=' + str(pressure_value) 
    
    try: 
        print("Data to be sent: " + url_to_post_to)
        print(payload_data)
        print("")
        print("Posting data...", end="")
                
                    
        response = wifi.post(
                                     url     = url_to_post_to,
                                     data    = payload_data,
                                     headers = headers_to_send,
                            )
            
        # Print HTML status code and use dictionary to convert this to human readable message
        print("HTML status code : " + str(response.status_code) + " " + html_status_code_dictionary['influx_db'][response.status_code]) #(response.data())
        #print(response.status_code)
        response.close()
            
        print("OK")
            
            
    except (ValueError, RuntimeError) as e:
        print("Failed to get data, retrying\n", e)
        wifi.reset()
        continue
    
    # clear payload data
    payload_data = ''
    
    time.sleep(1)

```


### Example 2: Data + Timestamp
In the following example the payload data of our sensor sends a timestamp alongside the data to the [InfluxDB](https://www.influxdata.com/products/influxdb/) timeseries database:

```{code} python

# including optional timestamp
measurementName,tagKey=tagValue fieldKey="fieldValue" time

# excluding timestamp, timestamp provided by host machine
measurementName,tagKey=tagValue fieldKey="fieldValue"

```
This timestamp should be time is in nanoseconds starting at January 1st 1970 at 00:00:00 UTC. However, whilst InfluxDB expects timestamps to be in nanoseconds the Arduino `time` module does not give this level of precision. Therefore, the timestamp will be modified first by defining the timestamp (`time_now = time.time()`) and then adding the additional precision *000* to match the required precision (`str(time_now)  + '000' `). The timestamp precision is then incorporated into the URL (`&precision=ms`).


```{code} python
---

---
####################
# PACKAGES:
import time
import board
import busio
from digitalio import DigitalInOut
from adafruit_esp32spi import adafruit_esp32spi
from adafruit_esp32spi import adafruit_esp32spi_wifimanager

# Get additional packages
from helper_functions import sensor_sht40, sensor_SGP30, sensor_bme280, sensor_pmsa003i, board_led

# Get board and user specific variables
from params import board_params

# SSID and PASSWORD
# using adafruit style, where it is a dictionary with
# 'ssid' and 'password' as keys
try:
    from secrets import secrets_adafruit as secrets
except ImportError:
    print("WiFi SSID and Password required, please add them to secrets.py")
    raise

#############
# Dictionary

html_status_code_dictionary = {'influx_db' :    {204 : 'Success',
                                                 400 : 'Bad request',
                                                 401 : 'Unauthorized, check token',
                                                 404 : 'Not found, check url',
                                                 413 : 'Too large, requested payload is too large',
                                                 429 : 'Too many requests, check plan',
                                                 500 : 'Internal server error, the server has encountered an unexpected error',
                                                 503 : 'Service unavailable, series cardinality exceeeds plan',
                                                },
                               }


#############
# IoT

print("Start IoT device : " + board_params['board name'])
host_name  = board_params['board name']


#############
# WIFI
print("Initiate Wireless networking via ESP32")

#If you are using a board with pre-defined ESP32 Pins:
esp32_cs      = DigitalInOut(board.CS1)
esp32_ready   = DigitalInOut(board.ESP_BUSY)
esp32_reset   = DigitalInOut(board.ESP_RESET)

# uses the secondary SPI connected through the ESP32
#spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
spi           = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp           = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)
wifi          = adafruit_esp32spi_wifimanager.ESPSPI_WiFiManager(esp, secrets)
wifi.connect()
print('Connected to ' + secrets['ssid'])


#############
# SENSOR
# Create sensor object, communicating over the board's default I2C bus
print("Initiating board sensors")

co2_sensor         = sensor_SGP30()
co2_sensor.start_sensor(SCL_pin = board.SCL, SDA_pin = board.SDA,
                        eCO2_baseline        = 0x8e0b, TVOC_baseline = 0x94ce,
                        )

temperature_sensor = sensor_bme280()
temperature_sensor.start_sensor(SCL_pin = board.SCL, SDA_pin = board.SDA,
                                chain = True, i2c_chainaddress = co2_sensor.i2c_I2C)


#############
# DATA 
# Set up url, headers, and initialise data message
payload_data = ''
url_to_post_to  = secrets['influxdb']['url']+ '/api/v2/write?org='+secrets['influxdb']['org']+'&bucket='+ secrets['influxdb']['bucket'] + '&precision=ms'
headers_to_send = {'Authorization': 'Token ' + secrets['influxdb']['token'],
                    'Content-Type': 'application/x-www-form-urlencoded',
                   }

#############
# Main

# Loop: Measure, count, and send
while True:
    
    print("")
    print("--------------------------")
    print("Acquiring data...")
        
    ## GET SENSOR MEASUREMENTS
    ## carbon dioxide air quality
    eCO2_value, TVOC_value, Ethanol_value, H2_value                       = co2_sensor.measure_all()
    temperature_value, rel_humidity_value, pressure_value, altitude_value = temperature_sensor.measure_all()
    time_now                                                              = time.time()
    
      
    # add co2 data to payload
    payload_data = payload_data + co2_sensor.sensor_name +         ',host=' + host_name + ' ' + 'co2='+str(eCO2_value)+',gas=' + str(TVOC_value) + ',ethanol=' + str(Ethanol_value) + ',H2=' + str(H2_value)  + ' ' + str(time_now)  + '000' 
        
    # alternative, uncomment to change the co2 data payload for a temperature data payload
    #payload_data = payload_data + temperature_sensor.sensor_name + ',host=' + host_name + ' ' + 'temperature=' + str(temperature_value)  + ',humidity=' + str(rel_humidity_value) + ',pressure=' + str(pressure_value) + ' ' + str(time_now)  + '000' 
    
    try: 
        print("Data to be sent: " + url_to_post_to)
        print(payload_data)
        print("")
        print("Posting data...", end="")
                
                    
        response = wifi.post(
                                     url     = url_to_post_to,
                                     data    = payload_data,
                                     headers = headers_to_send,
                            )
            
        # Print HTML status code and use dictionary to convert this to human readable message
        print("HTML status code : " + str(response.status_code) + " " + html_status_code_dictionary['influx_db'][response.status_code]) #(response.data())
        #print(response.status_code)
        response.close()
            
        print("OK")
            
            
    except (ValueError, RuntimeError) as e:
        print("Failed to get data, retrying\n", e)
        wifi.reset()
        continue
    
    # clear payload data
    payload_data = ''
    
    time.sleep(1)

```


#### Example 3: Batch
Finally, the data will be sent in batches rather than individually to our [InfluxDB](https://www.influxdata.com/products/influxdb/) timeseries database. Batch processing reduces the number of transfer and data traffic allowing for data aggregation and pre-processing of data (e.g., adding quality flags). However, batch processing adds **latency** - no longer real time - to the methodology leading to a lag between the real and virtual worlds. 

```{code} python

#################### PACKAGES
import time
import board
import busio
from digitalio import DigitalInOut
from adafruit_esp32spi import adafruit_esp32spi
from adafruit_esp32spi import adafruit_esp32spi_wifimanager

# Get additional packages
from helper_functions import sensor_sht40, sensor_SGP30, sensor_bme280, sensor_pmsa003i, board_led

# Get board and user specific variables
from params import board_params

# SSID and PASSWORD
# using adafruit style, where it is a dictionary with
# 'ssid' and 'password' as keys
try:
    from secrets import secrets_adafruit as secrets
except ImportError:
    print("WiFi SSID and Password required, please add them to secrets.py")
    raise

#############
# Dictionary

html_status_code_dictionary = {'influx_db' :    {204 : 'Success',
                                                 400 : 'Bad request',
                                                 401 : 'Unauthorized, check token',
                                                 404 : 'Not found, check url',
                                                 413 : 'Too large, requested payload is too large',
                                                 429 : 'Too many requests, check plan',
                                                 500 : 'Internal server error, the server has encountered an unexpected error',
                                                 503 : 'Service unavailable, series cardinality exceeeds plan',
                                                },
                               }


#############
# IoT

print("Start IoT device : " + board_params['board name'])
host_name  = board_params['board name']


#############
# WIFI
print("Initiate Wireless networking via ESP32")

#If you are using a board with pre-defined ESP32 Pins:
esp32_cs      = DigitalInOut(board.CS1)
esp32_ready   = DigitalInOut(board.ESP_BUSY)
esp32_reset   = DigitalInOut(board.ESP_RESET)

# uses the secondary SPI connected through the ESP32
#spi = busio.SPI(board.SCK, board.MOSI, board.MISO)
spi           = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp           = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)
wifi          = adafruit_esp32spi_wifimanager.ESPSPI_WiFiManager(esp, secrets)
wifi.connect()
print('Connected to ' + secrets['ssid'])


#############
# SENSOR
# Create sensor object, communicating over the board's default I2C bus
print("Initiating board sensors")

co2_sensor         = sensor_SGP30()
co2_sensor.start_sensor(SCL_pin = board.SCL, SDA_pin = board.SDA,
                        eCO2_baseline        = 0x8e0b, TVOC_baseline = 0x94ce,
                        )

temperature_sensor = sensor_bme280()
temperature_sensor.start_sensor(SCL_pin = board.SCL, SDA_pin = board.SDA,
                                chain = True, i2c_chainaddress = co2_sensor.i2c_I2C)


#############
# DATA 
# Set up counters
counter      = 0     # total counts
data_counter = 0     # a counter that stores data on the 10th iteration
send_counter = 0     # a counter that sends data on the 10th iteration of the 10th data counter
sent_counter = 0


# Set up url, headers, and initialise data message
payload_data = ''
url_to_post_to  = secrets['influxdb']['url']+ '/api/v2/write?org='+secrets['influxdb']['org']+'&bucket='+ secrets['influxdb']['bucket'] + '&precision=ms'
headers_to_send = {'Authorization': 'Token ' + secrets['influxdb']['token'],
                    'Content-Type': 'application/x-www-form-urlencoded',
                   }

#############
# Main

# Loop: Measure, count, and send
while True:
        
    data_counter = data_counter + 1
    counter      = counter + 1
    print("")
    print("--------------------------")
    print("Counter: " + str(counter) + " ; Data Counter: " + str(data_counter) + " ; Send Counter: " + str(send_counter) + " ; Sent Counter: " + str(sent_counter))
    print("Acquiring data...")
        
    ## GET SENSOR MEASUREMENTS
    ## carbon dioxide air quality
    eCO2_value, TVOC_value, Ethanol_value, H2_value                       = co2_sensor.measure_all()
    temperature_value, rel_humidity_value, pressure_value, altitude_value = temperature_sensor.measure_all()
    time_now                                                              = time.time()
    
    
    if data_counter == 10:
        
        # add co2 data to payload
        payload_data = payload_data + co2_sensor.sensor_name +         ',host=' + host_name + ' ' + 'co2='+str(eCO2_value)+',gas=' + str(TVOC_value) + ',ethanol=' + str(Ethanol_value) + ',H2=' + str(H2_value)  + ' ' + str(time_now)  + '000' +'\n' 
        
        # add temperature data to payload
        payload_data = payload_data + temperature_sensor.sensor_name + ',host=' + host_name + ' ' + 'temperature=' + str(temperature_value)  + ',humidity=' + str(rel_humidity_value) + ',pressure=' + str(pressure_value) + ' ' + str(time_now)  + '000' +'\n' 
        
        data_counter = 0
        send_counter = send_counter + 1
        print("")
        print("Data stored: ")
        print(payload_data)
    
    if send_counter == 10:
         
        try:
            
            print("Data to be sent: " + url_to_post_to)
            print(payload_data)
            print("")
            print("Posting data...", end="")
            
                
            response = wifi.post(
                                 url     = url_to_post_to,
                                 data    = payload_data,
                                 headers = headers_to_send,
                                )
            
            # Print HTML status code and use dictionary to convert this to human readable message
            print("HTML status code : " + str(response.status_code) + " " + html_status_code_dictionary['influx_db'][response.status_code]) #(response.data())
            #print(response.status_code)
            response.close()
            
            print("OK")
            sent_counter = sent_counter + 1
            
        except (ValueError, RuntimeError) as e:
            print("Failed to get data, retrying\n", e)
            wifi.reset()
            continue
            
        response     = None
        send_counter = 0
        payload_data = ''
        print("SGP30 baseline values : ")
        print(hex(co2_sensor.get_baseline_TVOC()),hex(co2_sensor.get_baseline_eCO2()))
        
    
    time.sleep(1)




```
