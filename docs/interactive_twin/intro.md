```{figure} /images/Section_Divider_3.jpg
---

name: Section_divider_3-fig
---

```
# Interactive Digital Twin 

## Rationale

Let us continue our journey investigating Digital Twins with an example of using a simple set of sensors that can be connected to a network and which have the capacity to perform a simple action in response to changes in a variable. This example will expand from the [previous section](https://wur.gitlab.io/digitaltwin/docs/standalone_twin/intro.html#) setting up a series of simple internet of things (IoT) sensor device by incorparting the generation of actions from the acquired data. The aim of this section is to present to you the reader examples of:

- How to collect data from sensors and relay that information to the user.
- How to use the data gathered to add meaning, or knowledge, to the collected data to help inform the user’s decision-making process.
- How to generate a response.
- How to monitor these responses.

The example will focus on a plant monitoring experiment a common enough physical computing experiment as it requires a few seeds, some soil, and a pot. But in terms of experimental design it is also a problem that has readily (1) definable variables (e.g., temperature, light, soil moisture) and (2) controllable actions (e.g., water via a pump) all whilst (3) involving a bit of *randomness* associated with living organisms. So, let us once again define *who are users* and *what are our goals*.


## Who are the users?
Anyone who has a green thumb but who forgets to water their green friend


## What is our goal?
The Digital Twin should provide actionable knowledge regarding:

- the ambient conditions experienced by the plant;
- a record of the growing conditions
- optimisation of the water processing 
