# Sensing light
## Light


Light response curves ([Coe and Lin, 2018](https://doi.org/10.1007/978-1-4939-7786-4_5);  [Herrmann et al., 2020](https://doi.org/10.1007/s11120-019-00681-2); [Ögren and Evans, 1993](https://doi-org.vu-nl.idm.oclc.org/10.1007/BF00195075))




## Photoperiodicity

```{figure} /images/length_of_day.jpg
---
name: length_of_day-fig
alt: Graph of daylength as a function of day in year and latitude
class: bg-primary mb-1
width: 100%
align: center
---
**Length of Day.** *Day length in hours as a function of latitude on the Earth and day of the Year.*
```

````{admonition} Click the button to reveal the figure code
:class: dropdown

Length of day is calculated by latitude on the Earth and solar declination. Solar declination is an angle made between an imaginary line drawn from the center of the Earth toward the Sun and the Earth's celestrial equator, an imaginary plane extending outward from the Earth's equator. Modern solar declination ranges between -23.5°  ≤ δ ≤  +23.5° throughout the year, with its value being 0 at the equinoxes; -23.5° during Winter Solstice; and +23.5° during Summer Solstice. What this means is that the Tropics of Cancer and Capricorn experience the most direct sunlight during the Summer and Winter solstice respectively. Solstice, or when the Sun stands still, indicate either the shortest (Winter Solstice) or longest (Summer Solstice) day. Equinox, or equal night, is when day and night length are equal. Note that due to refraction by the atmosphere the Sun will still appear in the sky despite it having already dipped below the horizon prolonging the day by several minutes.

You can construct the figure and playaround with calculating for different latitudes the Length of Day (LOD) with the following code:

```{code} python
#### PACKAGES
import math
import numpy as np

# For plotting
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection

#### DEFINE FUNCTIONS
def solar_declination_approx(day_in_N_day):
    """ Approximate the solar declination using simplification of day in year

    # PURPOSE:
    # determine solar declination, delta, as approximated by day in year
    # where day_in_N_day starts at Jan 1 as 1, Jan 2 as 2 and so forth.
    
    # PACKAGES:
    # import math
    
    # INPUT:
    # day_in_N_day = day in year counting from 1 onwards, so Jan 1 = 1, Jan 2 = 2,...
    
    # EXAMPLES:
    
    # Example 1
    N_day             = np.linspace(0,365,365)
    # create output array
    declination_sun = []
    
    for item in N_day:    
        temp = solar_declination_approx(item)
        # convert output into degrees for plotting
        declination_sun.append(math.degrees(temp))
        
    # Example 2
    declination_sun = [math.degrees(solar_declination_approx(item)) for item in np.linspace(0,365,365)]
    
    """
    # PACKAGES
    import math
    
    # CODE:
    output = math.radians(23.45)*(math.sin((math.radians(360)*(day_in_N_day-80))/365))
    
    return output


def length_of_the_day_complex(lat_on_earth,solar_decl):
    """
    # PURPOSE:
    # compute length of the day (= LOD) using a slightly more complex argument
    
    # REFERENCE:
    # http://www.atmos.albany.edu/facstaff/brose/classes/ATM623_Spring2015/Notes/Lectures/Lecture11%20--%20Insolation.html
    
    # PACKAGES
    # import math
    
    # INPUT:
    # lat_on_earth = latitude on the earth, in radians
    # solar_decl   = solar declination, in radians
    
    # OUTPUT:
    # length of day in hours
    
    # EXAMPLE:
    # lat_on_earth = math.radians(90)
    # solar_decl   = math.radians(solar_decl)
    # length_of_the_day_complex(lat_on_earth,solar_decl)
    """
    # PACKAGES:
    import math
    
    # CODE:
    
    # Set latitudinal limits by which 0 and 24 hours of daylight will be defined
    lat_limits =  90-abs(math.degrees(solar_decl))
    
    # Set 24 hour of daylight (i.e., polar circle in 'Summer')
    if abs(math.degrees(lat_on_earth)) >= lat_limits  and np.sign(lat_on_earth) == np.sign(solar_decl):
        output = 24
        
    # Set 0 hour of daylight (i.e., polar circle in 'Winter')
    elif abs(math.degrees(lat_on_earth)) >= lat_limits  and np.sign(lat_on_earth) != np.sign(solar_decl):
        output = 0
    else:
        output = (24/math.pi)*math.acos((-math.tan(lat_on_earth)*math.tan(solar_decl)))
    
    return output


#### GENERATE DATA
# create output days
N_day             = np.linspace(0,365,365)
latitude_on_Earth = np.linspace(-90,90,180)

# create output array
declination_sun = []


for item in N_day:    
    
    temp = solar_declination_approx(item)
    
    # convert output into degrees for plotting
    declination_sun.append(math.degrees(temp))
    

lod_90 = []
lod_70 = []
lod_60 = []
lod_amsterdam = []
lod_50 = []
lod_30 = []
lod_0  = []

for item_sun_dec in declination_sun:

    lod_90.append(length_of_the_day_complex(math.radians(90),math.radians(item_sun_dec))) 
    lod_70.append(length_of_the_day_complex(math.radians(70),math.radians(item_sun_dec))) 
    lod_60.append(length_of_the_day_complex(math.radians(60),math.radians(item_sun_dec))) 
    lod_amsterdam.append(length_of_the_day_complex(math.radians(52.366),math.radians(item_sun_dec))) 
    lod_50.append(length_of_the_day_complex(math.radians(50),math.radians(item_sun_dec))) 
    lod_30.append(length_of_the_day_complex(math.radians(30),math.radians(item_sun_dec))) 
    lod_0.append(length_of_the_day_complex(math.radians(0),math.radians(item_sun_dec))) 

## PLOT DATA
# MAKE PLOT
fig = plt.figure(tight_layout=True, figsize=(8,6))
gs  = gridspec.GridSpec(1, 1)
ax1 = fig.add_subplot(gs[0, :])

# PLOT LINES
#line_lod_90, = ax1.plot(N_day, lod_90, lw=2, color = 'black')
line_lod_70, = ax1.plot(N_day, lod_70, lw=2, color = 'blue')
line_lod_60, = ax1.plot(N_day, lod_60, lw=2, color = 'green')
#line_lod_50, = ax1.plot(N_day, lod_50, lw=2, color = 'red')
line_lod_Amsterdam, = ax1.plot(N_day, lod_amsterdam, lw=2, color = 'red')
line_lod_30, = ax1.plot(N_day, lod_30, lw=2, color = 'orange')
line_lod_0,  = ax1.plot(N_day, lod_0 , lw=2, color = 'grey',dashes = [6,2])

# PLOT VERTICAL LINES
t = np.linspace(0,24,24)
s = []
[s.append(80) for item in t];
line_vernal_equinox = ax1.plot(s,t, lw=1, color = 'grey' , dashes = [6,2])
s = []
[s.append(265) for item in t];
line_autumn_equinox = ax1.plot(s,t, lw=1, color = 'grey' , dashes = [6,2])
s = []
[s.append(172) for item in t];
line_summer_solstice = ax1.plot(s,t, lw=1, color = 'grey' , dashes = [6,2])
s = []
[s.append(355) for item in t];
line_winter_solstice = ax1.plot(s,t, lw=1, color = 'grey' , dashes = [6,2])

# ANNOTATE
ax1.text(70 , 16 , 'Vernal Equinox'  , rotation=90);
ax1.text(250, 4, 'Autumnal Equinox', rotation=90);
ax1.text(160, 4, 'Summer Solstice' , rotation=90);
ax1.text(345, 16 , 'Winter Solstice' , rotation=90);
ax1.text(225, 20 , '70°N' , rotation=0, color = 'blue');
ax1.text(200, 18 , '60°N' , rotation=0, color = 'green');
#ax1.text(190, 16.25 , '50°N' , rotation=0, color = 'red');
ax1.text(180, 16.7 , '52.3°N' , rotation=0, color = 'red');
ax1.text(180, 14.5 , '30°N' , rotation=0, color = 'orange');
ax1.text(200, 12.5 , 'Equator' , rotation=0, color = 'grey');

# Set axis and title labels
ax1.set_ylabel('Length of day (in hours)')
ax1.set_xlabel('Day of Year')
ax1.set_xlim(0, 365)
ax1.set_ylim(0, 24)

# FIGURE CAPTION
# Set and plot caption titles
caption_text1 = "Figure 2:  "
caption_text2 = "Length of the day in hours for the Northern Hemisphere"
caption_text3 = "Red line is the length of day in hours for Amsterdam (52.3°N)."

fig.text(0.5, -0.02, caption_text1, ha='center')
fig.text(0.5, -0.08, caption_text2, ha='center')
fig.text(0.5, -0.14, caption_text3, ha='center');


```
````

## Photosynthesis

The energy for photosynthesis to convert carbon dioxide (CO2) and water (H2O) into glucose and oxygen (O2) is provided by Light. As the intensity of the ambient light increases the relative photosynthesis and therefore amount of glucose also increases. Until a saturation point is met and the intensity of light no longer increases photosynthesis. The intensity of the photosynthesis will also be impacted by the concentration of CO2 and temperature ([Runkle, 2015]()). At lower CO2 concentrations the light saturation will be met at a lower light intensity than at high CO2 concentrations. 


## Intensity
It is not just the intensity of the light but also its composition. Photosynthetically active radiation (PAR) is the light between 400 and 700 nm that is used in photosynthesis. This part of the light spectrum overlaps with the visible light wavelength. 

([Cleveland and Morris, 2013](https://doi.org/10.1016/B978-0-08-046405-3.00010-3); [Kirk, 2015](https://doi.org/10.1016/B978-0-12-802329-7.00002-X); [Phillips and Milo, 2016](http://book.bionumbers.org/how-much-energy-is-carried-by-photons-used-in-photosynthesis/))



```{figure} /images/Planck_atmosphere_SMALLER.jpg
---
name: Planck-fig
alt: Graph of spectral irradiance per wavelength for sunlight
class: bg-primary mb-1
width: 100%
align: center
---
**Spectral irradiance of Sunlight.** *Top of Atmosphere (orange),  surface (Red) and the expected spectral irradiance of a Blackbody at 5778 K (black) . Data Source: [Data](http://rredc.nrel.gov/solar/spectra/am1.5/ASTMG173/ASTMG173.html) from United States Department of Energy, National Renewable Energy Laboratory, Reference Solar Spectral Irradiance: [ASTM G-173](https://www.nrel.gov/grid/solar-resource/spectra-am1.5.html)*
```

````{admonition} Click the button to reveal the figure code
:class: dropdown

Planck's law defines the distribution of the radiated flux B(lambda,T) as a function of the wavelength. The equation for the black body spectrum as a function of wavelength ($\lambda$) and Temperature ($T$) is:

$$ B(\lambda, T) =\frac{2\pi hc^{2}}{\lambda^{5}} \cdot \frac{1}{e^{hc/k_{b}T\lambda} -1 } $$

where, $B$ is the spectral radiance, $h$ is planck's constant ($6.626068 \cdot 10^{-34}$ in Joule per second), $k_{b}$ is Boltzmanns constant ($1.38066 \cdot 10^{-23}$ in joule$^{-1}$), and $c$ is the velocity of light in a vacuum ($2.997925 \cdot 10^{8}$ in meters per second). 

```{code} python
## DATA
data_directory    = " "   # need to add data directory
data_fname        = "astmg173.xls"

data_sheetname    = "SMARTS2"
data_column_names = ['Wvlgth nm', 'Etr W*m-2*nm-1', 'Global tilt',  'W*m-2*nm-1', 'Direct+circumsolar W*m-2*nm-1']
data_metadata     = { 'method' : 'ASTM G173-03 Reference Spectra Derived from SMARTS v. 2.9.2',
                     'url'     : 'https://www.nrel.gov/grid/solar-resource/spectra-am1.5.html',
                     'url alt' : 'http://rredc.nrel.gov/solar/spectra/am1.5/ASTMG173/ASTMG173.html',
                     'source'  :  'Data from United States Department of Energy, National Renewable Energy Laboratory, Reference Solar Spectral Irradiance: ASTM G-173', 
                     }

## VARIABLES
temperature_of_sun = 5778    # in kelvin

# Define spectra
EM_light_spectra = {
                    # spectra                  : [start, end, abbreviation, units]
                   'Ultraviolet'               : [0.005,0.4   ,'UV'     ,'um'],
                   'Ultraviolet (UVC)'         : [0.1  ,0.28  ,'UVC'    ,'um'],
                   'Ultraviolet (UVB)'         : [0.28 ,0.315 ,'UVB'    ,'um'],
                   'Ultraviolet (UVA)'         : [0.315,0.4   ,'UVA'    ,'um'],
                   'Visible'                   : [0.4  ,0.7   ,'Visible','um'],
                   'Infrared (IR-A)'           : [0.7  ,1.4   , 'IR-A'  ,'um'],
                   'Near Infrared'             : [0.75 ,1.4   ,'NIR'    ,'um'],
                   'Short-wavelength Infrared' : [1.4  ,3     ,'SWIR'   ,'um'],
                   'Mid-wavelength Infrared'   : [3    ,8     ,'MWIR'   ,'um'],
                   'Long-wavelength Infrared'  : [8    ,15    ,'LWIR'   ,'um'],
                   'Far Infrared'              : [15   ,1000  ,'FIR'    ,'um'],
                    }


## PACKAGES
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.ticker import (MultipleLocator) 


## FUNCTIONS
def plancks_law(wavelength_lambda, Temperature):
    """
    # PURPOSE
    # Planck's law defines the distribution of the radiated flux E(lambda,T) as a function of the wavelength, lambda. 
    # Note, when the wavelength tends to either zero or to infinity then E(lambda,T) tends to zero. As temperature increases
    # the E(lambda,T) increases. As temperature increases the maximum wavelength tends to move toward shorter wavelengths.
    
    # REFERENCE:
    # Melieres and Marechal, 2015. Climate Change. Past, Present and Future Wiley & Sons Ltd, UK, pp 391
    # Dolman, 2019. Biogeochemical cycles and climate. Oxford University Press, pp 251
    # (note equation 4.2 of Dolman (2019), pg 45)
    # https://physics.stackexchange.com/questions/241741/black-body-spectrum-plot
    
    # INPUT:
    # Temperature       = temperature of a surface in Kelvin
    # wavelength_lambda = wavelength
    
    # OUTPUT:
    # the energy flux in (Wm-2um-1)
    """
    # PACKAGES
    import numpy as np
    # VARIABLES
    plancks_constant_h              = 6.626068*10**-34   # in Joule sec
    boltzmanns_constant_k           = 1.38066*10**-23    # in joule deg-1
    velocity_of_light_in_a_vacuum_c = 2.997925*10**8     # in m/s
    
    # CODE
    lhs_num  = 2 * np.pi * plancks_constant_h * (velocity_of_light_in_a_vacuum_c**2)
    lhs_den  = wavelength_lambda**5
    rhs_num  = 1
    expo     = (plancks_constant_h * velocity_of_light_in_a_vacuum_c) / (boltzmanns_constant_k * Temperature * wavelength_lambda)
    rhs_den  = np.exp(expo) -1
    output   = (lhs_num/lhs_den) * (rhs_num/rhs_den)
    
    return output


## LOAD
df = pd.read_excel(data_directory + data_fname, sheet_name = data_sheetname,
                   skiprows = 0, header = 1)


# MODEL
# make planck estimated constant
# for planck equation wavelength is in m, we want nm so multiply x_values by 10**-9:
x_values  = np.linspace(0*10**-9,4000*10**-9,4000)

# using a Sun temperature in kelvin of 5778
blackbody_Sun_planck      = [plancks_law(value, temperature_of_sun) for value in x_values]

# Then we need to correct the spectral radiance, first by multiplying by the 
# surface area of the sun (4*pi*r**2) and divide by the radius of a sphere 
# that covers the Earth Sun distance (4*pi*R**2), which
# leaves us with mulitplying by r2/R2 which has a value of:
r2_R2 = 2.17*(10**-5)

# multiply values by 10**-9 to get nm and then 
blackbody_Sun_planck_unit = [item * (10**-9)*r2_R2 for item in blackbody_Sun_planck]

# convert x_values back into nm
x_values_unit             = [item / (10**-9) for item in x_values]


## PLOT
# MAKE PLOT
fig = plt.figure(tight_layout=True, figsize=(14,8))
gs  = gridspec.GridSpec(1, 1)
ax1 = fig.add_subplot(gs[0, :])

# PLOT LINES
line_black_body_meas,= ax1.plot(df['Wvlgth nm'], df['Etr W*m-2*nm-1'], lw=0.4, color = 'darkorange', zorder = 12)
line_black_body_meas,= ax1.plot(df['Wvlgth nm'], df['Direct+circumsolar W*m-2*nm-1'], lw=0.4, color = 'darkred', zorder = 12)
line_black_body_est, = ax1.plot( x_values_unit,blackbody_Sun_planck_unit , lw=1.2, color = 'black', label = 'Blackbody ' + str(temperature_of_sun) + ' K', zorder = 12)

# PLOT VERTICAL LINES
for item in EM_light_spectra.keys():
    
    tmp       = np.ones((10,2))
    tmp[:,0]  = tmp[:,0] *(EM_light_spectra[item][1] * 1000)
    tmp[:,1]  = np.linspace(0,2.5, 10)
    
    line_EM,= ax1.plot(tmp[:,0], tmp[:,1], ls= '--', lw=0.8, label = EM_light_spectra[item][2],zorder = 10)

# PLOT AREA
area_meas   = ax1.fill_between(df['Wvlgth nm'],df['Etr W*m-2*nm-1'], y2=0,  color = 'white', alpha = 1, zorder = 11)
area_meas   = ax1.fill_between(df['Wvlgth nm'],df['Etr W*m-2*nm-1'], y2=0,  color = 'darkorange', alpha = 0.4, label = 'Top of Atmosphere', zorder = 12)
area_meas2  = ax1.fill_between(df['Wvlgth nm'],df['Direct+circumsolar W*m-2*nm-1'], y2=0,  color = 'darkred', alpha = 0.4, label = 'Surface', zorder = 12)

# # PLOT ANNOTATIONS
# # ANNOTATE Absorption paths
ax1.text(260 , 0.06, 'O$_{3}$' , rotation=0, color = 'black', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(710 , 0.08, 'O$_{2}$' , rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(860 , 0.08, 'H$_{2}$O', rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(1050, 0.08, 'H$_{2}$O', rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(1320, 0.08, 'H$_{2}$O', rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(1810, 0.06, 'H$_{2}$O', rotation=0, color = 'white', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(1980, 0.14, 'CO$_{2}$', rotation=0, color = 'black', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.text(2600, 0.1 , 'H$_{2}$O', rotation=0, color = 'black', size = 16, fontweight = 'extra bold', zorder = 15);

# # ANNOTATE EM spectrum
ax1.text(220 , 2.30, 'UVC'     , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(265 , 2.15, 'UVB'     , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(340 , 2.3 , 'UVA'     , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(500 , 2.3 , 'Visible' , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(975 , 2.2 , 'IR-A \n NIR' , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);
ax1.text(1950 , 2.2 , 'Short-wavelength Infrared' , rotation=0, color = 'black', size = 10,  fontweight = 'light', zorder = 15);

# # ANNOTATE Arrows  
TEXT              = "Blackbody at 5778 K "
xy_arrow_x        = 1050
xy_arrow_y        = 0.7
xy_text_x         = 1600
xy_text_y         = 1.2
connection_to_use = "arc3,rad=.2"
color_to_use      = "black"
    
ax1.annotate(TEXT,xy=(xy_arrow_x,xy_arrow_y),size =14,color = color_to_use, xycoords='data',xytext=(xy_text_x, xy_text_y), textcoords='data',bbox=dict(boxstyle="round", facecolor='white', edgecolor='white',),
            arrowprops=dict(arrowstyle="->", color=color_to_use,shrinkA=5, shrinkB=5, patchA=None, patchB=None,connectionstyle=connection_to_use,),)

text_formula = r'$B(\lambda, T) =\frac{2\pi hc^{2}}{\lambda^{5}} \cdot \frac{1}{e^{hc/k_{b}T\lambda} -1 } $'
ax1.text(1600 , 1.35, text_formula , rotation=0, color = 'black', size = 16, fontweight = 'extra bold', zorder = 15);
ax1.annotate("With \natmospheric \nabsorption",xy=(1650,0.28), size = 12, color = "darkred", xycoords='data',xytext=(2100, 0.5), textcoords='data',bbox=dict(boxstyle="round", facecolor='white', edgecolor='white',),
            arrowprops=dict(arrowstyle="->", color="darkred",shrinkA=5, shrinkB=5, patchA=None, patchB=None,connectionstyle=connection_to_use,),)
   

ax1.annotate("Without \natmospheric \nabsorption",xy=(630,1.8), size = 12, color = "darkorange", xycoords='data',xytext=(800, 1.5), textcoords='data',bbox=dict(boxstyle="round", facecolor='white', edgecolor='white',),
            arrowprops=dict(arrowstyle="->", color="darkorange",shrinkA=5, shrinkB=5, patchA=None, patchB=None,connectionstyle=connection_to_use,),)

# AXIS
# Set axis and title labels
ax1.set_ylabel('Spectral Irradiance ($W/m^{2}/nm$)', size = 16, fontweight = 'bold')
ax1.set_xlabel('Wavelength (nm)', size = 16, fontweight = 'bold')
ax1.tick_params(direction='out', length=6, )
ax1.set_xlim(200, 4000)
ax1.set_ylim(0, 2.5)
ax1.xaxis.set_minor_locator(MultipleLocator(50))
ax1.yaxis.set_minor_locator(MultipleLocator(0.1))
ax1.grid(True, ls = '--', c = 'lightgrey', alpha = 0.4)
ax1.legend()

# SAVE
plt.savefig('Planck_atmosphere.jpg', dpi = 650, format = 'jpg')

```
````

## Components
- Arduino Nano RP2040 Connect (€21.00) with headers (€26.95)
- Pimoroni LTR-559 Light and Proximity Sensor
- (*optional*) Pimoroni BH1745 Colour Sensor
- (*optional*) Pimoroni VEML6075 UVA/UVB Sensor
- (*optional*) Adafruit AS7341 10 channel spectrometer 415, 445, 480, 515, 555, 590, 630, 680 nm Clear & Near IR Sensor
- 7 x Jumper Wire male-to-male ( < €1.00)


## Layout

```{figure} /images/Fritz_air_v2_generic.jpg
---
name: breadboard_half_generic-fig2
alt: Picture of a Half (300 point) Breadboard with generic sensor i2c set-up 
class: bg-primary mb-1
width: 100%
align: center
---
**Set-up of the light sensor.** *Schematic and Fritzing diagram showing how to set-up the electronic components. The pins used on the Arduino Nano RP2040 Connect are red wire: 3v3, black wire: Ground, yellow wire: SDA (A4), and orange wire: SCL (A5). The 3v3 and Ground are connected to the Breadboard's rails to allow for multiple wires to attached, whereas the SDA and SCL are connected directly. The colour of the holes in the breadboard denote *connections* with green highlighting those holes associated with a wire or header.*

```

#### Breadboard

```{figure} /images/Breadboard.jpg
---
name: breadboard_half-fig3
alt: Picture of a Half (300 point) Breadboard 
class: bg-primary mb-1
width: 100%
align: center
---
**Half (300 point) Breadboard.** *Explanation of a Breadboard.*
```

First, let us arrange the wiring on the breadboard. With a half breadboard there are four sets of holes ({numref}`breadboard_half-fig3`), two outer sets of two rows each that are called **rails** ({numref}`breadboard_half-fig3`) and two inner sets of 5 rows and 30 columns ({numref}`breadboard_half-fig3`). The rails run horizontally and all holes in a row are connected, these can be used for power and ground. The two inner sets of holes are not connected. Within a set anything plugged into the same column will be connected so if you connect one hole in a column to ground the remaining 4 holes can be used to connect to ground.

####  Wire up components
Placing the breadboard on a table landscape ({numref}`breadboard_half_generic-fig2`) let us first place the Arduino Nano RP2040 Connect into the breadboard. If the breadboard is numbered, the microcontroller slots into the holes between d1 to d16 and h1 to h16, ensuring that the board's usb faces outwards. By placing the board into the breadboard each column in the breadboard will be associated with the individual pins. Place a jumper wire between the columns associated with a 3v3 pin and the power rail (denoted by *+*). Next do the same action but for a ground pin, connecting it to the ground rail (denoted by *-*). 

Irrespective of the sensor (i.e., Pimoroni or Adafruit) we will be using holes d26 to d30 as the location by which the sensor will be connected. If the breadboard you are using is not labelled then {numref}`breadboard_half_generic-fig2` gives you the holes we are referring to. From d26 to d30 the columns should be associated with: 2-5v; SDA; SCL; no pin; and, ground. For the 2-5v connect the column (d26) with the power rain, likewise connect d30 to the ground rain. The default data (SDA) and clock (SCL) pins for the default I2C bus require you to connect the 8th (pin A4) and 9th (pin A5) pins of the Arduino (in the breadboard b8 and b9) with d27 and d28, respectively. The column associated with d29 remains unconnected. 

Depending upon the purchased sensor (i.e., Pimoroni or Adafruit) either (Pimoroni) slot in the breakout extender into the e26 to e30 or (Adafruit) connect the male ends of a JST-PH-to-male wire into e26 (*2-5v = red wire*), e27 (*SDA = blue wire*), e28 (*SCL = yellow wire*), and e30 *(ground = black wire*).
 


### Software libraries

- LTR-559 Light and Proximity Sensor 

To be able to read the `LTR-559 Light and Proximity Sensor` the library `pimoroni_circuitpython_ltr559` is used. This library is part of the Pimoroni Github (downloadable [here](https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559/blob/master/pimoroni_circuitpython_ltr559.py)) and can be directly copied into the `libs` folder of the arduino board.


### Code
#### Example 1

```{code} python
---

---
""" Example 1: LTR-559  with CircuitPython

# Notes:
        > IR/UV-filtering
        > 50.60Hz flicker rejection
        > 0.01 lux to 64,000 lux light detection range
        > ~5cm proximity detection range
        
# Modified from:
        https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559
        https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559/blob/master/pimoroni_circuitpython_ltr559.py

"""

# packages
import time
import board
from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559

# variables 

# Define I2C bus, for this example the default board.SCL and board.SDA are used
i2c = board.I2C()  

# Create sensor object, communicating over the board's default I2C bus
ltr559 = Pimoroni_LTR559(i2c, address=0x23)

# initiate loop and print out sensor variables
while True:

    lux       = ltr559.lux 
    proximity = ltr559.prox
    print("\nLux: %0.2f Lux" % (lux))
    print("Proximity: %0.1f %%" % proximity)

    time.sleep(1)
```

Which will result in:

```{figure} /images/test_check_ltr559.jpg
---
name: output_ltr559-fig
alt: Picture of the output of LTR559 sensor using Circuitpython 
class: bg-primary mb-1
width: 100%
align: center
---
**Output for LTR559.** 
```


