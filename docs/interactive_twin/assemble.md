# Assemble

A series of sensors have been developed that allow for different aspects of the plant to be monitored. However, the previous modules have treated each sensor as having its own microcontroller and unique code set which can cause redundancy, waste, and increase the work necessary for code-fixes. In this module the sensors will be brought together in a couple of IoT devices. 

## IoT Sensor: Air Quality, Light, and Moisture

## Layout

```{figure} /images/MODIFIED_double_i2c_soil_moisture_s.jpg
---
name: assemble_iot_sensor_ASSEMBLE-fig
alt: Fritzing diagram for IoT Sensor
class: bg-primary mb-1
width: 100%
align: center
---
**Set-up of the IoT Sensor.** *Fritzing breadboard diagram showing how to set-up the various electronic components: two I2C sensors and a soil moisture sensor. The pins used on the Arduino Nano RP2040 Connect for the first I2C sensor are red wire: 3v3, black wire: Ground, yellow wire: SDA (pin A4), and orange wire: SCL (pin A5). The 3v3 and Ground are connected to the Breadboard’s rails to allow for multiple wires to attached, whereas the SDA and SCL are connected directly. For the second I2C power and ground come from the rails whilst the white wire leads from the second SDA (pin D13) and the blue wire from the second SCL (pin D11). For the moisture sensor there are three pins, - + A, connected to the board via three pins, pin A0 (purple wire), pin D2 (brown wire), and a ground pin (grey wire).*
```

## Packages

The CircuitPython code outlined here requires the following packages: 



```{code-block} python
---

---
import time

# packages related to board
import board
import busio
from busio import I2C

# packages for soil sensor
from analogio import AnalogIn
from digitalio import DigitalInOut, Direction, Pull

# packages for i2c sensors (add to lib folder)
import adafruit_bme680
from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559

# packages for wifi
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from   adafruit_esp32spi import adafruit_esp32spi
import adafruit_requests as requests

```

with `adafruit_bme680`, `pimoroni_circuitpython_ltr559`, `adafruit_minimqtt`, `adafruit_esp32spi`, and `adafruit_requests` needing to be added by the user to the ` /lib` folder of the CircuitPy directory. 

The following classes `sensor_bme680`, `sensor_ltr559`, `sensor_capacitive_soil_moisture`, `wifi_module`, and  `MQTT_comms` can be run as modules in a package by adding a new folder to the ` /lib` folder and calling it `wur_circuitpython_functions`, create an empty text file in this folder and save it as `__init__.py`. Next copy and paste each of the following sets of code into individual empty text files and rename them as `sensor_bme680.py`, `sensor_ltr559.py`, `sensor_capacitive_soil_moisture.py`, `wifi_module.py`, and  `MQTT_comms.py` respectively. 


`````{admonition} Click here for Package: Sensor BME680!
:class: tip, dropdown

Package Sensor BME680 can be downloaded [here](https://gitlab.com/wur/digitaltwin/-/blob/main/code/lib/wur_circuitpython_functions/sensor_bme680.py), or copy and pasted from here:


````{admonition} Sensor BME680!
:class: tip, dropdown

```{code-block} python
---

---
""" sensor_bme680.py

# Rationale:
    Start up, run, and print variables of I2C sensor BME680

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html

# Requirements:
    import time
    import board
    import busio
    from busio import I2C
    import adafruit_bme680


# Contains:
    class : sensor_bme680
            which contains: [_init_,
                            start_sensor, measure_all,
                            measure_gas, measure_rel_humidity,
                            measure_altitude, measure_pressure,
                            measure_temperature, print_sensor_values
                            ]

# References:
    
    
# Examples:

    # RUN BME680 sensor
    # import packages
    from helper_functions import sensor_bme680
    import board
    import time
        
    # intialise sensor
    sensor1 = sensor_bme680()
    sensor1.start_sensor(SCL_pin = board.D11, SDA_pin = board.D13)
        
    # collect data
    while True:
        sensor1.measure_all()
        sensor1.print_sensor_values()
        time.sleep(10)
        
"""

#Packages
import time
import board
import busio
from busio import I2C
import adafruit_bme680


# Class
class sensor_bme680:
    """ CLASS: MEASURE BME680 i2c address 0x76 and 0x77  
        
        
    """

    
    def __init__(self):
        """ Initialise class
        
        
        """
        # VARIABLES
        # Internal class variables
        self.sensor_name             = "BME680"
        self.i2c_address             = 0x76
        self.i2c_address_alternative = 0x77
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        
        # Data values
        self.temperature             = None
        self.gas                     = None
        self.rel_humidity            = None
        self.pressure                = None
        self.altitude                = None
        
        self.temperature_correction  = None
        self.sea_level_pressure_correction = None
        
        # Sensor variable metadata
        self.variable_dictionary = {'Temperature' : {'variable name' : 'temperature' , 'units symbol' : 'C'   , 'units' : 'C'          , 'format' : '0.1f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : 'temperature_offset'},
                                    'Gas'         : {'variable name' : 'gas'         , 'units symbol' : 'ohm' , 'units' : 'ohm'        , 'format' : 'd'   , 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Humidity'    : {'variable name' : 'rel_humidity', 'units symbol' : '%%'  , 'units' : 'percentage' , 'format' : '0.1f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    'Pressure'    : {'variable name' : 'pressure'    , 'units symbol' : 'hPa' , 'units' : 'hPa'        , 'format' : '0.3f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : 'pressure_sea_level'},
                                    'Altitude'    : {'variable name' : 'altitude'    , 'units symbol' : 'm'   , 'units' : 'meters'     , 'format' : '0.2f', 'data_type' : 'measured', 'provenance' : 'sensor BME680', 'dependent_variable' : None},
                                    }
    
    
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
               
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
        
        Output:
            self.bme680
        
        Packages
            from busio import I2C
            import adafruit_bme680
        
        """
        
        # Variables
        if frequency is not None:
            self.i2c_frequency = frequency
        else:
            frequency = self.i2c_frequency
        
        # Functions
        # Create sensor object, communicating over the board's second I2C bus
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        #self.i2c_address = i2c_address
        self.bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c_address,address=0x76,  debug=False)
    
    
    def measure_all(self, pressure_sea_level = 1011, temperature_offset = -5):
        """ FUNC: measure all variables, return (gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature)
        
        Input:
            None
            
        Output:
            gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature
            
        """
        
        gas_value          = self.measure_gas()
        rel_humidity_value = self.measure_rel_humidity()
        altitude_value     = self.measure_altitude()
        pressure_value     = self.measure_pressure(pressure_sea_level)
        measured_temperature, corrected_temperature = self.measure_temperature(temperature_offset)
        
        return gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature
    
    
    def measure_gas(self):
        """ FUNC: measure gas in ohm
        
        Input:
            None
            
        Output:
            gas_value = gas value in ohm
            
        Packages:
            import adafruit_bme680
        
        """
        # Functions
        gas_value = self.bme680.gas
        
        # store value
        self.gas  = gas_value
        
        return gas_value
    
    
    def measure_rel_humidity(self):
        """ FUNC: measure relatiuve humidity in %
        
        Input:
            None
        
        Output:
            rel_humidity_value = relative humidity value in percentage
        
        Packages:
            import adafruit_bme680
        
        """
        # Functions
        rel_humidity_value = self.bme680.relative_humidity
        
        # store value
        self.rel_humidity  = rel_humidity_value
        
        return rel_humidity_value
    
    
    def measure_altitude(self):
        """ FUNC: measure altitude in meters
        
        Input:
            None
            
        Output:
            altitude_value = altitude in m
        
        Packages:
            import adafruit_bme680
        
        """
        # Functions
        altitude_value = self.bme680.altitude
        
        # store value
        self.altitude  = altitude_value
        
        return altitude_value
    
    
    def measure_pressure(self, pressure_sea_level = 1011):
        """ FUNC: measure pressure in hpa
        
        Input:
            pressure_sea_level = sea level pressure in hPA at location's sea level
        
        Output:
            pressure_value     = pressure value in hPA
        
        Packages:
            import adafruit_bme680
        
        """
        # Variable
        # store pressure_sea_level correction parameter
        self.sea_level_pressure_correction = pressure_sea_level
        
        # Function        
        # change this to match the location's pressure (hPa) at sea level
        self.bme680.sea_level_pressure = pressure_sea_level
        # get pressure value
        pressure_value = self.bme680.pressure
        # store variable
        self.pressure  = pressure_value
        
        return pressure_value
        
        
        
    def measure_temperature(self, temperature_offset = -5):
        """ FUNC: Measure temperature in Celcius
            
            Input:
                temperature_offset    = offset temperature (default -5)
                
            Output:
                measured_temperature  = measured temperature
                corrected_temperature = corrected temperature (measured_temperature + temperature_offset)
                
            Package:
                import adafruit_bme680
            
            Notes:
                You will usually have to add an offset to account for the temperature of
                the sensor. This is usually around 5 degrees but varies by use. Use a
                separate temperature sensor to calibrate this one.
            
        """
        # Variable
        # store temperature_offset correction parameter
        self.temperature_correction = temperature_offset
          
        # Function    
        measured_temperature  = self.bme680.temperature
        corrected_temperature = measured_temperature + temperature_offset
        
        # store variable
        self.temperature = corrected_temperature
        
        return measured_temperature, corrected_temperature
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
            
        Output:
            None
        
        Notes:
            print("\nTemperature: %0.1f C" % (temperature))
            print("Gas: %d ohm" % gas)
            print("Humidity: %0.1f %%" % rel_humidity)
            print("Pressure: %0.3f hPa" % pressure)
            print("Altitude = %0.2f meters" % altitude)
        
        """
        # Get internal data values
        temperature  = self.temperature
        gas          = self.gas
        rel_humidity = self.rel_humidity
        pressure     = self.pressure
        altitude     = self.altitude
        
        # Print values
        # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
        temperature_text = "\nTemperature: %" + self.variable_dictionary['Temperature']['format'] + " " +  self.variable_dictionary['Temperature']['units symbol']
        gas_text         = "Gas: %" + self.variable_dictionary['Gas']['format'] + " " +  self.variable_dictionary['Gas']['units symbol'] 
        humidity_text    = "Humidity: %" + self.variable_dictionary['Humidity']['format'] + " " +  self.variable_dictionary['Humidity']['units symbol'] 
        pressure_text    = "Pressure: %" + self.variable_dictionary['Pressure']['format'] + " " +  self.variable_dictionary['Pressure']['units symbol']  
        altitude_text    = "Altitude = %" + self.variable_dictionary['Altitude']['format'] + " " +  self.variable_dictionary['Altitude']['units symbol'] 
        
        print()
        print(temperature_text % (temperature))
        print(gas_text  % gas)
        print(humidity_text  % rel_humidity)
        print(pressure_text  % pressure)
        print(altitude_text % altitude)

# Functions
# None


# Dictionaries
# None

```
````
`````




`````{admonition} Click here for Package: Sensor LTR559!
:class: tip, dropdown

Package Sensor LTR559 can be downloaded [here](https://gitlab.com/wur/digitaltwin/-/blob/main/code/lib/wur_circuitpython_functions/sensor_ltr559.py), or copy and pasted from here:


````{admonition} Sensor LTR559!
:class: tip, dropdown

```{code-block} python
---

---
""" sensor_ltr559.py

# Rationale:
    Start up, run, and print variables of I2C sensor LTR559
    # Purpose: Light & Proximity sensor/ optical presence & proximity sensor
    # i2C: 0x23
        

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_air_quality.html    

# Requirements:
    from board import SCL, SDA
    from busio import I2C
    from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559

# Contains:
    class : sensor_ltr559
            which contains: [   _init_, start_sensor,
                                measure_lux,  measure_proximity,
                                measure_all, print_sensor_values]

# References:
    https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559
    https://github.com/pimoroni/Pimoroni_CircuitPython_LTR559/blob/master/pimoroni_circuitpython_ltr559.py

# Notes:
    > IR/UV-filtering
    > 50.60Hz flicker rejection
    > 0.01 lux to 64,000 lux light detection range
    > ~5cm proximity detection range    
    
    
# Examples:



"""

#Packages
#import time
#from adafruit_bus_device.i2c_device import I2CDevice
#from adafruit_register.i2c_bits import RWBits, ROBits
#from adafruit_register.i2c_bit import RWBit, ROBit
#from micropython import const
#import Pimoroni_CircuitPython_LTR559

#import time
import board
from busio import I2C
from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        

# Class
class sensor_ltr559:
    """CLASS: MEASURE LTR559
    
    """
    
    
    def __init__(self):
        """ Initialise class
                
        
        """
        
        # VARIABLES
        # Internal class variables
        
        # sensor
        self.sensor_name             = "LTR559"
        self.i2c_address             = 0x23
        self.i2c_address_alternative = None
        self.i2c_I2C                 = None
        self.i2c_frequency           = 100000   # use 'slow' 100KHz frequency
        self.LTR559                  = None
                
        # measured variables
        self.lux                     = None
        self.proximity               = None
        
        # calculated variables
        # None
        
        # metadata
        self.variable_dictionary = {'Light'     : {'variable name' : 'lux'       , 'units symbol' : 'lux' , 'units' : 'lux' , 'format' : '0.2f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor LTR559', 'dependent_variable' : None},
                                    'Proximity' : {'variable name' : 'proximity' , 'units symbol' : '%%'  , 'units' : None  , 'format' : '0.1f', 'data_type' : 'measured', 'method' : None, 'provenance' : 'sensor LTR559', 'dependent_variable' : None},
                                    }
        
    
    def start_sensor(self, SCL_pin = None, SDA_pin = None, frequency = None, chain = False, i2c_chainaddress = None):
        """ FUNC: create sensor object
                
        Input:
            SCL_pin          =      GPIO pin related to SCL, e.g., board.SCL, board.D11
            SDA_pin          =      GPIO pin related to SDA, e.g., board.SDA, board.D13
            frequency        =      i2c frequency, default is None but uses internal class value of 100,000
            chain            =      If the i2C sensor is in a chain. True or False. Default is False
            i2c_chainaddress =      The i2c address of the 'main' sensor
            
        Output:
            None, modify self.LTR559
        
        Packages:
            from busio import I2C
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559  
        
        Notes:
        
        
        """
        
        # Variables
        if frequency is not None:
            
            self.i2c_frequency = frequency
            
        else:
            
            frequency = self.i2c_frequency
        
        # Functions
        # Create library object on our I2C port
        if chain is False:
            
            i2c_address  = I2C(SCL_pin,SDA_pin, frequency = frequency)
            self.i2c_I2C = i2c_address
            
        elif chain is True:
            
            i2c_address  = i2c_chainaddress
            self.i2c_I2C = i2c_address
            
        else:
            
            raise ValueError()
        
        self.LTR559 = Pimoroni_LTR559(i2c_address)
    
    
    def measure_lux(self):
        """Measure lux
        
        Input:
            None
        
        Output:
            lux = sensor value of lux
            
        Packages:
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        """
        # Functions
        lux      = self.LTR559.lux   # Get Lux value from light sensor
        self.lux = lux
        
        return lux
        
     
    def measure_proximity(self):
        """Measure proximity
        
        Input:
            None
        
        Output:
            prox = sensor value of proximity
        
        Packages:
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        """
        # Functions
        prox           = self.LTR559.prox   # Get Lux value from light sensor
        self.proximity = prox
        
        return prox
        
    
    def measure_all(self):
        """ FUNC: measure all, returns (lux_value, prox_value)
        
        Input:
            None
        
        Output:
            lux_value, prox_value
        
        Packages:
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        """
        lux_value   =  self.measure_lux()
        prox_value  =  self.measure_proximity()
        
        return lux_value, prox_value
    
    
    def print_sensor_values(self):
        """ PRINT: print all sensor values
        
        Input:
            None
        
        Output:
            None, print output
        
        Packages:
            from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
        
        Notes:
            print("\nLux: %0.2f Lux" % (lux))
            print("Proximity: %0.1f %%" % proximity)
        
        """
        # functions
        lux          = self.lux
        proximity    = self.proximity
        
        # Print values
        lux_text       = "\nLux: %"      + self.variable_dictionary['Light']['format']      + " " +  self.variable_dictionary['Light']['units symbol']  
        proximity_text = "Proximity: %"  + self.variable_dictionary['Proximity']['format']  + " " +  self.variable_dictionary['Proximity']['units symbol']
        
        print(lux_text    % (lux))
        print(proximity_text % (proximity))


# Functions
# None

# Dictionaries
# None


```
````
`````


`````{admonition} Click here for Package: Capacitive Soil Moisture Sensor!
:class: tip, dropdown

Package for the Capacitive Soil Moisture Sensor can be downloaded [here](https://gitlab.com/wur/digitaltwin/-/blob/main/code/lib/wur_circuitpython_functions/sensor_capacitive_soil_moisture.py), or copy and pasted from here:


````{admonition} Click here for Package: Capacitive Soil Moisture Sensor!
:class: tip, dropdown

```{code-block} python
---

---
""" sensor_capacitive_soil_moisture.py

# Rationale:
    Capacitive Soil Moisture Sensor with CircuitPython
    # Purpose: Measure soil moisture using Capacitive Soil Moisture Sensor SKU SEN0193
    # i2C    : None


# Authors:
    WUR Digital Twin Platform Methodology


# Date
    06/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/sensing_moisture.html 


# Requirements:
    import board
    import time
    from analogio import AnalogIn
    from digitalio import DigitalInOut, Direction, Pull


# Contains:
    class : soil_sensor
            which contains: [ _init_, start_sensor,
                            measure_soil, measure_all,
                            print_sensor_values,
                            modify_divide, modify_add,         # generic functions
                            modify_subtract, modify_multiply,  # generic functions
                            ]


# References:
    https://wiki.dfrobot.com/Capacitive_Soil_Moisture_Sensor_SKU_SEN0193
    https://andywarburton.co.uk/raspberry-pi-pico-soil-moisture-sensor/


# Notes:
    > Capacitive_Soil_Moisture_Sensor_SKU_SEN0193
        
    > RED = power; BLACK = Ground; BLUE = Analog pin
    connect red power wire to digital pin (e.g., D2),
    connect black ground wire to GND pin, connect
    blue wire to analog pin (e.g., A0)
    
    
    > Typical ranges with value = value/100:
    Dry   = [520, 430]
    Wet   = [430, 350]
    Water = [350, 260]  
        
    
# Examples:



"""

# Packages
import board
import time
from analogio import AnalogIn
from digitalio import DigitalInOut, Direction, Pull


# Class
class soil_sensor:
    """CLASS: Capacitive Soil Moisture Sensor with CircuitPython
    
    
    """
    
    
    def __init__(self):
        """ Initialise class
        
        Input:
            None
            
        Output:
            None
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        """
        
        # sensor
        self.sensor_name               = "Capacitive Soil Moisture Sensor"
        self.analog_soil_sensor        = None
        self.analog_soil_sensor_power  = None
                
        # measured variables
        self.sensor_value              = None
        
        # calculated variables
        self.sensor_value_modified     = None
        self.sensor_interpretation     = None
        
        # metadata
        self.variable_dictionary = {'Moisture'          : {'variable name' : 'soil moisture' , 'units symbol' : ' ' , 'units' : ' ' , 'format' : 'i'   , 'data_type' : 'measured'  , 'method' : None, 'provenance' : 'Capacitive Soil Moisture Sensor', 'dependent_variable' : None},
                                    'Moisture modified' : {'variable name' : 'soil moisture' , 'units symbol' : ' ' , 'units' : ' ' , 'format' : '0.2f', 'data_type' : 'calculated', 'method' : None, 'provenance' : 'Capacitive Soil Moisture Sensor', 'dependent_variable' : 'sensor_value'},
                                    }
    
    
    ###### Generic Functions
    def modify_divide(self, value, modifier = None):
        """ FUNC - DIVIDE: modify value via divide
                
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output = value / modifier
        
        return output 
    
    
    def modify_add(self, value, modifier = None):
        """ FUNC - ADD: modify value via add
                
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output = value + modifier
        
        return output
    
    
    def modify_subtract(self, value, modifier = None):
        """ FUNC - SUBTRACT: modify value via subtract
                
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output =  value - modifier
        
        return output
    

    def modify_multiply(self, value, modifier = None):
        """ FUNC - MULTIPLY: modify value via multiply
        
        Input:
            value    = value to be modified
            modifier = value that value should be modified by
            
        Output:
            output   = modified value
        """
        if modifier is None:
            
            raise ValueError(" modifier must be given ")
        
        output =  value * modifier
        
        return output
    
    
    ###### Sensors
    def start_sensor(self, power_pin = None, analog_pin = None):
        """ FUNC: create sensor object
        
        Input:
            power_pin        =      power pin,  e.g., digital pin board.D2
            analog_pin       =      analog pin, e.g., board.A0
            
        Output:
        
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
        
            
        """
        
        # Start-up SOIL SENSOR
        # setup the moisture sensor power pin and turn it off by default
        self.analog_soil_sensor_power  = DigitalInOut(power_pin)
        self.analog_soil_sensor_power  = Direction.OUTPUT
        self.analog_soil_sensor_power  = False
        
        
        # set the analog read pin for the moisture sensor
        self.analog_soil_sensor        = AnalogIn(analog_pin)
    
    
    def measure_soil(self):
        """ FUNC: Measure soil moisture
        
        Input:
            None
        
        Output:
            sensor_value = soil analog sensor value
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        """
        # Turn on sensor
        self.analog_soil_sensor_power = True
        
        # measured variables
        sensor_value      = self.analog_soil_sensor.value
        self.sensor_value = sensor_value
        
        return sensor_value
    
    
    def calculate_soil(self, modifier = 100):
        """ FUNC: Calculate modified soil moisture (= soil moisture/100)
        
        Input:
            modifier = arbitrary value for reducing output value,
                        default = 100
        
        Output:
            sensor_value_mod = soil analog sensor value modified
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        """
        # Check if the sensor value has been measured
        if self.sensor_value is None:
            
            self.analog_soil_sensor_power = True
        
        # measured variables
        sensor_value      = self.analog_soil_sensor.value
        self.sensor_value = sensor_value
        
        
        # calculated variables
        sensor_value_mod            = self.modify_divide(sensor_value, modifier)
        self.sensor_value_modified  = sensor_value_mod
        
        return sensor_value_mod
    
    
    def measure_all(self, modifier = 100):
        """ FUNC: measure all, returns (sensor_value, sensor_value_mod)
        
        Input:
            modifier = arbitrary value for reducing output value,
                        default = 100
        
        Output:
            sensor_value, sensor_value_mod
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        """
        # Turn on sensor
        self.analog_soil_sensor_power = True
        
        # measured variables
        sensor_value      = self.measure_soil()
        self.sensor_value = sensor_value
        
        
        # calculated variables
        sensor_value_mod            = self.calculate_soil(modifier)
        self.sensor_value_modified  = sensor_value_mod
        
        return sensor_value, sensor_value_mod
    
    
    def print_sensor_values(self, modified_or_not = 'modified'):
        """ PRINT: print all sensor values
        
        Input:
            modified_or_not = to print the modified value or not, default is'modified', options are 'modified' or 'not modified'
        
        Output:
            None, print sensor values
        
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        
        Note:
            print("\Soil Moisture: %0.2f " % (moisture))
        
        """
        # functions
        if modified_or_not == 'modified':
            
            moisture      = self.sensor_value_modified
            
            # Print values
            # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
            moisture_text = "\nSoil Moisture: %"      + self.variable_dictionary['Moisture modified']['format']      + " " +  self.variable_dictionary['Moisture modified']['units symbol']
            print(moisture_text  % (moisture))
        
        
        elif modified_or_not == 'not modified':
            
            moisture      = self.sensor_value
        
            # Print values
            # self.variable_dictionary [variable][option] with options being: 'variable name','units symbol', 'units','format'
            moisture_text = "\nSoil Moisture: %"      + self.variable_dictionary['Moisture']['format']      + " " +  self.variable_dictionary['Moisture']['units symbol']  
            print(moisture_text  % (moisture))
        
        
        else:
            
            raise ValueError(' modified_or_not should either be modified or not modified ')
        
    
    def print_sensor_interpretation(self,
                                        Dry   = [520, 430, 'Dry'  ],
                                        Wet   = [430, 350, 'Wet'  ],
                                        Water = [350, 260, 'Water'],
        ):
        """ FUNC: print interpretation
        
        Input:
            Dry    = list of: highest value, lowest value, and interpretation for Dry sensor conditions using modified (/100) values
            Wet    = list of: highest value, lowest value, and interpretation for Wet sensor conditions using modified (/100) values
            Water  = list of: highest value, lowest value, and interpretation for sensor in Water using modified (/100) values
            
        Output:
            None, modify self.sensor_interpretation and print interpretation
            
        Packages:
            import board
            import time
            from analogio import AnalogIn
            from digitalio import DigitalInOut, Direction, Pull
            
        Note:
            values for Dry, Wet, and Water come from:
            https://wiki.dfrobot.com/Capacitive_Soil_Moisture_Sensor_SKU_SEN0193
            but can be modified by changing their inputs
            
        """

        if self.sensor_value_modified is not None:
            
            if self.sensor_value_modified > Dry[0]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' GREATER THAN list (Flag)')
                self.sensor_interpretation = 'Flag - Higher than known values'
                
            elif Dry[1] <= self.sensor_value_modified <= Dry[0]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' meaning ' + str(Dry[2]))
                self.sensor_interpretation = Dry[2]
                
            elif Wet[1] <= self.sensor_value_modified <= Wet[0]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' meaning ' + str(Wet[2]))
                self.sensor_interpretation = Wet[2]
                
            elif Water[1] <= self.sensor_value_modified <= Water[0]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' meaning ' + str(Water[2]))
                self.sensor_interpretation = Water[2]
                
            elif self.sensor_value_modified > Water[1]:
                
                print('Sensor value is : ' + str(self.sensor_value_modified) + ' LESS THAN list (Flag)')
                self.sensor_interpretation = 'Flag - Lower than known values'
            
            else:
                raise ValueError('Value does not match known values')
                
        else:
            print('No sensor value recorded use measure_soil or measure_all')
            
        
# Functions
# None

# Dictionaries
# None

```
````
`````

`````{admonition} Click here for Package: WiFi Module!
:class: tip, dropdown

Package for the WiFi module can be downloaded [here](https://gitlab.com/wur/digitaltwin/-/blob/main/code/lib/wur_circuitpython_functions/wifi_module.py), or copy and pasted from here:


````{admonition}  WiFi Module
:class: tip, dropdown


```{code-block} python
---

---
""" wifi_module.py

# Rationale:
    Networking via wifi

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    12/08/2022 

# Licence:
    
    
# URL:
    

# Requirements:
    #Packages
    import board
    import busio
    from digitalio import DigitalInOut
        
    # need to add following libraries to lib. folder:
    import adafruit_requests as requests
    import adafruit_esp32spi.adafruit_esp32spi_socket as socket
    from   adafruit_esp32spi import adafruit_esp32spi

# Contains:
    class : wifi_connect
            which contains: [_init_, get_status, check_status,
                            get_connection_status, get_mac_address,
                            print_mac_address, get_firmware_vers,
                            print_firmware_vers, scan_networks,
                            scan_networks_ALL_INFO, print_ssid,
                            print_ip, connect_to_network, check_connection]
    

# References:
    https://docs.circuitpython.org/projects/esp32spi/en/latest/index.html
    
# Examples:
    ## Example Arduino nano Rp2040 Connect

    # packages
    from wur_circuitpython_functions import wifi_module
    import board

    try:
        from secrets import secrets
    except:
        raise ValueError(' Requires a secrets dictionary containing ssid and password key-value pairs ')
    
    get_connected = wifi_module.connect(pin_esp32_cs       = board.CS1,
                                        pin_esp32_ready    = board.ESP_BUSY,
                                        pin_esp32_reset    = board.ESP_RESET,
                                        pin_SCK            = board.SCK1,
                                        pin_SDO            = board.MOSI1,
                                        pin_SDI            = board.MISO1)
    ap            = get_connected.scan_networks()
    get_connected.connect_to_network(secrets["ssid"], secrets["password"])


    ## Example Raspberry Pico  with ESP32, e.g., Pimoroni WiFi Pack 
        https://shop.pimoroni.com/products/pico-wireless-pack?variant=32369508581459
        
        # packages
        from wur_circuitpython_functions import wifi_module
        import board
        
        try:
            from secrets import secrets
        except:
            raise ValueError(' Requires a secrets dictionary containing ssid and password key-value pairs ')
        
        get_connected = wifi_module.connect(pin_esp32_cs       = board.GP13,
                                            pin_esp32_ready    = board.GP14,
                                            pin_esp32_reset    = board.GP15,
                                            pin_SCK            = board.GP10,
                                            pin_SDO            = board.GP11,
                                            pin_SDI            = board.GP12)
        ap            = get_connected.scan_networks()
        get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
"""
    
#Packages
import board
import busio
from digitalio import DigitalInOut
    
# need to add following libraries to lib. folder:
import adafruit_requests as requests
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from   adafruit_esp32spi import adafruit_esp32spi


# Class
class connect:
    """ CLASS: Wireless networking
    
    Rationale:
        Networking via wifi
        
    REFERENCES:
        https://docs.circuitpython.org/projects/esp32spi/en/latest/index.html
    
    EXAMPLE:
    
    # packages
    from helper_functions import wifi_connect
    
    # set up wifi
    get_connected = connect()
    ap            = get_connected.scan_networks()
    get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
    """

    
    
    def __init__(self, 
                 pin_esp32_cs       = None,
                 pin_esp32_ready    = None,
                 pin_esp32_reset    = None,
                 pin_SCK            = None,
                 pin_SDO            = None,
                 pin_SDI            = None,
                 
                 ):
        """ Initialise class
        
        Input:
            pin_esp32_cs       = CS pin, default = board.CS1,
            pin_esp32_ready    = Ready pin, default = board.ESP_BUSY,
            pin_esp32_reset    = Reset pin, default = board.ESP_RESET,
            pin_SCK            = SCK pin, default = board.SCK1,
            pin_SDO            = SDO pin, default = board.MOSI1,
            pin_SDI            = SDI pin, default = board.MISO1,
        
            
            Note 1: for Arduino Nano Connect RP2040
            connect(
                pin_esp32_cs = board.CS1,
                 pin_esp32_ready    = board.ESP_BUSY,
                 pin_esp32_reset    = board.ESP_RESET,
                 pin_SCK            = board.SCK1,
                 pin_SDO            = board.MOSI1,
                 pin_SDI            = board.MISO1
                 )
            
            Note 2: for rapsberry pi pico with wifi shield the pins are: 
        
                connect(

                        pin_esp32_cs       = board.GP13,
                        pin_esp32_ready    = board.GP14,
                        pin_esp32_reset    = board.GP15,
                        pin_SCK            = board.GP10,
                        pin_SDO            = board.GP11,
                        pin_SDI            = board.GP12)
                
                )
        
        Packages:
            import board
            import busio
            from digitalio import DigitalInOut
                
            # need to add following libraries to lib. folder:
            import adafruit_requests as requests
            import adafruit_esp32spi.adafruit_esp32spi_socket as socket
            from   adafruit_esp32spi import adafruit_esp32spi
            
        """

        # init packages
        import adafruit_requests as requests
        import adafruit_esp32spi.adafruit_esp32spi_socket as socket

        # VARIABLES
        # Internal variables
        
        # Dictionaries
        # used for scan networks give all info (for function
        # scan_networks_ALL_INFO)
        # Authorisation mode dictionaries giving key and value 
        self.authmode_dict = {0: 'open',
                              1: 'WEP',
                              2: 'WPA-PSK',
                              3: 'WPA2-PSK',
                              4: 'WPA/WPA2-PSK'}
        
        # Hidden mode dictionaries giving key and value 
        self.hidden_dict   = {0: 'visible',
                              1: 'hidden'}
        
        
        # ESP32 pins
        self.esp32_cs      = DigitalInOut(pin_esp32_cs)
        self.esp32_ready   = DigitalInOut(pin_esp32_ready)
        self.esp32_reset   = DigitalInOut(pin_esp32_reset)

        # uses the secondary SPI connected through the ESP32
        self.spi = busio.SPI(pin_SCK, pin_SDO, pin_SDI)

        self.esp = adafruit_esp32spi.ESP_SPIcontrol(self.spi, self.esp32_cs, self.esp32_ready, self.esp32_reset)
        
        # as per the docs: "requests library the type of socket we're using (socket type varies by connectivity
        # type - we'll be using the adafruit_esp32spi_socket for this example). We'll also
        # set the interface to an esp object. This is a little bit of a hack, but it lets
        # us use requests like CPython does."
        # socket is called via the package: import adafruit_esp32spi.adafruit_esp32spi_socket as socket
        self.requests = requests
        self.requests.set_socket(socket, self.esp)
        
        # Print board status, firmware version, and mac address
        self.check_status()
        self.print_firmware_vers()
        self.print_mac_address()
        
        
    def get_status(self):
        """ RETURN: ESP board status
        
        Input:
            None
            
        Output:
            status_of_ = binary value of status
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        status_of_ =  self.esp.status
        
        return status_of_
            
      
    def check_status(self):
        """PRINT: Compare status vs idle mode
        
        Input:
            None
            
        Output:
            print 'ESP32 found and in idle mode' if get_status == 0
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        if self.get_status() == adafruit_esp32spi.WL_IDLE_STATUS:
            
            print("ESP32 found and in idle mode")
        
    
    def get_connection_status(self):
        """ RETURN: Connection status
        
        Input:
            None
            
        Output:
            connection_status = returns True or False if connected
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
            
        """
        # function
        connection_status = self.esp.is_connected
        
        #print(connection_status)
        
        return connection_status
        
        
    def get_mac_address(self):
        """ RETURN: Mac Address
        
        Input:
            None

        Output:
            mac_address = MAC address of board
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """ 
        # function
        mac_address = self.esp.MAC_address
        
        return mac_address
            
    
    def print_mac_address(self):
        """PRINT: MAC address
        
        Input:
            None
            
        Output:
            print mac address
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        print("MAC addr:", [hex(i) for i in self.get_mac_address()])
        
      
    def get_firmware_vers(self):
        """ RETURN: Firmware version
        
        Input:
            None
        
        Output:
            firmware = board firmware version
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        firmware = self.esp.firmware_version
        return firmware
        
    
    def print_firmware_vers(self):
        """ PRINT: Firmware version
        
        Input:
            None
            
        Output:
            print firmware version
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        print("Firmware vers.", self.get_firmware_vers())
        
    
    def scan_networks(self):
        """ FUNC: Scan for network Access Points
        
        Input:
            None
        
        Output:
            scan_result = Returns a list of dictionaries with ‘ssid’, ‘rssi’ and
                            ‘encryption’ entries, one for each AP found
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        # inform user
        print('scanning for access points')
        
        # scan for access points (ap)        
        # returns values as: ssid, bssid, channel, RSSI, authmode, hidden
        scan_result = self.esp.scan_networks()
                
        # print
        for ap in scan_result:
            #print("\t%s\t\tRSSI: %d" % (str(ap['ssid'], 'utf-8'), ap['rssi']))
            print(ap)
                
        return scan_result
    
    
    def scan_networks_ALL_INFO(self):
        """ FUNC: Scan for network Access Points give all info
        
        Input:
            None
        
        Output:
            scan_result = Returns a list of dictionaries with ‘ssid’, ‘rssi’ and
                            ‘encryption’ entries, one for each AP found
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        # inform user
        print('scanning for access points')
        
        # scan for access points (ap)        
        # returns values as: ssid, bssid, channel, RSSI, authmode, hidden
        scan_result = self.esp.scan_networks()
                
        # print
        for ap in scan_result:
            
                       
            hidden_value = self.hidden_dict[ap['hidden']]
            autho_value  = self.authmode_dict[ap['authmode']]
            
            print("\tSSID: %s \n\t\tRSSI: %d \n\t\t Channel: %s\t\t Hidden: %s\t\t Authentication: %s" % (str(ap['ssid'], 'utf-8'), ap['rssi'],str(channel),hidden_value,autho_value))
            
           
        return scan_result
    
    
    def print_ssid(self):
        """ PRINT: SSID
        
        Input:
            None

        Output:
            ssid = returns ssid 
            (also prints ssid)

        Package:
            from   adafruit_esp32spi import adafruit_esp32spi

        Example:
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
            get_connected.print_ssid()
        
        """
        # function
        ssid = str(self.esp.ssid, "utf-8")
        print(ssid)
        return ssid
    

    def print_ip(self):
        """ PRINT: IP address
        
        Input:
            None

        Output:
            pretty_ip_address = IP address
            (also prints IP address)

        Package:
            from   adafruit_esp32spi import adafruit_esp32spi

        Example:
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
            get_connected.print_ip()
        
        """
        # function
        pretty_ip_address = str(self.esp.pretty_ip(self.esp.ip_address)) 
        print(pretty_ip_address)

        return pretty_ip_address

    
    def connect_to_network(self, ssid_to_connect, ssid_password):
        """ CONNECT: Connect device to network
        
        Input:
            ssid_to_connect = network ID
            ssid_password   = network password
            
        Output:
            None (connection to network)
        
        Package:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        Example:
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
        
        """
        # function
        print("Connecting to AP...")
        
        # check if connected
        while not self.get_connection_status():
            
            try:
                self.esp.connect_AP(ssid_to_connect, ssid_password)
                
            except RuntimeError as e:
                print("could not connect to AP, retrying: ", e)
                continue
        
        print("Connected to", str(self.esp.ssid, "utf-8"), "\tRSSI:", self.esp.rssi)
        print("My IP address is", self.esp.pretty_ip(self.esp.ip_address))
    
    
    def check_connection(self,
                        PING_test = "google.com",
                        HOST_test = "adafruit.com",
                        TEXT_URL  = "http://wifitest.adafruit.com/testwifi/index.html",
                        JSON_URL  = "http://api.coindesk.com/v1/bpi/currentprice/USD.json"
                         ):
        """ FUNC: check wifi connection
        
        Input:
            PING_test = destination for test checking timing (returns ms timing)
            HOST_test = destination for test checking ip lookup
            TEXT_URL  = destination for test checking fetching text
            JSON_URL  = destination for test checking fetching json
            
        Output:
            prints values
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        Reference:
            https://learn.adafruit.com/pyportal-oblique-strategies/internet-connect
        
        """
        # functions
        print("Checking internet connection...")
        
        # IP look up test
        print("IP lookup Test:")
        print("IP lookup adafruit.com: %s" % self.esp.pretty_ip(self.esp.get_host_by_name(HOST_test)))
        
        # PING test, returns a millisecond timing value to a destination IP address or hostname
        print("Ping Test:")
        print("Ping google.com: %d ms" % self.esp.ping(PING_test))
        
        # TEXT retrieval test
        print("Text retrieval Test:")
        # esp._debug = True
        print("Fetching text from", TEXT_URL)
        r = self.requests.get(TEXT_URL)
        print("-" * 40)
        print(r.text)
        print("-" * 40)
        r.close()

        # JSON retrieval test
        print("JSON retrieval Test:")
        print()
        print("Fetching json from", JSON_URL)
        r = self.requests.get(JSON_URL)
        print("-" * 40)
        print(r.json())
        print("-" * 40)
        r.close()

        print("Done!")


# Functions
# None


# Dictionaries
# None



```
````
`````


`````{admonition} Click here for Package: MQTT communication!
:class: tip, dropdown

Package for the MQTT communication module can be downloaded [here](https://gitlab.com/wur/digitaltwin/-/blob/main/code/lib/wur_circuitpython_functions/MQTT_comms.py), or copy and pasted from here:


````{admonition}  MQTT_comms 
:class: tip, dropdown


```{code-block} python
---

---
""" MQTT_comms.py

# Rationale:
    MQTT Machine-to-Machine Communcication protocol

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    12/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/networking_MQTT.html

# Requirements:
    import adafruit_esp32spi.adafruit_esp32spi_socket as socket
    import adafruit_minimqtt.adafruit_minimqtt as MQTT

# Contains:
    class : MQTT_comms
            which contains: [_init_, start_mqqt_interface,
                            new_client_object, connect,
                            connection_status, publish,
                            subscribe,
                            print_connect, print_subscribe,
                            print_unsubscribe, print_disconnect,
                            print_message, print_publish]

# References:
    https://learn.adafruit.com/mqtt-in-circuitpython/circuitpython-wifi-usage
    
"""

#Packages
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import adafruit_minimqtt.adafruit_minimqtt as MQTT


# Class
class MQTT_comms:
        
    def __init__(self, esp):
        """ initialise class
        
        Input:
            esp = esp connection
        
        Packages:
            import adafruit_esp32spi.adafruit_esp32spi_socket as socket
            import adafruit_minimqtt.adafruit_minimqtt as MQTT
        
        Reference:
            https://learn.adafruit.com/mqtt-in-circuitpython/circuitpython-wifi-usage
        
        """
        # VARIABLES
        # _init_ packages
        import adafruit_esp32spi.adafruit_esp32spi_socket as socket
        import adafruit_minimqtt.adafruit_minimqtt as MQTT
        
        # Internal class variables
        self.broker   = None
        self.port     = None
        self.username = None
        self.password = None
        self.topic    = None
        
        self.payload   = None
        
        # socket and MQTT from module package call
        self.socket   = socket
        self.MQTT     = MQTT
        
        # user defined
        self.esp      = esp
        
        # FUNCTION
        # Start interface
        self.start_mqqt_interface(self.socket, self.esp)   
    
    
    def start_mqqt_interface(self, socket, esp):
        """ FUNC: start MQTT interface
        
        Initialize MQTT interface with the esp interface
        
        # Input:
            socket = web socket
            esp    = esp interface
        
        # Packages
            import adafruit_minimqtt.adafruit_minimqtt as MQTT
        """
        
        # functions            
        self.MQTT.set_socket(socket, esp)  
        
        
    def new_client_object(self, broker_to_use = None, port_to_use = None,
                                username_to_use = None, password_to_use = None,
                                topic_to_use = None):
        """FUNC: Initialize a new MQTT Client object

        Initialize a new MQTT Client object
        
        """     
        
        # functions
        self.mqtt_client = self.MQTT.MQTT(
                                            broker     = broker_to_use,
                                            port       = port_to_use,
                                            username   = username_to_use,
                                            password   = password_to_use,
                                           #socket_pool = socket,
                                        )

        # Connect callback handlers to mqtt_client
        self.mqtt_client.on_connect      = self.print_connect
        self.mqtt_client.on_disconnect   = self.print_disconnect
        self.mqtt_client.on_subscribe    = self.print_subscribe
        self.mqtt_client.on_unsubscribe  = self.print_unsubscribe
        self.mqtt_client.on_publish      = self.print_publish
        self.mqtt_client.on_message      = self.print_message

        self.mqtt_topic = topic_to_use
        
        # Class internal variables
        # set variables
        self.broker   = broker_to_use
        self.port     = port_to_use
        self.username = username_to_use
        self.password = password_to_use
        self.topic    = topic_to_use
    
    
    # FUNCTIONS: Connect
    def connect(self):
        """CONNECT: connect to broker

        """
        print("Attempting to connect to %s" % self.mqtt_client.broker)
        self.mqtt_client.connect()
    
    
    def connection_status(self):
        """CONNECT: check connection status
        
        """
        return self.mqtt_client.is_connected()
    
    
    # FUNCTIONS: Publish
    def publish(self, feed, value, retain=False, qos=0):
        """PUBLISH: publish value to feed
        
        Input:
            retain = whether the msg is saved by the broker
            qos    = 0 Quality of Service level for the topic, defaults to zero.
                    Conventional options are 0 (send at most once), 1 (send at least once), or 2 (send exactly once).
        
        """
        self.mqtt_client.publish(feed,value, retain, qos)
    
    
    # FUNCTIONS: Subscribe
    def subscribe(self, feed, qos=0):
        """SUBSCRIBE: subscribe value to feed
        
        Input:
            qos    = 0 Quality of Service level for the topic, defaults to zero.
                    Conventional options are 0 (send at most once), 1 (send at least once), or 2 (send exactly once).
        
        """
        self.mqtt_client.subscribe(feed, qos)    
    
    
    # FUNCTIONS: Print
    def print_connect(self, mqtt_client, userdata, flags, rc):
        """EVENT: Connect
        This function will be called when the mqtt_client is connected # successfully to the broker.
        
        """
        print("Connected to MQTT Broker!")
        print("Flags: {0}\n RC: {1}".format(flags, rc))


    def print_subscribe(self, client, userdata, topic, granted_qos):
        """EVENT: Subscribe
        This method is called when the client subscribes to a new feed.

        """
        print("Subscribed to {0} with QOS level {1}".format(topic, granted_qos))

    
    def print_unsubscribe(self, client, userdata, topic, pid):
        """EVENT: unsubscribe
        This method is called when the client unsubscribes from a feed.

        """
        print("Unsubscribed from {0} with PID {1}".format(topic, pid))


    def print_disconnect(self, mqtt_client, userdata, rc):
        """EVENT: disconnect
        Disconnected function will be called when the client disconnects.
        
        """
        print("Disconnected from MQTT Broker!")

    
    def print_message(self, client, feed_id, payload):
        """ FUNC: message
        # Message function will be called when a subscribed feed has a new value.
        # The feed_id parameter identifies the feed, and the payload parameter has
        # the new value.
        """
        self.payload   = payload
        print("Feed {0} received new value: {1}".format(feed_id, payload))

    
    def print_publish(self, mqtt_client, userdata, topic, pid):
        """ FUNC: publish
        # This method is called when the mqtt_client publishes data to a feed.
        """
        print("Published to {0} with PID {1}".format(topic, pid))


# Functions
# None


# Dictionaries
# None
```
````
`````

These can then be imported in Circuitpython via the following statement:

```{code-block} python
---

---
from wur_circuitpython_functions import sensor_bme680, sensor_ltr559, sensor_capacitive_soil_moisture, wifi_module, MQTT_comms


```

## Secrets
Next add an blank textfile to the main directory of Circuitpy, copy and paste the following statements making sure to replace the values for each key by values that match your setup:


```{code-block} python
---

---
secrets = {
    'ssid' : 'YOUR_WIFI_NAME',
    'password' : 'WIFI_PASSWORD',

    'mqtt_broker': 'MQTT Broker address (e.g., 192.168.1.108)',
    'mqtt_port': 1883,
    'mqtt_username': 'mosquitto_username',
    'mqtt_password': 'mosquitto_password',
    'mqtt_topic': 'sensors/garden'
}
```

Save the file as `secrets.py`. It can now be imported as:

```{code-block} python
---

---
from secrets import secrets

```

Although it can be prudent to import it as a try statement:

```{code-block} python
---

---

try:
    from secrets import secrets
except ImportError:
    print("To run, add a dictionary containing keys ssid and password keys to dictionary called secrets in a file called secrets.py ")
    raise

```

## Metadata

```{code-block} python
---

---
params = {
    
            }
```

## Code

````{admonition} Click here for the Basic Sensor!
:class: tip, dropdown
```{code-block} python
---

---
""" Combined Air Quality (BME680), Light (LTR559), and moisture sensors with CircuitPython

# Pins:
    # sensor 1: LTR559
    board_I2C_0_SCL    = board.SCL
    board_I2C_0_SDA    = board.SDA

    # sensor 2: BME680
    board_I2C_1_SCL    = board.D11
    board_I2C_1_SDA    = board.D13 

    # sensor 3: Moisture sensor
    board_soil_analog  = board.A0
    board_soil_digital = board.D2

"""
##########
# PACKAGES
import time
import board
from busio import I2C
import adafruit_bme680
from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
from analogio import AnalogIn
from digitalio import DigitalInOut, Direction, Pull

print("Start...sensor")

##########
# PINS 
# board (arduino nano rp2040 connect) specific i2c sda, scl
# sensor 1: LTR-559
board_I2C_0_SCL    = board.SCL
board_I2C_0_SDA    = board.SDA

# sensor 2: BME680
board_I2C_1_SCL    = board.D11
board_I2C_1_SDA    = board.D13

# board (arduino nano rp2040 connect) specific moisture sensor
# set the analog read pin for the moisture sensor
board_soil_analog  = board.A0
# set the digital pin for the moisture sensor
board_soil_digital = board.D2


###########
# VARIABLES 
location_sea_level_pressure  = 1013.25   # location's pressure (hPa) at sea level
temperature_offset_from_true = -5        # sensor needs to be calibrated


########
# BOARD
# Define I2C controllers, for this example both the default board.SCL and board.SDA 
# and the second I2C controller is used
board_I2C_0                 = I2C(board_I2C_0_SCL, board_I2C_0_SDA)  
board_I2C_1                 = I2C(board_I2C_1_SCL, board_I2C_1_SDA)

soil_sensor_analog_in       = AnalogIn(board_soil_analog) 
# setup the moisture sensor power pin and turn it off by default
soil_sensor_power           = DigitalInOut(board_soil_digital)
soil_sensor_power.direction = Direction.OUTPUT
soil_sensor_power.value     = False

#########
# SENSOR OBJECTS
# Create sensor object, communicating over the board's default I2C bus
ltr559 = Pimoroni_LTR559(board_I2C_0, address=0x23, enable_interrupts=True)

# Create sensor object, communicating over the board's default I2C bus
bme680 = adafruit_bme680.Adafruit_BME680_I2C(board_I2C_1, address=0x76, debug=False)
# or use alternate address, by uncommenting:
#bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, address=0x77, debug=False)

# modify output
bme680.sea_level_pressure = location_sea_level_pressure

##########
# Main
# initiate loop and print out sensor variables
while True:
    
    lux       = ltr559.lux 
    proximity = ltr559.prox

    
    
    soil_sensor_power.value = True
    sensor_value  = soil_sensor_analog_in.value
    
    print("")
    print("")
    print("Lux: %0.2f Lux" % (lux))
    print("Proximity: %0.1f %%" % proximity)
    print("Soil :  " + str(sensor_value/100))
    print("Temperature: %0.1f C"  % (bme680.temperature + temperature_offset_from_true))
    print("Gas: %d ohm"             % bme680.gas)
    print("Humidity: %0.1f %%"      % bme680.relative_humidity)
    print("Pressure: %0.3f hPa"     % bme680.pressure)
    print("Altitude = %0.2f meters" % bme680.altitude)
    
    time.sleep(10)


```
````

````{admonition} Click here for the Sensor code!
:class: tip, dropdown

This example includes the code for collecting sensor data using the no additional packages.

```{code-block} python
---

---
""" main.py or code.py
    Combined Air Quality (BME680), Light (LTR559), and moisture sensors
    connected to WiFi and sending data via MQTT using CircuitPython

# Pins:
    # sensor 1: LTR559
    board_I2C_0_SCL    = board.SCL
    board_I2C_0_SDA    = board.SDA

    # sensor 2: BME680
    board_I2C_1_SCL    = board.D11
    board_I2C_1_SDA    = board.D13 

    # sensor 3: Moisture sensor
    board_soil_analog  = board.A0
    board_soil_digital = board.D2



"""
##########
# PACKAGES
# Board packages
import time
import board
import busio
from busio import I2C
from analogio import AnalogIn
from digitalio import DigitalInOut, Direction, Pull

# Sensor packages
from pimoroni_circuitpython_ltr559 import Pimoroni_LTR559
import adafruit_bme680

# Wireless communication packages
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from   adafruit_esp32spi import adafruit_esp32spi
import adafruit_requests as requests

try:
    from secrets import secrets
except ImportError:
    print("To run, add a dictionary containing keys ssid and password keys to dictionary called secrets in a file called secrets.py ")
    raise

print("Start...sensor")



################
# CLASS

class MQTT_comms:
        
    def __init__(self, esp):
        """ MQTT_comms.py

        # Rationale:
        MQTT Machine-to-Machine Communcication protocol
        
        Input:
            esp = esp connection
        
        Packages:
            import adafruit_esp32spi.adafruit_esp32spi_socket as socket
            import adafruit_minimqtt.adafruit_minimqtt as MQTT
        
        Reference:
            https://learn.adafruit.com/mqtt-in-circuitpython/circuitpython-wifi-usage
        
        """
        # VARIABLES
        # Internal class variables
        self.broker   = None
        self.port     = None
        self.username = None
        self.password = None
        self.topic    = None
        
        # socket and MQTT from module package call
        self.socket   = socket
        self.MQTT     = MQTT
        
        # user defined
        self.esp      = esp
        
        # FUNCTION
        # Start interface
        self.start_mqqt_interface(self.socket, self.esp)   
    
    
    def start_mqqt_interface(self, socket, esp):
        """ FUNC: start MQTT interface
        
        Initialize MQTT interface with the esp interface
        
        # Input:
            socket = web socket
            esp    = esp interface
        
        # Packages
            import adafruit_minimqtt.adafruit_minimqtt as MQTT
        """
        
        # functions            
        self.MQTT.set_socket(socket, esp)  
        
        
    def new_client_object(self, broker_to_use = None, port_to_use = None,
                                username_to_use = None, password_to_use = None,
                                topic_to_use = None):
        """FUNC: Initialize a new MQTT Client object

        Initialize a new MQTT Client object
        
        """     
        
        # functions
        self.mqtt_client = self.MQTT.MQTT(
                                            broker     = broker_to_use,
                                            port       = port_to_use,
                                            username   = username_to_use,
                                            password   = password_to_use,
                                           #socket_pool = socket,
                                        )

        # Connect callback handlers to mqtt_client
        self.mqtt_client.on_connect      = self.print_connect
        self.mqtt_client.on_disconnect   = self.print_disconnect
        self.mqtt_client.on_subscribe    = self.print_subscribe
        self.mqtt_client.on_unsubscribe  = self.print_unsubscribe
        self.mqtt_client.on_publish      = self.print_publish
        self.mqtt_client.on_message      = self.print_message

        self.mqtt_topic = topic_to_use
        
        # Class internal variables
        # set variables
        self.broker   = broker_to_use
        self.port     = port_to_use
        self.username = username_to_use
        self.password = password_to_use
        self.topic    = topic_to_use
    
    
    # FUNCTIONS: Connect
    def connect(self):
        """CONNECT: connect to broker

        """
        print("Attempting to connect to %s" % self.mqtt_client.broker)
        self.mqtt_client.connect()
    
    
    def connection_status(self):
        """CONNECT: check connection status
        
        """
        return self.mqtt_client.is_connected()
    
    
    # FUNCTIONS: Publish
    def publish(self, feed, value):
        """PUBLISH: publish value to feed

        """
        self.mqtt_client.publish(feed,value)
    
    
    # FUNCTIONS: Print
    def print_connect(self, mqtt_client, userdata, flags, rc):
        """EVENT: Connect
        This function will be called when the mqtt_client is connected # successfully to the broker.
        
        """
        print("Connected to MQTT Broker!")
        print("Flags: {0}\n RC: {1}".format(flags, rc))


    def print_subscribe(self, client, userdata, topic, granted_qos):
        """EVENT: Subscribe
        This method is called when the client subscribes to a new feed.

        """
        print("Subscribed to {0} with QOS level {1}".format(topic, granted_qos))

    
    def print_unsubscribe(self, client, userdata, topic, pid):
        """EVENT: unsubscribe
        This method is called when the client unsubscribes from a feed.

        """
        print("Unsubscribed from {0} with PID {1}".format(topic, pid))


    def print_disconnect(self, mqtt_client, userdata, rc):
        """EVENT: disconnect
        Disconnected function will be called when the client disconnects.
        
        """
        print("Disconnected from MQTT Broker!")

    
    def print_message(self, client, feed_id, payload):
        """ FUNC: message
        # Message function will be called when a subscribed feed has a new value.
        # The feed_id parameter identifies the feed, and the payload parameter has
        # the new value.
        """
        print("Feed {0} received new value: {1}".format(feed_id, payload))

    
    def print_publish(self, mqtt_client, userdata, topic, pid):
        """ FUNC: publish
        # This method is called when the mqtt_client publishes data to a feed.
        """
        print("Published to {0} with PID {1}".format(topic, pid))



##########
# PINS 
# board (arduino nano rp2040 connect) specific i2c sda, scl
# sensor 1: LTR-559
board_I2C_0_SCL    = board.SCL
board_I2C_0_SDA    = board.SDA

# sensor 2: BME680
board_I2C_1_SCL    = board.D11
board_I2C_1_SDA    = board.D13

# board (arduino nano rp2040 connect) specific moisture sensor
# set the analog read pin for the moisture sensor
board_soil_analog  = board.A0
# set the digital pin for the moisture sensor
board_soil_digital = board.D2

# WiFi
#  ESP32 pins NOTE: board specific pins
esp32_cs           = DigitalInOut(board.CS1)
esp32_ready        = DigitalInOut(board.ESP_BUSY)
esp32_reset        = DigitalInOut(board.ESP_RESET)

#  SPI pins NOTE: uses the secondary SPI connected through the ESP32
spi               = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp               = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)


###########
# WIFI Connect
requests.set_socket(socket, esp)

if esp.status == adafruit_esp32spi.WL_IDLE_STATUS:
    print("ESP32 found and in idle mode")

# PRINT firmware and mac address of board
print("Firmware vers.", esp.firmware_version)
print("MAC addr:", [hex(i) for i in esp.MAC_address])

print("Connecting to AP...")

while not esp.is_connected:
    
    try:
        esp.connect_AP(secrets["ssid"], secrets["password"])
    
    except RuntimeError as e:
        print("could not connect to AP, retrying: ", e)
        continue

print("Connected to", str(esp.ssid, "utf-8"), "\tRSSI:", esp.rssi)
print("My IP address is", esp.pretty_ip(esp.ip_address))


###########
# MQTT
mqtt_comms = MQTT_comms(esp)
mqtt_comms.start_mqqt_interface(socket, esp)
mqtt_comms.new_client_object(secrets["mqtt_broker"], secrets["mqtt_port"], secrets["mqtt_username"], secrets["mqtt_password"], secrets['mqtt_topic'])
mqtt_comms.connect()


###########
# VARIABLES 
location_sea_level_pressure  = 1013.25   # location's pressure (hPa) at sea level
temperature_offset_from_true = -5        # sensor needs to be calibrated


########
# BOARD
# Define I2C controllers, for this example both the default board.SCL and board.SDA 
# and the second I2C controller is used
board_I2C_0                 = I2C(board_I2C_0_SCL, board_I2C_0_SDA)  
board_I2C_1                 = I2C(board_I2C_1_SCL, board_I2C_1_SDA)

soil_sensor_analog_in       = AnalogIn(board_soil_analog) 
# setup the moisture sensor power pin and turn it off by default
soil_sensor_power           = DigitalInOut(board_soil_digital)
soil_sensor_power.direction = Direction.OUTPUT
soil_sensor_power.value     = False

#########
# SENSOR OBJECTS
# Create sensor object, communicating over the board's default I2C bus
#ltr559 = Pimoroni_LTR559(board_I2C_0, address=0x23, enable_interrupts=True)
ltr559 = Pimoroni_LTR559(board_I2C_1, address=0x23, enable_interrupts=False)

# Create sensor object, communicating over the board's default I2C bus
#bme680 = adafruit_bme680.Adafruit_BME680_I2C(board_I2C_1, address=0x76, debug=False)
bme680 = adafruit_bme680.Adafruit_BME680_I2C(board_I2C_0, address=0x76, debug=False)
# or use alternate address, by uncommenting:
#bme680 = adafruit_bme680.Adafruit_BME680_I2C(i2c, address=0x77, debug=False)


# modify output
bme680.sea_level_pressure = location_sea_level_pressure

ltr559.update_sensor()
lux = ltr559.get_lux()


##########
# Main
# initiate loop and print out sensor variables
while True:
    
    # CHECK NETWORKING
    # Check for wifi connection
    while esp.is_connected == False:
        try:
            esp.connect_AP(secrets["ssid"], secrets["password"])
        except:
            print('retrying')
            continue
    
    # Check MQTT client connection
    if mqtt_comms.connection_status() == False:
        mqtt_comms.connect()

    # SENSORS
    # Update ltr-559 sensor
    ltr559.update_sensor()
    
    #lux       = ltr559.lux
    lux_value               = ltr559.get_lux()
    
    #proximity = ltr559.prox
    proximity_value         = ltr559.get_proximity()
    
    # Turn on soil sensor
    soil_sensor_power.value = True
    soil_sensor_value       = soil_sensor_analog_in.value
    corrected_soil_sensor_  = soil_sensor_value/100
    
    
    # Get sensor measurements
    measured_temperature    = bme680.temperature 
    corrected_temperature   = bme680.temperature + temperature_offset_from_true
    gas_value               = bme680.gas
    pressure_value          = bme680.pressure
    altitude_value          = bme680.altitude
    rel_humidity_value      = bme680.relative_humidity
    
    # PRINT
    print("")
    print("")
    
    print("Lux: %0.2f Lux" % (lux_value))
    print("Proximity: %0.1f %%" % proximity_value)
    print("Soil :  " + str(corrected_soil_sensor_))
    print("Temperature: %0.1f C"  % (corrected_temperature))
    print("Gas: %d ohm"             % gas_value)
    print("Humidity: %0.1f %%"      % rel_humidity_value)
    print("Pressure: %0.3f hPa"     % pressure_value)
    print("Altitude = %0.2f meters" % altitude_value)
    
    
    
    message_header     = "arduino,location=Brett_Office,arduino=RP2040,iot=iot_1,sensor=BME680-LTR559-Soil"
    message_payload    = "corrected_temperature=" +str(corrected_temperature) + ",measured_temperature="+str(measured_temperature)+",pressure_value="+str(pressure_value)+",altitude_value="+str(altitude_value)+",rel_humidity_value="+str(rel_humidity_value)+",gas_value="+str(gas_value) + ",soil_sensor=" + str(corrected_soil_sensor_) + ",lux=" + str(lux_value) 
    message_whitespace = " "
    
    # Submit measurements
    mqtt_comms.publish(secrets["mqtt_topic"], message_header+message_whitespace+message_payload)

    time.sleep(5)
    
````



````{admonition} Click here for the Sensor code using wur_circuitpython_functions!
:class: tip, dropdown

This example includes functions for collecting sensor data via the wur_circuitpython_functions modules.

```{code-block} python
---

---
########################
##      PACKAGES      ##
########################
import time
import board
import busio
from busio import I2C
import microcontroller
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import adafruit_requests as requests

# download wur_circuitpython_functions from https://gitlab.com/wur/digitaltwin/-/tree/main/code/lib/wur_circuitpython_functions
# remember to copy the full /lib https://gitlab.com/wur/digitaltwin/-/tree/main/code/lib as it contains the addtional Adafruit
# Circuitpython packages 
from wur_circuitpython_functions import sensor_bme680, sensor_ltr559, sensor_capacitive_soil_moisture


########################
##   USER specified   ##
########################

# Specificiations for the board

# i2c pins
sensor_0_scl = board.SCL
sensor_0_sda = board.SDA
sensor_1_scl = board.D11
sensor_1_sda = board.D13

# digital/analog pins
sensor_2_power  = board.D2
sensor_2_analog = board.A0

# variables
location_sea_level_pressure  = 1013.25   # for BME680 location's pressure (hPa) at sea level, default is 1013.25
temperature_offset_from_true = -5        # for BME680 temperature sensor needs to be calibrated, default is -5
soil_sensor_modifier         = 100       # for the Capacitive Soil Moisture Sensor, default is 100

# Time interval of measurements in seconds
measurement_time_interval = 10


########################
##      SENSORS       ##
########################

# start up all sensor objects
sensor_0 = sensor_bme680.sensor_bme680()
sensor_0.start_sensor(SCL_pin = sensor_0_scl, SDA_pin = sensor_0_sda)

sensor_1 = sensor_ltr559.sensor_ltr559()
sensor_1.start_sensor(SCL_pin = sensor_1_scl, SDA_pin = sensor_1_sda)

sensor_2 = sensor_capacitive_soil_moisture.soil_sensor()
sensor_2.start_sensor(power_pin = sensor_2_power, analog_pin = sensor_2_analog)


########################
##      MAIN          ##
########################

while True:
        
    # step 1. Get sensor measurements
    gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature = sensor_0.measure_all()
    lux_value, prox_value                    = sensor_1.measure_all()
    soil_sensor_value, soil_sensor_value_mod = sensor_2.measure_all(modifier = soil_sensor_modifier)
        
        
    # step 2. Print sensor measurements
    sensor_0.print_sensor_values()
    sensor_1.print_sensor_values()
    sensor_2.print_sensor_values()
    
    time.sleep(measurement_time_interval)

```
````


````{admonition} Click here for the Sensor + WiFi + MQTT code!
:class: tip, dropdown

This example includes functions for collecting sensor data, connecting to WiFi, and sending the data via MQTT

```{code-block} python
---

---
""" main.py or code.py
    # Purpose:
        Combined Air Quality (BME680), Light (LTR559),
        and moisture sensors connected to WiFi and 
        sending data via MQTT using CircuitPython.

    # Pins:
        # sensor 1: LTR559
        board_I2C_0_SCL    = board.SCL
        board_I2C_0_SDA    = board.SDA

        # sensor 2: BME680
        board_I2C_1_SCL    = board.D11
        board_I2C_1_SDA    = board.D13 

        # sensor 3: Moisture sensor
        board_soil_analog  = board.A0
        board_soil_digital = board.D2

"""
########################
##      PACKAGES      ##
########################
import time
import board
import busio
from busio import I2C
import microcontroller
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import adafruit_requests as requests

# download wur_circuitpython_functions from https://gitlab.com/wur/digitaltwin/-/tree/main/code/lib/wur_circuitpython_functions
# remember to copy the full /lib https://gitlab.com/wur/digitaltwin/-/tree/main/code/lib as it contains the addtional Adafruit
# Circuitpython packages 
from wur_circuitpython_functions import sensor_bme680, sensor_ltr559, sensor_capacitive_soil_moisture, wifi_module, MQTT_comms

try:
    from secrets import secrets
except ImportError:
    print("To run, add a dictionary containing keys ssid and password keys to dictionary called secrets in a file called secrets.py ")
    raise


########################
##   USER specified   ##
########################

# Specificiations for the board

# i2c pins
sensor_0_scl = board.SCL
sensor_0_sda = board.SDA
sensor_1_scl = board.D11
sensor_1_sda = board.D13

# digital/analog pins
sensor_2_power  = board.D2
sensor_2_analog = board.A0

# wifi pins
sensor_wifi_esp32_cs    = board.CS1
sensor_wifi_esp32_ready = board.ESP_BUSY
sensor_wifi_esp32_reset = board.ESP_RESET
sensor_wifi_SCK         = board.SCK1
sensor_wifi_SDO         = board.MOSI1
sensor_wifi_SDI         = board.MISO1

# variables
location_sea_level_pressure  = 1013.25   # for BME680 location's pressure (hPa) at sea level, default is 1013.25
temperature_offset_from_true = -5        # for BME680 temperature sensor needs to be calibrated, default is -5
soil_sensor_modifier         = 100       # for the Capacitive Soil Moisture Sensor, default is 100

# number of times to retry mqtt message sending
retry_count  = 10

# Time interval of measurements in seconds
measurement_time_interval = 10


########################
##   MESSAGE FORMAT   ##
########################

# mqtt payload with format: measurementName,tagKey=tagValue fieldKey="fieldValue"
iot_measurement_name  = "arduino"           # measurementName
iot_measurement_tags  = {     
        
                        "location" : "Brett_Office",                  # tagKey=tagValue  location tag, location of measurements, e.g., Brett_Office
                        "board"    : "arduino_nano_rp2040_connect",   # tagKey=tagValue  board tag, microcontroller,             e.g., arduino_nano_rp2040_connect
                        "iot_UID"  : "BM_000001",                     # tagKey=tagValue  iot_ID tag, IoT UID unique identifier,  e.g., BM_000001
                        "sensor"   : "bme680_ltr559_soil",            # tagKey=tagValue  sensor tag, the sensor tags,            e.g., bme680_ltr559_soil
    
                        }

iot_measurement_delimiters = {
    
                              'whitespace' : " ",
                              'comma'      : ",",
                              'equals'     : "=",

                              }


iot_measurement_delimiters['whitespace']
iot_measurement_delimiters['comma']
iot_measurement_delimiters['equals']



iot_tag_string = [iot_measurement_delimiters['comma'] + key + iot_measurement_delimiters['equals'] + iot_measurement_tags[key] for key in iot_measurement_tags]
iot_tag_string = ''.join(iot_tag_string)   # joins list together i.e., ',location=Brett_Office,iot_UID=BM_000001,sensor=bme680_ltr559_soil,board=arduino_nano_rp2040_connect'

# mqtt payload header:  "measurementName,tagKey=tagValue "
iot_header     = iot_measurement_name + iot_tag_string + iot_measurement_delimiters['whitespace']


########################
##      SENSORS       ##
########################

# start up all sensor objects
sensor_0 = sensor_bme680.sensor_bme680()
sensor_0.start_sensor(SCL_pin = sensor_0_scl, SDA_pin = sensor_0_sda)

sensor_1 = sensor_ltr559.sensor_ltr559()
sensor_1.start_sensor(SCL_pin = sensor_1_scl, SDA_pin = sensor_1_sda)

sensor_2 = sensor_capacitive_soil_moisture.soil_sensor()
sensor_2.start_sensor(power_pin = sensor_2_power, analog_pin = sensor_2_analog)


########################
##      WiFi          ##
########################

# connect to SSID using secrets["ssid"] and secrets["password"]
get_connected = wifi_module.connect(pin_esp32_cs = sensor_wifi_esp32_cs,
                                    pin_esp32_ready = sensor_wifi_esp32_ready, pin_esp32_reset = sensor_wifi_esp32_reset,
                                    pin_SCK = sensor_wifi_SCK, pin_SDO = sensor_wifi_SDO, pin_SDI = sensor_wifi_SDI)

get_connected.connect_to_network(secrets["ssid"], secrets["password"])

while not get_connected.get_connection_status():
    
    try:
        
        get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
    except RuntimeError as e:
        
        print("could not connect to AP, retrying: ", e)
        continue
    
    
########################
##      MQTT          ##
########################

# Set up and connect to MQTT
sensor_mqtt_comms = MQTT_comms.MQTT_comms(get_connected.esp)
sensor_mqtt_comms.start_mqqt_interface(socket, get_connected.esp)
sensor_mqtt_comms.new_client_object(secrets["mqtt_broker"], secrets["mqtt_port"], secrets["mqtt_username"], secrets["mqtt_password"], secrets['mqtt_topic'])

#sensor_mqtt_comms.connect()  # do try statement
count_MQTT = 0
check      = False
while not check:
    try:
        sensor_mqtt_comms.connect()
        check = True
        
    except RuntimeError as e:
        count_MQTT += 1
        
        print("CONNECTION - Could not connect to target AP, retrying: ", e)
        
        if count_MQTT == retry_count:
            break
        
        continue



########################
##      MAIN          ##
########################

# Step 0. Subscribe to topic
sensor_mqtt_comms.subscribe(secrets['mqtt_topic'])

while True:
    
    # Step 1. Check WiFi connection
    while get_connected.get_connection_status() == False:
        count_AP = 0
        
        try:
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
        
        except:
            
            count_AP +=1
            print('retrying')
            
            if count_AP == retry_count:
                
                break
            
            continue
    
    
    # Step 2. Check MQTT client connection
    if sensor_mqtt_comms.connection_status() == False:
        
        sensor_mqtt_comms.connect()
            
        
    # step 3. Get sensor measurements
    gas_value, rel_humidity_value, altitude_value, pressure_value, measured_temperature, corrected_temperature = sensor_0.measure_all()
    lux_value, prox_value                    = sensor_1.measure_all()
    soil_sensor_value, soil_sensor_value_mod = sensor_2.measure_all(modifier = soil_sensor_modifier)
        
        
    # step 4. Print sensor measurements
    sensor_0.print_sensor_values()
    sensor_1.print_sensor_values()
    sensor_2.print_sensor_values()
        
        
    # step 5. Make message payload
    # Payload version: fieldKey="fieldValue" 
    message_payload      = "corrected_temperature=" +str(corrected_temperature) + ",measured_temperature="+str(measured_temperature)+",pressure_value="+str(pressure_value)+",altitude_value="+str(altitude_value)+",rel_humidity_value="+str(rel_humidity_value)+",gas_value="+str(gas_value) + ",soil_sensor=" + str(soil_sensor_value_mod) + ",lux=" + str(lux_value) 
        
    # Payload version: fieldKey="fieldValue" time
    # time_now = time.time()
    # message_payload_time =  str(time_now)  + '000'
    
        
    # step 6. Submit measurements:
    sensor_mqtt_comms.publish(secrets["mqtt_topic"], iot_header + message_payload)
    
    
    # step 7. Get subscriptions/ Poll the message queue
    print("Subscription:")
    rc = sensor_mqtt_comms.mqtt_client.loop(60)
    print("...")
    
    if sensor_mqtt_comms.payload != None:
        
        print("Message: " + sensor_mqtt_comms.payload)
        
        # Reset payload
        sensor_mqtt_comms.payload = None
    
    time.sleep(measurement_time_interval)



```
````
<br><br>

## IoT Sensor: Water Pump

## Layout
```{figure} /images/Fritz_pump.jpg
---
name: Fritz_pump_ASSEMBLE-fig
alt: Pump wiring layout
---
*Set-up of the water pumping. Schematic and Fritzing diagram showing how to set-up the electronic components.*
```

## Packages

The CircuitPython code outlined here requires the following packages: 

```{code-block} python
---

---
import time
import digitalio
import board

# packages for wifi and mqtt
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from   adafruit_esp32spi import adafruit_esp32spi
import adafruit_requests as requests

```

with `adafruit_minimqtt`, `adafruit_esp32spi`, and `adafruit_requests` needing to be added by the user to the ` /lib` folder of the CircuitPy directory. 

The following classes `wifi_module` and  `MQTT_comms` can be run as modules in a package by adding a new folder to the ` /lib` folder and calling it `wur_circuitpython_functions`, create an empty text file in this folder and save it as `__init__.py`. Next copy and paste each of the following sets of code into individual empty text files and rename them as `wifi_module.py`, and  `MQTT_comms.py` respectively. 


`````{admonition} Click here for Package:  WiFi Module!
:class: tip, dropdown

````{admonition} Package:  WiFi Module!
:class: tip, dropdown

```{code-block} python
---

---
""" wifi_module.py

# Rationale:
    Networking via wifi

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    12/08/2022 

# Licence:
    
    
# URL:
    

# Requirements:
    #Packages
    import board
    import busio
    from digitalio import DigitalInOut
        
    # need to add following libraries to lib. folder:
    import adafruit_requests as requests
    import adafruit_esp32spi.adafruit_esp32spi_socket as socket
    from   adafruit_esp32spi import adafruit_esp32spi

# Contains:
    class : wifi_connect
            which contains: [_init_, get_status, check_status,
                            get_connection_status, get_mac_address,
                            print_mac_address, get_firmware_vers,
                            print_firmware_vers, scan_networks,
                            scan_networks_ALL_INFO, print_ssid,
                            print_ip, connect_to_network, check_connection]
    

# References:
    https://docs.circuitpython.org/projects/esp32spi/en/latest/index.html
    
# Examples:
    ## Example Arduino nano Rp2040 Connect

    # packages
    from wur_circuitpython_functions import wifi_module
    import board

    try:
        from secrets import secrets
    except:
        raise ValueError(' Requires a secrets dictionary containing ssid and password key-value pairs ')
    
    get_connected = wifi_module.connect(pin_esp32_cs       = board.CS1,
                                        pin_esp32_ready    = board.ESP_BUSY,
                                        pin_esp32_reset    = board.ESP_RESET,
                                        pin_SCK            = board.SCK1,
                                        pin_SDO            = board.MOSI1,
                                        pin_SDI            = board.MISO1)
    ap            = get_connected.scan_networks()
    get_connected.connect_to_network(secrets["ssid"], secrets["password"])


    ## Example Raspberry Pico  with ESP32, e.g., Pimoroni WiFi Pack 
        https://shop.pimoroni.com/products/pico-wireless-pack?variant=32369508581459
        
        # packages
        from wur_circuitpython_functions import wifi_module
        import board
        
        try:
            from secrets import secrets
        except:
            raise ValueError(' Requires a secrets dictionary containing ssid and password key-value pairs ')
        
        get_connected = wifi_module.connect(pin_esp32_cs       = board.GP13,
                                            pin_esp32_ready    = board.GP14,
                                            pin_esp32_reset    = board.GP15,
                                            pin_SCK            = board.GP10,
                                            pin_SDO            = board.GP11,
                                            pin_SDI            = board.GP12)
        ap            = get_connected.scan_networks()
        get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
"""
    
#Packages
import board
import busio
from digitalio import DigitalInOut
    
# need to add following libraries to lib. folder:
import adafruit_requests as requests
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from   adafruit_esp32spi import adafruit_esp32spi


# Class
class connect:
    """ CLASS: Wireless networking
    
    Rationale:
        Networking via wifi
        
    REFERENCES:
        https://docs.circuitpython.org/projects/esp32spi/en/latest/index.html
    
    EXAMPLE:
    
    # packages
    from helper_functions import wifi_connect
    
    # set up wifi
    get_connected = connect()
    ap            = get_connected.scan_networks()
    get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
    """

    
    
    def __init__(self, 
                 pin_esp32_cs       = None,
                 pin_esp32_ready    = None,
                 pin_esp32_reset    = None,
                 pin_SCK            = None,
                 pin_SDO            = None,
                 pin_SDI            = None,
                 
                 ):
        """ Initialise class
        
        Input:
            pin_esp32_cs       = CS pin, default = board.CS1,
            pin_esp32_ready    = Ready pin, default = board.ESP_BUSY,
            pin_esp32_reset    = Reset pin, default = board.ESP_RESET,
            pin_SCK            = SCK pin, default = board.SCK1,
            pin_SDO            = SDO pin, default = board.MOSI1,
            pin_SDI            = SDI pin, default = board.MISO1,
        
            
            Note 1: for Arduino Nano Connect RP2040
            connect(
                pin_esp32_cs = board.CS1,
                 pin_esp32_ready    = board.ESP_BUSY,
                 pin_esp32_reset    = board.ESP_RESET,
                 pin_SCK            = board.SCK1,
                 pin_SDO            = board.MOSI1,
                 pin_SDI            = board.MISO1
                 )
            
            Note 2: for rapsberry pi pico with wifi shield the pins are: 
        
                connect(

                        pin_esp32_cs       = board.GP13,
                        pin_esp32_ready    = board.GP14,
                        pin_esp32_reset    = board.GP15,
                        pin_SCK            = board.GP10,
                        pin_SDO            = board.GP11,
                        pin_SDI            = board.GP12)
                
                )
        
        Packages:
            import board
            import busio
            from digitalio import DigitalInOut
                
            # need to add following libraries to lib. folder:
            import adafruit_requests as requests
            import adafruit_esp32spi.adafruit_esp32spi_socket as socket
            from   adafruit_esp32spi import adafruit_esp32spi
            
        """

        # init packages
        import adafruit_requests as requests
        import adafruit_esp32spi.adafruit_esp32spi_socket as socket

        # VARIABLES
        # Internal variables
        
        # Dictionaries
        # used for scan networks give all info (for function
        # scan_networks_ALL_INFO)
        # Authorisation mode dictionaries giving key and value 
        self.authmode_dict = {0: 'open',
                              1: 'WEP',
                              2: 'WPA-PSK',
                              3: 'WPA2-PSK',
                              4: 'WPA/WPA2-PSK'}
        
        # Hidden mode dictionaries giving key and value 
        self.hidden_dict   = {0: 'visible',
                              1: 'hidden'}
        
        
        # ESP32 pins
        self.esp32_cs      = DigitalInOut(pin_esp32_cs)
        self.esp32_ready   = DigitalInOut(pin_esp32_ready)
        self.esp32_reset   = DigitalInOut(pin_esp32_reset)

        # uses the secondary SPI connected through the ESP32
        self.spi = busio.SPI(pin_SCK, pin_SDO, pin_SDI)

        self.esp = adafruit_esp32spi.ESP_SPIcontrol(self.spi, self.esp32_cs, self.esp32_ready, self.esp32_reset)
        
        # as per the docs: "requests library the type of socket we're using (socket type varies by connectivity
        # type - we'll be using the adafruit_esp32spi_socket for this example). We'll also
        # set the interface to an esp object. This is a little bit of a hack, but it lets
        # us use requests like CPython does."
        # socket is called via the package: import adafruit_esp32spi.adafruit_esp32spi_socket as socket
        self.requests = requests
        self.requests.set_socket(socket, self.esp)
        
        # Print board status, firmware version, and mac address
        self.check_status()
        self.print_firmware_vers()
        self.print_mac_address()
        
        
    def get_status(self):
        """ RETURN: ESP board status
        
        Input:
            None
            
        Output:
            status_of_ = binary value of status
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        status_of_ =  self.esp.status
        
        return status_of_
            
      
    def check_status(self):
        """PRINT: Compare status vs idle mode
        
        Input:
            None
            
        Output:
            print 'ESP32 found and in idle mode' if get_status == 0
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        if self.get_status() == adafruit_esp32spi.WL_IDLE_STATUS:
            
            print("ESP32 found and in idle mode")
        
    
    def get_connection_status(self):
        """ RETURN: Connection status
        
        Input:
            None
            
        Output:
            connection_status = returns True or False if connected
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
            
        """
        # function
        connection_status = self.esp.is_connected
        
        #print(connection_status)
        
        return connection_status
        
        
    def get_mac_address(self):
        """ RETURN: Mac Address
        
        Input:
            None

        Output:
            mac_address = MAC address of board
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """ 
        # function
        mac_address = self.esp.MAC_address
        
        return mac_address
            
    
    def print_mac_address(self):
        """PRINT: MAC address
        
        Input:
            None
            
        Output:
            print mac address
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        print("MAC addr:", [hex(i) for i in self.get_mac_address()])
        
      
    def get_firmware_vers(self):
        """ RETURN: Firmware version
        
        Input:
            None
        
        Output:
            firmware = board firmware version
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        firmware = self.esp.firmware_version
        return firmware
        
    
    def print_firmware_vers(self):
        """ PRINT: Firmware version
        
        Input:
            None
            
        Output:
            print firmware version
            
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        print("Firmware vers.", self.get_firmware_vers())
        
    
    def scan_networks(self):
        """ FUNC: Scan for network Access Points
        
        Input:
            None
        
        Output:
            scan_result = Returns a list of dictionaries with ‘ssid’, ‘rssi’ and
                            ‘encryption’ entries, one for each AP found
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        # inform user
        print('scanning for access points')
        
        # scan for access points (ap)        
        # returns values as: ssid, bssid, channel, RSSI, authmode, hidden
        scan_result = self.esp.scan_networks()
                
        # print
        for ap in scan_result:
            #print("\t%s\t\tRSSI: %d" % (str(ap['ssid'], 'utf-8'), ap['rssi']))
            print(ap)
                
        return scan_result
    
    
    def scan_networks_ALL_INFO(self):
        """ FUNC: Scan for network Access Points give all info
        
        Input:
            None
        
        Output:
            scan_result = Returns a list of dictionaries with ‘ssid’, ‘rssi’ and
                            ‘encryption’ entries, one for each AP found
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        """
        # function
        # inform user
        print('scanning for access points')
        
        # scan for access points (ap)        
        # returns values as: ssid, bssid, channel, RSSI, authmode, hidden
        scan_result = self.esp.scan_networks()
                
        # print
        for ap in scan_result:
            
                       
            hidden_value = self.hidden_dict[ap['hidden']]
            autho_value  = self.authmode_dict[ap['authmode']]
            
            print("\tSSID: %s \n\t\tRSSI: %d \n\t\t Channel: %s\t\t Hidden: %s\t\t Authentication: %s" % (str(ap['ssid'], 'utf-8'), ap['rssi'],str(channel),hidden_value,autho_value))
            
           
        return scan_result
    
    
    def print_ssid(self):
        """ PRINT: SSID
        
        Input:
            None

        Output:
            ssid = returns ssid 
            (also prints ssid)

        Package:
            from   adafruit_esp32spi import adafruit_esp32spi

        Example:
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
            get_connected.print_ssid()
        
        """
        # function
        ssid = str(self.esp.ssid, "utf-8")
        print(ssid)
        return ssid
    

    def print_ip(self):
        """ PRINT: IP address
        
        Input:
            None

        Output:
            pretty_ip_address = IP address
            (also prints IP address)

        Package:
            from   adafruit_esp32spi import adafruit_esp32spi

        Example:
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
            get_connected.print_ip()
        
        """
        # function
        pretty_ip_address = str(self.esp.pretty_ip(self.esp.ip_address)) 
        print(pretty_ip_address)

        return pretty_ip_address

    
    def connect_to_network(self, ssid_to_connect, ssid_password):
        """ CONNECT: Connect device to network
        
        Input:
            ssid_to_connect = network ID
            ssid_password   = network password
            
        Output:
            None (connection to network)
        
        Package:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        Example:
            get_connected = wifi_connect()
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
        
        """
        # function
        print("Connecting to AP...")
        
        # check if connected
        while not self.get_connection_status():
            
            try:
                self.esp.connect_AP(ssid_to_connect, ssid_password)
                
            except RuntimeError as e:
                print("could not connect to AP, retrying: ", e)
                continue
        
        print("Connected to", str(self.esp.ssid, "utf-8"), "\tRSSI:", self.esp.rssi)
        print("My IP address is", self.esp.pretty_ip(self.esp.ip_address))
    
    
    def check_connection(self,
                        PING_test = "google.com",
                        HOST_test = "adafruit.com",
                        TEXT_URL  = "http://wifitest.adafruit.com/testwifi/index.html",
                        JSON_URL  = "http://api.coindesk.com/v1/bpi/currentprice/USD.json"
                         ):
        """ FUNC: check wifi connection
        
        Input:
            PING_test = destination for test checking timing (returns ms timing)
            HOST_test = destination for test checking ip lookup
            TEXT_URL  = destination for test checking fetching text
            JSON_URL  = destination for test checking fetching json
            
        Output:
            prints values
        
        Packages:
            from   adafruit_esp32spi import adafruit_esp32spi
        
        Reference:
            https://learn.adafruit.com/pyportal-oblique-strategies/internet-connect
        
        """
        # functions
        print("Checking internet connection...")
        
        # IP look up test
        print("IP lookup Test:")
        print("IP lookup adafruit.com: %s" % self.esp.pretty_ip(self.esp.get_host_by_name(HOST_test)))
        
        # PING test, returns a millisecond timing value to a destination IP address or hostname
        print("Ping Test:")
        print("Ping google.com: %d ms" % self.esp.ping(PING_test))
        
        # TEXT retrieval test
        print("Text retrieval Test:")
        # esp._debug = True
        print("Fetching text from", TEXT_URL)
        r = self.requests.get(TEXT_URL)
        print("-" * 40)
        print(r.text)
        print("-" * 40)
        r.close()

        # JSON retrieval test
        print("JSON retrieval Test:")
        print()
        print("Fetching json from", JSON_URL)
        r = self.requests.get(JSON_URL)
        print("-" * 40)
        print(r.json())
        print("-" * 40)
        r.close()

        print("Done!")


# Functions
# None


# Dictionaries
# None


```
````
`````

`````{admonition} Click here for Package: MQTT communication!
:class: tip, dropdown

````{admonition} Package: MQTT communication!
:class: tip, dropdown

```{code-block} python
---

---
""" MQTT_comms.py

# Rationale:
    MQTT Machine-to-Machine Communcication protocol

# Authors:
    WUR Digital Twin Platform Methodology

# Date
    12/08/2022 

# Licence:
    
    
# URL:
    https://wur.gitlab.io/digitaltwin/docs/interactive_twin/networking_MQTT.html

# Requirements:
    import adafruit_esp32spi.adafruit_esp32spi_socket as socket
    import adafruit_minimqtt.adafruit_minimqtt as MQTT

# Contains:
    class : MQTT_comms
            which contains: [_init_, start_mqqt_interface,
                            new_client_object, connect,
                            connection_status, publish,
                            subscribe,
                            print_connect, print_subscribe,
                            print_unsubscribe, print_disconnect,
                            print_message, print_publish]

# References:
    https://learn.adafruit.com/mqtt-in-circuitpython/circuitpython-wifi-usage
    
"""

#Packages
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import adafruit_minimqtt.adafruit_minimqtt as MQTT


# Class
class MQTT_comms:
        
    def __init__(self, esp):
        """ initialise class
        
        Input:
            esp = esp connection
        
        Packages:
            import adafruit_esp32spi.adafruit_esp32spi_socket as socket
            import adafruit_minimqtt.adafruit_minimqtt as MQTT
        
        Reference:
            https://learn.adafruit.com/mqtt-in-circuitpython/circuitpython-wifi-usage
        
        """
        # VARIABLES
        # _init_ packages
        import adafruit_esp32spi.adafruit_esp32spi_socket as socket
        import adafruit_minimqtt.adafruit_minimqtt as MQTT
        
        # Internal class variables
        self.broker   = None
        self.port     = None
        self.username = None
        self.password = None
        self.topic    = None
        
        self.payload   = None
        
        # socket and MQTT from module package call
        self.socket   = socket
        self.MQTT     = MQTT
        
        # user defined
        self.esp      = esp
        
        # FUNCTION
        # Start interface
        self.start_mqqt_interface(self.socket, self.esp)   
    
    
    def start_mqqt_interface(self, socket, esp):
        """ FUNC: start MQTT interface
        
        Initialize MQTT interface with the esp interface
        
        # Input:
            socket = web socket
            esp    = esp interface
        
        # Packages
            import adafruit_minimqtt.adafruit_minimqtt as MQTT
        """
        
        # functions            
        self.MQTT.set_socket(socket, esp)  
        
        
    def new_client_object(self, broker_to_use = None, port_to_use = None,
                                username_to_use = None, password_to_use = None,
                                topic_to_use = None):
        """FUNC: Initialize a new MQTT Client object

        Initialize a new MQTT Client object
        
        """     
        
        # functions
        self.mqtt_client = self.MQTT.MQTT(
                                            broker     = broker_to_use,
                                            port       = port_to_use,
                                            username   = username_to_use,
                                            password   = password_to_use,
                                           #socket_pool = socket,
                                        )

        # Connect callback handlers to mqtt_client
        self.mqtt_client.on_connect      = self.print_connect
        self.mqtt_client.on_disconnect   = self.print_disconnect
        self.mqtt_client.on_subscribe    = self.print_subscribe
        self.mqtt_client.on_unsubscribe  = self.print_unsubscribe
        self.mqtt_client.on_publish      = self.print_publish
        self.mqtt_client.on_message      = self.print_message

        self.mqtt_topic = topic_to_use
        
        # Class internal variables
        # set variables
        self.broker   = broker_to_use
        self.port     = port_to_use
        self.username = username_to_use
        self.password = password_to_use
        self.topic    = topic_to_use
    
    
    # FUNCTIONS: Connect
    def connect(self):
        """CONNECT: connect to broker

        """
        print("Attempting to connect to %s" % self.mqtt_client.broker)
        self.mqtt_client.connect()
    
    
    def connection_status(self):
        """CONNECT: check connection status
        
        """
        return self.mqtt_client.is_connected()
    
    
    # FUNCTIONS: Publish
    def publish(self, feed, value, retain=False, qos=0):
        """PUBLISH: publish value to feed
        
        Input:
            retain = whether the msg is saved by the broker
            qos    = 0 Quality of Service level for the topic, defaults to zero.
                    Conventional options are 0 (send at most once), 1 (send at least once), or 2 (send exactly once).
        
        """
        self.mqtt_client.publish(feed,value, retain, qos)
    
    
    # FUNCTIONS: Subscribe
    def subscribe(self, feed, qos=0):
        """SUBSCRIBE: subscribe value to feed
        
        Input:
            qos    = 0 Quality of Service level for the topic, defaults to zero.
                    Conventional options are 0 (send at most once), 1 (send at least once), or 2 (send exactly once).
        
        """
        self.mqtt_client.subscribe(feed, qos)    
    
    
    # FUNCTIONS: Print
    def print_connect(self, mqtt_client, userdata, flags, rc):
        """EVENT: Connect
        This function will be called when the mqtt_client is connected # successfully to the broker.
        
        """
        print("Connected to MQTT Broker!")
        print("Flags: {0}\n RC: {1}".format(flags, rc))


    def print_subscribe(self, client, userdata, topic, granted_qos):
        """EVENT: Subscribe
        This method is called when the client subscribes to a new feed.

        """
        print("Subscribed to {0} with QOS level {1}".format(topic, granted_qos))

    
    def print_unsubscribe(self, client, userdata, topic, pid):
        """EVENT: unsubscribe
        This method is called when the client unsubscribes from a feed.

        """
        print("Unsubscribed from {0} with PID {1}".format(topic, pid))


    def print_disconnect(self, mqtt_client, userdata, rc):
        """EVENT: disconnect
        Disconnected function will be called when the client disconnects.
        
        """
        print("Disconnected from MQTT Broker!")

    
    def print_message(self, client, feed_id, payload):
        """ FUNC: message
        # Message function will be called when a subscribed feed has a new value.
        # The feed_id parameter identifies the feed, and the payload parameter has
        # the new value.
        """
        self.payload   = payload
        print("Feed {0} received new value: {1}".format(feed_id, payload))

    
    def print_publish(self, mqtt_client, userdata, topic, pid):
        """ FUNC: publish
        # This method is called when the mqtt_client publishes data to a feed.
        """
        print("Published to {0} with PID {1}".format(topic, pid))


# Functions
# None


# Dictionaries
# None
```
````
`````

These can then be imported in Circuitpython via the following statement:

```{code-block} python
---

---
from wur_circuitpython_functions import wifi_module, MQTT_comms


```

## Secrets

Next add an blank textfile to the main directory of Circuitpy, copy and paste the following statements making sure to replace the values for each key by values that match your setup:


```{code-block} python
---

---
secrets = {
    'ssid' : 'YOUR_WIFI_NAME',
    'password' : 'WIFI_PASSWORD',

    'mqtt_broker': 'MQTT Broker address (e.g., 192.168.1.108)',
    'mqtt_port': 1883,
    'mqtt_username': 'mosquitto_username',
    'mqtt_password': 'mosquitto_password',
    'mqtt_topic': 'sensors/garden'
}
```

Save the file as `secrets.py`. It can now be imported as:

```{code-block} python
---

---
from secrets import secrets

```

Although it can be prudent to import it as a try statement:

```{code-block} python
---

---

try:
    from secrets import secrets
except ImportError:
    print("To run, add a dictionary containing keys ssid and password keys to dictionary called secrets in a file called secrets.py ")
    raise

```

## Metadata

```{code-block} python
---

---
params = {
    
            }
```

## Code

````{admonition} Click here for the Basic Actuator!
:class: tip, dropdown


```{code-block} python
---

---
""" main.py or code.py

"""
import time
import digitalio
import board

pump = digitalio.DigitalInOut(board.D2)
pump.direction = digitalio.Direction.OUTPUT

while True:
    if pump.value == True:
        pump.value = False
        print("Pump off!")
    else:
        pump.value = True
        print("Pump on!")
    time.sleep(5)

```
````

````{admonition} Click here for the Actuator + WiFi + MQTT code!
:class: tip, dropdown

This example includes functions for connecting to WiFi, receiving data via MQTT. The message and `pump_0.value = True` should be added by yourself:

```{code-block} python
---

---
""" main.py or code.py
    # Purpose:
        Pump actuator connected to WiFi and 
        receiving information via MQTT using 
        CircuitPython.

    # Pins:
        # pump 1: 
        board_pump_digital = board.D2

"""

########################
##      PACKAGES      ##
########################
import time
import digitalio
import board
import microcontroller
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
import adafruit_requests as requests

# download wur_circuitpython_functions from https://gitlab.com/wur/digitaltwin/-/tree/main/code/lib/wur_circuitpython_functions
# remember to copy the full /lib https://gitlab.com/wur/digitaltwin/-/tree/main/code/lib as it contains the addtional Adafruit
# Circuitpython packages 
from wur_circuitpython_functions import wifi_module, MQTT_comms

try:
    from secrets import secrets
except ImportError:
    print("To run, add a dictionary containing keys ssid and password keys to dictionary called secrets in a file called secrets.py ")
    raise


########################
##   USER specified   ##
########################

# Specificiations for the board

# i2c pins
# None

# digital/analog pins
pump_0                  = board.D2

# wifi pins
sensor_wifi_esp32_cs    = board.CS1
sensor_wifi_esp32_ready = board.ESP_BUSY
sensor_wifi_esp32_reset = board.ESP_RESET
sensor_wifi_SCK         = board.SCK1
sensor_wifi_SDO         = board.MOSI1
sensor_wifi_SDI         = board.MISO1

# variables
# None

# number of times to retry mqtt message sending
retry_count  = 10

# Time interval of measurements in seconds
measurement_time_interval = 10


########################
##   MESSAGE FORMAT   ##
########################

# mqtt payload with format: measurementName,tagKey=tagValue fieldKey="fieldValue"
# None


########################
##      ACTUATORS       ##
########################

# start up all actuators 
pump_0.direction = digitalio.Direction.OUTPUT


########################
##      WiFi          ##
########################

# connect to SSID using secrets["ssid"] and secrets["password"]
get_connected = wifi_module.connect(pin_esp32_cs = sensor_wifi_esp32_cs,
                                    pin_esp32_ready = sensor_wifi_esp32_ready, pin_esp32_reset = sensor_wifi_esp32_reset,
                                    pin_SCK = sensor_wifi_SCK, pin_SDO = sensor_wifi_SDO, pin_SDI = sensor_wifi_SDI)

get_connected.connect_to_network(secrets["ssid"], secrets["password"])

while not get_connected.get_connection_status():
    
    try:
        
        get_connected.connect_to_network(secrets["ssid"], secrets["password"])
    
    except RuntimeError as e:
        
        print("could not connect to AP, retrying: ", e)
        continue
    
    
########################
##      MQTT          ##
########################

# Set up and connect to MQTT
sensor_mqtt_comms = MQTT_comms.MQTT_comms(get_connected.esp)
sensor_mqtt_comms.start_mqqt_interface(socket, get_connected.esp)
sensor_mqtt_comms.new_client_object(secrets["mqtt_broker"], secrets["mqtt_port"], secrets["mqtt_username"], secrets["mqtt_password"], secrets['mqtt_topic'])

#sensor_mqtt_comms.connect()  # do try statement
count_MQTT = 0
check      = False
while not check:
    try:
        sensor_mqtt_comms.connect()
        check = True
        
    except RuntimeError as e:
        count_MQTT += 1
        
        print("CONNECTION - Could not connect to target AP, retrying: ", e)
        
        if count_MQTT == retry_count:
            break
        
        continue



########################
##      MAIN          ##
########################

# Step 0. Subscribe to topic
sensor_mqtt_comms.subscribe(secrets['mqtt_topic'])

while True:
    
    # Step 1. Check WiFi connection
    while get_connected.get_connection_status() == False:
        count_AP = 0
        
        try:
            get_connected.connect_to_network(secrets["ssid"], secrets["password"])
        
        except:
            
            count_AP +=1
            print('retrying')
            
            if count_AP == retry_count:
                
                break
            
            continue
    
    
    # Step 2. Check MQTT client connection
    if sensor_mqtt_comms.connection_status() == False:
        
        sensor_mqtt_comms.connect()
            
    
    # step 3. Get subscriptions/ Poll the message queue
    print("Subscription:")
    rc = sensor_mqtt_comms.mqtt_client.loop(60)
    print("...")
    
    if sensor_mqtt_comms.payload != None:
        
        print("Message: " + sensor_mqtt_comms.payload)
        
        # Reset payload
        sensor_mqtt_comms.payload = None
    
    
    
    time.sleep(measurement_time_interval)


```
