# Plant requirements


```{figure} /images/Plant_requirements.jpg
---

name: Plant_requirements-fig
---
**Requirements for (Green) Plant Photosynthesis.** 

```
Our second Digital Twin instance will be involved in maintaining and monitoring (green) plants. Maintaining a plant requires us to ensure a continuation of the growth and cellular processes of our plants. The most important processes being photosynthesis and respiration. 

## Photosynthesis
Sunlight provides the energy from which plants chemically combine carbon dioxide (CO2) and water (H2O) to create glucose (C6H12O6) and oxygen (O2).  

### Light

### Gas


### Water


## Other Requirements
### Temperature

### Nutrients

