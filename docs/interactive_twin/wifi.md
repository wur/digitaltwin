# Wireless Communication

An important aspect for any device is to be able to communicate with other devices. The most commonly used methods are either through WiFi, Bluetooth, ethernet or other radio signals. In this section the focus will be on connecting your Arduino, or other wifi enabled, device to a wireless network using that WiFi networks name (SSID) and corresponding password.

## Components
The components used for this module are as followed:

- Arduino Nano RP2040 Connect (€21.00) with headers (€26.95)

This module makes use of the `adafruit_esp32spi` and `adafruit_requests` libraries (available [here](https://circuitpython.org/libraries)) to utilise the wifi module on the Arduino Nano RP2040 Connect board. We will make use of the `wifi_connect` module that is part of the `helper_functions`. All should be added to the `lib` folder on the CIRCUITPY drive.


## Code
### Secrets
Create a new file and call it `secrets.py`. In this file we will create a python dictionary to store information using the curly brackets (`{}`). A dictionary is made up of key and value pairs (`key: value`), calling the key will return the value (`dict[key]`). The value can be number, string, list, or even another dictionary. This dictionary will be used to store sensitive information, e.g., passwords and tokens, that is required for your code to run but are unique to you. Allowing you to share your code without risk of accidentally sharing personal details. 

Copy and paste the following replacing `YOUR_WIFI_NAME` and `WIFI_PASSWORD` with the details of your network:

```{code-block} python

# dictionary for sensitive variables
secrets = {
            'ssid'     : 'YOUR_WIFI_NAME',
            'password' : 'WIFI_PASSWORD' ,
            }

```
To call the `secrets.py` file add the following line to the top of your programme script. This will import the `secrets` dictionary from `secrets.py` as its own object: 

```{code-block} python
from secrets import secrets
```

The variables can then be called like so:

```{code-block} python
secrets['ssid']
```

### Sensor Metadata
If we have devices talking to each other it is important that both sides of the conversation, those listening and replying, know who they are talking with. As such we need to give each board its own [metadata](https://wur.gitlab.io/digitaltwin/docs/standalone_twin/metadata.html).Therefore, in addition to our `secrets.py` file we will create a new file and call it `param.py`. Like `secrets.py` we will store all of this information in a dictionary:

```{code-block} python
""" param.py: File for sensor parameters
    
    REQUIREMENTS
        None
    
    CONTAINS:
        params  = dictionary for sensor metadata
                  Keys:
                  'board name'        = identifier for microcontroller.  (type: string)
                  'board'             = board identifier, i.e., type of board. (type: string)
                  'language'          = programming language of board. (type: string)
                  'language version'  = version of 'language'. (type: string)
                  'sensors'           = sensor peripherials attached to board. (type: string)
                  'actuators'         = actuator peripherials attached to board. (type: string)
                  'location'          = location of board. (type: string)
                  'timezone'          = timezone of 'location'. (type: string)
    
"""

params = {
            'board name'         : 'Connect_2',
            'board'              : 'Arduino Nano RP2040 Connect with rp2040',
            'language'           : 'Adafruit CicuitPython',
            'language version'   : '7.3.0',
            'sensors'            : ['BME280',
                                    #'SGP30',
                                    #'SHT40',                                
                                    ],
            'actuators'          : [],
            'location'           : 'Amsterdam',
            'timezone'           : 'Europe/Amsterdam',
                
    
         }


```
<br></br>

### Examples
Note that the following examples are [based upon](https://learn.adafruit.com/circuitpython-on-the-arduino-nano-rp2040-connect/wifi) the ESP32 SPI connections on the Arduino Nano RP2040 Connect board which differ from other ESP32 SPI connections (i.e., impacting the pins used to define variable `esp32_cs`, `esp32_ready`, `esp32_reset`, `spi`). As the Nina W102 uBlox module, responsible for the wireless communication, uses a secondary rather than the default SPI connection to communicate with the embedded RP2040 microcontroller. **Make sure to adapt the following when using a different board.**


#### Example 1: Wireless accesspoint scan
Scan the current location for wireless accesspoints:

```{code-block} python
""" Scan for wireless accesspoints with a Arduino Nano RP2040 Connect board

Adapted from the Adafruit_CircuitPython_ESP32SPI library example 
https://learn.adafruit.com/circuitpython-on-the-arduino-nano-rp2040-connect/wifi


"""

# packages
import board
import busio
from digitalio import DigitalInOut
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from   adafruit_esp32spi import adafruit_esp32spi
import adafruit_requests as requests

        
# Variables
#  ESP32 pins NOTE: board specific pins
esp32_cs      = DigitalInOut(board.CS1)
esp32_ready   = DigitalInOut(board.ESP_BUSY)
esp32_reset   = DigitalInOut(board.ESP_RESET)

#  SPI pins NOTE: uses the secondary SPI connected through the ESP32
spi = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)
        
requests.set_socket(socket, esp)


if esp.status == adafruit_esp32spi.WL_IDLE_STATUS:
    print("ESP32 found and in idle mode")

# PRINT firmware and mac address of board
print("Firmware vers.", esp.firmware_version)
print("MAC addr:", [hex(i) for i in esp.MAC_address])


# SCAN for wireless networks, print the ssid and signal strength
# note esp.scan_networks() also contains the following keys 
# 'ssid', 'bssid', 'channel', 'RSSI', 'authmode', 'hidden'
# with values:
# authmode_ = {0: 'open', 1: 'WEP',2: 'WPA-PSK',3: 'WPA2-PSK', 4: 'WPA/WPA2-PSK'}
# hidden_   = {0: 'visible', 1: 'hidden'}

for wifi_access_point in esp.scan_networks():
    print("\t%s\t\tRSSI: %d" % (str(wifi_access_point['ssid'], 'utf-8'), wifi_access_point['rssi']))


```

Running this code will give you:

```{figure} /images/test_check_wifi.jpg
---

name: scan_access-fig
---
**Output.**

```


<br></br>

#### Example 2: Connect to WiFi
Connect to the specified wireless accesspoint denoted in the `secrets.py` file:

```{code-block} python
""" Connect to WiFi Network with a Arduino Nano RP2040 Connect board

Adapted from the Adafruit_CircuitPython_ESP32SPI library example 
https://learn.adafruit.com/circuitpython-on-the-arduino-nano-rp2040-connect/wifi


"""

# packages
import board
import busio
from digitalio import DigitalInOut
import adafruit_esp32spi.adafruit_esp32spi_socket as socket
from   adafruit_esp32spi import adafruit_esp32spi
import adafruit_requests as requests


# get SSID and password from secrets file
try:
    from secrets import secrets
except ImportError:
    print("To run, add a dictionary containing keys ssid and password keys to dictionary called secrets in a file called secrets.py ")
    raise
        
# Variables
#  ESP32 pins NOTE: board specific pins
esp32_cs      = DigitalInOut(board.CS1)
esp32_ready   = DigitalInOut(board.ESP_BUSY)
esp32_reset   = DigitalInOut(board.ESP_RESET)

#  SPI pins NOTE: uses the secondary SPI connected through the ESP32
spi = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)
        
requests.set_socket(socket, esp)


if esp.status == adafruit_esp32spi.WL_IDLE_STATUS:
    print("ESP32 found and in idle mode")

# PRINT firmware and mac address of board
print("Firmware vers.", esp.firmware_version)
print("MAC addr:", [hex(i) for i in esp.MAC_address])

print("Connecting to AP...")

while not esp.is_connected:
    
    try:
        esp.connect_AP(secrets["ssid"], secrets["password"])
    
    except RuntimeError as e:
        print("could not connect to AP, retrying: ", e)
        continue

print("Connected to", str(esp.ssid, "utf-8"), "\tRSSI:", esp.rssi)
print("My IP address is", esp.pretty_ip(esp.ip_address))

```

Running this code will give you:

```{figure} /images/test_conect_to_wifi.jpg
---

name: connected_wifi-fig
---
**Output.**

```

<br></br>

#### Example 3: Connecting
Using the `helper_functions.py`:

```{code-block} python
from helper_functions import wifi_connect, board_led
from secrets import secrets

####################################
###           WiFi               ###
####################################

get_connected = wifi_connect()
get_connected.connect_to_network(secrets["ssid"], secrets["password"])

####################################
###          END STARTUP         ###
####################################

led.blink(6, False)

####################################
###          LOOP                ###
####################################

while True:
    pass 
    # Your code here
```


<br></br>

### Examples: Web Server Gateway Interface

It can also be possible to apply a **Web Server Gateway Interface** ([WSGI](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface)) server that interfaces with the ESP32 chip over **serial peripheral interface** (SPI) - an interface that allows for the exchange of one bit at a time (*serial*) between two devices, with one in control. [Using](https://github.com/adafruit/Adafruit_CircuitPython_WSGI/blob/main/adafruit_wsgi/wsgi_app.py) the `wsgi_app` (collated [here](https://gitlab.com/wur/digitaltwin/-/tree/main/code/Python/adafruit_wsgi) ), a specified port on the ESP32 chip is opened to listen for incoming HTTP requests calling an application object whenever a new HTTP request (`POST`, `GET`, `PUT`, `PATCH`, and `DELETE`) is received. 

#### Example 4: static folder

For this example, add a new folder to the CircuitPy drive called `static` and make an `index.html` file with the following text: 

```{code-block} html

<!DOCTYPE_HTML>
<html>
    <head>
        <meta name = "viewport" content = "width =device-width">
    </head>
    <body>
        <center><h2> ESP32 Web server in Micropython </h2></center>
        <center>
        <form>
            <button type='submit' name = "LED" value='1'> LED ON </button>
            <button type='submit' name = "LED" value='0'> LED OFF</button>
        </form>
        </center>
        <center><p> LED is now <strong> led_state </strong>.</p></center>
    </body>
</html>

```

Next copy and paste the following Circuitpython code and save as `code.py`:

```{code-block} python
""" WSGI server compatible web applications

Modified from:
    `adafruit_esp32spi_wsgiserver` 
     https://github.com/adafruit/Adafruit_CircuitPython_ESP32SPI/blob/main/adafruit_esp32spi/adafruit_esp32spi_wsgiserver.py
    `wsgi_app` 
    https://github.com/adafruit/Adafruit_CircuitPython_WSGI/blob/main/adafruit_wsgi/wsgi_app.py

Requirements:
    add /adafruit_wsgi folder to /lib of CircuitPy drive, may need to redownload the latest version with the following:
    https://github.com/adafruit/Adafruit_CircuitPython_WSGI

    add a folder /static to the CircuitPy drive

"""
##########
# Packages
import board
import busio
import adafruit_esp32spi.adafruit_esp32spi_wifimanager as wifimanager
import adafruit_esp32spi.adafruit_esp32spi_wsgiserver as server
from digitalio import DigitalInOut, Direction
from adafruit_esp32spi import adafruit_esp32spi
from adafruit_wsgi.wsgi_app import WSGIApp

# Get wifi access point (AP) ssid and password from a secrets.py file:
try:
    from secrets import secrets
except ImportError:
    print("Please add a WiFi secrets dictionary with keys 'ssid', 'password' to secrets.py")
    raise



########### 
# Functions
def serve_file(file_path, directory=None, chunk_size = 8912):
    """ Serve file 
    
    Rationale:
        Function in a function, passes second function as output
        to read out html file
        
    Input:
        file_path  = file name with path (e.g., /static/index.html)
                     if filename only ("/index.html") add directory
        directory  = directory containing file in file_path (e.g.,
                     "/static"), default = None
        chunk_size = threshold value of the amount of information
                     to read, default = 8912
    
    Output:
        resp_iter() = a function that reads out the html file
    """
    
    full_path = file_path if not directory else directory + file_path
    
    def resp_iter():
        """ Read out file 
        
        Variables:
            full_path   = full path (directory + file name) to file
            chunk_size  = threshold value of the amount of information
                          to read, default of def serve_file = 8912
        Output:
            yield chunk = read out file in full_path limited to threshold
                          defined by chunk_size (default = 8912)
        """
        
        with open(full_path, "rb") as file:
            
            while True:
                
                chunk = file.read(chunk_size)
                #print(chunk)
                
                if chunk:
                    
                    #print(chunk)
                    yield chunk
                    
                else:
                    break
                
    return resp_iter()



##############
# Hardware

# Peripherials:
status_light           = DigitalInOut(board.LED) 
status_light.direction = Direction.OUTPUT 

# ESP32 Pins:
esp32_cs               = DigitalInOut(board.CS1)
esp32_ready            = DigitalInOut(board.ESP_BUSY)
esp32_reset            = DigitalInOut(board.ESP_RESET)

# SPI pins
# NOTE: board specific for arduino nano rp2040 connect
spi = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)



###########
# Programme
print("ESP32 SPI simple web app... Start")

## Connect to wifi with secrets dictionary:
wifi = wifimanager.ESPSPI_WiFiManager(esp, secrets, debug=True)
wifi.connect()

# Create web application:
web_app = WSGIApp()

# registering the following functions to be called on specific
# HTTP GET requests routes:
@web_app.route("/led_on")
def led_on(request):
    """ LED On
    
    Input:
        request
        
    Output:
        
        "200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], serve_file("/index.html","/static" )
        This is in the format: status  = "200 OK"
                               headers = [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")]
                               data    = serve_file("/index.html","/static" )
    
    Requirements:
        serve_file = function to read out (html) file
        
    """
    
    print("led on!")
    
    # Modify hardware:
    status_light.value = True
    
    # Return browser page:
    return ("200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], serve_file("/index.html","/static" ))


@web_app.route("/led_off")
def led_off(request):
    """ LED Off
    
    Input:
        request
        
    Output:
        
        "200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], serve_file("/index.html","/static" )
        This is in the format: status  = "200 OK"
                               headers = [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")]
                               data    = serve_file("/index.html","/static" )
    
    Requirements:
        serve_file = function to read out (html) file
        
    """
    print("led off!")
    status_light.value = False
    return ("200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], serve_file("/index.html","/static" ))


# Setup server:
server.set_interface(esp)

# Passing in port 80 and our web_app as the application: 
wsgiServer = server.WSGIServer(80, application=web_app)

# Print the IP address of the board:
print("To connect, open this IP in your browser: ", esp.pretty_ip(esp.ip_address))
# print(esp.get_time())

# Start the server:
wsgiServer.start()



##########
# Main

while True:
    
    try:
        
        # Here the server poll for incoming requests
        wsgiServer.update_poll()
        
        
        ################################################################
        ##                                                            ##
        ##                    <delete me and add..>                   ##
        ##                    Background tasks here.                  ##
        ##                                                            ##
        ##         E.g., reading sensors, time, weather api, etc.     ##
        ##                                                            ##
        ##                                                            ##
        ################################################################
        
        
    except (ValueError, RuntimeError) as e:
        # in the event of 'failure' print Value and Runtime error's as
        # e and try and reset wifi 
        print("Failed to update server, restarting ESP32\n", e)
        
        wifi.reset()
        
        continue

```



<br></br>

#### Example 5: html page function

Another approach of using WSGI is with the following piece of code, the html page is called as a function allowing us to modify different variables and auto-generate different pages e.g., an index page, LED on page, and a LED off page: 


```{code-block} python
""" wsgi_simples.py WSGI Simple example


"""
##########
# Packages
import board
import busio
import adafruit_esp32spi.adafruit_esp32spi_wifimanager as wifimanager
import adafruit_esp32spi.adafruit_esp32spi_wsgiserver as server
from digitalio import DigitalInOut, Direction
from adafruit_esp32spi import adafruit_esp32spi
from adafruit_wsgi.wsgi_app import WSGIApp

# Get wifi access point (AP) ssid and password from a secrets.py file:
try:
    from secrets import secrets
except ImportError:
    print("Please add a WiFi secrets dictionary with keys 'ssid', 'password' to secrets.py")
    raise


########### 
# Functions
def webpage_generator(html = ""):
    """ GENERATOR: Webpage
    
    Input:
        html = string containing html, default is empty string ""
    
    Output:
        yield html
    
    """
    
    yield html
    

def two_button_web_page(
                         ip_address,
                         title             = "ESP32 WSGI",
                         title_body        = "ESP32 Web Server Gateway Interface in Circuitpython",
                         webpage_a_address = "/led_on",
                         webpage_b_address = "/led_off",
                         button_a_title    = "LED ON",
                         button_b_title    = "LED OFF",
                         variable          = "LED",
                         state             = " LED ON ",
                         helper_url        = "https://wur.gitlab.io/digitaltwin/docs/interactive_twin/intro.html",
                         type_of_page      = 'non value',
                         
                         ):
    """Func.: Two button webpage
    
    Input:
    
        ip_address        = base URL (ip address)
        title             = text to be displayed as page title, e.g., on browser tab
        title_body        = title text in <body> </body>
        webpage_a_address = url page where button a goes, default = "/led_on",
        webpage_b_address = url page where button b goes, default = "/led_off",
        button_a_title    = "LED ON",
        button_b_title    = "LED OFF",
        variable          = variable used in state text, default = "LED",
        state             = variable used as state text, default = " LED ON "
        helper_url        = url to go to for help, default = this git repository
    
    Note:
        in the following first call the variable empty ("") then register the
        functions, setup the server to get IP address, and finally create the 
        variable properly to include the IP address
        
    Example:
        
        led_index_html  = two_button_web_page(ip)
        led_on_html     = two_button_web_page(ip, state = " LED ON ")
        led_off_html    = two_button_web_page(ip, state = " LED OFF ")
    
    """
    if type_of_page  == 'non value':
        
        html_page = """
                    <!DOCTYPE_HTML>
                    <html lang="en-GB">
                    <head>
                        <meta charset="utf-8">
                        <meta name = "viewport" content="width=device-width, initial-scale=1">
                        <br>
                        <br>
                        <title>""" + title + """</title>
                    </head>
                    
                    <body>
                         <center><h2>""" + title_body + """</h2></center>
                         <hr>
                         <br>
                         <center> Get <a href=""" + helper_url + """ target ="_blank"> help </a> with this file. </center>
                         <br>
                         <hr>
                         <center>
                         
                            <form>
                                <button formaction=""" + webpage_a_address + """>"""+ button_a_title +"""</button>
                                <button formaction=""" + webpage_b_address + """>"""+ button_b_title +"""</button>
                            </form>
                            
                        </center>
                        <br><br>
                        <center><p>""" +  variable + """ is now <strong>""" + state + """</strong>.</p></center>
                    </body>
                    
                    </html>"""
        return html_page
    
    
    
    else:
        raise ValueError('type_of_page should be non value or value')



##############
# Hardware

# Peripherials:
status_light           = DigitalInOut(board.LED) 
status_light.direction = Direction.OUTPUT 

# ESP32 Pins:
esp32_cs               = DigitalInOut(board.CS1)
esp32_ready            = DigitalInOut(board.ESP_BUSY)
esp32_reset            = DigitalInOut(board.ESP_RESET)

# SPI pins
# NOTE: board specific for arduino nano rp2040 connect
spi = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)


###########
# Programme
print("ESP32 SPI simple web app... Start")

## Connect to wifi with secrets dictionary:
wifi = wifimanager.ESPSPI_WiFiManager(esp, secrets, debug=True)
wifi.connect()

# Create web application:
web_app = WSGIApp()

# registering the following functions to be called on specific
# HTTP GET requests routes:
led_index_html  = ""
led_on_html     = ""
led_off_html    = ""

@web_app.route("", "GET")
def index(request):
    """ Get index
    
    Input:
        request
        
    Output:
        
        "200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], serve_file("/index.html","/static" )
        This is in the format: status  = "200 OK"
                               headers = [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")]
                               data    = serve_file("/index.html","/static" )
    
    Requirements:
        serve_file = function to read out (html) file
        
    """
    
    print("led on!")
    
    # Modify hardware:
    status_light.value = True
    
    # Return browser page:
    return ("200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], webpage_generator(led_index_html))


@web_app.route("/led_on", "GET")
def led_on(request):
    """ LED On
    
    Input:
        request
        
    Output:
        
        "200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], serve_file("/index.html","/static" )
        This is in the format: status  = "200 OK"
                               headers = [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")]
                               data    = serve_file("/index.html","/static" )
    
    Requirements:
        serve_file = function to read out (html) file
        
    """
    
    print("led on!")
    
    # Modify hardware:
    status_light.value = True
    
    # Return browser page:
    return ("200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], webpage_generator(led_on_html))


@web_app.route("/led_off", "GET")
def led_off(request):
    """ LED Off
    
    Input:
        request
        
    Output:
        
        "200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], serve_file("/index.html","/static" )
        This is in the format: status  = "200 OK"
                               headers = [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")]
                               data    = serve_file("/index.html","/static" )
    
    Requirements:
        serve_file = function to read out (html) file
        
    """
    print("led off!")
    status_light.value = False
    return ("200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], webpage_generator(led_off_html))


# Setup server:
server.set_interface(esp)

# Passing in port 80 and our web_app as the application: 
wsgiServer = server.WSGIServer(80, application=web_app)

# Print the IP address of the board:
print("To connect, open this IP in your browser: ", esp.pretty_ip(esp.ip_address))
# print(esp.get_time())

# Start the server:
wsgiServer.start()

##########
# Main

###########
# Variables
ip              = esp.pretty_ip(esp.ip_address)
led_index_html  = two_button_web_page(ip)
led_on_html     = two_button_web_page(ip, state = " LED ON ")
led_off_html    = two_button_web_page(ip, state = " LED OFF ")

##########
# Loop
while True:
    
    try:
        
        # Here the server poll for incoming requests
        wsgiServer.update_poll()
        
        
        ################################################################
        ##                                                            ##
        ##                    <delete me and add..>                   ##
        ##                    Background tasks here.                  ##
        ##                                                            ##
        ##         E.g., reading sensors, time, weather api, etc.     ##
        ##                                                            ##
        ##                                                            ##
        ################################################################
        
        
    except (ValueError, RuntimeError) as e:
        # in the event of 'failure' print Value and Runtime error's as
        # e and try and reset wifi 
        print("Failed to update server, restarting ESP32\n", e)
        
        wifi.reset()
        
        continue



```
