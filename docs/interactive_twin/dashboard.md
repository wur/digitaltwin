# Dashboard

```{figure} /images/test_influxdb.jpg
---
name: Test_influxDB-fig
alt: Data from Sensors presented in InfluxDB
class: bg-primary mb-1
width: 100%
align: center
---
**Our project so far**.
```

Having set up the sensors, the communication network, and the database storage it is time to present the data to an audience, either ourselves or an interested audience. Yet our current set-up (({numref}`Test_influxDB-fig` )is far from ideal. Therefore, in this module the aim will be to setup a Dashboard to better visualise our data using Node-RED. 


## Node-RED

```{figure} /images/MQTT_Schematic.jpg
---
name: MQTT_Schematic_WUR-fig
alt: MQTT approach
class: bg-primary mb-1
width: 100%
align: center
---

**From data to data visualisation**. *A schematic of how the data is sent from the IoT sensor to a dashboard. (1) The Arduino Nano RP2040 writes sensor data to the Eclipse Mosquitto Package for MQTT, (2) Telegraf subscribes to MQTT topics listening for new messages which on receipt (3) are written to InfluxDB. From InfluxDB the data is (4) transmitted to Node-RED to produce a dashboard (5) viewable by an external party.* 
```

[Node-RED](https://nodered.org/about/#flow-based-programming) [came about](https://www.youtube.com/watch?v=Bbg1017amZs) as an approach to visualising and mapping MQTT topics and which has become a tool for other uses. It is a low-code flow based programming for event-driven applications. Let us break down that last sentence. Low code is an approach to software developement that minimizes the coding component, requiring little to no code. Usually a graphical environment is employed that allows a user to drag and drop components as a visual programming language (e.g., Scratch, Simulink, etc.). Flow based programming replaces lines of code with a graphical approach to creating programmes, that have a series of boxes (or *operators*) that represent functions that are linked together by inputs, outputs, and inputs and outputs. Think of it as a factory conveyor belt upon which data flows between the various machines. The focus here is on the end product and the larger architecture rather than the individual machines that manipulate or transform the data. Finally, event-driven is an application that is written in such a way as to respond to triggers (e.g., actions or changes) from either a user or the system.  

```{figure} /images/nodered_workflow.png
---
name: Node_RED_workflow-fig
alt: An example of a Node-RED workflow
class: bg-primary mb-1
width: 100%
align: center
---
**An example of Node-RED in use**

```

````{margin} 
```{note}
More information can be found at https://nodered.org

```
````

As a programming tool Node-RED enables the developer to combine hardware devices, APIs, and online services by providing the user with a browser-based editor that has a wide range of nodes in its palette. The user can graphically drag and drop these nodes into new configurations making it easy to wire together flows. Once the application works it can be deployed to its runtime in a single-click.


```{figure} /images/nodered_dashboard.png
---
name: Node_RED_dashboard-fig
alt: An example of a Node-RED workflow
class: bg-primary mb-1
width: 100%
align: center
---
**A dashboard produced with Node-RED**. 

```

## Installation

### Docker

Node-RED can be installed through [various means](https://nodered.org) but for our current setup the easiest method is to use docker. Open a Terminal, connect to the Raspberry Pi via SSH, and enter the following commands:

```{code} terminal

docker run -it -d \
    --name nodered
    --restart=always \
    -p 1880:1880 \
    -v node_red_dt_data:/data \
    nodered/node-red

```

These commands will start the Node-RED application on your Raspberry Pi and make the application accessible through port 1880 allowing you to navigate to the browser-based editor via: `http://raspberrypi.local:1880`.

If you want to install Node-RED differently, for example, in the cloud please have a look at https://nodered.org/#get-started.


### Modules

In Node-RED you can install additional modules to expand the possibilities within Node-RED. In this example we will use two additional `Palettes`. These are `node-red-contrib-influxdb` and `node-red-dashboard`. To install them click on the `hamburger` icon at the top right and click on `Manage palette`. 

![](../../images/nodered_palette.png)

In the search bar search for the two modules you need (`node-red-contrib-influxdb` and `node-red-dashboard`) and click on install. This should update your interface by expanding the options on the left of your Node-RED interface.


## Workflows

### Historical data

The first we will create is the historical data overview.

![](../../images/nodered_historical.png)

You will need the following modules

- Inject, from the common section
- influxdb in, to obtain the data stored in your influxdb
- json, to convert the object from influxdb to a javascript object
- function, to process the data into an acceptable format
- chart, to visualize the historical data

### Inject

With the inject function we can trigger the workflow at the start and if needed at a certain time interval.
In this case we trigger this workflow every 60 minutes which in turn will update the entire graph.

![](../../images/nodered_inject.png)

### Influxdb

To obtain the data from influxdb we need to provide a server, organization and a query.

The query can be placed in the query box which will obtain all data from the `last day (-1d)` from the `data` bucket.

```
from(bucket: "data")
  |> range(start: -1d)
```

![](../../images/nodered_influxdb.png)

To setup the server, click on the pencil icon after the server option. 
Here you need to set the settings as followed:
- Give the server a name
- Set the version to 2.0
- Set the URL (use the IP address)
- Fill in the token you can obtain from InfluxDB to access the API.
- Enable verify server certificate
- Click update to save the settings

Once all of this is set you can click on done. Now you can connect the `inject` module to the `influxdb in` module.

### JSON

To be able to manipulate the data obtained from InfluxDB the data which is in the JSON format needs to be converted to a java script object. This can be achieved using the JSON module.

- Insert the module
- Open the module
- Action, Always convert to JavaScript Object
- Property, msg.payload
- Click done
- Connect the output of the influx module to the input of the JSON module.

#### Debug

To be able to know if this part is working you can glue in a `DEBUG` module.
This will show all data it receives in the debug log on the right panel.

- Add a DEBUG module
- Connect the JSON output to the DEBUG input
- Make sure the square behind `debug` is activated (green)
- Click deploy on the top left

Now it will start the workflow and you should see a message from debug in your console.

![](../../images/nodered_debug.png)

If you do not see any message make sure your settings for your influx module is correct and it is able to connect.

### Function

In a normal Node-RED workflow you continously retrieve data either at every second, minute, hour, day or anything in between. However since we are trying to visualize historical data here (from the last day) and only update this every hour (using the inject function) it is most memory and cpu efficient to submit the whole dataset as one graph.

From the Influx module we receive the content via a `msg.payload` for all the different sensors we currently use. For each sensor value we obtain the sensor type, date and value. This is stored in a list of lists (`data[seriesIndex]`) for each sensor separately with the x and y values. Once finished we modify the msg.payload to contain the series, data and labels which is then submitted to the chart.

```
var labels = []
var data = []
var series = []

for (let index = 0; index < msg.payload.length; index++) {
    const content = msg.payload[index];
    // Get time stamp in milliseconds
    var date = new Date(content._time)
    var x = date.getTime();
    var y = content._value;
    var type = content._field;
    let seriesIndex = series.indexOf(type);
    if (seriesIndex == -1) {
        series.push(type)
        seriesIndex = series.indexOf(type);
        data.push([])
    }
    data[seriesIndex].push({"x" : x, "y" : y})
}

msg.payload = [
    {
        "series": series,
        "data": data,
        "labels": [],
        }
    ];
return msg;
```

### Chart

After the installation of the UI module additional modules on the left should become visible. One of the newly added options is the `Chart` option. You can connec the output of the previous function directly to this chart. Make sure there is a `Group` in the chart properties tab which is connected to a `Tab`. These are sections used for the visualization so Node-RED knows where these views should become available.

## Dashboard

If all went correctly you should be able to see a simple chart in your dashboard at `http://raspberrypi.local:1880/ui/`. Since this view only contains a historical chart you might want to go back to the editor and modify the dasboard layout.

![](../../images/nodered_layout.png)

When you `hover` over `Main` you should be able to see the layout button. In this way you can easily drag and drop the different visualizations. You can modify the overall width you want to use for your dashboard by changing the widht value in the top right.

![](../../images/nodered_layout2.png)

You might have noticed that the `chart` scales depending on the width provided. If you want to make this more flexible you have to go back to the chart itself and change the `auto` size to a predefined width and height. However it does not matter if you make it 1x1 or 5x20. If you `set` it yourself you can modify it in the `layout` view. So my advice is to always modify the auto to a 1x1 and change it in the layout afterwards.
