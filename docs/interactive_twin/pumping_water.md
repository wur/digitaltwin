# Automated watering

To be able to change and adapt the amount of soil moisture to both the current and future needs of our plant(s) we can either get the Digital Twin to send the user a message to water it or to produce some form of automated watering system. Ideally, at higher conceptual levels of a Digital Twin the Twin will supplant the user in performing tasks independently via automation. In this section an automated watering IoT will be developed. A submersible pump placed into a water tank reservoir will be governed by our IoT. Turning on the (submersible) pump either at fixed timepoints to pump water to its destination or triggered through external sources. 

## Components

The components used for this module are as followed:
- Arduino Nano RP2040 Connect (€21.00) with headers (€26.95)
- 3 x Jumper Wire male-to-male pins ( < €1.00)
- 3 x Jumper Wire male-to-female  ( < €1.00)
- One 10k Ohm Resistor ( < €1.00)
- A IRL540 Power mosfet with logic-level gate drive ( < €1.00)
- Mini submersible pump 2.5V - 6V (€4.00)
- Silicon tubing
- A water reservoir, e.g., a glass, tuperware container, etc. 
- (*optional*) A receptical, e.g., a sink, glass, tuperware container, etc. 
- A PCB Terminal Block Connector 
- A mini, 170 point, Breadboard (€4.00)
- A screwdriver

## Layout

```{figure} /images/Fritz_pump.jpg
---
name: Fritz_pump-fig
alt: Pump wiring layout
---
*Set-up of the water pumping. Schematic and Fritzing diagram showing how to set-up the electronic components.*
```
Wiring the submersible pump up can be done by following the fritzing diagram ({numref}`Fritz_pump-fig`).

### Mini Breadboard
First, let us arrange the wiring on the breadboard. With a mini breadboard there are two sets of holes, one on the top half and the other on the bottom half of the breadboard, separated by an empty grove and on occasion screw holes. Holding the breadboard landscape, you will notice that each set of holes is arranged in 5 rows and 17 columns ({numref}`Fritz_pump-fig`). The two sets of holes are not connected. Within a set anything plugged into the same column will be connected so if you connect one hole in a column to ground the remaining 4 holes can be used to connect to ground. 

### Wire up components
Placing the breadboard on a table landscape let us first place the 10k Ohm Resistor into the upper set of holes in the breadboard by directing each end of the resistor into a different column, you will need to bend the legs so that they can fit into the holes. Next plug in the three legs of the IRL540 Power mosfet into a different place on the breadboard, again so that each leg is placed in it's own seperate column. You now need to connect a jumper wire from the left leg of the resistor to the left most leg of the mosfet and a jumper wire from the right leg of the resistor to the right most leg of the mosfet. The upper set of holes has become crowded for our next component, the PCB Terminal Block Connector, lets place it in the lower set of breadboard holes again so that each of its legs is in a different column. Now using a jumper wire connect one of the terminal points to middle leg of the mosfet. Place each bare wire of the submersible pump into its own port in the PCB Terminal Block Connector, ensuring that the ground wire of the submersible pump goes into the port you just connected to the middle leg of the mosfet. Taking your screwdriver, tighten the screws so that the wire can not be removed by gently tugging. 

### Connect Microcontroller
You may first want to load the code (see below) prior to this step. Ensuring that the Microcontroller is unplugged and unpowered, take the three remaining male-to-female jumper wires. Slot the female ends of the wires over a Digital I/O pin, a power pin and a ground pin. In our example these will be the D2, 3v3, and GND pins of the Arduino Nano RP2040 Connect. Take the jumper wire connected to the GND pin and put the male end into a hole in the column with the right leg of the resistor. Next take the jumper wire connected to the D2 pin and put the male end into a hole in the column with the left leg of the resistor. Finally, take the jumper wire connected to the 3v3 pin and put the male end into a hole in the column connected to the port of the PCB Terminal Block Connector with the pump's power wire. 

### Submerge the pump
Remember we are *putting water near and around electronics* which can go wrong quite easily, so take your time to first get things ready, safely. For instance, placing the electronics higher than the water reservoir can avoid accidents from leaks; having a cloth or paper towel around to mop up any spillage; and, it can be prudent to use a portable powerbank to test the set-up, giving you a bit more space to work. Before following the next steps ensure that the electronics are **turned off**. Likewise, before placing the submersible pump into any liquid ensure that the water proofing of the cable looks good.

Attach the silicon tubing to the *nozzle* of the pump, place the pump into the reservoir, and then ensure that the other end of the silicon tubing is placed into something (e.g., your plant pot, sink, another cup, etc.). Fill the reservoir with water, ensuring that the pump is submersed. Have a final check before attaching the power to the Arduino.      


## Code
### Example: Infinite loop

In this example an infinite loop is used with 5 seconds interval to turn on or off the pump using a mosfet logic-level gate which can be used to trigger the pump.

```{code} python
---
---
import time
import digitalio
import board

pump = digitalio.DigitalInOut(board.D2)
pump.direction = digitalio.Direction.OUTPUT

while True:
    if pump.value == True:
        pump.value = False
        print("Pump off!")
    else:
        pump.value = True
        print("Pump on!")
    time.sleep(5)
```

### Example: Blink LED
Blink the on board LED to show that the board is working using `board_led` in `helper_functions`:

```{code} python
---
---
import time
import digitalio
import board
from helper_functions import board_led

led = board_led()
led.turn_on_led()
led.blink(6, False)

pump = digitalio.DigitalInOut(board.D2)
pump.direction = digitalio.Direction.OUTPUT

while True:
    if pump.value == True:
        pump.value = False
        led.turn_off_led()
        print("Pump off!")
    else:
        pump.value = True
        led.turn_on_led()
        print("Pump on!")
    time.sleep(5)
```

<br></br>

### Example: Fixed timepoints


<br></br>

### Example: External commands
Connect to the water pump via [WSGI](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/wifi.html) using the `wsgi_app` module. Because this set-up is unattached to the computer the LED light will blink to note that changes are occuring using `board_led` in `helper_functions`:

```{code} python
---
---
##########
# Packages
import board
import busio
import time
import adafruit_esp32spi.adafruit_esp32spi_wifimanager as wifimanager
import adafruit_esp32spi.adafruit_esp32spi_wsgiserver as server
from digitalio import DigitalInOut, Direction
from adafruit_esp32spi import adafruit_esp32spi
from adafruit_wsgi.wsgi_app import WSGIApp
from helper_functions import board_led

# Get wifi access point (AP) ssid and password from a secrets.py file:
try:
    from secrets import secrets
except ImportError:
    print("Please add a WiFi secrets dictionary with keys 'ssid', 'password' to secrets.py")
    raise


########### 
# Functions
def webpage_generator(html = ""):
    """ GENERATOR: Webpage
    
    Input:
        html = string containing html, default is empty string ""
    
    Output:
        yield html
    
    """
    
    yield html
    

def two_button_web_page(
                         ip_address,
                         title             = "ESP32 WSGI",
                         title_body        = "ESP32 Web Server Gateway Interface in Circuitpython",
                         webpage_a_address = "/led_on",
                         webpage_b_address = "/led_off",
                         button_a_title    = "LED ON",
                         button_b_title    = "LED OFF",
                         variable          = "LED",
                         state             = " LED ON ",
                         helper_url        = "https://wur.gitlab.io/digitaltwin/docs/interactive_twin/intro.html",
                         type_of_page      = 'non value',
                         
                         ):
    """Func.: Two button webpage
    
    Input:
    
        ip_address        = base URL (ip address)
        title             = text to be displayed as page title, e.g., on browser tab
        title_body        = title text in <body> </body>
        webpage_a_address = url page where button a goes, default = "/led_on",
        webpage_b_address = url page where button b goes, default = "/led_off",
        button_a_title    = "LED ON",
        button_b_title    = "LED OFF",
        variable          = variable used in state text, default = "LED",
        state             = variable used as state text, default = " LED ON "
        helper_url        = url to go to for help, default = this git repository
    
    Note:
        in the following first call the variable empty ("") then register the
        functions, setup the server to get IP address, and finally create the 
        variable properly to include the IP address
        
    Example:
        
        led_index_html  = two_button_web_page(ip)
        led_on_html     = two_button_web_page(ip, state = " LED ON ")
        led_off_html    = two_button_web_page(ip, state = " LED OFF ")
    
    """
    if type_of_page  == 'non value':
        
        html_page = """
                    <!DOCTYPE_HTML>
                    <html lang="en-GB">
                    <head>
                        <meta charset="utf-8">
                        <meta name = "viewport" content="width=device-width, initial-scale=1">
                        <br>
                        <br>
                        <title>""" + title + """</title>
                    </head>
                    
                    <body>
                         <center><h2>""" + title_body + """</h2></center>
                         <hr>
                         <br>
                         <center> Get <a href=""" + helper_url + """ target ="_blank"> help </a> with this file. </center>
                         <br>
                         <hr>
                         <center>
                         
                            <form>
                                <button formaction=""" + webpage_a_address + """>"""+ button_a_title +"""</button>
                                <button formaction=""" + webpage_b_address + """>"""+ button_b_title +"""</button>
                            </form>
                            
                        </center>
                        <br><br>
                        <center><p>""" +  variable + """ is now <strong>""" + state + """</strong>.</p></center>
                    </body>
                    
                    </html>"""
        return html_page
    
    
    
    else:
        raise ValueError('type_of_page should be non value or value')



##############
# Hardware

# Peripherials - LED/status light:
led = board_led()
led.turn_on_led()
led.blink(6, False)

# Peripherials - Water pump
pump                   = DigitalInOut(board.D2)
pump.direction         = Direction.OUTPUT

# ESP32 Pins:
esp32_cs               = DigitalInOut(board.CS1)
esp32_ready            = DigitalInOut(board.ESP_BUSY)
esp32_reset            = DigitalInOut(board.ESP_RESET)

# SPI pins
# NOTE: board specific for arduino nano rp2040 connect
spi = busio.SPI(board.SCK1, board.MOSI1, board.MISO1)
esp = adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready, esp32_reset)


###########
# Programme
print("ESP32 SPI simple web app... Start")
led.blink(6, False)

## Connect to wifi with secrets dictionary:
wifi = wifimanager.ESPSPI_WiFiManager(esp, secrets, debug=True)
wifi.connect()

# Create web application:
web_app = WSGIApp()

# registering the following functions to be called on specific
# HTTP GET requests routes:
pump_index_html  = ""
pump_on_html     = ""
pump_off_html    = ""

@web_app.route("", "GET")
def index(request):
    """ Get index
    
    Input:
        request
        
    Output:
        webpage
    
    Requirements:
        serve_file = function to read out (html) file
        
    """
    
    print("Pump off!")
    
    # Modify hardware:
    pump.value = False
    led.blink(4, False)
    # Return browser page:
    return ("200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], webpage_generator(pump_index_html))


@web_app.route("/pump_on", "GET")
def pump_on(request):
    """ LED On
    
    Input:
        request
        
    Output:
        webpage
        
    Requirements:
        serve_file = function to read out (html) file
        
    """
    
    print("Pump on!")
    
    # Modify hardware:
    pump.value = True
    led.blink(4, True)
    # Return browser page:
    return ("200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], webpage_generator(pump_on_html))


@web_app.route("/pump_off", "GET")
def pump_off(request):
    """ Pump Off
    
    Input:
        request
        
    Output:
        webpage
    
    Requirements:
        serve_file = function to read out (html) file
        
    """
    print("Pump off!")
    
    # Modify hardware:
    pump.value = False
    led.blink(4, False)
    # Return browser page:
    return ("200 OK", [("Server", "esp32WSGIServer"), ("Connection", "close"),("Content-Type","text/html")], webpage_generator(pump_off_html))


# Setup server:
server.set_interface(esp)

# Passing in port 80 and our web_app as the application: 
wsgiServer = server.WSGIServer(80, application=web_app)

# Print the IP address of the board:
print("To connect, open this IP in your browser: ", esp.pretty_ip(esp.ip_address))
# print(esp.get_time())

# Start the server:
wsgiServer.start()

##########
# Main

###########
# Variables
ip               = esp.pretty_ip(esp.ip_address)
pump_index_html  = two_button_web_page(ip, title = "Water pump", title_body = "Water pump with Circuitpython via ESP32 WSGI",
                                        webpage_a_address = "/pump_on", webpage_b_address = "/pump_off", button_a_title = "Pump ON",
                                        button_b_title    = "Pump OFF", variable = "Pump", state = "Pump OFF")
pump_on_html     = two_button_web_page(ip, title = "Water pump", title_body = "Water pump with Circuitpython via ESP32 WSGI",
                                        webpage_a_address = "/pump_on", webpage_b_address = "/pump_off", button_a_title = "Pump ON",
                                        button_b_title    = "Pump OFF", variable = "Pump", state = "Pump ON")
pump_off_html    = two_button_web_page(ip, title = "Water pump", title_body = "Water pump with Circuitpython via ESP32 WSGI",
                                        webpage_a_address = "/pump_on", webpage_b_address = "/pump_off", button_a_title = "Pump ON",
                                        button_b_title    = "Pump OFF", variable = "Pump", state = "Pump OFF")

##########
# Loop
led.blink(6, False)

while True:
    
    try:
        
        # Here the server poll for incoming requests
        wsgiServer.update_poll()
        
        
    except (ValueError, RuntimeError) as e:
        # in the event of 'failure' print Value and Runtime error's as
        # e and try and reset wifi 
        print("Failed to update server, restarting ESP32\n", e)
        
        wifi.reset()
        
        continue

```

Visiting the `ip` address should lead to the following:


```{figure} /images/test_automated_watering.jpg
---
name: Automated_watering_browser-fig
alt: Automated watering in an internet browser
---
*Connecting to the Arduino via WSGI via another computer or electronic device on the same network.*
```
