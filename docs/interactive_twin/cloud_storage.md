# Cloud Storage: Set-up

The data that is being acquired from the sensors can be stored locally on the (arduino, pico, sensor, ...) device. Local storage however makes it difficult to analyse and visualize the data that was obtained from the sensors as it requires us to connect to the device physically to extract the data. To prevent the creation of what would be **local silo's** we will push the data acquired by our sensors into a more dedicated data store and one that is specifically designed to store **time series data**. In this case we will focus on [InfluxDB](https://www.influxdata.com/products/influxdb/) which is an essential open source time series toolkit.

## Communication methods

Achieving communication between the sensor and the repository can be done with several different approaches:

- **Direct write.** The simplest approach will be to write the data directly into the Time Series database. For instance, if our database and  sensor is the same device.<br>

- **HTTP**. HyperText Transfer Protocol (HTTP) is a one-to-one protocol for document transfer that has a request response model, with several request methods: GET, POST, PUT, DELETE. The GET method will be familiar to you, in order to load this webpage the web browser retrieves the webpage from a web server with GET. In terms of an Internet of Things (IoT) device the sensor will make a POST request.<br>

- **MQTT protocol.** [MQTT](https://mqtt.org/faq/) (sometimes referred to as Message Queue Telemetry Transport) is a message protocol for multiple machine(s)-to-machine(s) communication that is based on a publish and subscribe model. Alongside this publish and subscribe model there is a messaging priority system and quality of service (QoS): *fire and forget*, *at least once*, or *once and only once*. Because of both its simplicity and being lightweight MQTT  has been used for many years as a message protocol for small and large scale machine communication including Internet of Things (IoT). Since versions v5.0 and v3.1.1 it has become [standardised](https://mqtt.org/faq/) by the Organization for Advancement of Structured Information (OASIS).  
<br>


## MQTT 

```{figure} /images/arduino_mqtt_influx_grafana.png
---
name: arduino_mqtt_influx_graph-fig
alt: MQTT approach
class: bg-primary mb-1
width: 100%
align: center
---
**MQTT approach.** *The approach using the MQTT messaging service.*
```

In this module the focus will be on how to implement the MQTT approach ({numref}`arduino_mqtt_influx_graph-fig` ), as it is an ideal method for sending and receiving of data between devices. If however, you wish to apply an alternative HTTP approach it can be found in Section [Cloud Storage: Alternative](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/storage_alternative.html). 

In both approaches a [Docker](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation.html) environment will be used which is an open-source project that we will use for the deployment of applications as portable, self-sufficient containers. This will make our life easier as we do not need to go through complex installation procedures to make the application(s) work.

## Requirements

- Computer
- WiFi network and access to the Internet
- Raspberry Pi with 64-bit [OS](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html)


## Docker

### Installation

Using Docker we can containerise everything which will make our life vastly easier. For this part we need both Docker and Docker Compose since we are working with multiple applications that need to exchange data between them we will set up a Docker network.

- **Docker**.  https://docs.docker.com/engine/install/
- **Docker-Compose**. https://docs.docker.com/compose/install/


Prior to the next steps in this module ensure that you have set up the [Raspberry Pi](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation.html) with SSH and installed [Docker](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation.html) and [Portainer](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation_portainer.html) following the instructions [outlined previously](https://wur.gitlab.io/digitaltwin/docs/setup/raspberry_pi_setup.html).

```{admonition} Click here for a reminder on SSH!
:class: tip, dropdown
**Reminder SSH**<br>
To connect to the Raspberry Pi via a host computer perform the following steps:

- Network. Ensure both the host computer and Raspberry Pi are on the same Network.
- Get IP address.
  - Open a terminal on the host computer. Type `ping raspberrypi.local` replacing `raspberrypi` with the hostname defined on set-up.
  - If you don't have a headless setup you can get the hostname and IP address of the Pi by typing `hostname -I` into a Terminal on the Pi. 
- SSH connect. In the Terminal on the host computer type: ssh pi@[ip] replacing pi with the username and ip with the IP address of the Raspberry Pi.
- Add IP to the list of known hosts by typing `yes`.
- Enter Password associated with username.  
```




## Setup: Docker Network

For the communication between the different applications we will set up an internal Docker IoT Network. In the Terminal the syntax follows the structure `docker COMMAND SUBCOMMAND` with the initial `docker` statement telling the Terminal what application you are using followed by the commands and subcommands. For [network](https://docs.docker.com/engine/reference/commandline/network/) the command is `docker network` which has subcommands: connect to a network (`connect`); create a network (`create`); disconnect from a network (`disconnect`); show detailed information (`inspect`); list networks (`ls`); remove unused networks (`prune`); and, remove a network (`rm`). To create a network begin by entering the following in a Terminal console :

```{code-block} console
---

---

docker network create [OPTIONS] NETWORK

```
where, the variable `[OPTIONS]` allows a user to specify a driver, gateway, configuration amongst other [things](https://docs.docker.com/engine/reference/commandline/network_create/#options) and the variable `NETWORK` is the network's name. For our example our network's name will be `iot` with no additional `[OPTIONS]` are specified:

```{code-block} console
---

---

docker network create iot

```

Having created our `iot` network the next step is to **[pull](https://docs.docker.com/engine/reference/commandline/pull/)** an **image** from a repository. Where, *pull* refers to a data request originating from the client for data from another program or computer. In Docker to pull an image the [following](https://docs.docker.com/engine/reference/commandline/pull/) command line (cli) syntax is used: 

```{code-block} console
---

---

docker pull [OPTIONS] NAME[:TAG|@DIGEST]

```

## Setup: MQTT

Here, at this step the `Eclipse Mosquitto` package will be pulled since it is directly available through Docker as an open source package. To **pull** the image (https://hub.docker.com/_/eclipse-mosquitto) onto your device use the following command:

```{code-block} console
---

---

docker pull eclipse-mosquitto

```

`Eclipse Mosquitto` is a [package](https://mosquitto.org/man/mqtt-7.html) for [MQTT](https://mqtt.org/), an open standard frequently used in Internet of Things applications sometimes referred to by its previous name as the Message Queuing Telemetry Transport. This protocol provides a lightweight machine-to-machine method of carrying out messaging using a *publish* and *subscribe* model. This makes it suitable for the proposed IoT messaging that we are planning to use between our various sensors, storage, and eventual data visualisation. Once the repository has been pulled the Docker Container needs to be started. 

## Docker Containers

Reading this it is possible that you have not started a **[Docker container](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation.html)** before. Therefore, the following steps will showcase the variety of Docker Container modes prior to setting the correct options for our project. If you want to skip to the end, then skip to the *Docker Container: Detach* [section](https://wur.gitlab.io/digitaltwin/docs/interactive_twin/cloud_storage.html#docker-container-detach)
<br></br>

### Docker Container: Local Mode

To start a **[Docker container](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation.html)** the command `docker run` first creates a writeable container layer over the user specified image and then starts it. `docker run` has the following [syntax](https://docs.docker.com/engine/reference/commandline/run/):

```{code-block} console
---

---

 docker run [OPTIONS] IMAGE [COMMAND] [ARG...]

```

Using the `eclipse-mosquitto` image that has just been pulled execute the following command:

```{code-block} console
---

---

docker run -it --rm \
  --name mosquitto \
  --network=iot \
  -p 1883:1883 -p 9001:9001 \
  eclipse-mosquitto

```

Let us explain these commands. The example above runs a container with the name `mosquitto` using the `eclipse-mosquitto` image specified by the final line. Between `run` and the `IMAGE` various `[OPTIONS]` are specified. For instance, the container name which is assigned by the command `--name`. The network the container is connected to specified by `--network` which in this example is `iot`. The container's port(s) to the host are specified by `-p` which can alternatively be called as `--publish`. The command `--rm` automatically removes the container on exit. 

The `-it` [command](https://docs.docker.com/engine/reference/commandline/run/#assign-name-and-allocate-pseudo-tty---name--it) instructs Docker to (`-t`) allocate a pseudo-TTY (or pseudo terminal pty/tty) that has the functions of a *physical terminal*, without actually being one, connected to (`-i`) a standard input stream (or stdin) a communication input channel where data is sent to and read by a program. `-it` creates an interactive bash shell in the container which should give the following output:

```{code-block} console
---

---

1650541756: mosquitto version 2.0.14 starting
1650541756: Config loaded from /mosquitto/config/mosquitto.conf.
1650541756: Starting in local only mode. Connections will only be possible from clients running on this machine.
1650541756: Create a configuration file which defines a listener to allow remote access.
1650541756: For more details see https://mosquitto.org/documentation/authentication-methods/
1650541756: Opening ipv4 listen socket on port 1883.
1650541756: Opening ipv6 listen socket on port 1883.
1650541756: Error: Address not available
1650541756: mosquitto version 2.0.14 running

```

As you can see from the third line our container starts in `local only mode` which is great for local testing but if you are running multiple containers each with their own IP addresses, i.e., the goal of our project, this will not be sufficient. 
<br></br>

#### Docker Container: External Clients

To allow connections from clients that are not on the local machine a config file (`mosquitto.conf`) needs to be created containing the following lines: 

```{code-block} console
---

---
allow_anonymous true
listener 1883

```
<br>

````{admonition} Click here for a reminder on nano!
:class: tip, dropdown

**Reminder on nano. Creating a file in the terminal**<br>

If you are unsure of how to create a file in the terminal then use the `nano` text editor program. The nano program allows for text editing without exiting the terminal environment, calling nano followed by a filename will open it if the file exists or create the file and then open it if it does not. For our purpose (a configuration file) create the file by typing:

```{code-block} console
---

---
nano mosquitto.conf

```

This will change the terminal interface into a text editor program. You can now paste the text for the config file and past it in the editor using the standard copy and paste shortcuts: 

```{code-block} console
---

---
allow_anonymous true
listener 1883

```
<br>

Once finished with editing the document use the keyboard shortcut `CTRL + O` this should bring up a message at the bottom `File Name to Write: mosquitto.conf` confirming by hitting `enter`. It should then mention the number of lines that were written to the file `[ Wrote 2 lines ]`. After that you can use the keyboard shortcut `CTRL + X` to exit the program.

````


Place the newly created config file in your current directory. Stop the current `mosquitto` docker container and if needed remove (`rm`) the current instance `docker rm mosquitto`. Now you can start the mosquitto container with the config file by adding the line `-v $PWD/mosquitto.conf:/mosquitto/config/mosquitto.conf \` to the `docker run` commands:

```{code-block} console
---

---
docker run -it --rm \
  --name mosquitto \
  --network=iot \
  --restart=always \
  -v $PWD/mosquitto.conf:/mosquitto/config/mosquitto.conf \
  -p 1883:1883 -p 9001:9001 \
  eclipse-mosquitto

```

Where, `-v` (also callable as `--volume`) bind [mounts a volume](https://docs.docker.com/engine/reference/commandline/run/#mount-volume--v---read-only).  Notice that when this command is run with the addition of the configuration file the output is slightly different:

```{code-block} console
---

---

1650541909: mosquitto version 2.0.14 starting
1650541909: Config loaded from /mosquitto/config/mosquitto.conf.
1650541909: Opening ipv4 listen socket on port 1883.
1650541909: Opening ipv6 listen socket on port 1883.
1650541909: mosquitto version 2.0.14 running
1650541910: New connection from 172.20.0.4:48280 on port 1883.

```

The third line has changed from "*Starting in local only mode. Connections will only be possible from clients running on this machine.*" to "*Opening ipv4 listen socket on port 1883.*" the port defined in our config file. 

### Test MQTT: send a message

To test if the MQTT system works we can try to send a message by typing the next couple of commands into our terminal. However, you might notice that with our present setup you cannot type any other command into this system because it is occupied by the program that is running in the docker instance.

`````{margin} 
````{note}
**screen command**

The linux screen command: 

```{code} terminal

screen

```
can push and pull running terminal applications to the back and foreground. For further information search online for `How to Use Linux’s screen Command` to find out more.

````
`````

There are multiple ways around this such as Linux's screen command (see Note: *screen command*). However, in this module we will send the docker instance to the background so that it will disappear from our terminal allowing further commands to be entered. [Using](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation_portainer.html) `portainer` you can lookup the container and view its log.

### Docker Container: detach

To send a docker run to the background we use the detach functionality (or daemon mode), callable as either  `-d` or `--detach`, that will send the container to the background and print the container's ID. In addition, because we have sent the container to the background its adviseable to ensure that the container stays up even after a reboot lest we forget to restart our Container when or if we reboot our Raspberry Pi ("out of sight, out of mind"). To ensure the container restarts add the line `--restart=always` option to modify the restart parameter from it's default of 'no'. Note that the command whilst similar is slightly different than the previous:

```{code-block} console
---

---
docker run -d \
  --restart=always \
  --name mosquitto \
  --network=iot \
  -v $PWD/mosquitto.conf:/mosquitto/config/mosquitto.conf \
  -p 1883:1883 -p 9001:9001 \
  eclipse-mosquitto 

```

### Messaging

Now that the Docker instance is running in the background you should now be able to submit messages using the `mosquitto_pub` application. This application is a simple MQTT version 5/3.1.1 client that will publish a single, simple, message for a given topic before exiting. 

Running a command in a running Docker container can be performed with the following `exec` [command](https://docs.docker.com/engine/reference/commandline/container_exec/) that has several possible `[OPTIONS]` (`--detach`, `--detach-keys`, `--env`, `--env-file`, `--interactive`, `--privileged`, `--tty`, `--user`, and `--workdir`):

```{code-block} console
---

---

docker container exec [OPTIONS] CONTAINER COMMAND [ARG...]

```

For our instance replace `CONTAINER` with the Docker container's name (`mosquitto`) and the `COMMAND` with `mosquitto_pub`: 


```{code-block} console
---

---

docker container exec mosquitto mosquitto_pub \
  -t '/sensors/sensorX/temperature' \
  -m '20'

```

This command will be executed in the previously named (`--name`) mosquito container using the `mosquitto_pub` [application](https://mosquitto.org/man/mosquitto_pub-1.html) that is available within. With `-t` and `-m` being the MQTT topic and message. A message is published onto a topic and the topic is treated as a hierarchy with levels in the hierarchy seperated by a slash (`/`) separator. Note, in receiving a message the levels in the hierarchy can be replaced with two placeholders (or if the placeholder is a single character a **wildcard**):  `+` and `#`:

- `+` can be used to replace a single level of the hierarchy, for example for our topic '/sensors/sensorX/temperature' the following alternatives will get information on all within that level of the hierarchy: 
  - '/+/sensorX/temperature'
  - '/sensors/+/temperature'
  - '/sensors/sensorX/+'
  - '/sensors/+/+'
  - '/+/+/+'

- `#` can be used to replace all remaining levels of the hierarchy, for example for our topic '/sensors/sensorX/temperature' the following alternatives will work:
  - '#'
  - '/sensors/#'
  - '/sensors/sensorX/#'
  - '/+/#'
  
To double check if the message has been well received we have to open the messaging log which is viewable in [portainer](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation_portainer.html). The portainer instance will be running at `http://raspberrypi.local:9000/`, replacing `raspberrypi` with the hostname set at the set-up of your Raspberry Pi.

Following login on the local instance, go to containers, and there you should be able to see an instance running with the `eclipse-mosquitto` image. In the third column under `quick actions` you can click on the `logs` to view if the connection was established.

```{code-block} console
---

---

1654267489: mosquitto version 2.0.14 running
1654267627: New connection from 127.0.0.1:54762 on port 1883.
1654267627: New client connected from 127.0.0.1:54762 as auto-F149189B-7C76-4BEF-9208-DC2BBFED416B (p2, c1, k60).

```


MQTT also allows you to send JavaScript Object Notation or JSON messages, a lightweight human readable data interchange format, so that you can more easily publish multiple messages in one go. For example, let us send another MQTT message but this time replacing '20' with a JSON message giving both the variable and value:

```{code-block} console
---

---
docker container exec mosquitto mosquitto_pub \
  -t '/room1/sensorX/temperature' \
  -m '{"degrees": 20, "humidity": 35}'

```

You can now publish your messages to the MQTT broker however, as there is no one listening on the other end. Lets now move on to the problem of storing the data that is passing through the MQTT broker.

## Setting up InfluxDB

For this problem, as mentioned earlier, this project will be using [InfluxDB](https://www.influxdata.com/products/influxdb/) a time series database allowing you to easily handle large amounts of time related information ([Dotis-Georgiou, 2021](https://www.influxdata.com/blog/running-influxdb-2-0-and-telegraf-using-docker/) ). This application is often used in Internet of Things applications, can easily handle large amounts of data, and is officially available as a Docker image on the [Docker Hub](https://hub.docker.com/_/influxdb). Open a new terminal tab and pull the image:

```{code-block} console
---

---

docker pull influxdb

```

Next, start the InfluxDB container:

```{code-block} console
---

---

docker run -d \
  --restart=always \
  --network=iot \
  -p 8086:8086 \
  -v $PWD/influxdb-data:/var/lib/influxdb \
  --name influxdb \
  influxdb

```

This command will start the InfluxDB image as part of the `iot` docker network that we setup previously. To ensure that the data that is received by InfluxDB does not disappear when you shut down the program we have to `mount` the location in the docker image to a local folder via the `-v` subcommand (`-v $PWD/influxdb-data:/var/lib/influxdb`). With this option enabled when the program writes data to the docker folder it will write it automatically on your computer instead of inside the virtual environment. Finally, `-p` makes the program accessible via port 8086 allowing a connection to made via http://hostname.local.port (e.g., http://raspberrypi.local:8086)


### Configuring InfluxDB

Accessing the server at [http://raspberrypi.local:8086](http://raspberrypi.local:8086) you should be greeted by an InfluxDB welcome screen...

![](../../images/influxdb-welcome.png)

#### User account

After clicking *Get started* the welcome screen should transition to create an initial user. Fill in the blanks for Username, Password, Initial Organization Name, and Initial Bucket Name to how it suits you and your project best. The *organization* represents a shared workspace whilst a *bucket* is where the timeseries data is stored, please be aware that the variables for the `organization` (e.g. home) and `bucket` (e.g. data) name are used later on! Clicking continue takes you to the final complete screen with three options: quick start, advanced, and configure later. You can investigate these different setups later, but for now we will 'configure later'...

#### Token setup

To be able to access the InfluxDB database from other applications we use an Application Programming Interface (API) token for the connection. API tokens are similar to passwords, in that provide a unique identifier, that allows an application to authenticate and access the API. Using this approach we dont have to write down our username and password. The tokens can have different rights allowing you to tweak what each token is allowed to do. To create a token:

- Click on load data
- Click on the `API TOKENS` tab
- Your user should have a token available
- Click on the token and you should be able to see what kind of permissions it has.

![](../../images/influxdb-setup-token.png)

As you can see this default token has access to everything. Read write to buckets, dashboards, organizations etc...

For security reasons you should generate a read/write token and not an all access token. During the generation of a read write API token you can select which bucket it has read and write access to using the scoped view.

- Exit this view and click on the `GENERATE API TOKEN` button.

![](../../images/influxdb-setup-readwrite-token.png)

- This token should now become visible in your overview.
- Click on the token, check the permissions and if all is ok
  - Copy the API token string for example: `icrJc9p21kAFbQavs79_Yw___THIS_TOKEN_IS_FAKE___wIK2ZHQC8RC0cIO1Dl77_vtqBzjtjaUehYJJQ==`
  - if you double click it make sure that when present the `==` at the end of the line are copied with it as well... (*speaking from experience*)
  - Save it somewhere for later, or keep the window open.

## Telegraf

To pass data from the MQTT Broker to InfluxDB we need an agent. We achieve this by using [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/), Influx's open source data collection agent that will aid in the collection of data from our sensors. Telegraf reads the data directly from our MQTT data source and outputs it into InfluXDB. Written in Go, Telegraf has no external dependencies, and requires only a minimal memory footprint making it ideal for our project. It is a *plugin* based system with four types of plugin: input, output, processors, and aggregators with the processors and aggregators allowing for data transformation (e.g., data clean-up). These plugins come with various options including for both data input and output streams, even allowing [flexibility](https://www.influxdata.com/blog/plugin-spotlight-exec-execd/) in coding language. For use case we make use of the MQTT input and InfluxDB output plugins. As a low code solution the only real requirement is to modify the configuration file for Telegraf (`telegraf.conf`). Open the Terminal text editor by entering the following command:

```{code-block} console
---

---
sudo nano telegraf.conf

```

The configuration file needs to contain the following content making sure to adjust the values for *token*, *organization*, and *bucket*: 

```{code-block} console
---

---
################################################################################
##                           OUTPUT PLUGINS                                   ##
################################################################################
[[outputs.influxdb_v2]]
  urls = ["http://influxdb:8086"]
  token = "_wxOT4bEASCCSjvuScVvcQVWSR_REPLACE_BY_YOUR_TOKEN_5DyYOVw8yArpjvYYMaHC0ECrA=="
  organization = "home"
  bucket  = "data"
################################################################################
##                            INPUT PLUGINS                                   ##
################################################################################
[[inputs.mqtt_consumer]]
  servers = ["tcp://mosquitto:1883"]
  topics = [
    "sensors/#",
  ]

```

```{margin} Modifying the file
Again use `nano` to edit the file. 
With `CTRL+W` you can search in the file.
Use `CTRL+O` to save and `CTRL+X` to exit.
```

Let us explain what the values for the configuration file mean which has been divided into an output and input plugin section. For the output to InfluxDB `[[outputs.influxdb_v2]]` the urls should be somewhat self explanatory. It connects to the internal Docker container with the `hostname` influxdb at `port` 8086. The token is the API token that was just generated (see *Token setup*) and can be retrieved from the InfluxDB interface webpage at `http://raspberrypi.local:8086`. Or if you are running InfluxDB on another device using that device's IP address and port. Likewise, the organization and bucket can all be obtained through the web interface (see *User account*). For the input from MQTT `[[inputs.mqtt_consumer]]` the plugin connects to the mosquitto broker server and can listen to a range of topics that might be of interest. For now it listens to all messages that are submitted to the `sensors/` topic and any message below that using the `#` wildcard such as `sensors/bedroom/temperature`. Once this information has been added save the file as a `telegraf.conf` file. 

The configuration file should be mounted (`-v`) during Docker startup (`docker run`). If you saved the file in the current directory then you can use `-v` followed by the filename and location `$PWD/telegraf.conf` to mount the file to the telegraf container. Let us first test the setup by running the commands:

```{code-block} console
---

---

docker run --rm \
  --network=iot \
  -v $PWD/telegraf.conf:/etc/telegraf/telegraf.conf:ro \
  --name telegraf \
  telegraf


```

The output message should be like this:

```{code-block} console
---

---

2022-04-21T12:23:20Z I! Using config file: /etc/telegraf/telegraf.conf
2022-04-21T12:23:20Z I! Starting Telegraf 1.22.1
2022-04-21T12:23:20Z I! Loaded inputs: mqtt_consumer
2022-04-21T12:23:20Z I! Loaded aggregators: 
2022-04-21T12:23:20Z I! Loaded processors: 
2022-04-21T12:23:20Z I! Loaded outputs: influxdb_v2
2022-04-21T12:23:20Z I! Tags enabled: host=47479a897392
2022-04-21T12:23:20Z I! [agent] Config: Interval:10s, Quiet:false, Hostname:"47479a897392", Flush Interval:10s
2022-04-21T12:23:20Z I! [inputs.mqtt_consumer] Connected [tcp://mosquitto:1883]

```

If the output is the same as above and everything seems ok you can kill this process by using the keyboard command `CTRL-C`. The subcommand `--rm` in our test will automatically remove the container on exit. Now let us start up a new container and start it up in daemon mode (`-d`) with an always restart option:

```{code-block} console
---

---

docker run -d \
  --restart=always \
  --network=iot \
  -v $PWD/telegraf.conf:/etc/telegraf/telegraf.conf:ro \
  --name telegraf \
  telegraf

```


## Messaging

Now we are ready to submit a message (`-m`) for a topic (`-t`) to the MQTT broker which should be obtained by the Telegraf instance that then pushes the data into InfluxDB. 

```{code-block} console
---

---

docker container exec mosquitto mosquitto_pub \         
  -t 'sensors/bedroom/temperature' \
  -m 'bedroom_temperature celsius=20'

```

### Visualizing your data

If you open the InfluxDB webpage interface ([http://raspberrypi.local:8086](http://raspberrypi.local:8086) ) and go to `Explore` you should see your `data` bucket with a measurement `bedroom_temperature` and a field `celcius`. By selecting the `bedroom_temperature` measurement you can click `submit` and the graph should now show your measurements. Please note that when you only have 1 measurement it could be difficult to see the graph. Move your mouse over the graph to show the measurement value or click on `View raw data` to show the actual values as a table.

## Summary

In short, three different docker containers are needed. 1) MQTT Broker to send & receive messages. 2) The InfluxDB to store all measurements permenantly. 3) Telegraf to synchronise the messages from the MQTT Broker and the InfluxDB. 


````{admonition} Click here for all the code
:class: tip, dropdown

**Step 1: Install Docker** <br>
Follow the [instructions](https://wur.gitlab.io/digitaltwin/docs/setup/virtualisation.html) for setting up Docker. 

**Step 2: Install Portainer** <br>
Install Portainer with the following command in the Terminal:

```{code-block} console
---

---
docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
```

Open Portainer via http://raspberrypi:9000/ replacing the hostname and port if necessary. If this is the first instance of opening Portainer it will require you to set a username and password.

**Step 3: Create Docker Network** <br>
Create a Docker network called `iot` with the following command in the Terminal:

```{code-block} console
---

---

docker network create iot

```

**Step 4: Run MQTT Broker** <br>
Next run the MQTT Broker by entering the Docker run command and subcommands: 

```{code-block} console
---

---

docker run -d -it \
  --restart=always
  --name mosquitto \
  --network=iot \
  -v $PWD/mosquitto.conf:/mosquitto/config/mosquitto.conf \
  -p 1883:1883 -p 9001:9001 \
  eclipse-mosquitto    

```

**Step 5: Run InfluxDB** <br>
Next run the InfluxDB by entering the Docker run command and subcommands: 

```{code-block} console
---

---

docker run -d \
  --restart=always \
  --network=iot \
  -p 8086:8086 \
  -v $PWD/influxdb-data:/var/lib/influxdb \
  --name influxdb \
  influxdb

```

**Step 6: Set up InfluxDB environment** <br>
Navigate to http://hostname.local:port (e.g., [http://raspberrypi.local:8086](http://raspberrypi.local:8086) ) and set up the username, password, organization, and bucket. Get an API token. 


**Step 7: Make Telegraf configuration file** <br>
Open the Terminal text editor with a new file called `telegraf.conf`:

```{code-block} console
---

---
sudo nano telegraf.conf

```

Then enter the configuration information for Telegraf replacing urls, token, organization, and topics with the correct values for your project:

```{code-block} console
---

---
################################################################################
##                           OUTPUT PLUGINS                                    #
################################################################################
[[outputs.influxdb_v2]]
  urls = ["http://influxdb:8086"]
  token = "_wxOT4bEASCCSjvuScVvcQVWSR_REPLACE_BY_YOUR_TOKEN_5DyYOVw8yArpjvYYMaHC0ECrA=="
  organization = "home"
  bucket  = "data"
################################################################################
##                            INPUT PLUGINS                                   ##
################################################################################
[[inputs.mqtt_consumer]]
  servers = ["tcp://mosquitto:1883"]
  topics = [
    "sensors/#",
  ]

```

**Step 8: Run Telegraf** <br>
Next run the Telegraf by entering the Docker run command and subcommands: 

```{code-block} console
---

---

docker run -d \
  --restart=always \
  --network=iot \
  -v $PWD/telegraf.conf:/etc/telegraf/telegraf.conf:ro \
  --name telegraf \
  telegraf

```

````

-------
#### Sources

```{note}

This module was based on the blog post by Lucas S.
https://lucassardois.medium.com/handling-iot-data-with-mqtt-telegraf-influxdb-and-grafana-5a431480217

```

