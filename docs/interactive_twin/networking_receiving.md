# MQTT Receiving data

After setting up your raspberry pi or local machine with a MQTT broker, Telegraf and InfluxDB instance and a sensor node that regularly pushes data to the MQTT broker we can initiate another listener to subscribe to a topic and to anticipate on the message.

Since there are no other modules that submit signals in which this module needs to act upon this example is purely to show how to receive a message and potentially react on it.

The steps for receiving are very similar to the transmission of information. However in this case we do not have a sensor that requires to be read. So we purely connect to the WiFi, authenticate with the broker and start listening!

Once a package is received we use the payload object to obtain the content.

## Components

The components used for this module are as followed:

- Arduino Nano RP2040 Connect (€21,00)

## Code

```{code-block} python
from helper_functions import wifi_connect, MQTT_comms, board_led
from secrets import secrets

import adafruit_esp32spi.adafruit_esp32spi_socket as socket

####################################
###          Led                ###
####################################

led = board_led()
led.turn_on_led()

####################################
###           WiFi               ###
####################################

get_connected = wifi_connect()
get_connected.connect_to_network(secrets["ssid"], secrets["password"])

####################################
###          MQTT                ###
####################################

mqtt_comms = MQTT_comms(get_connected.esp)
mqtt_comms.start_mqqt_interface(socket, get_connected.esp)
mqtt_comms.new_client_object(secrets["mqtt_broker"], secrets["mqtt_port"], secrets["mqtt_username"], secrets["mqtt_password"], secrets['mqtt_topic'])
mqtt_comms.connect()

# Setup a feed named 'onoff' for subscribing to changes
feed = secrets['mqtt_topic']
mqtt_comms.subscribe(feed)

####################################
###          END STARTUP         ###
####################################

led.blink(6, False)

####################################
###          LOOP                ###
####################################


while True:
    print(mqtt_comms.payload)
    rc = mqtt_comms.mqtt_client.loop(60)
    if mqtt_comms.payload != None:
        print("Message: " + mqtt_comms.payload)
        # Reset payload
        mqtt_comms.payload = None
```

## Secrets

```{code-block} python
# Secrets used for the different sensors

secrets = {
    'ssid' : 'YOUR_WIFI_NAME',
    'password' : 'WIFI_PASSWORD',

    'mqtt_broker': 'MQTT Broker address (e.g., 192.168.1.108)',
    'mqtt_port': 1883,
    'mqtt_username': 'mosquitto_username',
    'mqtt_password': 'mosquitto_password',
    'mqtt_topic': 'sensors/garden'
}
```